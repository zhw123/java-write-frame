package com.guava.cache.lean.controller;

import com.guava.cache.lean.entity.UserInfo;
import com.guava.cache.lean.service.GuavaCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GuavaCacheController {
    @Autowired
    private GuavaCacheService guavaCacheService;
    @Autowired
    private CacheManager cacheManager;

    @GetMapping("/users/{name}")
    public UserInfo getUser(@PathVariable String name) {
        System.out.println("==================");
        UserInfo user = guavaCacheService.getUserByName(name);
        System.out.println(cacheManager.toString());
        return user;
    }
}
