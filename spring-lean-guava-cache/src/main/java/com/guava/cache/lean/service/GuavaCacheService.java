package com.guava.cache.lean.service;

import com.guava.cache.lean.entity.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @Cacheable 触发缓存入口（这里一般放在创建和获取的方法上）
 * @CacheEvict 触发缓存的eviction（用于删除的方法上）
 * @CachePut 更新缓存且不影响方法执行（用于修改的方法上，该注解下的方法始终会被执行）
 * @Caching 将多个缓存组合在一个方法上（该注解可以允许一个方法同时设置多个注解）
 * @CacheConfig 在类级别设置一些缓存相关的共同配置（与其它缓存配合使用）
 */
@Service
@CacheConfig(cacheManager = "guavaCacheManager")
public class GuavaCacheService {
    /**
     * 获取用户信息(此处是模拟的数据)
     */
    /**
     * 缓存 key 是 username 的数据到缓存 users 中，
     * 如果没有指定 key，则方法参数作为 key 保存到缓存中
     */
    @Cacheable(value = "Userdata", key = "#username")
    public UserInfo getUserByName(String username) {
        UserInfo user = getUserFromList(username);
        System.out.println("数据库中获取数据");
        return user;
    }
    /**
     * 从模拟的数据集合中筛选 username 的数据
     */
    private UserInfo getUserFromList(String username) {
        List<UserInfo> userDaoList = UserDataFactory.getUserDaoList();
        for (UserInfo user : userDaoList) {
            if (Objects.equals(user.getName(), username)) {
                return user;
            }
        }
        return null;
    }

}
