package com.guava.cache.lean.demo;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheStats;

import java.util.concurrent.TimeUnit;

/**
 *  监控
 */
public class GavaCacheStatsCounterDemo {
    public static void main(String[] args) throws InterruptedException {
        Cache<String, String> cache = CacheBuilder.newBuilder()
                // 基于时间失效,写入之后开始计时失效
                .expireAfterWrite(2000, TimeUnit.MILLISECONDS)
                // 缓存容量
                .maximumSize(5)
                .recordStats()
                .build();
        cache.put("key", "value");
        CacheStats cacheStats = cache.stats();
        System.out.println(cacheStats.toString());
//
    }
}