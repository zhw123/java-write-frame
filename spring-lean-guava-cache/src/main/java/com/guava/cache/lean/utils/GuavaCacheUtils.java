package com.guava.cache.lean.utils;

import com.google.common.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class GuavaCacheUtils {

    @Autowired
    Cache<String, Object> guavaCache;

    /**
     * 添加或更新缓存
     *
     * @param key
     * @param value
     */
    public void putAndUpdateCache(String key, Object value) {
        guavaCache.put(key, value);
    }


    /**
     * 获取对象缓存
     *
     * @param key
     * @return
     */
    public <T> T getObjCacheByKey(String key, Class<T> t) {
        //通过key获取缓存中的value，若不存在直接返回null
        guavaCache.getIfPresent(key);
        return (T) guavaCache.asMap().get(key);
    }

    /**
     * 根据key删除缓存
     *
     * @param key
     */
    public void removeCacheByKey(String key) {
        // 从缓存中删除
        guavaCache.asMap().remove(key);
    }

}
