package com.guava.cache.lean.config;

import com.google.common.cache.*;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.concurrent.TimeUnit;

@EnableCaching
@Configuration
public class GuavaCacheConfig {
    @Bean
    public Cache<String, Object> guavaCache() {
        return CacheBuilder.newBuilder()
                // 设置最后一次写入或访问后经过固定时间过期
                .expireAfterWrite(6000, TimeUnit.SECONDS)
                // 初始的缓存空间大小
                .initialCapacity(100)
                // 缓存的最大条数
                .maximumSize(1000)
                .build();
    }

    @Bean(name = "guavaCacheManager")
    public CacheManager oneHourCacheManager(){
        CacheBuilder cacheBuilder = CacheBuilder.newBuilder()
                .initialCapacity(5000) //初始大小
                .maximumSize(100000)  //最大大小
                //写入/更新之后1小时过期
                .expireAfterWrite(1, TimeUnit.HOURS);
        //默认 使用CacheLoader
        GuavaCacheManager cacheManager = new GuavaCacheManager();
        cacheManager.setCacheBuilder(cacheBuilder);
        return cacheManager;
    }
}

