package com.lean.shardingsphere;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan("com.lean.shardingsphere.mapper")
@SpringBootApplication
public class SpringLeanShardingsphereApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanShardingsphereApplication.class, args);
    }

}
