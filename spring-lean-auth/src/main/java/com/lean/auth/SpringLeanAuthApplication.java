package com.lean.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SpringLeanAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanAuthApplication.class, args);
    }

}
