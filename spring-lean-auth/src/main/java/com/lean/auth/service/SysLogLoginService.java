package com.lean.auth.service;


import com.lean.auth.entity.SysLogLoginEntity;
import com.lean.auth.page.PageResult;
import com.lean.auth.query.SysLogLoginQuery;
import com.lean.auth.vo.SysLogLoginVO;

/**
 * 登录日志
 *
 * 
 */
public interface SysLogLoginService extends BaseService<SysLogLoginEntity> {

    PageResult<SysLogLoginVO> page(SysLogLoginQuery query);

    /**
     * 保存登录日志
     *
     * @param username  用户名
     * @param status    登录状态
     * @param operation 操作信息
     */
    void save(String username, Integer status, Integer operation);
}
