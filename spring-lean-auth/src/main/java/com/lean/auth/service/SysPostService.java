package com.lean.auth.service;


import com.lean.auth.entity.SysPostEntity;
import com.lean.auth.page.PageResult;
import com.lean.auth.query.SysPostQuery;
import com.lean.auth.vo.SysPostVO;

import java.util.List;

/**
 * 岗位管理
 *
 * 
 */
public interface SysPostService extends BaseService<SysPostEntity> {

    PageResult<SysPostVO> page(SysPostQuery query);

    List<SysPostVO> getList();

    void save(SysPostVO vo);

    void update(SysPostVO vo);

    void delete(List<Long> idList);
}
