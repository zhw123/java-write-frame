package com.lean.auth.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.parameters.Parameter;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.customizers.OperationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * Swagger配置
 */
@Configuration
public class SwaggerConfig {

    @Bean
    public OperationCustomizer operationCustomizer() {
        return (operation, handlerMethod) -> operation.addParametersItem(
                new Parameter()
                        .in("header")
                        .required(true)
                        .description("token 验证")
                        .name("Authorization"));
    }

    @Bean
    public GroupedOpenApi userApi(OperationCustomizer operationCustomizer) {
        String[] paths = {"/**"};
        String[] packagedToMatch = {"com.lean.auth"};
        return GroupedOpenApi.builder().group("auth")
                .pathsToMatch(paths)
                .addOperationCustomizer(operationCustomizer)
                .packagesToScan(packagedToMatch).build();
    }

    @Bean
    public OpenAPI customOpenAPI() {
        Contact contact = new Contact();
        contact.setName("xxx");

        return new OpenAPI().info(new Info()
                .title("auth")
                .description("auth")
                .contact(contact)
                .version("1.0")
                .termsOfService("https://xxx.xom")
                .license(new License().name("auth")
                        .url("https://xxx.com")));
    }
}
