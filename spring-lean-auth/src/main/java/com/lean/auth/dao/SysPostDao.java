package com.lean.auth.dao;

import com.lean.auth.entity.SysPostEntity;
import org.apache.ibatis.annotations.Mapper;

/**
* 岗位管理
*/
@Mapper
public interface SysPostDao extends BaseDao<SysPostEntity> {

}
