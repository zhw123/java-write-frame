package com.lean.flyway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 每个版本发布时单独分发数据库变动脚本，甚至简单粗暴的将开发库直接导出并导入生产环境
 * 解决方式：
 * Flyway 和 Liquibase 都支持数据库重构和版本控制所需的所有功能，并且这两个工具都可以完美集成在 Maven 或 Gradle 构建脚本以及 Spring Boot 生态系统中。
 * Flyway 使用 SQL 来定义数据库更改，因此您可以定制 SQL 脚本以与 Oracle 或 PostgreSQL 等数据库很好地配合使用。另一方面，Liquibase 不使用 SQL ，而是通过使用 XML ， YAML 或 JSON 来定义数据库更改来引入抽象层。因此，Liquibase 更适合用于拥有多数据源的应用中。但是，如果想完全控制 SQL ，Flyway 是首选工具，因为我们可以使用定制的 SQL 甚至 Java 代码修改数据库。
 * Flyway 相比 Liquibase 在社区更活跃。
 * 简单来说：
 * 面向 SQL，选择 Flyway
 * 不面向 SQL，选择 Liquibase
 * <p>
 * SQL 报错
 * 通过 Spring Boot 自动执行 migration 时要注意，一旦 migration 执行失败，应用启动会终止。出现 migration 执行失败时，需要将 Schema History Table 表中的失败记录处理掉，才能再次执行 migration，否则应用会一直无法启动。
 * <p>
 * out-of-order
 * 多人开发时，可能会出现 A 写了 V1 脚本，B 写了 V2 脚本，B 的代码先合并进去了，V2 脚本先执行了，此时 A 的 V1 脚本受版本号只能增加的要求不能再执行。
 * 这种情况可以通过将 spring.flyway.out-of-order 设置为 true 来暂时取消这个限制，不过还是强烈建议 A 将 V1 脚本版本号改为 V3。
 *
 * 设置脚本
 * 首先需要在classpath目录下，创建一个名为db.migration的文件夹。
 * 然后向该文件夹内，增加SQL脚本。编写SQL脚本需要注意以下几点：
 * 1、仅需要执行一次的脚本，以V开头，后面跟上0~9的数字组合，数字之间可以使用.或者_进行分割。然后再以两个下划线 __进行分割，其后跟上文件名称，最后以.sql结尾。
 * 如： V1__create_user_ddl.sql、V2__create_user.sql。
 * 2、需要重复执行的SQL，则需要以R开头。后面再以两个下划线分割，其后跟文件名称，最后以.sql结尾。
 * 如：R__truncate_user_dml.sql
 * R文件变更后，启动并不会报错！
 * 1、启动后，会判断R文件的内容信息是否发生变更。
 * 2、如果未变更，则不会新增数据！
 * 3、如果存在变更，则会将脚本再执行一遍！
 *
 * sql 目录中存放脚本文件，脚本名称命名方式
 *
 * 命名规范 : V+版本号(纯数字或者以小数点或者以下划线分隔的数字)+__(双下划线)+描述字符串(支持英文和下划线).
 * 版本号：自增，每次都需要增加一位 ，如1，2，3,建议以1.0开始
 * 描述字符串:建议是以操作类型(INIT,UPDATE)加上微服务名称，如果是更新的，再增加更新时间(也可不加)。
 *
 * 版本化迁移： 执行一遍，版本号唯一，有重复会报错：  格式：V+版本号 +双下划线+描述+结束符  v2__add_new_table.sql  描述：add_new_table  版本号：2 结束符：.sql
 * 重复的迁移，不需要版本号，脚本发生变化启动就会执行： 格式：R+双下划线+描述+结束符 R__create_view.sql  描述：create_view   结束符：.sql
 * 撤消迁移： 格式：U+版本号 +双下划线+描述+结束符
 *
 *
 * 项目第一次启动数据库会生成一张历史记录表：lyway_schema_history。
 * lyway_schema_history是一个flyway管理各个版本关系的表，主要用于维护和管理开发者本地的脚本版本信息。
 *
 * 注意事项
 * 1、不支持降级，支持事务回滚，千万不要手动更改数据库表结构！！！更新前，做好备份！
 * 2、在resources资源文件夹下的sql路径要与配置路径保持一致,如上面配置文件夹db/migration
 * 3、不要使用@PostConstruct注解做数据查询处理，因为启动微服务时会先启动PostConstruct里的方法，后启动flyway，可能造成方法无法读取还没有创建的数据库表；
 * 如果要使用，则使用注解@DependsOn(“flywayInitializer”)
 * 4、V7.1.5_20211123_1100__init.sql第三个是两个下划线，请注意！
 * 5、使用R可以重新执行已经修改过的sql文件，如：R__V7.1.5_20211123_1100__init.sql
 *
 */
@SpringBootApplication
public class SpringFlywayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringFlywayApplication.class, args);
    }

}
