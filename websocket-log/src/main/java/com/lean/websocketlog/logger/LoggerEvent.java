package com.lean.websocketlog.logger;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName: LoggerEvent
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/6/17 13:44
 */
//进程日志事件内容载体
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoggerEvent {
    private LoggerMessage log;
}
