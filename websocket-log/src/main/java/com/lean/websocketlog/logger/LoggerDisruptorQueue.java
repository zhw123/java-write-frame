package com.lean.websocketlog.logger;


import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
/**
 * @ClassName: LoggerDisruptorQueue
 * @Description: Disruptor 环形队列
 * @Author: zhanghongwei
 * @Date: 2022/6/17 13:46
 */
@Component
public class LoggerDisruptorQueue {

    private Executor executor = Executors.newCachedThreadPool();

    private LoggerEventFactory factory = new LoggerEventFactory();

    private int bufferSize = 2 * 1024;

    //环形队列
    private Disruptor<LoggerEvent> disruptor = new Disruptor<>(factory, bufferSize, executor);

    private static RingBuffer<LoggerEvent> ringBuffer;


    @Autowired
    LoggerDisruptorQueue(LoggerEventHandler eventHandler) {
        disruptor.handleEventsWith(eventHandler);
        this.ringBuffer = disruptor.getRingBuffer();
        disruptor.start();
    }

    public static void publishEvent(LoggerMessage log) {
        long sequence = ringBuffer.next();
        try {
            LoggerEvent event = ringBuffer.get(sequence);
            event.setLog(log);
        } finally {
            ringBuffer.publish(sequence);
        }
    }

}
