package com.lean.smartdoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSmartDocApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSmartDocApplication.class, args);
    }

}
