package com.lean.smartdoc.controller;

import com.lean.smartdoc.entity.ResponseResult;
import com.lean.smartdoc.entity.AddressParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/address")
public class AddressController {
    /**
     * Add a new address.
     *
     * @param addressParam param
     * @return address
     */
    @PostMapping("add")
    public ResponseResult<String> add(AddressParam addressParam) {
        return ResponseResult.success("success");
    }

}
