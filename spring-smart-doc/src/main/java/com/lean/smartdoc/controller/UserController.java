package com.lean.smartdoc.controller;

import java.util.Collections;
import java.util.List;

import com.lean.smartdoc.entity.ResponseResult;
import com.lean.smartdoc.entity.UserParam;
import com.lean.smartdoc.vo.AddressVo;
import com.lean.smartdoc.vo.UserVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    /**
     * Add user.
     *
     * @param userParam user param
     * @return user
     */
    @PostMapping("add")
    public ResponseResult<String> add(@RequestBody UserParam userParam) {
        return ResponseResult.success("success");
    }

    /**
     * User list.
     *
     * @return user list
     * @since 1.2
     */
    @GetMapping("list")
    public ResponseResult<List<UserVo>> list() {
        List<UserVo> userVoList =
            Collections.singletonList(UserVo.builder().name("dai").age(18).address(AddressVo.builder().city("SZ").zipcode("10001").build()).build());
        return ResponseResult.success(userVoList);
    }
}
