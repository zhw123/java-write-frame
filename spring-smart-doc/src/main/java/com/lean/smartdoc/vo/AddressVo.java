package com.lean.smartdoc.vo;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class AddressVo {
    /**
     * city.
     */
    private String city;

    /**
     * zip code.
     */
    private String zipcode;
}
