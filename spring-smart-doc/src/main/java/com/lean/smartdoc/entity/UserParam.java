package com.lean.smartdoc.entity;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class UserParam {

    /**
     * username.
     */
    private String name;

    /**
     * user age.
     */
    private int age;

    /**
     * user address.
     */
    private AddressParam address;
}
