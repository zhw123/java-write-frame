package com.lean.smartdoc.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
@AllArgsConstructor
public class AddressParam {

    /**
     * city.
     */
    private String city;

    /**
     * zip code.
     */
    private String zipcode;
}
