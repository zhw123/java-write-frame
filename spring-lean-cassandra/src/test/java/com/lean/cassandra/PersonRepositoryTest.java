package com.lean.cassandra;

import com.lean.cassandra.entity.Person;
import com.lean.cassandra.repository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.cassandra.core.query.CassandraPageRequest;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;


@SpringBootTest
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository repository;

    @Test
    public void save() {
        Person person = new Person();
        person.setId(1);
        person.setName("xxb");
        person.setAge(18);
        repository.save(person);
    }

    @Test
    public void saveAll() {
        List<Person> list = new LinkedList<>();
        for (int i = 1; i < 20; i++) {
            Person person = new Person();
            person.setId(i + 1);
            person.setAge(18 + i);
            person.setName("test" + (i + 1));
            person.setNameCn("测试" + (i + 1));
            list.add(person);
        }
        repository.saveAll(list);
    }

    @Test
    public void findById() {
        Person person = repository.findById(1).orElse(null);
        System.out.println(person);
    }

    @Test
    public void findByName() {
        Person person = repository.findByName("xxb");
        System.out.println(person);
    }

    @Test
    public void update() {
        Person person = repository.findById(1).orElse(null);
        person.setAge(20);
        repository.save(person);
        System.out.println(person);
    }

    @Test
    public void all() {
        List<Person> personList = repository.findAll();
        System.out.println(personList);
    }

    @Test
    public void count() {
        long count = repository.count();
        System.out.println(count);
    }

    @Test
    public void delete() {
        repository.deleteById(1);
        Person person = repository.findById(1).orElse(null);
        System.out.println(person);
    }

    @Test
    public void deleteAll() {
        repository.deleteAll();
        long count = repository.count();
        System.out.println(count);
    }

    @Test
    public void page() {
        Pageable pageable = CassandraPageRequest.of(0, 5);
        List<Person> list = repository.findAll(pageable).getContent();
        System.out.println(list);
    }

}
