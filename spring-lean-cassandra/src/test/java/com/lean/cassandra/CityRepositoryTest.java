package com.lean.cassandra;

import com.lean.cassandra.entity.City;
import com.lean.cassandra.entity.Person;
import com.lean.cassandra.repository.CityRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedList;
import java.util.List;
@SpringBootTest
public class CityRepositoryTest {
    @Autowired
    private CityRepository cityRepository;

    @Test
    public void save() {
        List<String> list = new LinkedList<>();
        for (int i = 0; i < 20; i++) {
            Person person = new Person();
            person.setId(i + 1);
            person.setAge(18 + i);
            person.setName("test" + (i + 1));
            person.setNameCn("测试" + (i + 1));
            list.add(person.toString());
        }
        City city = new City();
        city.setId(1);
        city.setName("深圳");
        city.setPersons(list);
        cityRepository.save(city);
    }
}
