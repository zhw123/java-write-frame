package com.lean.cassandra.entity;

import lombok.Data;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@Table
public class Person {

    @PrimaryKey
    private Integer id;
    private String name;
    @Column(value = "name_cn")
    private String nameCn;
    private Integer age;

}
