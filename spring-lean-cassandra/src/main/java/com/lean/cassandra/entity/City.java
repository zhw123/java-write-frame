package com.lean.cassandra.entity;

import lombok.Data;
import org.springframework.data.cassandra.core.mapping.*;

import java.util.List;

@Data
@Table
public class City {
    @PrimaryKey
    private int id;
    private String name;
    private List<String> persons;
}
