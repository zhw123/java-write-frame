package com.lean.cassandra.entity;

import lombok.Data;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@Table("testdb")
public class TestDb {
    @PrimaryKeyColumn(value = "key_one",type = PrimaryKeyType.PARTITIONED)
    private String keyOne;
    @PrimaryKeyColumn(value = "key_two",type = PrimaryKeyType.CLUSTERED)
    private String keyTwo;
    @Column("value")
    private double value;
}
