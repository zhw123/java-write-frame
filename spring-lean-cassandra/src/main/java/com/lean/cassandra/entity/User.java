package com.lean.cassandra.entity;


import lombok.Data;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;
import java.util.List;


@Data
@Table("t_user")
public class User implements Serializable {

    @PrimaryKey
    private int id;
    private String name;
    private int age;
    private List<String> books;
}
