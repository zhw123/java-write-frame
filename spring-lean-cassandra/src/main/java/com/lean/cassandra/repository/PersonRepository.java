package com.lean.cassandra.repository;

import com.lean.cassandra.entity.Person;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;


public interface PersonRepository extends CassandraRepository<Person, Integer> {

    /**
     * 根据名字查询
     *
     * @param name 名字
     * @return Person
     */
    @AllowFiltering
    Person findByName(String name);
}

