package com.lean.cassandra.service;

import com.lean.cassandra.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.core.query.Query;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private CassandraTemplate cassandraTemplate;

    public void saveUser(User user){
        cassandraTemplate.insert(user);
    }

    public void batchSaveUser(List<User> userList){
        cassandraTemplate.batchOps().insert(userList);
    }

    public void updateUser(User user){
        cassandraTemplate.update(user);
    }

    public User getById(Serializable id) {
        return cassandraTemplate.selectOneById(id, User.class);
    }

    public User getObj(Query query) {
        return cassandraTemplate.selectOne(query, User.class);
    }

    public List<User> listObjs(Query query) {
        return cassandraTemplate.select(query, User.class);
    }
}
