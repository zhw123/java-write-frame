package com.lean.cassandra.service;

import com.lean.cassandra.entity.TestDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.core.query.Criteria;
import org.springframework.data.cassandra.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestDbService {


    @Autowired
    private CassandraTemplate cassandraTemplate;


    public TestDb save(TestDb testDb) {
        return cassandraTemplate.insert(testDb);
    }

    public List<TestDb> query(TestDb testDb) {
        return cassandraTemplate.select("select * from testdb where key_one = '"+testDb.getKeyOne()+"';", TestDb.class);
    }

    public boolean delete(TestDb testDb) {
        return cassandraTemplate.delete(Query.query(Criteria.where("key_one").is(testDb.getKeyOne())).and(Criteria.where("key_two").lte(testDb.getKeyTwo())), TestDb.class);
    }
}
