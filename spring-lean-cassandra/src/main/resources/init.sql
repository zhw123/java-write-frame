CREATE TABLE person
(
    id      int,
    name    text,
    name_cn text,
    age     int,
    PRIMARY KEY (id)
);
CREATE TABLE city
(
    id      int,
    name    text,
    persons list<text>,
    PRIMARY KEY (id)
);

CREATE TABLE t_user
(
    id    int,
    name  text,
    age   int,
    books list<text>,
    PRIMARY KEY (id)
);
CREATE TABLE testdb (
    key_one text,
    key_two text,
    value double,
    PRIMARY KEY (key_one,key_two )
);
