package com.lean.word;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 *https://poi.apache.org/index.html
 */
public class WordPoiTest {

    public static void main(String[] args) throws Exception {

        XWPFDocument xwpfDocument = generateWordXWPFDocument();
        xwpfDocument.write(new FileOutputStream("./doc/aa.docx"));
    }

    public static XWPFDocument generateWordXWPFDocument() {
        //1.构建文档对象
        XWPFDocument doc = new XWPFDocument();

        // 标题
        XWPFParagraph xwpfParagraph = doc.createParagraph();
        xwpfParagraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun xwpfRun = xwpfParagraph.createRun();
        xwpfRun.setBold(true);
        xwpfRun.setFontFamily("宋体");
        xwpfRun.setText("Springboot 生成Word文档");
        xwpfRun.setFontSize(22);

        // 段落
        createChapter(doc, "1. 概述",18);
        createChapter(doc, "1.1 介绍POI",12);
        createParagraph(doc,
            "Apache POI 项目的任务是创建和维护 Java API，以便根据 Office Open XML 标准 (OOXML) 和 Microsoft 的 OLE 2 复合文档格式 (OLE2) 处理各种文件格式。简而言之，您可以使用 Java 读写 MS Excel 文件。此外，您还可以使用 Java 读写 MS Word 和 MS PowerPoint 文件。Apache POI 是您的 Java Excel 解决方案（适用于 Excel 97-2008）。我们有完整的 API 用于移植其他 OOXML 和 OLE2 格式，欢迎其他人参与。");
        createChapter(doc, "1.2 二进制分布",12);
        createParagraph(doc, "poi-bin-5.2.3-20220909.tgz （58 MB，签名（.asc），校验和：SHA-256， SHA-512）");
        createParagraph(doc, "poi-bin-5.2.3-20220909.zip （58 MB，签名 (.asc)，校验和：SHA-256、 SHA-512）");

        // Chapter 2
        createChapter(doc, "2. 实现案例",18);
        createChapter(doc, "2.1 表单展示",12);
        createParagraph(doc, "假数据为例");

        // 表格
        XWPFParagraph paragraph = doc.createParagraph();
        //创建table 表格 5行 5 列
        XWPFTable table = paragraph.getDocument().createTable(5, 5);
        //设置宽度
        table.setWidth(500);
        //设置每行 内外边距
        table.setCellMargins(40, 40, 40, 40);

        // 表格属性
        CTTblPr tablePr = table.getCTTbl().addNewTblPr();
        // 表格宽度
        CTTblWidth width = tablePr.addNewTblW();
        width.setW(BigInteger.valueOf(8000));

        //设置标题
        List<XWPFTableCell> tableCells = table.getRow(0).getTableCells();
        tableCells.get(0).setText("标题一");
        tableCells.get(1).setText("标题二");
        tableCells.get(2).setText("标题三");
        tableCells.get(3).setText("标题四");
        tableCells.get(4).setText("标题五");

        //填充数据
        for (int i = 1; i < 5; i++) {
            tableCells = table.getRow(i).getTableCells();
            for (int j = 0; j < 5; j++) {
                tableCells.get(j).setText("a"+j);
            }
        }

        createChapter(doc, "2.2 图片展示",12);
        createParagraph(doc, "以导出图片为例");
        // 图片
        InputStream stream = null;
        try {
            XWPFParagraph paragraph2 = doc.createParagraph();
            Resource resource = new ClassPathResource("a.jpeg");
            stream = new FileInputStream(resource.getFile());
            XWPFRun run = paragraph2.createRun();
            //填充图片对象 设置 宽高
            run.addPicture(stream, Document.PICTURE_TYPE_PNG, "Generated", Units.toEMU(256), Units.toEMU(256));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return doc;
    }


    public static void createChapter(XWPFDocument doc, String content,Integer fontSize) {
        XWPFParagraph xwpfParagraph = doc.createParagraph();
        //设置位置 居中 左 右
        xwpfParagraph.setAlignment(ParagraphAlignment.LEFT);
        XWPFRun xwpfRun = xwpfParagraph.createRun();
        //加粗
        xwpfRun.setBold(true);
        //文本
        xwpfRun.setText(content);
        //字体大小
        xwpfRun.setFontSize(fontSize);
    }


    public static void createParagraph(XWPFDocument doc, String content) {
        XWPFParagraph xwpfParagraph = doc.createParagraph();
        XWPFRun xwpfRun = xwpfParagraph.createRun();
        xwpfRun.setText(content);
        xwpfRun.setFontSize(11);
    }
}
