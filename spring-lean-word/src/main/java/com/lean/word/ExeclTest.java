package com.lean.word;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.google.common.collect.Lists;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class ExeclTest {

    public static void main(String[] args) throws Exception{

        //导入 execl
//        SXSSFWorkbook sxssfWorkbook = generateExcelWorkbook();
//        sxssfWorkbook.write(new FileOutputStream("./doc/a.xlsx"));
//        sxssfWorkbook.dispose();

        //解析 execl
        parseExeclFile("./doc/a.xlsx");


    }

    /**
     * 生成 execl
     * @return
     */
    public static SXSSFWorkbook generateExcelWorkbook() {

        SXSSFWorkbook workbook = new SXSSFWorkbook();
        Sheet sheet = workbook.createSheet();

        int rows = 1;
        int cols = 1;
        // 表头
        Row head = sheet.createRow(rows++);
        String[] columns = new String[] {"标题一", "标题二", "标题三", "标题四", "标题五"};
        //每行宽度
        int[] colWidths = new int[] {5000, 5000, 5000, 5000, 5000};

        CellStyle headStyle = workbook.createCellStyle();
        // 字体
        Font font = workbook.createFont();
        font.setBold(true);
        headStyle.setFont(font);

        // 位置
        headStyle.setAlignment(HorizontalAlignment.CENTER);
        headStyle.setVerticalAlignment(VerticalAlignment.TOP);

        // 设置表格样式
        headStyle.setBorderBottom(BorderStyle.THIN);
        headStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        headStyle.setBorderLeft(BorderStyle.THIN);
        headStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        headStyle.setBorderRight(BorderStyle.THIN);
        headStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        headStyle.setBorderTop(BorderStyle.THIN);
        headStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

        headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        for (int i = 0; i < columns.length; ++i) {
            sheet.setColumnWidth(cols, colWidths[i]);
            //设置样式
            Cell cell = head.createCell(cols++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(columns[i]);
        }
        // 设置表格样式
        CellStyle contentStyle = workbook.createCellStyle();
        // 字体
        Font contentFont = workbook.createFont();
        contentFont.setBold(true);
        contentStyle.setFont(contentFont);

        // 位置
        contentStyle.setAlignment(HorizontalAlignment.CENTER);
        contentStyle.setVerticalAlignment(VerticalAlignment.TOP);

        // 设置表格样式
        contentStyle.setBorderBottom(BorderStyle.THIN);
        contentStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        contentStyle.setBorderLeft(BorderStyle.THIN);
        contentStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        contentStyle.setBorderRight(BorderStyle.THIN);
        contentStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        contentStyle.setBorderTop(BorderStyle.THIN);
        contentStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

        //填充数据
        List<Integer> dataList=new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            dataList.add(i);
        }
        List<List<Integer>> subList = Lists.partition(dataList, 5);
        for (List<Integer> list : subList) {
            cols = 1;
            Row row = sheet.createRow(rows++);
            for (Integer value : list) {
                Cell cell = row.createCell(cols++);
                cell.setCellStyle(contentStyle);
                cell.setCellValue(value);
            }
        }
        return workbook;
    }


    // ========================导入================================//

    public static void parseExeclFile(String filePath) throws Exception {
        XSSFWorkbook book = new XSSFWorkbook(new FileInputStream(filePath));
        XSSFSheet sheet = book.getSheetAt(0);
        // 解析数据
        int cols;
        for (int i = 1; i < sheet.getLastRowNum(); i++) {
            // 排除 表头
            XSSFRow row = sheet.getRow(i + 1);
            cols = 1;
            for (int j = 0; j < 5; j++) {
                int cellLongValue = Integer.parseInt("" + (int)(row.getCell(cols++)).getNumericCellValue());
                System.out.print(cellLongValue);
                System.out.print(" ");
            }
            System.out.println("\n");
        }

        book.close();
    }
}
