package com.lean.word;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * http://deepoove.com/poi-tl/
 */
@SpringBootApplication
public class SpringLeanWordApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanWordApplication.class, args);
    }

}
