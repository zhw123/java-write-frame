package com.lean.springsecurity.core.properties;

import lombok.Data;

/**
 *
 * comment:
 */
@Data
public class VerifyCodeProperties {

    //图形验证码的配置
    private ImageVerifyCodeProperties image = new ImageVerifyCodeProperties();

    //短信验证码的配置
    private BaseVerifyCodeProperties sms = new BaseVerifyCodeProperties();

}
