package com.lean.springsecurity.core.verify.update;

import com.lean.springsecurity.core.verify.BaseVerifyCode;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 
 * 
 * comment: 验证码生成接口
 */
public interface IVerifyCodeGenerator {

    /**
     * 生成验证码的接口
     * @param request
     * @return
     */
    public BaseVerifyCode generateVerifyCode(ServletWebRequest request);

}
