package com.lean.springsecurity.core.verify;


import org.springframework.security.core.AuthenticationException;

/**
 * 
 * 
 * comment:自定义的验证码异常
 */
public class VerifyCodeException extends AuthenticationException {

    public VerifyCodeException(String detail) {
        super(detail);
    }
}
