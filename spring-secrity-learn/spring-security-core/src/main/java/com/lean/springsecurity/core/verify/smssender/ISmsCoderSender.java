package com.lean.springsecurity.core.verify.smssender;

/**
 * 
 * 
 * comment:发送短信验证码的接口
 */
public interface ISmsCoderSender {

    public void sendSms(String mobile,String verifyCode);

}
