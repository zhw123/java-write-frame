package com.lean.springsecurity.core.properties;

/**
 * comment: 登录类型的配置
 */
public enum LoginType {
    REDIRECT,
    JSON
}
