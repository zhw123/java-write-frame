package com.lean.springsecurity.example.dto;

import lombok.Data;
import lombok.ToString;

/**
 */
@Data
@ToString
public class UserQueryCondition {

    private String username;

    private String age;

    private String ageTo;

    private String xxx;


}
