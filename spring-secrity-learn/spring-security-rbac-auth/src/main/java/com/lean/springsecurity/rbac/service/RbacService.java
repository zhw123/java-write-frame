package com.lean.springsecurity.rbac.service;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * 
 * 
 * comment:
 */
public interface RbacService {

    public boolean hasPermission(HttpServletRequest request, Authentication authentication);

}
