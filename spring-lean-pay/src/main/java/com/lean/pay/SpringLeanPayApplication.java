package com.lean.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

//引入Spring Task
@EnableScheduling
@SpringBootApplication
public class SpringLeanPayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanPayApplication.class, args);
    }

}
