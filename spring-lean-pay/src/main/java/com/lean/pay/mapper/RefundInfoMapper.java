package com.lean.pay.mapper;

import com.lean.pay.entity.RefundInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface RefundInfoMapper extends BaseMapper<RefundInfo> {

}
