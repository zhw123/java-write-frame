package com.lean.pay.mapper;

import com.lean.pay.entity.OrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

}
