package com.lean.pay.mapper;

import com.lean.pay.entity.PaymentInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface PaymentInfoMapper extends BaseMapper<PaymentInfo> {
}
