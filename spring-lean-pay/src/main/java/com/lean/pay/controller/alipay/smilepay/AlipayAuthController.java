package com.lean.pay.controller.alipay.smilepay;

import com.lean.pay.service.AlipayAuthService;
import com.lean.pay.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 刷脸付
 *      无需手机，刷脸支付： 当不便使用手机或没有手机时，用户亦可“刷脸”完成——通过线下支付机具读取脸部完成自助结账等支付行为，快捷安全方便。 商家多一种方案，用户多一种选择，同样方便安全。
 * https://opendocs.alipay.com/open/20180402104715814204/intro?ref=api
 */
@CrossOrigin
@RestController
@RequestMapping("/api/ali-pay-smilepay")
@Api(tags = "支付宝-刷脸付")
@Slf4j
public class AlipayAuthController {

    @Autowired
    private AlipayAuthService alipayAuthService;


    /**
     * 刷脸支付初始化
     * 通过该接口获取刷脸支付服务的初始化信息，详细字段描述请参考产品说明文档
     * @return
     */
    @ApiOperation("刷脸支付初始化")
    @RequestMapping("/smilepay/initialize")
    public R initSmilepay(){
        String initSmilepay = alipayAuthService.initSmilepay();
        return R.ok().data("result",initSmilepay);
    }

    /**
     * 人脸ftoken查询消费接口
     * @return
     */
    @ApiOperation("刷脸支付初始化")
    @RequestMapping("/customer/ftoken/query")
    public R customerFtokenQuery(){
        String query = alipayAuthService.customerFtokenQuery();
        return R.ok().data("result",query);
    }

}
