package com.lean.pay.controller.alipay.phoneweb;

import com.lean.pay.service.AlipayPhonePayService;
import com.lean.pay.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * H5页面
 *手机网站支付是指商户在移动端网页展示商品或服务，用户在商户页面确认使用支付宝支付时，
 * 浏览器自动跳转支付宝APP或支付宝网页完成付款的支付产品。该产品在签约完成后，需要技术集成方可使用。
 * https://opendocs.alipay.com/open/203/105288
 */
@CrossOrigin
@RestController
@RequestMapping("/api/ali-phone-web-pay")
@Api(tags = "手机网站支付--H5")
@Slf4j
public class AlipayPhonePayController {

    @Autowired
    private AlipayPhonePayService alipayPhonePayService;

    /**
     * 手机网站支付接口
     * 外部商户创建订单并支付
     */
    @ApiOperation("手机网站支付接口")
    @GetMapping("/trade/page/create/{productId}")
    public R createOrderPay(@PathVariable Long productId){
        log.info("统一收单线下交易预创建的调用");
        String form = alipayPhonePayService.createOrderPay(productId);
        return R.ok().data("form", form);
    }

    /**
     * 统一收单交易查询
     * 该接口提供所有支付宝支付订单的查询，商户可以通过该接口主动查询订单状态，完成下一步的业务逻辑。
     * 需要调用查询接口的情况：
     * 当商户后台、网络、服务器等出现异常，商户系统最终未接收到支付通知；
     * 调用支付接口后，返回系统错误或未知交易状态情况；
     * 调用alipay.trade.pay，返回INPROCESS的状态；
     * 调用alipay.trade.cancel之前，需确认支付状态；
     */
    @ApiOperation("统一收单交易查询")
    @GetMapping("/trade/page/query/{orderNo}")
    public R queryOrderPay(@PathVariable String orderNo){
        log.info("统一收单交易查询的调用");
        String result = alipayPhonePayService.queryOrderPay(orderNo);
        return R.ok().data("result", result);
    }

    /**
     * 统一收单交易退款接口
     *
     */
    @ApiOperation("统一收单交易退款接口")
    @GetMapping("/trade/page/refund")
    public R refundOrder(String orderNo, String reason){
        log.info("统一收单交易退款接口的调用");
        alipayPhonePayService.refundOrder(orderNo,reason);
        return R.ok();
    }

    /**
     * 统一收单交易退款查询
     * 背景：
     *      商户可使用该接口查询自已通过alipay.trade.refund提交的退款请求是否执行成功。
     *
     * 注意：
     *      1. 该接口的返回码10000，仅代表本次查询操作成功，不代表退款成功，当接口返回的refund_status值为REFUND_SUCCESS时表示退款成功，否则表示退款没有执行成功。
     *      2. 如果退款未成功，商户可以调用退款接口重试，重试时请务必保证退款请求号和退款金额一致，防止重复退款。
     *      3. 发起退款查询接口的时间不能离退款请求时间太短，建议之间间隔10秒以上。
     */
    @ApiOperation("统一收单交易退款查询")
    @GetMapping("/trade/page/refundQuery")
    public R refundQueryOrder(String orderNo){
        log.info("统一收单交易退款接口的调用");
        String result = alipayPhonePayService.refundQueryOrder(orderNo);
        return R.ok().setMessage("查询成功").data("result", result);
    }


    /**
     * 统一收单交易关闭接口
     * 背景：
     *      用于交易创建后，用户在一定时间内未进行支付，可调用该接口直接将未付款的交易进行关闭。
     */
    @ApiOperation("用户取消订单")
    @PostMapping("/trade/close/{orderNo}")
    public R cancel(@PathVariable String orderNo){

        log.info("取消订单");
        alipayPhonePayService.cancelOrder(orderNo);
        return R.ok().setMessage("订单已取消");
    }

    /**
     * alipay.data.dataservice.bill.downloadurl.query(查询对账单下载地址)
     * 背景：
     *      为方便商户快速查账，支持商户通过本接口获取商户离线账单下载地址
     * 文档：
     *      https://opendocs.alipay.com/open/02ekfm?ref=api
     */
    @ApiOperation("获取账单url")
    @GetMapping("/bill/downloadurl/query/{billDate}/{type}")
    public R queryTradeBill(
            @PathVariable String billDate,
            @PathVariable String type)  {
        log.info("获取账单url");
        String downloadUrl = alipayPhonePayService.queryBill(billDate, type);
        return R.ok().setMessage("获取账单url成功").data("downloadUrl", downloadUrl);
    }

}
