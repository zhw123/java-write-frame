package com.lean.pay.controller.alipay.red;

import com.alipay.api.domain.CashCampaignInfo;
import com.lean.pay.service.AlipayRedPayService;
import com.lean.pay.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 营销活动送红包
 *
 * 营销活动送红包是为支付宝签约商家提供的一种营销工具。
 * 商家可以根据自己的营销需求创建现金红包活动，设计大转盘游戏、刮刮卡、成为会员即送现金等丰富多彩的玩法。可以通过控制活动预算、用户领取次数、领取频率等，让活动设计更贴合商家需求。
 * 用户领取现金红包后，资金将直接进支付宝余额，并在 App 首页的红包应用中，查看到出资商家的品牌信息。
 * 若红包没有被领取完，资金将会在红包失效时（有效时间由商家创建现金红包时指定）退回到商家的支付宝账户中。
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/api/ali-red-pay")
@Api(tags = "营销活动送红包")
@Slf4j
public class AlipayRedPayController {

    @Autowired
    private AlipayRedPayService alipayRedPayService;

    /**
     * 创建现金活动
     *  商户通过开放平台创建商户活动
     */
    @ApiOperation("1.创建现金活动")
    @GetMapping("/marketing/campaign/cash/create")
    public R createRedActivity(){
        log.info("创建现金活动的调用");
        Map<String,Object> resultMap= alipayRedPayService.createRedActivity();
        return R.ok().setData(resultMap);
    }


    /**
     * 触发现金红包活动
     *  用户通过开放平台参与现金活动
     */
    @ApiOperation("2.触发现金红包活动")
    @GetMapping("/marketing/campaign/cash/trigger")
    public R triggerRedActivity(String crowdNo){
        log.info("创建现金活动的调用");
        String result= alipayRedPayService.triggerRedActivity(crowdNo);
        return R.ok().data("result",result);
    }


    /**
     * 更改现金活动状态
     *  商户更改现金活动状态，启动/暂停/终止
     * @param crowdNo 现金活动号，通过alipay.marketing.campaign.cash.create(创建现金活动)接口创建现金活动获取。
     * @param campStatus 金活动修改后的状态。支持修改为：PAUSE：活动暂停。READY：活动开始。CLOSED：活动结束。
     * @return
     */
    @ApiOperation("更改现金活动状态")
    @GetMapping("/marketing/campaign/cash/status/modify")
    public R modifyRedActivityStatus(String crowdNo,String campStatus){
        log.info("更改现金活动状态的调用");
        alipayRedPayService.modifyRedActivityStatus(crowdNo,campStatus);
        return R.ok();
    }


    /**
     * 现金活动列表查询
     *  商户通过开放平台查询自己创建的现金活动列表
     * @Param pageNum :分页查询时的页码，从1开始
     * @Param pageSize :分页查询时每页返回的列表大小，每页数据最大为 50。
     */
    @ApiOperation("现金活动列表查询")
    @GetMapping("/marketing/campaign/cash/list/query")
    public R redActivityList(String pageSize,String pageNum){
        log.info("创建现金活动列表查询的调用");
        List<CashCampaignInfo> cashCampaignInfos = alipayRedPayService.redActivityList(pageSize, pageNum);
        Map<String,Object> resultMap=new HashMap<>();
        resultMap.put("pageNum",pageNum);
        resultMap.put("pageSize",pageSize);
        resultMap.put("list",cashCampaignInfos);
        return R.ok().setData(resultMap);
    }


    /**
     * 现金活动详情查询
     *  商户通过开放平台查询自己创建的现金活动详情
     */
    @ApiOperation("现金活动详情查询")
    @GetMapping("/marketing/campaign/cash/detail/query")
    public R redActivityDetail(String crowdNo){
        log.info("现金活动详情查询的调用");
        String result=  alipayRedPayService.redActivityDetail(crowdNo);
        return R.ok().data("result",result);
    }


}
