package com.lean.pay.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.*;
import com.alipay.api.request.*;
import com.alipay.api.response.*;
import com.lean.pay.service.AlipayRedPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class AlipayRedPayServiceImpl implements AlipayRedPayService {

    @Resource
    private AlipayClient alipayClient;

    @Resource
    private Environment config;

    private static final SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public Map<String, Object> createRedActivity() {
        try {
            AlipayMarketingCampaignCashCreateRequest request = new AlipayMarketingCampaignCashCreateRequest();
            AlipayMarketingCampaignCashCreateModel model = new AlipayMarketingCampaignCashCreateModel();
            // 红包名称,商户在查询列表、详情看到的名字,同时也会显示在商户付款页面。
            model.setCouponName("XXX周年庆红包");
            // 活动结束时间,必须大于活动开始时间, 基本格式:yyyy-MM-dd HH:mm:ss,活动有效时间最长为6个月，过期后需要创建新的活动。
            model.setEndTime(getBeforeXMonthDate(simpleDateFormat.format(new Date()), 6));
            // 商户打款后的跳转链接，从支付宝收银台打款成功后的跳转链接。不填时，打款后停留在支付宝支付成功页。
            // 商户实际跳转会自动添加crowdNo作为跳转参数。示例: http://www.yourhomepage.com?crowdNo=XXX
            model.setMerchantLink("http://www.weibo.com");
            // 活动文案，用户在账单、红包中看到的账单描述、红包描述。
            model.setPrizeMsg("XXX送您大红包");
            // 现金红包的发放形式,。枚举支持：
            // *fixed：固定金额。
            // *random：随机金额。选择随机金额时，单个红包的金额在平均金额的0.5~1.5倍之间浮动。
            model.setPrizeType("random");
            //用户在当前活动参与次数、频率限制。支持日(D)、周(W)、月(M)、终身(L)维度的限制，整个字段不填时默认值为L1（即用户终生仅能参与1次）。
            //注意：
            //1. 终身(L)限制必选且最大值为 100。
            //2. 日(D)、周(W)、月(M)限制可选且最多只能选择一个（即限制日领取次数就不能再限制月领取次数），频率设置必须小于等于终身(L)的次数。
            //3. 多个配置之间使用"|"进行分隔。如 D3|L4（表示限制用户终生只能参与4次，单日只能参与3次）。
            //4. 允许多次领取时，活动触发接口需要传入out_biz_no来配合。
            model.setSendFreqency("D3|L10");
            // 活动开始时间，必须大于活动创建的时间。入参支持如下两种形式：
            // 1. 填入固定时间，时间格式为 yyyy-MM-dd HH:mm:ss，如 2020-12-10 22:28:30
            // 2. 填字符串 NowTime，表示创建即生效。
            model.setStartTime("2022");
            // 活动发放的现金总金额,最小金额1.00元,最大金额10000000.00元。每个红包的最大金额不允许超过200元,最小金额不得低于0.20元。 实际的金额限制可能会根据业务进行动态调整。
            model.setTotalMoney("10000000.00");
            // 红包发放个数，最小为 1 个，最大10000000个。
            // 说明：不同的发放形式（即prize_type）会使得发放个数含义不同：
            // 1. 若prize_type 为 fixed（固定金额），则每个用户领取的红包金额为total_money除以total_num得到固定金额。
            // 2. 若prize_type为 random（随机金额），则每个用户领取的红包金额为total_money除以total_num得到的平均金额值的0.5~1.5倍。
            // 由于金额是随机的，在红包金额全部被领取完时，有可能total_num有所剩余、或者大于设置值的情况。
            model.setTotalNum("1000");
            request.setBizModel(model);
            AlipayMarketingCampaignCashCreateResponse response = alipayClient.execute(request);
            if (response.isSuccess()) {
                System.out.println("调用成功");
                Map<String,Object> resultMap=new HashMap<>();
                resultMap.put("crowdNo",response.getCrowdNo());
                resultMap.put("originCrowdNo",response.getOriginCrowdNo());
                resultMap.put("payUrl",response.getPayUrl());
                return resultMap;
            } else {
                System.out.println("调用失败");
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("创建现金活动失败");
        }
    }

    @Override
    public String triggerRedActivity(String crowdNo) {
        try {
            AlipayMarketingCampaignCashTriggerRequest request = new AlipayMarketingCampaignCashTriggerRequest();
            AlipayMarketingCampaignCashTriggerModel model=new AlipayMarketingCampaignCashTriggerModel();
            //现金活动号，通过alipay.marketing.campaign.cash.create(创建现金活动)接口创建现金活动获取。
            model.setCrowdNo(crowdNo);
            //用户登录账号名，一般为邮箱或手机号。
            //注意：user_id与login_id二选一必填，同时传入时以user_id为准。
            model.setLoginId("username@gmail.com");
            //此字段如果传入金额，就忽略prize_type算法，按照传入的金额发奖。
            // 如果不传或者小于等于0，则按照活动创建时指定的prize_type为fixed或者random算法发奖
            //分为单位
            model.setOrderPrice("100");
            //领取红包的外部业务号，只由可由字母、数字、下划线组成。同一个活动中不可重复，
            // 相同的外部业务号会被幂等并返回之前的结果。不填时，系统会生成一个默认固定的外部业务号。
            model.setOutBizNo("201702101356");
            //用户支付宝唯一标识，2088开头。
            //注意：user_id与login_id二选一必填，同时传入时以user_id为准。
            model.setUserId("2088102164186692");
            request.setBizModel(model);
            AlipayMarketingCampaignCashTriggerResponse response = alipayClient.execute(request);
            if(response.isSuccess()){
                System.out.println("调用成功");
                return  response.getBody();
            } else {
                System.out.println("调用失败");
                return null;
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("触发现金红包活动失败");
        }
    }

    @Override
    public void modifyRedActivityStatus(String crowdNo, String campStatus) {
        try {
            AlipayMarketingCampaignCashStatusModifyRequest request = new AlipayMarketingCampaignCashStatusModifyRequest();
            AlipayMarketingCampaignCashStatusModifyModel model=new AlipayMarketingCampaignCashStatusModifyModel();
            //金活动修改后的状态。支持修改为：
            //*PAUSE：活动暂停。
            //*READY：活动开始。
            //*CLOSED：活动结束。
            model.setCampStatus(campStatus);
            model.setCrowdNo(crowdNo);
            request.setBizModel(model);
            AlipayMarketingCampaignCashStatusModifyResponse response = alipayClient.execute(request);
            if(response.isSuccess()){
                System.out.println("调用成功");
            } else {
                System.out.println("调用失败");
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("触发更改现金活动状态失败");
        }
    }

    @Override
    public List<CashCampaignInfo> redActivityList(String pageSize, String pageNum) {
        try {
            AlipayMarketingCampaignCashListQueryRequest request = new AlipayMarketingCampaignCashListQueryRequest();
            AlipayMarketingCampaignCashListQueryModel model=new AlipayMarketingCampaignCashListQueryModel();
            //要查询的活动状态,不填默认为 ALL 返回所有类型。枚举支持:
            //*ALL：所有类型的活动。
            //*CREATED：已创建未打款。
            //*PAID：已打款。
            //*READY：活动已开始。
            //*PAUSE：活动已暂停。
            //*CLOSED：活动已结束。
            //*SETTLE：活动已清算。
            model.setCampStatus("PAID");
            //分页查询时每页返回的列表大小，每页数据最大为 50。
            model.setPageIndex(pageSize);
            //分页查询时的页码，从1开始
            model.setPageSize(pageNum);
            request.setBizModel(model);
            AlipayMarketingCampaignCashListQueryResponse response = alipayClient.execute(request);
            if(response.isSuccess()){
                System.out.println("调用成功");
                return  response.getCampList();
            } else {
                System.out.println("调用失败");
                return null;
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("触发现金活动列表查询失败");
        }
    }

    @Override
    public String redActivityDetail(String crowdNo) {
        try {
            AlipayMarketingCampaignCashDetailQueryRequest request = new AlipayMarketingCampaignCashDetailQueryRequest();
            AlipayMarketingCampaignCashDetailQueryModel model=new AlipayMarketingCampaignCashDetailQueryModel();
            model.setCrowdNo(crowdNo);
            request.setBizModel(model);
            AlipayMarketingCampaignCashDetailQueryResponse response = alipayClient.execute(request);
            if(response.isSuccess()){
                System.out.println("调用成功");
                return response.getBody();
            } else {
                System.out.println("调用失败");
                return null;
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("触发现金活动详情查询失败");
        }
    }

    // 将月粒度日期格式yyyy-MM-dd格式向前推X个月或向后推X个月的方法
    public String getBeforeXMonthDate(String date, int month) {
        Calendar calendar = Calendar.getInstance();
        try {
            // 将传过来的日期设置给calendar
            calendar.setTime(simpleDateFormat.parse(date));
            // System.out.println("当前日期="+sdf.format(calendar.getTime()));
            // 将传过来的月份减去X个月或者加上X个月
            calendar.add(Calendar.MONTH, month);
            // System.out.println("向前推12月之前的日期="+sdf.format(calendar.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(calendar.getTime());
    }
}
