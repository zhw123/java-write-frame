package com.lean.pay.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.*;
import com.alipay.api.request.*;
import com.alipay.api.response.*;
import com.google.common.collect.Lists;
import com.lean.pay.entity.OrderInfo;
import com.lean.pay.entity.RefundInfo;
import com.lean.pay.enums.OrderStatus;
import com.lean.pay.enums.PayType;
import com.lean.pay.enums.wxpay.AliPayTradeState;
import com.lean.pay.service.AliPayFaceService;
import com.lean.pay.service.OrderInfoService;
import com.lean.pay.service.RefundInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;

@Service
@Slf4j
public class AliPayFaceServiceImpl implements AliPayFaceService {

    @Resource
    private OrderInfoService orderInfoService;

    @Resource
    private AlipayClient alipayClient;

    @Resource
    private Environment config;

    @Resource
    private RefundInfoService refundsInfoService;

    @Override
    public String createPayOrder(Long productId, Integer type) {
        try {
            // 生成订单
            log.info("生成订单");
            OrderInfo orderInfo = orderInfoService.createOrderByProductId(productId, PayType.ALIPAY.getType());
            // 调用支付宝接口 二维码扫码支付
            AlipayTradePayRequest request = new AlipayTradePayRequest();
            //配置需要的公共请求参数
            //支付完成后，支付宝向支付测试发起异步通知的地址
            request.setNotifyUrl(config.getProperty("alipay.notify-url"));
            //组装当前业务方法的请求参数
            AlipayTradePayModel  model = new AlipayTradePayModel();
            //商户订单号。由商家自定义，64个字符以内，仅支持字母、数字、下划线且需保证在商户端不重复。
            model.setOutTradeNo(orderInfo.getOrderNo());
            //订单总金额，单位为元，精确到小数点后两位，取值范围为 [0.01,100000000]，金额不能为 0。如果同时传入了【可打折金额】，【不可打折金额】，
            // 【订单总金额】三者，则必须满足如下条件：【订单总金额】=【可打折金额】+【不可打折金额】
            BigDecimal total = new BigDecimal(orderInfo.getTotalFee().toString()).divide(new BigDecimal("100"));
            model.setTotalAmount(total.toString());
            //订单标题。注意：不可使用特殊字符，如 /，=，& 等。
            model.setSubject(orderInfo.getTitle());
            if(type == 1){
                //支付授权码。
                //当面付场景传买家的付款码（25~30开头的长度为16~24位的数字，实际字符串长度以开发者获取的付款码长度为准）或者刷脸标识串（fp开头的35位字符串）；
                //周期扣款或代扣场景无需传入，协议号通过agreement_params参数传递；
                //支付宝预授权和新当面资金授权场景无需传入，授权订单号通过 auth_no字段传入。
                //注：交易的买家与卖家不能相同。
                model.setAuthCode("28763443825664394");
                //支付场景。枚举值：
                //bar_code：当面付条码支付场景；
                //security_code：当面付刷脸支付场景，对应的auth_code为fp开头的刷脸标识串；
                //周期扣款或代扣场景无需传入，协议号通过agreement_params参数传递；
                //支付宝预授权和新当面资金授权场景无需传入，授权订单号通过 auth_no字段传入。
                //默认值为bar_code。
                model.setScene("bar_code");
            }
            //用户与商户签署周期扣款协议后，商户可通过本接口做后续免密代扣操作
            if(type == 2){
                //产品码。商家和支付宝签约的产品码。 枚举值：CYCLE_PAY_AUTH：周期扣款产品；GENERAL_WITHHOLDING：代扣产品；
                // 注意：非当面付产品使用本接口时，本参数必填。请传入对应产品码。
                model.setProductCode("CYCLE_PAY_AUTH");
                //协议信息
                //代扣信息。代扣业务需要传入的协议相关信息，使用本参数传入协议号后scene和auth_code不需要再传值。
                //支付宝系统中用以唯一标识用户签约记录的编号（用户签约成功后的协议号 ）
                AgreementParams agreementParams=new AgreementParams();
                agreementParams.setAgreementNo("20170322450983769228");
                model.setAgreementParams(agreementParams);
            }
            //用户在商户侧授权冻结并享受服务后，商户使用授权单号通过本接口对用户已授权金额发起扣款
            if(type == 3){
                //产品码。商家和支付宝签约的产品码。 支付宝预授权场景传：PRE_AUTH_ONLINE；新当面资金授权场景传：PRE_AUTH；
                model.setProductCode("PRE_AUTH_ONLINE");
                //资金预授权单号。支付宝预授权和新当面资金授权场景下必填。
                model.setAuthNo("2016110310002001760201905725");
                //预授权确认模式。
                //适用于支付宝预授权和新当面资金授权场景。枚举值：
                //COMPLETE：转交易完成后解冻剩余冻结金额；
                //NOT_COMPLETE：转交易完成后不解冻剩余冻结金额；
                //默认值为NOT_COMPLETE。
                model.setAuthConfirmMode("COMPLETE");
            }
            request.setBizModel(model);
            // 执行请求，调用支付宝接口
            AlipayTradePayResponse response = alipayClient.execute(request);
            if (response.isSuccess()) {
                log.info("调用成功，返回结果 ===> " + response.getBody());
                return response.getBody();
            } else {
                log.info("调用失败，返回码 ===> " + response.getCode() + ", 返回描述 ===> " + response.getMsg());
                throw new RuntimeException("创建支付交易失败");
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("创建支付交易失败");
        }
    }

    @Override
    public String queryPayOrder(String orderNo) {
        try {
            log.info("查单接口调用 ===> {}", orderNo);
            AlipayTradeQueryRequest  request = new AlipayTradeQueryRequest();
            AlipayTradeQueryModel alipayTradeQueryModel=new AlipayTradeQueryModel();
            //支付宝交易号，和商户订单号不能同时为空
            alipayTradeQueryModel.setOutTradeNo(orderNo);
            request.setBizModel(alipayTradeQueryModel);
            AlipayTradeQueryResponse  response = alipayClient.execute(request);
            if (response.isSuccess()) {
                log.info("调用成功，返回结果 ===> " + response.getBody());
                return response.getBody();
            } else {
                log.info("调用失败，返回码 ===> " + response.getCode() + ", 返回描述 ===> " + response.getMsg());
                return null;//订单不存在
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("查单接口的调用失败");
        }
    }

    @Override
    public String refund(String orderNo, String reason) {
        try {
            log.info("调用退款API");
            //创建退款单
            RefundInfo refundInfo = refundsInfoService.createRefundByOrderNoForAliPay(orderNo, reason);
            //调用统一收单交易退款接口
            AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
            //组装当前业务方法的请求参数
            AlipayTradeRefundModel alipayTradeRefundModel=new AlipayTradeRefundModel();
            alipayTradeRefundModel.setGoodsDetail(Lists.newArrayList());
            //商户订单号。
            alipayTradeRefundModel.setOutTradeNo(orderNo);
            //退款金额。
            //需要退款的金额，该金额不能大于订单金额，单位为元，支持两位小数。
            //注：如果正向交易使用了营销，该退款金额包含营销金额，支付宝会按业务规则分配营销和买家自有资金分别退多少，默认优先退买家的自有资金。
            // 如交易总金额100元，用户支付时使用了80元自有资金和20元无资金流的营销券，商家实际收款80元。如果首次请求退款60元，则60元全部从商家收款资金扣除退回给用户自有资产；
            // 如果再请求退款40元，则从商家收款资金扣除20元退回用户资产以及把20元的营销券退回给用户（券是否可再使用取决于券的规则配置）。
            BigDecimal refund = new BigDecimal(refundInfo.getRefund().toString()).divide(new BigDecimal("100"));
            alipayTradeRefundModel.setRefundAmount(refund.toString());
            //退款原因说明
            alipayTradeRefundModel.setRefundReason(reason);
            //订单退款币种信息。支持英镑：GBP、港币：HKD、美元：USD、新加坡元：SGD、日元：JPY、加拿大元：CAD、澳元：AUD、欧元：EUR、新西兰元：NZD、韩元：KRW、
            // 泰铢：THB、瑞士法郎：CHF、瑞典克朗：SEK、丹麦克朗：DKK、挪威克朗：NOK、马来西亚林吉特：MYR、印尼卢比：IDR、菲律宾比索：PHP、毛里求斯卢比：MUR、
            // 以色列新谢克尔：ILS、斯里兰卡卢比：LKR、俄罗斯卢布：RUB、阿联酋迪拉姆：AED、捷克克朗：CZK、南非兰特：ZAR、人民币：CNY
            alipayTradeRefundModel.setRefundCurrency("CNY");

            //退款请求号。 标识一次退款请求，需要保证在交易号下唯一，如需部分退款，则此参数必传。
            //注：针对同一次退款请求，如果调用接口失败或异常了，重试时需要保证退款请求号不能变更，防止该笔交易重复退款。支付宝会保证同样的退款请求号多次请求只会退一次。
            //可选 需要在退款查询接口使用，如果没有填写，则默认是创建交易时的商户订单号
//            alipayTradeRefundModel.setOutRequestNo("");


            request.setBizModel(alipayTradeRefundModel);
            //执行请求，调用支付宝接口
            AlipayTradeRefundResponse response = alipayClient.execute(request);
            if (response.isSuccess()) {
                log.info("调用成功，返回结果 ===> " + response.getBody());
                //更新订单状态
                orderInfoService.updateStatusByOrderNo(orderNo, OrderStatus.REFUND_SUCCESS);
                //更新退款单
                refundsInfoService.updateRefundForAliPay(
                        refundInfo.getRefundNo(),
                        response.getBody(),
                        AliPayTradeState.REFUND_SUCCESS.getType()); //退款成功
                return response.getBody();
            } else {
                log.info("调用失败，返回码 ===> " + response.getCode() + ", 返回描述 ===> " + response.getMsg());
                //更新订单状态
                orderInfoService.updateStatusByOrderNo(orderNo, OrderStatus.REFUND_ABNORMAL);
                //更新退款单
                refundsInfoService.updateRefundForAliPay(
                        refundInfo.getRefundNo(),
                        response.getBody(),
                        AliPayTradeState.REFUND_ERROR.getType()); //退款失败
                return null;
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("创建退款申请失败");
        }
    }

    @Override
    public String queryRefund(String orderNo,String outRequestNo) {
        try {
            log.info("查询退款接口调用 ===> {}", orderNo);
            AlipayTradeFastpayRefundQueryRequest request = new AlipayTradeFastpayRefundQueryRequest();
            AlipayTradeFastpayRefundQueryModel alipayTradeFastpayRefundQueryModel=new AlipayTradeFastpayRefundQueryModel();
            //退款请求号。 请求退款接口时，传入的退款请求号，如果在退款请求时未传入，则该值为创建交易时的商户订单号。
            alipayTradeFastpayRefundQueryModel.setOutRequestNo(orderNo);
            //支付宝交易号。 和商户订单号不能同时为空
            alipayTradeFastpayRefundQueryModel.setOutTradeNo(orderNo);
            //退款请求号。 请求退款接口时，传入的退款请求号，如果在退款请求时未传入，则该值为创建交易时的商户订单号。
            alipayTradeFastpayRefundQueryModel.setOutRequestNo(StringUtils.isEmpty(outRequestNo)?orderNo:outRequestNo);
            request.setBizModel(alipayTradeFastpayRefundQueryModel);
            AlipayTradeFastpayRefundQueryResponse response = alipayClient.execute(request);
            if (response.isSuccess()) {
                log.info("调用成功，返回结果 ===> " + response.getBody());
                return response.getBody();
            } else {
                log.info("调用失败，返回码 ===> " + response.getCode() + ", 返回描述 ===> " + response.getMsg());
                //throw new RuntimeException("查单接口的调用失败");
                return null;//订单不存在
            }

        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("查单接口的调用失败");
        }
    }

    @Override
    public void cancelOrder(String orderNo) {
        //调用支付宝提供的统一收单交易撤销接口
        try {
            log.info("撤销接口的调用，订单号 ===> {}", orderNo);
            AlipayTradeCancelRequest request = new AlipayTradeCancelRequest();
            AlipayTradeCancelModel  alipayTradeCancelModel=new AlipayTradeCancelModel ();
            alipayTradeCancelModel.setOutTradeNo(orderNo);
            request.setBizModel(alipayTradeCancelModel);
            AlipayTradeCancelResponse  response = alipayClient.execute(request);
            if (response.isSuccess()) {
                log.info("调用成功，返回结果 ===> " + response.getBody());

            } else {
                log.info("调用失败，返回码 ===> " + response.getCode() + ", 返回描述 ===> " + response.getMsg());
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("撤销接口的调用失败");
        }
    }

    @Override
    public void closeOrder(String orderNo) {
        //调用支付宝提供的统一收单交易关闭接口
        try {
            log.info("关单接口的调用，订单号 ===> {}", orderNo);

            AlipayTradeCloseRequest request = new AlipayTradeCloseRequest();
            AlipayTradeCloseModel alipayTradeCloseModel=new AlipayTradeCloseModel();
            alipayTradeCloseModel.setOutTradeNo(orderNo);
            request.setBizModel(alipayTradeCloseModel);
            AlipayTradeCloseResponse response = alipayClient.execute(request);
            if (response.isSuccess()) {
                log.info("调用成功，返回结果 ===> " + response.getBody());
                //更新用户订单状态
                orderInfoService.updateStatusByOrderNo(orderNo, OrderStatus.CANCEL);
            } else {
                log.info("调用失败，返回码 ===> " + response.getCode() + ", 返回描述 ===> " + response.getMsg());
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("关单接口的调用失败");
        }
    }

    @Override
    public String queryBill(String billDate, String type) {
        try {
            AlipayDataDataserviceBillDownloadurlQueryRequest request = new AlipayDataDataserviceBillDownloadurlQueryRequest();
            AlipayDataDataserviceBillDownloadurlQueryModel alipayDataDataserviceBillDownloadurlQueryModel=new AlipayDataDataserviceBillDownloadurlQueryModel();
            //账单类型，商户通过接口或商户经开放平台授权后其所属服务商通过接口可以获取以下账单类型，支持：
            //trade：商户基于支付宝交易收单的业务账单；
            //signcustomer：基于商户支付宝余额收入及支出等资金变动的账务账单。
            alipayDataDataserviceBillDownloadurlQueryModel.setBillType(type);
            //账单时间：
            //* 日账单格式为yyyy-MM-dd，最早可下载2016年1月1日开始的日账单。不支持下载当日账单，只能下载前一日24点前的账单数据（T+1），当日数据一般于次日 9 点前生成，特殊情况可能延迟。
            //* 月账单格式为yyyy-MM，最早可下载2016年1月开始的月账单。不支持下载当月账单，只能下载上一月账单数据，当月账单一般在次月 3 日生成，特殊情况可能延迟。
            alipayDataDataserviceBillDownloadurlQueryModel.setBillDate(billDate);
            request.setBizModel(alipayDataDataserviceBillDownloadurlQueryModel);
            AlipayDataDataserviceBillDownloadurlQueryResponse response = alipayClient.execute(request);
            if (response.isSuccess()) {
                log.info("调用成功，返回结果 ===> " + response.getBody());
                return response.getBillDownloadUrl();
            } else {
                log.info("调用失败，返回码 ===> " + response.getCode() + ", 返回描述 ===> " + response.getMsg());
                throw new RuntimeException("申请账单失败");
            }

        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("申请账单失败");
        }
    }
}
