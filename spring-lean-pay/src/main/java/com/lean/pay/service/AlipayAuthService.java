package com.lean.pay.service;

public interface AlipayAuthService {

    /**
     * 刷脸支付初始化
     */
    String initSmilepay();

    /**
     * 人脸ftoken查询消费接口
     */
    String customerFtokenQuery();
}
