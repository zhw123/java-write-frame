package com.lean.pay.service.impl;
import com.alipay.api.domain.FaceExtInfo;
import com.alipay.api.domain.*;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.request.ZolozAuthenticationCustomerFtokenQueryRequest;
import com.alipay.api.request.ZolozAuthenticationSmilepayInitializeRequest;
import com.alipay.api.response.ZolozAuthenticationCustomerFtokenQueryResponse;
import com.alipay.api.response.ZolozAuthenticationSmilepayInitializeResponse;
import com.lean.pay.service.AlipayAuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AlipayAuthServiceImpl implements AlipayAuthService {

    @Autowired
    private AlipayClient alipayClient;

    @Override
    public String initSmilepay() {
        try {
            ZolozAuthenticationSmilepayInitializeRequest request = new ZolozAuthenticationSmilepayInitializeRequest();

            ZolozAuthenticationSmilepayInitializeModel model=new ZolozAuthenticationSmilepayInitializeModel();
            //设备指纹，用于唯一标识一台设备
            model.setApdidToken("40Kl9e97cQcMJka5gfT0arVPq7GTaR6yE5f73mj8zqBW9eOPZQEAAA==");
            //人脸识别应用名称
            model.setAppName("com.alipay.zoloz.smile");
            //人脸识别应用版本号
            model.setAppVersion("3.7.0.201809191618");
            //基础包版本号
            model.setBaseVer("7.1.1");
            //生物识别元信息
            model.setBioMetaInfo("3.13.0:12419136,6");
            //设备型号
            model.setDeviceModel("T2");
            //设备类型
            model.setDeviceType("android");
            //扩展信息
            FaceExtParams faceExtParams = new FaceExtParams();
            //务类型：7，基于1:N人脸搜索的刷脸支付场景；8，基于姓名和身份证号的刷脸支付场景。
            faceExtParams.setBizType("1");
            model.setExtInfo(faceExtParams);
            //机具信息，调用人脸识别SDK获取
            FaceMachineInfo faceMachineInfo = new FaceMachineInfo();
            //摄像头驱动版本号
            faceMachineInfo.setCameraDriveVer("1.0");
            //摄像头型号
            faceMachineInfo.setCameraModel("AstraP1");
            //摄像头名称
            faceMachineInfo.setCameraName("AstraP1");
            //摄像头版本号
            faceMachineInfo.setCameraVer("1.0");
            //机具编码
            faceMachineInfo.setExt("WyDAz27pfsEDANGk7QnmxZlv");
            //机具型号
            faceMachineInfo.setMachineCode("T2");
            //机具版本号
            faceMachineInfo.setMachineModel("7.1.1");
            //扩展信息
            faceMachineInfo.setMachineVer("扩展信息");

            model.setMachineInfo(faceMachineInfo);

            FaceMerchantInfo faceMerchantInfo = new FaceMerchantInfo();
            //区域编码
            faceMerchantInfo.setAreaCode("华中");
            //品牌编码
            faceMerchantInfo.setBrandCode("KFC");
            //机具Mac地址
            faceMerchantInfo.setDeviceMac("1c:aa:07:b0:e7:af");
            //机具分组编码
            faceMerchantInfo.setDeviceNum("TEST_ZOLOZ_TEST");
            //机具编码
            faceMerchantInfo.setGeo("120.10785,30.26708");
            //经纬度
            faceMerchantInfo.setGroup("group1");
            //商户ID
            faceMerchantInfo.setMerchantId("2088302068579978");
            //ISV ID
            faceMerchantInfo.setPartnerId("2088302068579978");
            //门店编码
            faceMerchantInfo.setStoreCode("TEST");
            //WI-FI Mac地址
            faceMerchantInfo.setWifimac("1c:aa:07:b0:e7:af");
            //WI-FI 名称
            faceMerchantInfo.setWifiname("Alipay-Test");

            model.setMerchantInfo(faceMerchantInfo);
            //操作系统版本号
            model.setOsVersion("7.1.1");
            //业务ID
            model.setRemoteLogId("bcf71c4290f288f4421ee9d49eedbf1e2840ba");
            //ZIM版本号
            model.setZimVer("1.0.0");
            request.setBizModel(model);
            ZolozAuthenticationSmilepayInitializeResponse response = alipayClient.execute(request);
            if(response.isSuccess()){
                System.out.println("调用成功");
                return response.getBody();
            } else {
                System.out.println("调用失败");
                return null;
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("刷脸支付初始化调用失败");
        }
    }

    @Override
    public String customerFtokenQuery() {

        try {
            ZolozAuthenticationCustomerFtokenQueryRequest request = new ZolozAuthenticationCustomerFtokenQueryRequest();
            ZolozAuthenticationCustomerFtokenQueryModel model=new ZolozAuthenticationCustomerFtokenQueryModel();
            //1、1：1人脸验证能力
            //2、1：n人脸搜索能力（支付宝uid入库）
            //3、1：n人脸搜索能力（支付宝手机号入库）
            //4、手机号和人脸识别综合能力
            model.setBizType("1");
            //人脸token
            model.setFtoken("fp0593e8d5c136277f13fd5bc36c13a7db7");
            //刷脸初始化流程中产生的zimId值
            model.setZimId("3b35b4677de2c69bb5bab69a4a5168d62");
            //人脸产品拓展参数
            FaceExtInfo faceExtInfo = new FaceExtInfo();
            //年龄区间判断的上限，闭区间
            faceExtInfo.setMaxAge("30");
            //年龄区间判断的下限，闭区间
            faceExtInfo.setMinAge("10");
            faceExtInfo.setQueryType("1;2;3等");
            model.setExtInfo(faceExtInfo);
            request.setBizModel(model);
            ZolozAuthenticationCustomerFtokenQueryResponse response = alipayClient.execute(request);
            if(response.isSuccess()){
                System.out.println("调用成功");
                return response.getBody();
            } else {
                System.out.println("调用失败");
                return null;
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("人脸ftoken查询消费接口调用失败");
        }
    }
}
