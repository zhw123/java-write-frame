package com.lean.pay.service;

public interface AlipayPhonePayService {
    /**
     * 外部商户创建订单并支付
     * @param productId
     * @return
     */
    String createOrderPay(Long productId);

    /**
     *统一收单交易查询
     * @param orderNo
     * @return
     */
    String queryOrderPay(String orderNo);

    /**
     * 统一收单交易退款接口
     * @param orderNo
     * @param reason
     */
    void refundOrder(String orderNo, String reason);

    /**
     * 统一收单交易退款查询
     * @param orderNo
     */
    String refundQueryOrder(String orderNo);

    /**
     * 统一收单交易关闭接口
     * @param orderNo
     */
    void cancelOrder(String orderNo);

    /**
     * 查询对账单下载地址
     * @param billDate
     * @param type
     * @return
     */
    String queryBill(String billDate, String type);
}
