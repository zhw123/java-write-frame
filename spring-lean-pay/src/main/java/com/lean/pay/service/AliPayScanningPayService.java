package com.lean.pay.service;


import java.util.Map;

public interface AliPayScanningPayService {

    /**
     * 统一收单线下交易预创建二维码
     * @param productId
     * @return
     */
    String tradeCreateCode(Long productId);

    /**
     * 统一收单交易创建接口
     * @param productId
     * @return
     */
    Map<String, Object> createTradePagePay(Long productId);

    /**
     * 统一收单交易创建接口
     * @param orderNo 原支付请求的商户订单号,和支付宝交易号不能同时为空
     * @return
     */
    String queryOrder(String orderNo);

    /**
     * 统一收单交易退款接口
     * @param orderNo
     * @param reason
     */
    void refund(String orderNo, String reason);

    /**
     * 统一收单交易退款查询
     * @param orderNo 退款请求号。 请求退款接口时，传入的退款请求号，如果在退款请求时未传入，则该值为创建交易时的商户订单号。
     * @return
     */
    String queryRefund(String orderNo);


    /**
     * 统一收单交易撤销接口
     * @param orderNo 原支付请求的商户订单号,和支付宝交易号不能同时为空
     */
    void cancelOrder(String orderNo);

    /**
     * 统一收单交易关闭接口
     * @param orderNo 原支付请求的商户订单号,和支付宝交易号不能同时为空
     */
    void closeOrder(String orderNo);

    /**
     *查询对账单下载地址
     * @param billDate 账单时间： * 日账单格式为yyyy-MM-dd，最早可下载2016年1月1日开始的日账单。不支持下载当日账单，只能下载前一日24点前的账单数据（T+1），
     *                 当日数据一般于次日 9 点前生成，特殊情况可能延迟。 * 月账单格式为yyyy-MM，最早可下载2016年1月开始的月账单。不支持下载当月账单，
     *                 只能下载上一月账单数据，当月账单一般在次月 3 日生成，特殊情况可能延迟。
     * @param type  账单类型，商户通过接口或商户经开放平台授权后其所属服务商通过接口可以获取以下账单类型，支持： trade：
     *              商户基于支付宝交易收单的业务账单； signcustomer：基于商户支付宝余额收入及支出等资金变动的账务账单。
     * @return
     */
    String queryBill(String billDate, String type);


}
