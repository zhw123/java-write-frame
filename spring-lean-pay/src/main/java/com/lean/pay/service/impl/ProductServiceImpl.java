package com.lean.pay.service.impl;

import com.lean.pay.entity.Product;
import com.lean.pay.mapper.ProductMapper;
import com.lean.pay.service.ProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {

}
