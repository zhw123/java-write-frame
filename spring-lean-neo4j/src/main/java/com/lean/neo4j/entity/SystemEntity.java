package com.lean.neo4j.entity;


import lombok.Data;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
@Data
@Node
public class SystemEntity {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
}
