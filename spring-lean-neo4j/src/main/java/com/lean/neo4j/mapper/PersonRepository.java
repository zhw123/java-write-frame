package com.lean.neo4j.mapper;

import com.lean.neo4j.entity.Person;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends Neo4jRepository<Person, Long> {
    Person findPersonEntityByName(String name);
}

