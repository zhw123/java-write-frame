package com.lean.neo4j;

import java.util.*;

import javax.annotation.Resource;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.alibaba.fastjson.JSON;
import com.lean.neo4j.entity.*;
import com.lean.neo4j.mapper.*;
import org.springframework.data.neo4j.core.Neo4jTemplate;
import org.springframework.data.neo4j.core.PreparedQuery;
import org.springframework.data.neo4j.repository.query.QueryFragmentsAndParameters;

@SpringBootTest
class SpringLeanNeo4jApplicationTests {



    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private SystemRepository systemRepository;

    @Test
    public void addSystemNode() {

        systemRepository.deleteAll();

        SystemEntity systemEntity = new SystemEntity();
        systemEntity.setName("系统A"); // 51
        systemRepository.save(systemEntity);
        System.out.println("系统A" + "----------" + systemEntity.getId());

        SystemEntity systemEntity1 = new SystemEntity();
        systemEntity1.setName("系统B");//  40
        systemRepository.save(systemEntity1);
        System.out.println("系统B" + "----------" + systemEntity1.getId());

        SystemEntity systemEntity2 = new SystemEntity();
        systemEntity2.setName("系统C");//  41
        systemRepository.save(systemEntity2);
        System.out.println("系统C" + "----------" + systemEntity2.getId());

        SystemEntity systemEntity3 = new SystemEntity();
        systemEntity3.setName("系统D");//  42
        systemRepository.save(systemEntity3);
        System.out.println("系统D" + "----------" + systemEntity3.getId());

        SystemEntity systemEntity4 = new SystemEntity();
        systemEntity4.setName("系统E");// 43
        systemRepository.save(systemEntity4);
        System.out.println("系统E" + "----------" + systemEntity4.getId());

        SystemEntity systemEntity5 = new SystemEntity();
        systemEntity5.setName("系统F");//  44
        systemRepository.save(systemEntity5);
        System.out.println("系统F" + "----------" + systemEntity5.getId());
    }

    @Test
    public void addInvokeRelation() {
        systemRepository.addInvokeRelation(51L, 40L);
        systemRepository.addInvokeRelation(51L, 41L);
        systemRepository.addInvokeRelation(42L, 51L);
        systemRepository.addInvokeRelation(42L, 41L);
        systemRepository.addInvokeRelation(42L, 41L);
    }

    @Test
    public void addConsumeRelation() {
        systemRepository.addConsumeRelation(43L, 44L);
        systemRepository.addConsumeRelation(42L, 43L);
    }

    /**
     * 删除指定节点直接的关系 DELETE <node1-name>,<node2-name>,<relationship-name>
     */
    @Test
    public void deleteConsumeRelation2() {
        Long from = 42L, to = 51L;
        systemRepository.deleteConsumeRelation(from, to);
    }

    @Test
    public void addProduceRelation() {
        Long from = 51L, to = 44L;
        systemRepository.addProduceRelation(from, to);
    }

    @Test
    public void findSystemById() {
        Long id = 51L;
        SystemEntity systemEntity = systemRepository.findSystemById(id);
        System.out.println(JSON.toJSONString(systemEntity));
    }

    @Test
    public void getAllSystemNode() {
        Iterable<SystemEntity> systemEntities = systemRepository.findAll();
        for (SystemEntity systemEntity : systemEntities) {
            System.out.println("查询所有的节点为：" + JSON.toJSONString(systemEntity));
            System.out.println(JSON.toJSONString(systemEntity));
        }
    }

    @Autowired
    private Neo4jTemplate neo4jTemplate;

    @Test
    void TestNoRepository() {
        // 删除所有节点和关系（删除节点会响应删除关联关系），避免后续创建节点重复影响
        neo4jTemplate.deleteAll(Movie.class);

        neo4jTemplate.deleteAll(Person.class);

        // 创建节点
        Movie movie = new Movie("流浪地球", "是由中国电影股份有限公司、北京京西文化旅游股份有限公司、郭帆文化传媒（北京）有限公司、北京登峰国际文化传播有限公司联合出品，由郭帆执导，吴京特别出演、屈楚萧、赵今麦、李光洁、吴孟达等领衔主演的科幻冒险电影");
        // 添加关系
        movie.getActorsAndRoles().add(new Roles(new Person(1994, "刘启"), Collections.singletonList("初级驾驶员")));
        movie.getActorsAndRoles().add(new Roles(new Person(5102, "刘培强"), Collections.singletonList("中国航天员")));
        movie.getActorsAndRoles().add(new Roles(new Person(1952, "韩子昂"), Collections.singletonList("高级驾驶员")));
        movie.getActorsAndRoles().add(new Roles(new Person(5102, "韩朵朵"), Collections.singletonList("初中生")));
        movie.getActorsAndRoles().add(new Roles(new Person(1981, "王磊"), Collections.singletonList("救援队队长")));
        movie.getActorsAndRoles().add(new Roles(new Person(1991, "李一一"), Collections.singletonList("技术观察员")));
        movie.getActorsAndRoles().add(new Roles(new Person(1974, "何连科"), Collections.singletonList("救援队队员")));
        movie.getActorsAndRoles().add(new Roles(new Person(1991, "Tim"), Collections.singletonList("中美混血儿")));
        movie.getDirectors().add(new Person(1974, "吴京"));
        // 存入图数据库持久化
        neo4jTemplate.save(movie);

        // 查询 操作 两种方式

        // 1.手写cypherQuery  toExecutableQuery

        // 2.调用neo4jTemplate提供的方法.

        List<Person> personList = neo4jTemplate.findAll(Person.class);
        System.out.println(personList);
        Map<String, Object> map = new HashMap<>();
        map.put("usedName", "王磊");
        QueryFragmentsAndParameters parameters = new QueryFragmentsAndParameters("MATCH (n:Person) where n.name = $usedName return n",map);
        Person person = neo4jTemplate.toExecutableQuery(Person.class, parameters).getSingleResult().get();
        System.out.println(person);

        // 3. 通过属性关系查询节点
        map = new HashMap<>();
        map.put("roles",Collections.singletonList("救援队队员"));
        // 方法1.使用toExecutableQuery查询
        parameters = new QueryFragmentsAndParameters("MATCH (n:Person) -[relation:ACTED_IN]-> (m:Movie) WHERE relation.roles = $roles RETURN n",map);
        Optional<Person> role = neo4jTemplate.toExecutableQuery(Person.class, parameters).getSingleResult();
        System.out.println(role);

        // 方法2.使用findOne查询
        role = neo4jTemplate.findOne("MATCH (person:Person) -[relation:ACTED_IN]-> (movie:Movie) WHERE relation.roles = $roles RETURN person",map,Person.class);
        System.out.println(role);

        Long userId = person.getId();
        // 更新
        person.setName("王磊2");
        neo4jTemplate.save(person);

        Optional<Person> person2 = neo4jTemplate.findById(userId, Person.class);
        System.out.println(person2);

    }

    /**
     * 使用repository操作图数据
     */
    @Test
    void testByRepository() {
        // 删除所有节点和关系（删除节点会响应删除关联关系），避免后续创建节点重复影响
        movieRepository.deleteAll();
        personRepository.deleteAll();
        // 创建节点
        Movie movie = new Movie("流浪地球", "是由中国电影股份有限公司、北京京西文化旅游股份有限公司、郭帆文化传媒（北京）有限公司、北京登峰国际文化传播有限公司联合出品，由郭帆执导，吴京特别出演、屈楚萧、赵今麦、李光洁、吴孟达等领衔主演的科幻冒险电影");
        // 添加关系
        movie.getActorsAndRoles().add(new Roles(new Person(1994, "刘启"), Collections.singletonList("初级驾驶员")));
        movie.getActorsAndRoles().add(new Roles(new Person(5102, "刘培强"), Collections.singletonList("中国航天员")));
        movie.getActorsAndRoles().add(new Roles(new Person(1952, "韩子昂"), Collections.singletonList("高级驾驶员")));
        movie.getActorsAndRoles().add(new Roles(new Person(5102, "韩朵朵"), Collections.singletonList("初中生")));
        movie.getActorsAndRoles().add(new Roles(new Person(1981, "王磊"), Collections.singletonList("救援队队长")));
        movie.getActorsAndRoles().add(new Roles(new Person(1991, "李一一"), Collections.singletonList("技术观察员")));
        movie.getActorsAndRoles().add(new Roles(new Person(1974, "何连科"), Collections.singletonList("救援队队员")));
        movie.getActorsAndRoles().add(new Roles(new Person(1991, "Tim"), Collections.singletonList("中美混血儿")));
        movie.getDirectors().add(new Person(1974, "吴京"));
        // 存入图数据库持久化
        neo4jTemplate.save(movie);

        // 查询
        Person person = personRepository.findPersonEntityByName("刘启");
        System.out.println(JSON.toJSONString(person));
        Movie movie1 = movieRepository.findMovieByTitle("流浪地球");
        System.out.println(JSON.toJSONString(movie1));

        Movie movie2 = movieRepository.findMovieById(movie.getId());
        System.out.println(JSON.toJSONString(movie2));

        // 注意：repository的save方法【对应的实体若id一致】则为修改，否则为新建。
        person.setBorn(1997);
        personRepository.save(person);
        person = personRepository.findPersonEntityByName("刘启");
        System.out.println(person);
    }

}
