package com.lean.process.common;

import java.io.Serializable;

/**
 * 执行结果返回信息(在批量执行时，使用较多)
 * @author dujy
 *
 */
public class OperateRetMsg implements Serializable{

	private static final long serialVersionUID = 5696845820722917786L;

	private String id;			// 某条数据的ID
	private String msg;			// 执行的结果信息(一般有错误时，用来存在错误详情)
	private int code;           //用于判断是否是平台报错
	private Object data;			// 需要往上级返回对象时，用此属性

	public OperateRetMsg() {
	}

	public OperateRetMsg(String id, String msg) {
		this.id = id;
		this.msg = msg;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
}
