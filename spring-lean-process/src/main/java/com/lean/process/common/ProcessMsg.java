package com.lean.process.common;

import java.io.Serializable;
import java.util.Date;

public class ProcessMsg implements Serializable{

	private static final long serialVersionUID = -4732484197154257579L;

	private int code 		= -1;			// 代码     0:执行中  1:已执行结束	-1:该数据不存在
	private String msg 		= "";			// 执行信息
	private Object data;                    // 存放数据
	private int num 		= 0;			// 计数 (需要的时侯，请填这项)
	private int successNum	= 0;			// 记录成功数
	private int totalNum 	= 0;			// 总计数 (需要的时侯，请填这项)
	private Date createDate = new Date();	// 创建时间 (用于checkProcessController中判断是否超时)

	public ProcessMsg(){
	}

	public ProcessMsg(String msg){
		this.msg = msg;
	}

	public ProcessMsg(int code, int num, String msg){
		this.code = code;
		this.num = num;
		this.msg = msg;
	}

	public ProcessMsg(int code, int num, String msg, Object data){
		this(code, num, msg);
		this.data = data;
	}

	public ProcessMsg(int code, int num, int totalNum, String msg){
		this(code, num, msg);
		this.totalNum = totalNum;
	}

	public ProcessMsg(int code, int num, int totalNum, String msg, Object data){
		this(code, num, msg);
		this.totalNum = totalNum;
		this.data = data;
	}

	public ProcessMsg(int code, int num, int totalNum, Object data){
		this(code, num, null);
		this.totalNum = totalNum;
		this.data = data;
	}

	public ProcessMsg(int code, int num, int totalNum, int successNum, String msg, Object data){
		this(code, num, msg);
		this.totalNum = totalNum;
		this.successNum = successNum;
		this.data = data;
	}

	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData(){ return data; }
	public void setData(Object data){ this.data = data; }
	public int getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}
	public int getSuccessNum() {
		return successNum;
	}
	public void setSuccessNum(int successNum) {
		this.successNum = successNum;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
