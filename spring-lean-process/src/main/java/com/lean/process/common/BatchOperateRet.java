package com.lean.process.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 批量操作返回结果
 *
 */
public class BatchOperateRet implements Serializable{

	private static final long serialVersionUID = -2078311072677715835L;

	private List<OperateRetMsg> successList = new ArrayList<>();			// 执行成功列表
	private List<OperateRetMsg> failList 	= new ArrayList<>();			// 执行失败列表
	private List<OperateRetMsg> duplicateList = new ArrayList<>();          // 拦截回草稿箱的数据
	private Object data;													// 需要带过去的数据

	public List<OperateRetMsg> getSuccessList() {
		return successList;
	}
	public void setSuccessList(List<OperateRetMsg> successList) {
		this.successList = successList;
	}
	public List<OperateRetMsg> getFailList() {
		return failList;
	}
	public void setFailList(List<OperateRetMsg> failList) {
		this.failList = failList;
	}

	public List<OperateRetMsg> getDuplicateList() {
		return duplicateList;
	}

	public void setDuplicateList(List<OperateRetMsg> duplicateList) {
		this.duplicateList = duplicateList;
	}

	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
}
