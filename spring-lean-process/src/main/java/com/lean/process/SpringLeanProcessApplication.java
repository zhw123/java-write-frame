package com.lean.process;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanProcessApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanProcessApplication.class, args);
    }

}
