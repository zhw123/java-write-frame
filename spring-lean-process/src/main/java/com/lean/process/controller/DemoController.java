package com.lean.process.controller;

import cn.hutool.core.util.RandomUtil;
import com.lean.process.common.DemoService;
import com.lean.process.common.ProcessMsg;
import com.lean.process.common.RedisUtil;
import com.lean.process.common.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/")
public class DemoController {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private DemoService demoService;

    /**
     * 1.批量处理 进度条
     * @return
     */
    @RequestMapping("/test")
    public Result test(){

        Long userId=1L;
        String redisKey = "xxxxxxxx:xxx:"+ RandomUtil.getRandom().nextInt(1000, 9999)+":" + userId;
        //处理逻辑业务
        demoService.test(redisKey);
        // 设置为正在执行
        ProcessMsg pm = new ProcessMsg(0, 0, "");
        //存储到redis中
        redisUtil.setProcessMsg(redisKey, pm);
        return Result.getInstance().success(pm);
    }


    /**
     * 2.检查是否处理完成
     * @param uuid
     * @param request
     * @return
     */
    @GetMapping(value = "/check-process")
    public Result checkProcess(String uuid, HttpServletRequest request) {
        Map<String,Object> map = new HashMap<>();
        getProcess(map, uuid, 10*60*1000);
        return Result.getInstance().success(map);
    }


    /**
     * 获取进度条信息
     * @param map			：对象
     * @param uuid			：uuid
     * @param timeOut unit ms
     */
    private void getProcess(final Map<String,Object> map,String uuid,long timeOut){
        ProcessMsg pm;
        if(uuid == null){
            pm = new ProcessMsg();
        } else {
            try{
                pm = redisUtil.getProcessMsg(uuid);
            } catch (Exception e){
                e.printStackTrace();
                pm = new ProcessMsg();
                pm.setCode(0);
            }
            // 如果没有该缓存数据，则创建一条 。(ProcessMsg的code默认为-1, 表示不存在)
            if(pm == null){
                pm = new ProcessMsg();
            } else {
                // 判断是否超时
                Date d = pm.getCreateDate();
                long cha = System.currentTimeMillis() - d.getTime();
                if(cha > timeOut){
                    pm.setCode(-1);
                    pm.setMsg("请求超时，请稍后重试!");
                }
                // 执行完毕或是有错误清除缓存
                if (pm.getCode() == 1) {
                    redisUtil.del(uuid);
                }
            }
        }
        map.put("processMsg", pm);
    }

}
