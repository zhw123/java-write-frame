package com.lean.itextpdf.test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

/**
 * @Author zhw
 * 裁剪 两种方式：1：裁剪后转成图片 2：裁剪后生成新的文件，在将新的文件写入到两外一个新的文件。
 **/
public class PdfTest {

    //智利
    public void mercadoZL() throws IOException, DocumentException {
        String oldFile = "./doc/mercado-zl.pdf";
        String newFile = "./doc/mercado-zl-3.pdf";
        Rectangle rectangle = new Rectangle(0, 45, 265, 475);
        Document document = new Document(rectangle);
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(newFile));
        document.open();

        PdfContentByte cb = pdfWriter.getDirectContent();// 得到层

        //第一步 读取旧文件
        PdfReader pdfReader = new PdfReader(oldFile);
        //旧文件
        pdfReader.selectPages(Collections.singletonList(1));
        document.newPage();
        PdfImportedPage page = pdfWriter.getImportedPage(pdfReader, 1);
        cb.addTemplate(page, 1.0f, 0, 0, 1.0f, -25, 20); //有截取时
        document.close();
        //重新 对新文件 进行
        String newFile2 = "./doc/mercado-zl-31.pdf";
        Rectangle rectangle2 = new Rectangle(0, 400, 300, 842);//new Rectangle(PageSize.A4);  //
        Document document2 = new Document(rectangle2);
        PdfWriter pdfWriter2 = PdfWriter.getInstance(document2, new FileOutputStream(newFile2));
        document2.open();
        PdfContentByte cb2 = pdfWriter2.getDirectContent();// 得到层
        //读取之前完成的新文件
        PdfReader pdfReader2 = new PdfReader(newFile);
        document2.newPage();
        PdfImportedPage page2 = pdfWriter2.getImportedPage(pdfReader2, 1);
        cb2.addTemplate(page2, 1.0f, 0, 0, 0.85f, 15, 470);
        document2.close();
    }
    //智利

    public void mercadoZL_A4() throws IOException, DocumentException {
        //Mercado Envíos Flex：https://print-label.upseller.com/pdf-cache/2022-06-14/4017/37c80213bd0d7518f511f6dfed46132a.pdf
        //Mercado Envíos Agências：https://print-label.upseller.com/pdf-cache/2022-07-06/4013/1a1e43cc434e0ed0d480ceb1fe7e63d8.pdf
        //Mercado Envíos：https://print-label.upseller.com/pdf-cache/2022-07-06/4013/15cb98baad8bbb724b03511f5c081c3e.pdf
        String oldFile = "./doc/mercado-zl.pdf";
        String newFile = "./doc/mercado-zl-1.pdf";
        Rectangle rectangle = new Rectangle(0, 45, 265, 475);
        Document document = new Document(rectangle);
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(newFile));
        document.open();

        PdfContentByte cb = pdfWriter.getDirectContent();// 得到层

        //第一步 读取旧文件
        PdfReader pdfReader = new PdfReader(oldFile);
        //旧文件
        pdfReader.selectPages(Collections.singletonList(1));
        document.newPage();
        PdfImportedPage page = pdfWriter.getImportedPage(pdfReader, 1);
        //cb.addTemplate(page, 0, 420);// 使用writer需要使用pdf的层,然后添加
        //cb.addTemplate(page, 1.0f, 0, 0, 1.0f, -25, 385);
        cb.addTemplate(page, 1.0f, 0, 0, 1.0f, -25, 20);
        document.close();
        //重新 对新文件 进行
        String newFile2 = "./doc/mercado-zl-A4.pdf";
        Rectangle rectangle2 = new Rectangle(PageSize.A4.getHeight(), PageSize.A4.getWidth());  //new Rectangle(0, 400, 300, 842);//new Rectangle(PageSize.A4);  //
        Document document2 = new Document(rectangle2);
        PdfWriter pdfWriter2 = PdfWriter.getInstance(document2, new FileOutputStream(newFile2));
        document2.open();
        PdfContentByte cb2 = pdfWriter2.getDirectContent();// 得到层
        //读取之前完成的新文件
        PdfReader pdfReader2 = new PdfReader(newFile);
        document2.newPage();
        PdfImportedPage page2 = pdfWriter2.getImportedPage(pdfReader2, 1);
        cb2.addTemplate(page2, 1.0f, 0, 0, 0.9f, 10, 190);
        List<String> pickContent = new ArrayList<>();
        pickContent.add("SKU INFO");
        pickContent.add("001pick red XXL*10");
        pickContent.add("002pick red XXL*10");
        pickContent.add("003pick red XXL*10");
        pickContent.add("004pick red XXL*10");
        pickContent.add("005pick red XXL*10");
        BaseFont font = BaseFont.createFont();
        //追加产品信息
        if(!CollectionUtils.isEmpty(pickContent)){
            String value = pickContent.get(0);
            //pickContent.remove(0);
            cb2.beginText();
            cb2.setFontAndSize(font, 8);
            cb2.setColorFill(BaseColor.BLACK);
            cb2.showTextAligned(Element.ALIGN_LEFT, value, 20, 180, 0);
            if(!CollectionUtils.isEmpty(pickContent)){
                for (int a = 1; a <= pickContent.size()-1; a++) {
                    cb2.showTextAligned(Element.ALIGN_LEFT,pickContent.get(a), 20, 180 - (a * 10), 0);
                }
            }
            cb2.endText();
        }

        //第二个
        PdfReader pdfReader3 = new PdfReader(newFile);
        PdfImportedPage page3 = pdfWriter2.getImportedPage(pdfReader3, 1);
        cb2.addTemplate(page3, 1.0f, 0, 0, 0.9f, 280, 190);
        if(!CollectionUtils.isEmpty(pickContent)){
            String value = pickContent.get(0);
            cb2.beginText();
            cb2.setFontAndSize(font, 8);
            cb2.setColorFill(BaseColor.BLACK);
            cb2.showTextAligned(Element.ALIGN_LEFT, value, 300, 180, 0);
            if(!CollectionUtils.isEmpty(pickContent)){
                for (int a = 1; a <= pickContent.size()-1; a++) {
                    cb2.showTextAligned(Element.ALIGN_LEFT,pickContent.get(a), 300, 180 - (a * 10), 0);
                }
            }
            cb2.endText();
        }
        //第三个
        PdfReader pdfReader4 = new PdfReader(newFile);
        PdfImportedPage page4 = pdfWriter2.getImportedPage(pdfReader4, 1);
        cb2.addTemplate(page4, 1.0f, 0, 0, 0.9f, 550, 190);
        if(!CollectionUtils.isEmpty(pickContent)){
            String value = pickContent.get(0);
            cb2.beginText();
            cb2.setFontAndSize(font, 8);
            cb2.setColorFill(BaseColor.BLACK);
            cb2.showTextAligned(Element.ALIGN_LEFT, value, 570, 180, 0);
            if(!CollectionUtils.isEmpty(pickContent)){
                for (int a = 1; a <= pickContent.size()-1; a++) {
                    cb2.showTextAligned(Element.ALIGN_LEFT,pickContent.get(a), 570, 180 - (a * 10), 0);
                }
            }
            cb2.endText();
        }
        document2.close();


    }

}
