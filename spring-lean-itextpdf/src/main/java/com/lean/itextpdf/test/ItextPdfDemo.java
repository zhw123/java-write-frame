package com.lean.itextpdf.test;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfName;

import com.itextpdf.text.pdf.*;

import java.awt.*;
import java.io.*;

import com.itextpdf.text.List;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import javax.swing.*;
import java.nio.charset.Charset;
import java.util.Map;

public class ItextPdfDemo {

    public static void main(String[] args) throws Exception {
        // createPdf();
        // pdfConstructor();
        // getDocument();
        // getPdfWriter();
        // getRectangle();
        // testChunk();
        // testPhrase();
        // testParagraph();
        // testList();
        // testImage();
        // testAnchor();
        // testChapterAndSection();
        // testPdfOutline();
        // testHeadFooter();
        // insertTableAndPdfPCell();
        // testShuiyin();
        // testShuiyin1();
        // testShuiyin2();
         insertHeadAndFoot();
        // insertHeadAndFoot2();
        // mergePdfFiles(new String[] {"./doc/12.pdf", "./doc/12.pdf"}, "./doc/a1.pdf");

        // mergePdf(new String[]{"./doc/12.pdf","./doc/12.pdf"},"./doc/a1.pdf");
        // deletePage();

        // pdf内容
        // String s = testPdfContent();
        // System.out.println(s);

        // 遮盖内容
        // deletePdfContent("./doc/99.pdf", "./doc/23w.pdf");
        //
        // java.util.List<String> list = new ArrayList<>();
        // list.add("./doc/99.pdf");
        // list.add("./doc/99.pdf");
        // list.add("./doc/99.pdf");
        // list.add("./doc/99.pdf");
        // //一打四
        // mergePdf(list,"./doc/c2.pdf");
//        insertHeadAndFoot3();
    }

    /**
     * 創建pdf
     */
    public static void createPdf() throws Exception {
        // 1-创建文本对象 Document
        Document document = new Document(PageSize.A4, 500, 150, 50, 50);
        FileOutputStream out = new FileOutputStream("./doc/1.pdf");
        // 2-初始化 pdf输出对象 PdfWriter
        PdfWriter.getInstance(document, out);
        // 3-打开 Document
        document.open();
        // 4-往 Document 添加内容
        document.add(new Paragraph("test！ PDF！！！"));
        // 5-关闭 Document
        document.close();
    }

    /**
     * pdf 内部构造
     */
    public static void pdfConstructor() throws Exception {
        PdfReader reader = new PdfReader("./doc/1.pdf");
        PdfStamper stamper = new PdfStamper(reader, null);
        Map<String, PdfLayer> pdfLayers = stamper.getPdfLayers();
        for (String key : pdfLayers.keySet()) {
            // 这里的key虽然也是图层的名称，
            // 但是是所有图层包括看不到的图层的名称，而且有多个同名的图层的话获取到的key是 图层(数字)
            PdfLayer pdfLayer = pdfLayers.get(key);
            // 判断PDF图层是否显示
            if (pdfLayer.isOnPanel()) {
                System.out.println(pdfLayer.get(PdfName.NAME).toString());
            }
        }
    }

    /**
     * @throws Exception
     */
    public static void getPdfWriter() throws Exception {
        // 横向打印
        Document document = new Document();
        // 解析器
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("./doc/2.pdf"));
        // 面框大小。允许的名称有：“crop”、“trim”、“art”和“bleach”。
        writer.setBoxSize("crop", PageSize.A6);
        writer.setPageEmpty(true);
        // 设置裁剪框大小
        writer.setCropBoxSize(PageSize.A8);
        // 设置密码为："123" 需要根绝itext版本添加加密算法依赖：http://mvnrepository.com/artifact/com.itextpdf/itextpdf/
        writer.setEncryption("密码".getBytes(), "123".getBytes(), PdfWriter.ALLOW_SCREENREADERS, PdfWriter.STANDARD_ENCRYPTION_128);
        // PDF版本
        writer.setPdfVersion(PdfWriter.PDF_VERSION_1_2);
        document.open();
        document.add(new Paragraph("PDF添加内容"));
        document.close();
    }

    public static void getDocument() throws Exception {
        // 横向打印
        Document document = new Document(PageSize.A4.rotate());
        // 解析器
        PdfWriter.getInstance(document, new FileOutputStream("./doc/3.pdf"));
        document.open();
        document.add(new Paragraph("PDF添加内容"));
        document.newPage();

        document.newPage();
        document.add(new Paragraph("New page"));
        // 监听器
        document.addDocListener(new DocListener() {
            @Override
            public void open() {
                System.out.println("打开文档");
            }

            @Override
            public void close() {
                System.out.println("关闭文档");
            }

            @Override
            public boolean newPage() {
                return false;
            }

            @Override
            public boolean setPageSize(Rectangle rectangle) {
                return false;
            }

            @Override
            public boolean setMargins(float v, float v1, float v2, float v3) {
                return false;
            }

            @Override
            public boolean setMarginMirroring(boolean b) {
                return false;
            }

            @Override
            public boolean setMarginMirroringTopBottom(boolean b) {
                return false;
            }

            @Override
            public void setPageCount(int i) {
                System.out.println("--" + i);
            }

            @Override
            public void resetPageCount() {

            }

            @Override
            public boolean add(Element element) throws DocumentException {
                return false;
            }
        });
        document.setJavaScript_onLoad("<script>alert('test')</script>");
        document.close();
    }

    public static void getRectangle() throws Exception {
        // 1-创建一个pdf文档,document
        Document document = new Document();// 默认PageSize.A4, 36, 36, 36, 36
        // 2-Rectangle（pdf页面）创建Document
        // 一般是四个参数表示：左下角的坐标和右上角的坐标
        Rectangle tRectangle = PageSize.A4;// PageSize封装了大量常用的Rectangle数据
        tRectangle = new Rectangle(800, 600);// 长宽
        tRectangle = new Rectangle(0, 0, 800, 600);// 等于上面

        // 其他页面属性：不能和PageSize封装的静态一起使用
        tRectangle.setBackgroundColor(BaseColor.BLACK);// 背景色
        tRectangle.setBorder(1220);// 边框
        tRectangle.setBorderColor(BaseColor.BLUE);
        tRectangle.setBorderWidth(244.2f);
        document = new Document(tRectangle);
        // 解析器
        PdfWriter.getInstance(document, new FileOutputStream("./doc/3.pdf"));
        document.open();
        document.newPage();
        document.add(new Paragraph("New page"));

        Chunk chunk = new Chunk("输出的内容", getChineseFont());
        chunk.setBackground(BaseColor.CYAN, 1f, 0.5f, 1f, 1.5f); // 设置背景色
        chunk.setTextRise(6); // 上浮
        chunk.setUnderline(0.2f, -2f); // 下划线
        document.add(chunk);

        document.add(Chunk.NEWLINE); // 新建一行
        Chunk chunk1 = new Chunk("新建一行", getChineseFont());
        chunk1.setBackground(BaseColor.RED); // 设置背景色
        document.add(chunk);

        document.close();

    }

    /**
     * 块
     *
     * @throws Exception
     */
    public static void testChunk() throws Exception {
        // 1-创建一个pdf文档,document
        Document document = new Document();// 默认PageSize.A4, 36, 36, 36, 36
        // 解析器
        PdfWriter.getInstance(document, new FileOutputStream("./doc/4.pdf"));
        document.open();
        document.newPage();
        String[] contries = new String[] {"美国", "英甲", "中国", "朝鲜", "日本"};
        for (int index = 1; index <= contries.length; index++) {
            String contry = contries[index - 1];
            Chunk chunk = new Chunk(contry, getChineseFont());
            // 字间隔
            // chunk.setWordSpacing(10f);
            // 行间距
            // chunk.setLineHeight(20f);
            document.add(chunk);
            // 块之间设置间隔
            document.add(new Chunk(" "));
            Font font = FontFactory.getFont("STSong-Light", "UniGB-UCS2-H", 6, Font.BOLD, BaseColor.WHITE);
            Chunk id = new Chunk(index + "", font);
            // 设置块的背景色
            id.setBackground(BaseColor.BLACK, 1f, 0.5f, 1f, 1.5f);
            // 设置上标，其中参数表示，离开基线的距离，如果设置负数就表示设置下标
            id.setTextRise(6);
            document.add(id);
            // 块之间设置间隔
            document.add(new Chunk("  "));
            // 换行 需要设置行间距 不然上移 覆盖
            // document.add(Chunk.NEWLINE);
        }
        document.close();
    }

    /**
     * 语句
     *
     * @throws Exception
     */
    public static void testPhrase() throws Exception {
        // 1-创建一个pdf文档,document
        Document document = new Document();// 默认PageSize.A4, 36, 36, 36, 36
        // 解析器
        PdfWriter.getInstance(document, new FileOutputStream("./doc/5.pdf"));
        document.open();
        document.newPage();
        String[] contries = new String[] {"美国", "英甲", "中国", "朝鲜", "日本"};
        Font BOLD_UNDERLINED = FontFactory.getFont("STSong-Light", "UniGB-UCS2-H", 12, Font.BOLD | Font.UNDERLINE);
        Font NORMAL = FontFactory.getFont("STSong-Light", "UniGB-UCS2-H", 12);
        for (int index = 1; index <= contries.length; index++) {
            String contry = contries[index - 1];
            Phrase director = new Phrase();
            director.add(new Chunk(contry, BOLD_UNDERLINED));
            director.add(new Chunk(",", BOLD_UNDERLINED));
            director.add(new Chunk(" ", NORMAL));
            director.add(new Chunk(contry, NORMAL));
            // 设置行间距
            director.setLeading(66f);
            document.add(director);
            // 内部换行
            document.add(Chunk.NEWLINE);
        }
        document.close();
    }

    /**
     * 段落
     *
     * @throws Exception
     */
    public static void testParagraph() throws Exception {
        // 1-创建一个pdf文档,document
        Document document = new Document();// 默认PageSize.A4, 36, 36, 36, 36
        // 解析器
        PdfWriter.getInstance(document, new FileOutputStream("./doc/6.pdf"));
        document.open();
        document.newPage();
        String[] contries = new String[] {"美国", "英甲", "中国", "朝鲜", "日本"};
        Font BOLD_UNDERLINED = FontFactory.getFont("STSong-Light", "UniGB-UCS2-H", 12, Font.BOLD | Font.UNDERLINE);
        Font NORMAL = FontFactory.getFont("STSong-Light", "UniGB-UCS2-H", 12);
        for (int index = 1; index <= contries.length; index++) {
            String contry = contries[index - 1];
            Paragraph p = new Paragraph();
            p.add(new Chunk("年代: ", BOLD_UNDERLINED));
            p.add(new Phrase("标题: ", NORMAL));
            p.add(new Phrase(contry, NORMAL));
            // 设置行间距
            p.setLeading(20f);
            document.add(p);
            // 内部换行
            document.add(Chunk.NEWLINE);
        }
        Paragraph paragraph =
            new Paragraph("这是一个缩进演示：段落是一系列块和（或）短句。同短句一样，段落有确定的间距。用户还可以指定缩排；" + "在边和（或）右边保留一定空白，段落可以左对齐、右对齐和居中对齐。添加到文档中的每一个段落将自动另起一行。说明：一个段落有一个且仅有一个间距，"
                + "如果你添加了一个不同字体的短句或块，原来的间距仍然有效，你可以通过SetLeading来改变间距，但是段落中所有内容将使用新的中的间距。更改分割符 通常，"
                + "当文本不能放在一行时，文本将被分割成不同的部分，iText首先会查找分割符，如果没有找到，文本将在行尾被截断。有一些预定的分割符如“ ”空格和“-”连字符，" + "但是你可以使用setSplitCharacter方法来覆盖这些默认值。", NORMAL);
        // 默认情况下，文本的对齐方式为左对齐
        // paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
        // 首行缩进（FirstLineIndent），左边缩进（indentationLeft），右边缩进（IndentationRight）
        paragraph.setFirstLineIndent(10f);
        paragraph.setIndentationLeft(10f);
        paragraph.setIndentationLeft(12f);
        document.add(paragraph);
        document.close();
    }

    /**
     * 列表
     *
     * @throws Exception
     */
    public static void testList() throws Exception {
        // 1-创建一个pdf文档,document
        Document document = new Document();// 默认PageSize.A4, 36, 36, 36, 36
        // 解析器
        PdfWriter.getInstance(document, new FileOutputStream("./doc/7.pdf"));
        document.open();
        document.newPage();
        String[] contries = new String[] {"美国", "英甲", "中国", "朝鲜", "日本"};
        Font NORMAL = FontFactory.getFont("STSong-Light", "UniGB-UCS2-H", 12);

        document.add(new Chunk("默认列表演示1：", NORMAL));
        document.add(Chunk.NEWLINE);

        List list = new com.itextpdf.text.List();
        for (int index = 1; index <= contries.length; index++) {
            String contry = contries[index - 1];
            list.add(new ListItem(contry, NORMAL));
        }
        document.add(list);
        document.add(Chunk.NEWLINE);

        document.add(new Chunk("不显示数字演示2：", NORMAL));
        document.add(Chunk.NEWLINE);

        // 编号
        list = new List(false);
        for (int index = 1; index <= contries.length; index++) {
            String contry = contries[index - 1];
            list.add(new ListItem(contry, NORMAL));
        }
        document.add(list);
        document.add(Chunk.NEWLINE);

        document.add(new Chunk("使用#作为列表符号3：", NORMAL));
        document.add(Chunk.NEWLINE);

        list = new List();
        // 设置编号
        list.setListSymbol("#");
        for (int index = 1; index <= contries.length; index++) {
            String contry = contries[index - 1];
            list.add(new ListItem(contry, NORMAL));
        }
        document.add(list);
        // 换行
        document.add(Chunk.NEWLINE);

        document.add(new Chunk("显示数字演示4：", NORMAL));
        document.add(Chunk.NEWLINE);
        list = new List(true);
        for (int index = 1; index <= contries.length; index++) {
            String contry = contries[index - 1];
            list.add(new ListItem(contry, NORMAL));
        }
        document.add(list);
        document.add(Chunk.NEWLINE);
        document.add(new Chunk("罗马数字列表演示5：", NORMAL));
        document.add(Chunk.NEWLINE);

        List list1 = new RomanList();
        for (int index = 1; index <= contries.length; index++) {
            String contry = contries[index - 1];
            list1.add(new ListItem(contry, NORMAL));
        }
        document.add(list1);

        document.add(Chunk.NEWLINE);
        document.add(new Chunk("希腊字母列表演示6：", NORMAL));
        document.add(Chunk.NEWLINE);

        List list2 = new GreekList();
        for (int index = 1; index <= contries.length; index++) {
            String contry = contries[index - 1];
            list2.add(new ListItem(contry, NORMAL));
        }
        document.add(list2);
        document.add(Chunk.NEWLINE);

        document.add(new Chunk("ZapfDingbatsNumberList演示7：", NORMAL));
        document.add(Chunk.NEWLINE);

        List list3 = new ZapfDingbatsNumberList(10);
        for (int index = 1; index <= contries.length; index++) {
            String contry = contries[index - 1];
            list3.add(new ListItem(contry, NORMAL));
        }
        document.add(list3);

        document.add(Chunk.NEWLINE);
        document.add(new Chunk("ZapfDingbatsList演示8：", NORMAL));
        document.add(Chunk.NEWLINE);

        List list4 = new ZapfDingbatsList(43, 30);
        for (int index = 1; index <= contries.length; index++) {
            String contry = contries[index - 1];
            list4.add(new ListItem(contry, NORMAL));
        }
        document.add(list4);

        document.add(Chunk.NEWLINE);
        document.add(new Chunk("列表嵌套演示9：", NORMAL));
        document.add(Chunk.NEWLINE);

        List rootList = new List(List.UNORDERED);
        rootList.add(new ListItem("Item 1"));
        // 子列表
        List sublist = new List(true, false, 30);
        sublist.setListSymbol(new Chunk("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        sublist.add("A");
        sublist.add("B");
        rootList.add(sublist);
        rootList.add(new ListItem("Item 2"));
        // 子列表
        sublist = new List(true, false, 30);
        sublist.setListSymbol(new Chunk("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        sublist.add("C");
        sublist.add("D");
        rootList.add(sublist);
        document.add(rootList);

        document.close();
    }

    /**
     * 图片
     *
     * @throws Exception
     */
    public static void testImage() throws Exception {
        // 1-创建一个pdf文档,document
        // 默认PageSize.A4, 36, 36, 36, 36
        Document document = new Document();
        // 解析器
        PdfWriter.getInstance(document, new FileOutputStream("./doc/9.pdf"));
        document.open();
        document.newPage();
        // 图片Image对象
        Image img = Image.getInstance("./doc/a.jpeg");
        img.setAlignment(Image.LEFT);
        img.setBorder(Image.BOX);
        img.setBorderWidth(10);
        img.setBorderColor(BaseColor.WHITE);
        img.scaleToFit(800, 72);// 大小
        img.setRotation(-20);// 旋转 弧度
        img.setRotationDegrees(-30);// 旋转 角度
        img.scalePercent(30);// 依照比例缩放
        document.add(img);
        document.close();
    }

    /**
     * 锚点、超链接
     *
     * @throws Exception
     */
    public static void testAnchor() throws Exception {
        // 1-创建一个pdf文档,document
        Document document = new Document();// 默认PageSize.A4, 36, 36, 36, 36
        // 解析器
        PdfWriter.getInstance(document, new FileOutputStream("./doc/10.pdf"));
        document.open();
        document.newPage();
        // Anchor超链接和锚点对象: internal and external links
        Paragraph paragraph = new Paragraph();
        Anchor dest = new Anchor("我是锚点，也是超链接", getChineseFont());
        dest.setName("CN"); // 设置锚点的名字
        dest.setReference("http://www.baidu.com");// 连接
        paragraph.add(dest);
        paragraph.add(String.format(": %d sites", 100));
        document.add(paragraph);

        Anchor anchor = new Anchor("连接到设置的CN锚点。", getChineseFont());
        anchor.setReference("#bookId");// 取到锚点
        document.add(anchor);

        document.close();
    }

    /**
     * Chapter, Section对象（大纲）
     *
     * @throws Exception
     */
    public static void testChapterAndSection() throws Exception {
        // 1-创建一个pdf文档,document
        Document document = new Document();// 默认PageSize.A4, 36, 36, 36, 36
        // 解析器
        PdfWriter.getInstance(document, new FileOutputStream("./doc/11.pdf"));
        document.open();
        Paragraph title = new Paragraph("一级标题", getChineseFont());
        Chapter chapter = new Chapter(title, 1);

        Paragraph title2 = new Paragraph("二级标题-1", getChineseFont());
        Section section = chapter.addSection(title2);
        section.setBookmarkTitle("sectionName");// 左边目录显示的名字，不写就默认名
        section.setIndentation(30);
        section.setIndentationLeft(5);
        section.setBookmarkOpen(false);
        section.setNumberStyle(Section.NUMBERSTYLE_DOTTED_WITHOUT_FINAL_DOT);

        Section section2 = chapter.addSection(new Paragraph("二级标题-2", getChineseFont()));
        section2.setIndentation(30);
        section2.setIndentationLeft(5);
        section2.setBookmarkOpen(false);
        section2.setNumberStyle(Section.NUMBERSTYLE_DOTTED_WITHOUT_FINAL_DOT);

        Section subsection = section.addSection(new Paragraph("三级标题-1", getChineseFont()));
        subsection.setIndentationLeft(10);
        // subsection.setNumberDepth(1);
        subsection.setNumberStyle(Section.NUMBERSTYLE_DOTTED);

        Section subsection2 = section2.addSection(new Paragraph("三级标题-2", getChineseFont()));
        subsection2.setIndentationLeft(10);
        subsection2.setNumberStyle(Section.NUMBERSTYLE_DOTTED);
        document.add(chapter);

        document.close();
    }

    /**
     * 目录 / 书签 更每一页相关
     *
     * @throws Exception
     */
    public static void testPdfOutline() throws Exception {
        // 1-创建一个pdf文档,document
        // 默认PageSize.A4, 36, 36, 36, 36
        Document document = new Document();
        // 解析器
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("./doc/11.pdf"));
        document.open();

        document.add(new Chunk("Chapter 1").setLocalDestination("1"));
        document.newPage();

        document.add(new Chunk("Chapter 2").setLocalDestination("2"));
        document.add(new Paragraph(new Chunk("Sub 2.1").setLocalDestination("2.1")));
        document.add(new Paragraph(new Chunk("Sub 2.2").setLocalDestination("2.2")));

        document.newPage();

        document.add(new Chunk("Chapter 3").setLocalDestination("3"));

        // 内容对象
        PdfContentByte cb = writer.getDirectContent();
        // 获取外部根目录
        PdfOutline root = cb.getRootOutline();
        // 一级目录
        new PdfOutline(root, PdfAction.gotoLocalPage("1", false), "Chapter 1");
        // 一级目录
        PdfOutline oline2 = new PdfOutline(root, PdfAction.gotoLocalPage("2", false), "Chapter 2");
        // 是否打开
        oline2.setOpen(false);
        // 添加二级子目录
        new PdfOutline(oline2, PdfAction.gotoLocalPage("2.1", false), "Sub 2.1");
        new PdfOutline(oline2, PdfAction.gotoLocalPage("2.2", false), "Sub 2.2");
        // 添加三级目录
        new PdfOutline(root, PdfAction.gotoLocalPage("3", false), "Chapter 3");

        document.close();
    }

    /**
     * Header, Footer 头 根部
     */
    public static void testHeadFooter() throws Exception {
        Document doc = new Document();
        PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream("./doc/12.pdf"));
        writer.setPageEvent(new PdfPageHelper());
        doc.open();
        // 添加内容
        doc.add(new Paragraph("1 page"));
        doc.close();
    }

    /**
     * 插入表格
     *
     * @throws Exception
     * @author ShaoMin
     */
    public static void insertTableAndPdfPCell() throws Exception {

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        // 使用PDFWriter进行写文件操作
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("./doc/13.pdf"));
        document.open();

        // 中文字体
        Font fontChinese = getChineseFont();

        Paragraph titlle = new Paragraph("测试表格", fontChinese);
        // 中间
        titlle.setAlignment(Element.ALIGN_CENTER);
        document.add(titlle);

        PdfPTable pTable = new PdfPTable(2);

        PdfPCell pdfPCell = new PdfPCell(new Paragraph("编号:" + "111111111", fontChinese));
        pdfPCell.setBorder(Rectangle.NO_BORDER);
        pTable.addCell(pdfPCell);

        pdfPCell = new PdfPCell(new Paragraph("名称" + "aaaa", fontChinese));
        pdfPCell.setBorder(Rectangle.NO_BORDER);
        pTable.addCell(pdfPCell);

        float[] widths = {1f, 1f}; // 设置列的宽度 百分比

        pTable.setWidths(widths);
        pTable.setSpacingBefore(15f);
        document.add(pTable);

        // 设置表头
        String[] tableHeader = new String[6];
        String[] tableCont = new String[6];

        int colNumber = 6;
        for (int i = 0; i < 6; i++) {
            tableHeader[i] = "表头" + (i + 1);
            tableCont[i] = "内容" + (i + 1);
        }
        // 创建有6列的表格 可以设置重复表头
        PdfPTable datatable = new PdfPTable(colNumber);
        // 设置表格上面空白行
        datatable.setSpacingBefore(15f);
        // 每个单元格宽度
        // PdfPTable datatable = new PdfPTable(new float[]{1, 1, 1, 1, 1, 1})
        // 第一行作为标题. 定义为标题的行应该保留在新页面上.
        datatable.setHeaderRows(1);
        // 定义表格的宽度
        int[] cellsWidth = {1, 1, 1, 1, 1, 1};
        // 设置列的宽度 百分比
        datatable.setWidths(cellsWidth);
        // 表格的总宽度
        // datatable.setTotalWidth(300f);
        // 表格的宽度百分比
        datatable.setWidthPercentage(100);
        // 单元格的间隔
        datatable.getDefaultCell().setPadding(2);
        // 边框宽度
        datatable.getDefaultCell().setBorderWidth(2);
        // 设置表格的底色
        datatable.getDefaultCell().setBackgroundColor(BaseColor.GREEN);
        datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

        // 添加表头元素
        for (int i = 0; i < colNumber; i++) {
            datatable.addCell(new Paragraph(tableHeader[i], fontChinese));
        }

        // 添加表格的内容
        for (int i = 0; i < colNumber; i++) {
            datatable.addCell(new Paragraph(tableCont[i], fontChinese));
        }

        // 空白表格
        for (int i = 0; i < colNumber; i++) {
            PdfPCell cell = new PdfPCell(new Paragraph(""));
            // 单元格高度
            cell.setFixedHeight(20);
            datatable.addCell(cell);
        }
        // 设置表格下面空白行
        datatable.setSpacingAfter(40f);
        // 把表格加入文档
        document.add(datatable);

        // 跨行跨列表格
        PdfPTable table = new PdfPTable(3);
        // 3列表格
        PdfPCell cell; // 单元格
        cell = new PdfPCell(new Phrase("跨三列", getChineseFont()));
        // 跨3列
        cell.setColspan(3);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("跨二行", getChineseFont()));
        // 跨2行
        cell.setRowspan(2);
        table.addCell(cell);
        table.addCell(new PdfPCell(new Phrase("第一行,第一列", getChineseFont())));
        table.addCell(new PdfPCell(new Phrase("第一行,第二列", getChineseFont())));
        table.addCell(new PdfPCell(new Phrase("第二行,第一列", getChineseFont())));
        table.addCell(new PdfPCell(new Phrase("第二行,第二列", getChineseFont())));

        document.add(table);

        // 表格的嵌套 4列
        PdfPTable tableFather = new PdfPTable(4);
        // 设置表格上面空白行
        tableFather.setSpacingBefore(20f);
        // 1行2列
        PdfPTable nested1 = new PdfPTable(2);
        // 设置无边框
        nested1.getDefaultCell().setBorderWidthBottom(Rectangle.NO_BORDER);
        nested1.getDefaultCell().setBorderWidthRight(Rectangle.NO_BORDER);
        nested1.getDefaultCell().setBorderWidthTop(Rectangle.NO_BORDER);
        nested1.getDefaultCell().setBorderWidthLeft(Rectangle.NO_BORDER);
        nested1.addCell("1.1");
        nested1.getDefaultCell().setBorderWidthLeft(1);
        nested1.addCell("1.2");

        // 2行1列
        PdfPTable nested2 = new PdfPTable(1);
        nested2.getDefaultCell().setBorderWidthBottom(Rectangle.NO_BORDER);
        nested2.getDefaultCell().setBorderWidthRight(Rectangle.NO_BORDER);
        nested2.getDefaultCell().setBorderWidthTop(Rectangle.NO_BORDER);
        nested2.getDefaultCell().setBorderWidthLeft(Rectangle.NO_BORDER);
        nested2.addCell("2.1");
        nested2.getDefaultCell().setBorderWidthTop(1);
        nested2.addCell("2.2");

        // 将表格插入到指定位置
        for (int i = 0; i < 12; i++) {
            switch (i) {
                case 1:
                    tableFather.addCell(nested1);
                    break;
                case 6:
                    tableFather.addCell(nested2);
                    break;
                default:
                    tableFather.addCell("cell " + i);
                    break;
            }

        }

        // 设置表格下面空白行
        tableFather.setSpacingAfter(40f);
        // 把表格加入文档
        document.add(tableFather);

        // 表格嵌套
        PdfPTable rootTable = new PdfPTable(tableFather);
        document.add(rootTable);

        PdfPTable pdfPTable = new PdfPTable(1);
        pdfPTable.setTotalWidth(300f);
        // 得到层
        PdfContentByte tContent = writer.getDirectContent();

        PdfPCell pdfPCell1 = new PdfPCell(new Paragraph("编号:" + "fsddd", fontChinese));
        pdfPTable.addCell(pdfPCell1);

        // 写入绝对位置
        pdfPTable.writeSelectedRows(0, -1, 0, -1, 100, 200, tContent);
        pTable.setSpacingBefore(15f);
        document.add(pdfPTable);

        document.close();
    }

    /**
     * 添加水印
     *
     * @author ShaoMin
     * @throws Exception
     *
     */
    public static void testShuiyin() throws Exception {
        FileOutputStream out = new FileOutputStream("./doc/13.pdf");
        Document document = new Document(PageSize.A4);
        PdfWriter writer = PdfWriter.getInstance(document, out);
        JLabel label = new JLabel();
        int textH = 0;
        int textW = 0;
        int interval = -5;
        String waterMarkName = "测试水印"; // 需要添加的水印文字
        label.setText(waterMarkName);
        FontMetrics metrics = label.getFontMetrics(label.getFont());
        textH = metrics.getHeight(); // 字符串的高, 只和字体有关
        textW = metrics.stringWidth(label.getText()); // 字符串的宽
        float opacity = 0.1f;// 水印字体透明度
        int fontsize = 12; // 水印字体大小
        int angle = 30; // 水印倾斜角度（0-360）
        int heightRatio = 2; // 数值越大每页竖向水印越少
        int widthRatio = 2; // 数值越大每页横向水印越少
        // 设置水印透明度
        PdfGState gs = new PdfGState();
        // 这里是透明度设置
        gs.setFillOpacity(opacity);
        // 这里是条纹不透明度
        gs.setStrokeOpacity(0.1f);
        document.open();
        // 在内容下方
        PdfContentByte under = writer.getDirectContentUnder();
        // 在内容上方
        // under = writer.getDirectContent();
        under.beginText();
        under.setGState(gs);
        BaseFont bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.EMBEDDED);
        under.setFontAndSize(bf, fontsize);
        Rectangle rectangle = document.getPageSize();
        // under.setTextMatrix(30, 30);
        // 水印文字成45度角倾斜
        for (int height = interval + textH; height < rectangle.getHeight() * 2; height = height + textH * heightRatio) {
            for (int width = interval + textW; width < rectangle.getWidth() * 1.5 + textW; width = width + textW * widthRatio) {
                // rotation:倾斜角度
                under.showTextAligned(Element.ALIGN_LEFT, waterMarkName, width - textW, height - textH, angle);
            }
        }
        under.endText();
        document.add(new Paragraph("测试", getChineseFont()));
        document.close();
    }

    /**
     * 添加水印
     *
     * @author ShaoMin
     * @throws IOException
     *
     */
    public static void testShuiyin1() throws Exception {
        FileOutputStream out = new FileOutputStream("./doc/14.pdf");
        // 读取器
        PdfReader reader = new PdfReader("./doc/12.pdf");
        // 解析器与输出
        PdfStamper stamp = new PdfStamper(reader, out);
        // 图片水印
        Image img = Image.getInstance("./doc/aaa.jpg");
        img.setAbsolutePosition(100, 100);// 位置
        // 在内容下方添加
        PdfContentByte under = stamp.getOverContent(1);// 拿到层,页数
        under.addImage(img);
        // 文字水印
        // 在内容上方添加
        PdfContentByte over = stamp.getOverContent(1);// 拿到层
        over.beginText();
        BaseFont bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        over.setFontAndSize(bf, 18);
        over.setTextMatrix(30, 30);
        over.showTextAligned(Element.ALIGN_LEFT, "文字水印", 230, 430, 45);
        over.endText();
        // 背景图
        Image img2 = Image.getInstance("./doc/a.jpeg");
        // 水印与背景的区别：背景只需要把绝对置为从 文档左下角开始。即设置setAbsolutePosition(0, 0)
        img2.setAbsolutePosition(0, 0);
        PdfContentByte under2 = stamp.getUnderContent(1);
        under2.addImage(img2);
        // 关闭
        stamp.close();
        reader.close();
    }

    /**
     * 添加水印
     *
     * @author ShaoMin
     * @throws IOException
     *
     */
    public static void testShuiyin2() throws Exception {
        FileOutputStream out = new FileOutputStream("./doc/15.pdf");
        Document doc = new Document();
        PdfWriter writer = PdfWriter.getInstance(doc, out);
        doc.open();
        writer.setPageEvent(new PdfPageHelper());
        doc.newPage();
        doc.add(new Chunk("aaa"));
        doc.close();
    }

    /**
     * 插入页眉页脚，需要使用监听器
     *
     * @author ShaoMin
     * @throws Exception
     *
     */
    public static void insertHeadAndFoot() throws Exception {
        FileOutputStream out = new FileOutputStream("./doc/16.pdf");
        Document doc = new Document();
        PdfWriter writer = PdfWriter.getInstance(doc, out);
        // 内部类，处理器
        writer.setPageEvent(new PdfPageEventHelper() {
            // 模板
            public PdfTemplate total;
            private  BaseFont bf = null;
            private Font basefont = null;

            {
                try {
                    bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
                    basefont = new Font(bf, 12, Font.NORMAL);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            /**
             * 文档打开时创建模板
             */
            public void onOpenDocument(PdfWriter writer, Document document) {
                total = writer.getDirectContent().createTemplate(50, 50);// 共 页 的矩形的长宽高
            }
            @Override
            public void onEndPage(PdfWriter writer, Document document) {
                PdfContentByte cb = writer.getDirectContent();
                // 写入页眉
                ColumnText.showTextAligned(cb, Element.ALIGN_LEFT, new Phrase("页眉", basefont), document.left(), document.top() + 20, 0);
                //写入页码
                int pageS = writer.getPageNumber();
                String foot1 = "第 " + pageS + " 页 /共";
                float len = bf.getWidthPoint(foot1, 12);
                Phrase footer = new Phrase(foot1, basefont);
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer,(document.rightMargin() + document.right() + document.leftMargin() - document.left() - len) / 2.0F + 20F, document.bottom() - 20, 0);
                cb.addTemplate(total, (document.rightMargin() + document.right() + document.leftMargin() - document.left()) / 2.0F + 20F, document.bottom() - 20);
            }

            /**
             * 关闭文档时，替换模板，完成整个页眉页脚组件
             */
            public void onCloseDocument(PdfWriter writer, Document document) {
                // 关闭文档的时候，将模板替换成实际的 Y 值,至此，page x of y 制作完毕，完美兼容各种文档size。
                total.beginText();
                total.setFontAndSize(bf, 12);// 生成的模版的字体、颜色
                String foot2 = " " + (writer.getPageNumber()) + " 页";
                total.showText(foot2);// 模版显示的内容
                total.endText();
                total.closePath();
            }

        });
        doc.open();
        doc.add(new Paragraph("1 page"));
        doc.newPage();
        doc.add(new Paragraph("2 page"));
        doc.close();
    }

    /**
     * 插入页眉页脚 指定位置
     *
     * @author ShaoMin
     * @throws Exception
     *
     */
    public static void insertHeadAndFoot2() throws Exception {
        FileOutputStream out = new FileOutputStream("./doc/17.pdf");
        Document doc = new Document(PageSize.A4);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PdfWriter.getInstance(doc, outputStream);
        doc.open();
        doc.add(new Paragraph("1 page"));
        doc.newPage();
        doc.add(new Paragraph("2 page"));
        doc.close();

        BaseFont bf = null;
        try {
            bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 读取现存在的pdf文件 追加页码
        PdfReader reader = new PdfReader(outputStream.toByteArray());
        int numberOfPages = reader.getNumberOfPages();
        PdfStamper stamp = new PdfStamper(reader, out);
        for (int i = 1; i <= numberOfPages; i++) {
            PdfContentByte cb = stamp.getOverContent(i);// 页数
            cb.beginText();
            cb.setFontAndSize(bf, 10);
            cb.showTextAligned(PdfContentByte.ALIGN_CENTER, "- " + (i) + " -", (doc.right() + doc.left()) / 2, doc.bottom(-20), 0);
            cb.endText();
        }
        stamp.close();
        reader.close();
    }

    /**
     * 插入页眉页脚 指定位置
     *
     * @throws Exception
     */
    public static void insertHeadAndFoot3() throws Exception {
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("./doc/11.pdf"));
        writer.setPageEvent(new MyHeaderFooter());
        document.open();
        document.add(new Paragraph("aaa"));
        document.close();

    }

    /**
     * PDF文件合并 使用PdfCopy
     *
     * @author
     * @param files
     */
    public static boolean mergePdfFiles(String[] files, String newfile) {
        boolean retValue = false;
        Document document = null;
        try {
            document = new Document();
            PdfCopy copy = new PdfCopy(document, new FileOutputStream(newfile));
            document.open();
            for (int i = 0; i < files.length; i++) {// 几个pdf文件循环
                PdfReader reader = new PdfReader(files[i]);
                int n = reader.getNumberOfPages();
                for (int j = 1; j <= n; j++) {// 一个文件有多少页循环
                    document.newPage();
                    // 从reader读取原始pdf每一页的数据追加进新的pdf中
                    PdfImportedPage page = copy.getImportedPage(reader, j);
                    copy.addPage(page);
                }
            }
            retValue = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
        return retValue;
    }

    /**
     * 合并PDF
     */
    public static void mergePdf(String[] files, String savepath) throws Exception {
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(savepath));
        document.open();
        PdfContentByte cb = writer.getDirectContent();// 得到层

        for (int i = 0; i < files.length; i++) {
            PdfReader reader = new PdfReader(files[i]);
            int n = reader.getNumberOfPages();
            for (int j = 1; j <= n; j++) {
                document.newPage();
                PdfImportedPage page = writer.getImportedPage(reader, j);
                // 使用writer需要使用pdf的层,然后后添加
                // 可以 放大 缩小
                cb.addTemplate(page, 0, 0);
                // 扩大比例
                // cb.addTemplate(page, 0.6f, 0, 0, 0.6f, 0, page.getHeight() + 10);
                // cb.addTemplate(page, 1.0f, 0, 0, 1.0f, 0, page.getHeight() + 10);
            }
        }
        document.newPage();
        Image img = Image.getInstance("./doc/a.jpeg");
        img.setAlignment(Image.LEFT);
        img.setBorder(Image.BOX);
        img.setBorderWidth(10);
        img.setBorderColor(BaseColor.WHITE);
        img.scaleToFit(800, 72);// 大小
        img.setRotation(-20);// 旋转 弧度
        img.setRotationDegrees(-30);// 旋转 角度
        img.scalePercent(30);// 依照比例缩放
        document.add(img);
        document.close();
    }

    /**
     * 删除页
     *
     * @author ShaoMin
     * @throws Exception
     *
     */
    public static void deletePage() throws Exception {
        FileOutputStream out = new FileOutputStream("./doc/a2.pdf");
        // 删除的方法在于读取，然后选择页数，然后在输出到另一个pdf
        PdfReader reader = new PdfReader("./doc/a1.pdf");// 读取pdf
        reader.selectPages("1,3");// 选择页数
        PdfStamper stamp = new PdfStamper(reader, out);// 输出
        stamp.close();
        reader.close();
    }

    /**
     * 读取PDF 内容
     *
     * @return
     * @throws Exception
     */
    public static String testPdfContent() throws Exception {
        PDDocument document = PDDocument.load(new FileInputStream("./doc/99.pdf"));
        document.getClass();
        // 使用PDFTextStripper 工具
        PDFTextStripper tStripper = new PDFTextStripper();
        // 设置文本排序，有规则输出
        tStripper.setSortByPosition(true);
        // 获取所有文字信息
        String info = tStripper.getText(document);
        return info;
    }

    /**
     * 删除pdf内容
     *
     * @param srcUrl
     * @param outputPdfFile
     * @throws Exception
     */
    public static void deletePdfContent(String srcUrl, String outputPdfFile) throws Exception {
        PdfReader reader = new PdfReader(srcUrl);
        Document doc = new Document(reader.getPageSize(1));
        PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(outputPdfFile));
        doc.open();
        doc.newPage();
        PdfContentByte cb = writer.getDirectContent();
        PdfImportedPage page = writer.getImportedPage(reader, 1);
        cb.addTemplate(page, 0, 0);
        // ⽩⾊底的覆盖层
        cb.saveState();
        cb.setColorFill(BaseColor.WHITE);
        cb.rectangle(0f, 0f, doc.getPageSize().getWidth(), doc.bottom(85));
        cb.fill();
        cb.restoreState();
        doc.close();
    }

    // 一打四
    public static void mergePdf(java.util.List<String> orginPdfList, String outputPdfFile) throws Exception {
        Document doc = new Document(PageSize.A4);
        PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(outputPdfFile));
        writer.setPageEvent(new PdfPageEventHelper() {
            @Override
            public void onStartPage(PdfWriter writer, Document document) {
                PdfContentByte cb = writer.getDirectContent();
                cb.saveState();
                cb.beginText();
                BaseFont bf = null;
                try {
                    bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                cb.setFontAndSize(bf, 10);
                float y = document.bottom(-20);
                cb.showTextAligned(PdfContentByte.ALIGN_CENTER, "-" + writer.getPageNumber() + "-", (document.right() + document.left()) / 2, y, 0);
                cb.endText();
                cb.restoreState();
            }
        });
        doc.open();
        float height = 0f;
        for (int i = 0; i < orginPdfList.size(); i++) {
            PdfReader reader = new PdfReader(orginPdfList.get(i));
            PdfContentByte cb = writer.getDirectContent();
            PdfImportedPage page = writer.getImportedPage(reader, 1);
            height = page.getHeight();
            if (i == 0) {
                // 设置 比例 放大或者缩小 以及防止位置
                cb.addTemplate(page, 0.95, 0, 0, 0.95, 0, height);
            }
            if (i == 1) {
                cb.addTemplate(page, 0.95, 0, 0, 0.95, 300, height);
            }
            if (i == 2) {
                cb.addTemplate(page, 0.95, 0, 0, 0.95, 0, height - 410);
            }
            if (i == 3) {
                cb.addTemplate(page, 0.95, 0, 0, 0.95, 300, height - 410);
            }
        }
        doc.close();
    }

    /**
     * html 转成pdf
     * @param src
     * @param target
     * @throws IOException
     * @throws DocumentException
     */
    public static void htmlToPdf(String src, String target) throws IOException, DocumentException {
        Document document = new Document(PageSize.B5, 20, 20, 30, 20);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(target));
        document.open();
        XMLWorkerHelper.getInstance().parseXHtml(writer, document, new FileInputStream(src), null, Charset.forName("UTF-8"), new XMLWorkerFontProvider() {
            @Override
            public Font getFont(final String fontname, final String encoding, final boolean embedded, final float size, final int style,
                final BaseColor color) {
                BaseFont bf = null;
                try {
                    bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
                } catch (DocumentException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Font font = new Font(bf, 6, style, color);
                font.setColor(color);
                return font;
            }
        });
        document.close();
    }

    /**
     * 支持中文
     *
     * @return
     */
    public static Font getChineseFont() {
        BaseFont bfChinese;
        Font fontChinese = null;
        try {
            // 支持中文
            bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
            fontChinese = new Font(bfChinese, 12, Font.NORMAL, BaseColor.BLUE);
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fontChinese;

    }

}
