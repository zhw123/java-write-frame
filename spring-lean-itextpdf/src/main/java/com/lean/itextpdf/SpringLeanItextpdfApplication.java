package com.lean.itextpdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanItextpdfApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanItextpdfApplication.class, args);
    }

}
