package com.lean.chat.entitiy;

import lombok.Data;

/**
 * @author Lenovo
 */
@Data
public class LoginUser {
	Integer id;
	String username;
	String password;
}
