package com.lean.chat.service;


import com.lean.chat.entitiy.LoginUser;
import com.lean.chat.mapper.LoginMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
/**
 * @author Lenovo
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 5,rollbackFor = Exception.class)
public class LoginService {
    @Autowired
    LoginMapper loginmapper;

    public String getpwdbyname(String name) {
        LoginUser loginUser=loginmapper.getUserByName(name);
        return loginUser==null?null:loginUser.getPassword();
    }

    public Integer getIdbyname(String name) {
        LoginUser loginUser=loginmapper.getUserByName(name);
        return loginUser==null?null:loginUser.getId();
    }

    public LoginUser getUserById(Integer id) {
        return loginmapper.getUserById(id);
    }
}
