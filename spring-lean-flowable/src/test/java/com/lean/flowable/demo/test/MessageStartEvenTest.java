package com.lean.flowable.demo.test;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

/**
 * 消息启动事件测试
 */
public class MessageStartEvenTest extends BaseConfiguation {

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("messagestartevent")
													.name("messagestartevent")
													.addClasspathResource("process/消息事件.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例
	 */
	@Test
	public void startProcessInstanceByKey() {
		String processDefinitionKey = "messagestartevent";
		runtimeService.startProcessInstanceByKey(processDefinitionKey);
	}
	/**
	 * 消息在接收messageEventReceived的时候，会触发对应边界消息事件。
	 */
	@Test
	public void messageEventReceived() {
		String messageName = "newInvoiceMessage";
		String executionId = "20007";
		runtimeService.messageEventReceived(messageName, executionId);
	}
	/**
	 * 启动消息流程实例
	 */
	@Test
	public void startProcessInstanceByMessage() {
		String messageName = "newInvoiceMessage";
		runtimeService.startProcessInstanceByMessage(messageName);
	}
	/**
	 * 完成用户任务
	 */
	@Test
	public void complete() {
		String taskId = "2508";
		taskService.complete(taskId);
	}
}
