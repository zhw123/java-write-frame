package com.lean.flowable.demo.test;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

/**
 * 信号事件测试
 */
public class SignalStartEventTest extends BaseConfiguation {

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("signalstartevent")
													.name("signalstartevent")
													.addClasspathResource("process/信号开始事件.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}

	/**
	 * 启动消息流程实例
	 */
	@Test
	public void startProcessInstanceByMessage() {
		String processDefinitionKey = "signalstartevent";
		runtimeService.startProcessInstanceByKey(processDefinitionKey);
	}
	/**
	 * 完成用户任务
	 */
	@Test
	public void complete() {
		String taskId = "302506";
		taskService.complete(taskId);
	}
}
