package com.lean.flowable.demo.test;

import java.util.HashMap;
import java.util.Map;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

/**
 *条件序列流--测试
 *
 */
public class ConditionSequenceFlowTest extends BaseConfiguation {

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("conditionsequenceflow")
													.name("conditionsequenceflow")
													.addClasspathResource("process/条件序列流.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例
	 *
	 */
	@Test
	public void start() {
		String processDefinitionKey = "conditionsequenceflow";
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("days", 13);
		runtimeService.startProcessInstanceByKey(processDefinitionKey,variables);
	}
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "22506";
		taskService.complete(taskId);
	}

}
