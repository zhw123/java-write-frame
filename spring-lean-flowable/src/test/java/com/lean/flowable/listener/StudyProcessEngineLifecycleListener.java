package com.lean.flowable.listener;

import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineLifecycleListener;

/**
 * 用来监听流程引擎的启动和停止
 */
public class StudyProcessEngineLifecycleListener implements ProcessEngineLifecycleListener {

    public void onProcessEngineBuilt(ProcessEngine processEngine) {
        System.out.println("流程引擎开始: " + processEngine);

    }

    public void onProcessEngineClosed(ProcessEngine processEngine) {
        System.out.println("流程引擎关闭: " + processEngine);

    }
}
