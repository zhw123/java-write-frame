package com.lean.flowable.testGateway;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.junit.Test;


/**
 *并行网关--测试
 *并行网关还可用于对流程中的并发进行建模。在流程模型中引入并发性的最直接的网关是并行网关，它允许您分叉到多个执行路径或连接多个传入的执行路径。
 *
 * 并行网关的功能基于传入和传出顺序流：
 *
 * **fork：**并行执行所有传出序列流，为每个序列流创建一个并发执行。
 * **join：**到达并行网关的所有并发执行在网关中等待，直到每个传入的序列流都到达执行。然后，该过程继续经过加入网关。
 * 请注意，如果同一并行网关有多个传入和传出顺序流，并行网关可以同时具有分叉和连接行为。在这种情况下，网关将首先连接所有传入的序列流，然后再分成多个并发的执行路径。
 */
public class ParallelGatewayTest extends BaseConfiguation {

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("forkJoin")
													.name("forkJoin")
													.addClasspathResource("process/gateway/并行网关.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例
	 *SELECT * FROM flowable.ACT_RU_EXECUTION;
	 * SELECT * FROM flowable.ACT_RU_TASK; 查询对应的任务id
	 */
	@Test
	public void start() {
		String processDefinitionKey = "forkJoin";
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionKey);
		System.out.println(processInstance.getProcessInstanceId());
	}
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "7505";
		taskService.complete(taskId);
	}

}
