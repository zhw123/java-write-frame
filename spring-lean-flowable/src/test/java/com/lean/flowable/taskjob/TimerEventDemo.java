package com.lean.flowable.taskjob;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

public class TimerEventDemo extends BaseConfiguation {

    /**
     * classpath方式部署 涉及三张表：ACT_RE_PROCDEF,ACT_RE_DEPLOYMENT,ACT_GE_BYTEARRAY
     */
    @Test
    public void deploy() {
        DeploymentBuilder deploymentBuilder = repositoryService.createDeployment().category("timertest").key("timertest").name("定时器事件实践")
            .addClasspathResource("/process/taskjob/定时器事件实践.bpmn20.xml");
        Deployment deploy = deploymentBuilder.deploy();

        System.out.println("流程ID: " + deploy.getId());
    }

    /**
     * 启动流程实例
     */
    @Test
    public void complete() {
        String taskId = "37503";
        taskService.complete(taskId);

    }

    /**
     * 睡眠20分钟,便于测试定时事件
     *
     * @throws InterruptedException
     */
    @Test
    public void sleep() throws InterruptedException {
        Long millis = 12000000L;
        Thread.sleep(millis);
    }

}
