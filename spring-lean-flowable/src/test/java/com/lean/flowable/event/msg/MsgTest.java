package com.lean.flowable.event.msg;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.repository.Deployment;
import org.junit.Test;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 消息事件 是指引用具名消息的事件。消息具有名字与载荷。与信号不同，消息事件只有一个接收者
 */
public class MsgTest extends BaseConfiguation {

    // 1.启动事件
    // 消息启动事件，也就是我们通过接收到某些消息后来启动流程实例，比如接收到了一封邮件，一条短信等
    /**
     * Deploy
     */
    @Test
    void testDeploy() throws Exception {
        Deployment deploy = repositoryService.createDeployment().addClasspathResource("/process/msg/消息启动事件.bpmn20.xml").name("消息启动事件").deploy();
        System.out.println("deploy.getId() = " + deploy.getId());
        System.out.println("deploy.getName() = " + deploy.getName());
        System.out.println("部署开始的时间：" + new Date());
    }

    /**
     * 部署后不会自动启动，我们需要接收相关的信息后来触发。 通过消息来启动一个流程实例
     */
    @Test
    void startFlow() throws Exception {

        // runtimeService.startProcessInstanceById("event006:1:0532e730-af02-11ec-8cf3-c03c59ad2248");
        // 注意：发送消息发送的应该是消息的名称而不是消息的ID
        runtimeService.startProcessInstanceByMessage("第一个消息");
        System.out.println("启动时间：" + new Date());
        // 我们得保证容器的运行，所以需要阻塞
        TimeUnit.MINUTES.sleep(1);
    }

    // 2.消息中间事件
    // 在流程运作中需要消息来触发的场景，案例演示，自动流程1 处理完成后，需要接收特定的消息之后才能进入到 自动流程2

    /**
     * Deploy
     */
    @Test
    void testDeploy1() throws Exception {
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("/process/msg/消息中间事件.bpmn20.xml").name("消息中间事件").deploy();
        System.out.println("deploy.getId() = " + deploy.getId());
        System.out.println("deploy.getName() = " + deploy.getName());
        System.out.println("部署开始的时间：" + new Date());
        // TimeUnit.MINUTES.sleep(3);
    }

    /**
     * 通过消息来启动一个流程实例
     */
    @Test
    void startFlow1() throws Exception {

        runtimeService.startProcessInstanceById("event008:1:9217aa5e-af0e-11ec-b11f-c03c59ad2248");
        // 注意：发送消息发送的应该是消息的名称而不是消息的ID
        // runtimeService.startProcessInstanceByMessage("第一个消息");
        System.out.println("启动时间：" + new Date());
        // 我们得保证容器的运行，所以需要阻塞
        // TimeUnit.MINUTES.sleep(1);
    }

    /**
     * 中间事件-发布消息
     */
    @Test
    void recevedMsg1() {
        // 需要查询到executionId
        String processExecutionId = "b5349e22-af0e-11ec-93e6-c03c59ad2248";
        // 我们需要根据流程实例编号找到对应的执行编号
        /* Execution execution = runtimeService.createExecutionQuery()
                .processInstanceId("event008:1:9217aa5e-af0e-11ec-b11f-c03c59ad2248")
                .singleResult();
        System.out.println("----------->"+execution.getId());*/
        runtimeService.messageEventReceived("第二个消息", processExecutionId);
    }



    // 3.边界事件

    //消息边界事件，如果在消息触发前还没有
    /**
     * Deploy
     */
    @Test
    void testDeploy2() throws Exception {
        //RepositoryService repositoryService = processEngine.getRepositoryService();
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("/process/msg/消息边界事件.bpmn20.xml")
                .name("消息边界事件")
                .deploy();
        System.out.println("deploy.getId() = " + deploy.getId());
        System.out.println("deploy.getName() = " + deploy.getName());
        System.out.println("部署开始的时间：" + new Date());
        //TimeUnit.MINUTES.sleep(3);
    }
    /**
     * 通过消息来启动一个流程实例
     */
    @Test
    void startFlow2() throws Exception{

        runtimeService.startProcessInstanceById("event009:1:f2096787-af11-11ec-b290-c03c59ad2248");
        // 注意：发送消息发送的应该是消息的名称而不是消息的ID
        //runtimeService.startProcessInstanceByMessage("第一个消息");
        System.out.println("启动时间：" + new Date());
        // 我们得保证容器的运行，所以需要阻塞
        //TimeUnit.MINUTES.sleep(1);
    }

    //如果人工处理在消息订阅前没有处理就会触发边界事件

    /**
     * 边界事件-发布消息
     */
    @Test
    void recevedMsg(){
        // 需要查询到executionId
        String processExecutionId = "1d503361-af12-11ec-89a4-c03c59ad2248";
        // 我们需要根据流程实例编号找到对应的执行编号
       /* Execution execution = runtimeService.createExecutionQuery()
                .processInstanceId("event008:1:9217aa5e-af0e-11ec-b11f-c03c59ad2248")
                .singleResult();
        System.out.println("----------->"+execution.getId());*/
        runtimeService.messageEventReceived("第三个消息",processExecutionId);
    }


}
