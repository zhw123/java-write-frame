package com.lean.flowable.event.end;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.Deployment;
import org.flowable.task.api.Task;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 结束事件顾名思义就是流程结束的事件，除了前面遇到的空结束事件外，结束事件还包括如下几种：
 *
 * 错误结束事件 中断结束事件 取消结束事件
 */
public class EndEventTest extends BaseConfiguation {

    // 1.错误结束事件
    // 当流程执行到达**错误结束事件（error end event）**时，结束执行的当前分支，
    // 并抛出错误。这个错误可以由匹配的错误边界中间事件捕获。
    // 如果找不到匹配的错误边界事件，将会抛出异常。
    // 错误结束事件的作用就是在执行到错误结束的节点位置会抛出对应的错误，供需要获取的事件来处理。
    /**
     * 部署
     */
    @Test
    public void test() throws Exception {
        Deployment deployment = processEngine.getRepositoryService().createDeployment().addClasspathResource("/process/event/错误结束事件.bpmn20.xml").name("错误结束事件").deploy();
        System.out.println("-----" + deployment.getId());
    }

    /**
     * 启动流程实例,设置流程变量中的flag=0触发错误结束事件 子流程中，当flag的数据为0的时候，就会触发错误结束事件
     */
    @Test
    public void startTest() throws Exception {
        // 设置对应的流程变量的值
        Map<String, Object> map = new HashMap<>();
        map.put("flag", 0);// 设置flag为0触发流程结束事件
        processEngine.getRuntimeService().startProcessInstanceById("event2005:1:ebec87e2-b028-11ec-b93f-c03c59ad2248", map);
        System.out.println("开始启动的时间：" + LocalDateTime.now().toString());
        // 需要在此阻塞比等待长的时间
        TimeUnit.MINUTES.sleep(3);
    }

    // 2.中断结束事件

    // 中断结束事件也称为终止结束事件，主要是对流程进行终止的事件，可以在一个复杂的流程中，如果某方想要提前中断这个流程，
    // 可以采用这个事件来处理，可以在并行处理任务中。如果你是在流程实例层处理，整个流程都会被中断，如果是在子流程中使用，
    // 那么当前作用和作用域内的所有的内部流程都会被终止。

    //具体操作：部署流程–>启动流程实例–>wanwu 处理任务【流程实例 flag <= 0】 触发任务

    //案例一
    @Test
    public void test01() throws Exception {

        Deployment deployment = processEngine.getRepositoryService().createDeployment().addClasspathResource("/process/event/中断结束事件.bpmn20.xml").name("终止流程1").deploy();
        System.out.println("-----");
    }

    @Test
    public void startTest01()  throws Exception{
        // 设置对应的流程变量的值
        Map<String,Object> map = new HashMap<>();
        map.put("flag",0);// 设置flag为0触发流程结束事件
        processEngine.getRuntimeService()
                .startProcessInstanceById("event3001:1:8b1663fa-b02d-11ec-b480-c03c59ad2248",map);
    }


    @Test
    public void completeTask01(){
        TaskService taskService = processEngine.getTaskService();
        Task task = taskService.createTaskQuery()
                .processDefinitionId("event3001:1:8b1663fa-b02d-11ec-b480-c03c59ad2248")
                .taskAssignee("wangwu")
                .singleResult();
        taskService.complete(task.getId());
    }

    //案例二

    //流程说明：人工任务一 -->zhangsan 人工任务二 -->lisi 子人工任务一 ： user1 子人工任务二 ： user2
    //子任务完成任务如果flag<=0,那么子流程中断结束，子人工任务二不会触发

    @Test
    public void test02() throws Exception{

        Deployment deployment = processEngine.getRepositoryService().createDeployment()
                .addClasspathResource("/process/event/中断结束子任务事件.bpmn20.xml")
                .name("终止流程2")
                .deploy();
        System.out.println("-----");
    }

    @Test
    public void startTest02()  throws Exception{
        // 设置对应的流程变量的值
        Map<String,Object> map = new HashMap<>();
        map.put("flag",0);// 设置flag为0触发流程结束事件
        processEngine.getRuntimeService()
                .startProcessInstanceById("event3002:1:92679bec-b03d-11ec-901e-c03c59ad2248",map);
    }

    //执行 user1  的子任务，因为现在流程变量 flag=0 所以会触发终止结束事件
    @Test
    public void completeTask02(){
        TaskService taskService = processEngine.getTaskService();
        Task task = taskService.createTaskQuery()
                .processDefinitionId("event3002:1:92679bec-b03d-11ec-901e-c03c59ad2248")
                .taskAssignee("user1")
                .singleResult();
        taskService.complete(task.getId());
    }

    //执行成功后我们可以发现子人工任务一没有了，但是也没有子人工任务二，但是主流程中的两个任务都还在



    //3.取消结束事件

    // 取消结束事件（cancel end event）只能与BPMN事务子流程（BPMN transaction subprocess）一起使用。
    // 当到达取消结束事件时，会抛出取消事件，且必须由取消边界事件（cancel boundary event）捕获。取消边界事件将取消事务，并触发补偿（compensation）。

    //结束取消事件我们只能在事务子流程中使用

    //流程说明：流程中定义了一个事务子流程和两个自动任务 然后在事务子流程中定义了两个人工任务用一个排他网关连接，flag<=0 的情况下会触发 取消结束事件
    // 触发取消结束事件后同时会被取消边界事件捕获而走 取消事件结束的自动任务 同时自动补偿任务也会触发，关联的自动任务也会触发
    //注意在设置的时候需要设置补偿自动任务为可补偿的

    /**
     * 部署流程
     * @throws Exception
     */
    @Test
    public void test03() throws Exception{
        Deployment deployment = processEngine.getRepositoryService().createDeployment()
                .addClasspathResource("/process/event/取消结束事件.bpmn20.xml")
                .name("取消结束事件")
                .deploy();
        System.out.println("-----");
    }

    /**
     * 启动流程实例,设置流程变量中的flag=0触发取消结束事件
     */
    @Test
    public void startTest03()  throws Exception{
        // 设置对应的流程变量的值
        Map<String,Object> map = new HashMap<>();
        map.put("flag",0);// 设置flag为0触发流程结束事件
        processEngine.getRuntimeService()
                .startProcessInstanceById("myProcess:3:effb46cf-b168-11ec-926f-c03c59ad2248",map);
    }

    /**
     * 完成任务
     */
    @Test
    public void completeTask03(){
        TaskService taskService = processEngine.getTaskService();
        Task task = taskService.createTaskQuery()
                .processDefinitionId("myProcess:3:effb46cf-b168-11ec-926f-c03c59ad2248")
                .taskAssignee("zhangsan")
                .singleResult();
        taskService.complete(task.getId());
    }


    //4.补偿事件

    // 通过补偿达到控制业务流程的目的就是补偿事件，比如我们正常的买机票的流程下订单购买，然后同时弹出支付流程页面。支付成功后就可以等待出票了，
    // 但是如果我们支付失败的话，这时要么重新支付，更换支付方式或者取消预订，这时取消预订我们就可以通过补偿事件来实现，


    //整个流程执行的过程是：任务开始后会并行的执行机票预订和微信支付，然后在微信支付是抛出payFail错误，
    // 同时错误边界事件会捕获到这个错误，然后执行到 补偿抛出中间事件，之后在机票预订的 补偿边界事件 被触发，对应的补偿触发器会执行对应的代码。

    /**
     * 部署流程
     * @throws Exception
     */
    @Test
    public void test04() throws Exception{
        Deployment deployment = processEngine.getRepositoryService().createDeployment()
                .addClasspathResource("/process/event/补偿事件.bpmn20.xml")
                .name("补偿事件")
                .deploy();
    }

    /**
     * 启动流程实例
     */
    @Test
    public void startTest04()  throws Exception{
        processEngine.getRuntimeService()
                .startProcessInstanceById("myProcess:4:a45abe9f-b1b5-11ec-9daf-c03c59ad2248");
    }

}
