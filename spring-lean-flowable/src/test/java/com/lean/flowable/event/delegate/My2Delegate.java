package com.lean.flowable.event.delegate;

import java.time.LocalDateTime;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

public class My2Delegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) {
        System.out.println("MyTwoDelegate---->执行了" + LocalDateTime.now().toString());
    }
}
