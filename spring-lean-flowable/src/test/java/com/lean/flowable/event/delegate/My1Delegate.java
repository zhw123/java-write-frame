package com.lean.flowable.event.delegate;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

import java.time.LocalDateTime;

public class My1Delegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) {
        System.out.println("完成自动审批任务-----》MyOneDelegate" + LocalDateTime.now().toString());
    }
}
