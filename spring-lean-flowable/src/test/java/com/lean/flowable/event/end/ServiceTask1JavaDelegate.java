package com.lean.flowable.event.end;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 * 服务任务--触发补偿边界事件
 *
 *
 */
public class ServiceTask1JavaDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) {
		//获取当前节点
		FlowElement flowElement = execution.getCurrentFlowElement();
		System.out.println("当前节点名称: " + flowElement.getName());

	}

}
