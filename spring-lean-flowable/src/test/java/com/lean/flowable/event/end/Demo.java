package com.lean.flowable.event.end;

import java.util.HashMap;
import java.util.Map;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.DynamicBpmnService;
import org.flowable.engine.FormService;
import org.flowable.engine.HistoryService;
import org.flowable.engine.IdentityService;
import org.flowable.engine.ManagementService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

public class Demo extends BaseConfiguation {

	/* 补偿事件及补偿处理器 */

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
				.createDeployment()
				.category("CompensationProcessor")
				.name("CompensationProcessor")
				.addClasspathResource("/process/event/补偿事件及补偿处理器.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("边界事件和多实例,流程ID: " + deploy.getId());
	}

	/**
	 * 启动流程实例
	 *
	 */
	@Test
	public void startProcessInstanceByKey() {
		String processDefinitionKey = "CompensationProcessor";
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("test", "补偿事件及补偿处理器");
		runtimeService.startProcessInstanceByKey(processDefinitionKey,variables);
	}

	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "45009";
		taskService.complete(taskId);
	}

	/* 补偿的执行顺序 */

	/**
	 * 部署
	 */
	@Test
	public void deploy1() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("compensationexecutionorder")
													.name("compensationexecutionorder")
													.addClasspathResource("/process/event/补偿的执行顺序.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("边界事件和多实例,流程ID: " + deploy.getId());
	}

	/**
	 * 启动流程实例
	 *
	 */
	@Test
	public void startProcessInstanceByKey1() {
		String processDefinitionKey = "compensationexecutionorder";
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("test", "补偿的执行顺序");
		runtimeService.startProcessInstanceByKey(processDefinitionKey,variables);
	}

	/**
	 * 完成任务
	 */
	@Test
	public void complete1() {
		String taskId = "120004";
		taskService.complete(taskId);
	}


}
