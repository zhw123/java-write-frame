package com.lean.flowable.event.sign;

import java.util.Map;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 * 服务任务
 *
 *
 */
public class ProcessSingalServiceTaskJavaDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) {
		System.out.println("==================流程级别信号事件====================");
		System.out.println("当前流程定义id: " + execution.getProcessDefinitionId());
		System.out.println("当前流程实例id: " + execution.getProcessInstanceId());
		//获取当前节点
		FlowElement flowElement = execution.getCurrentFlowElement();
		System.out.println("当前节点名称: " + flowElement.getName());

		Map<String, Object> vars = execution.getVariables();

		for(Map.Entry<String, Object> entity: vars.entrySet()) {
			System.out.println("-----------------变量的key: " + entity.getKey() + ",变量的value: "+ entity.getValue()+"-----------------");
		}
	}

}
