package com.lean.flowable.event.sign;

import java.util.HashMap;
import java.util.Map;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.DynamicBpmnService;
import org.flowable.engine.FormService;
import org.flowable.engine.HistoryService;
import org.flowable.engine.IdentityService;
import org.flowable.engine.ManagementService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

public class Demo extends BaseConfiguation {

	/**
	 * 部署抛出信号事件
	 */
	@Test
	public void deployThrowglobalsignalevent() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("throwglobalsignalevent")
													.name("throwglobalsignalevent")
													.addClasspathResource("/process/sign/抛出信号事件.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("引用外部流程,流程ID: " + deploy.getId());
	}
	/**
	 * 部署捕获全局事件
	 */
	@Test
	public void deployCatchglobalevent() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("catchglobalevent")
													.name("catchglobalevent")
													.addClasspathResource("/process/sign/捕获全局事件.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("外部流程,流程ID: " + deploy.getId());
	}
	/**
	 * 部署流程范围信号事件
	 */
	@Test
	public void deployProcesssignalevent() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("processsignalevent")
													.name("processsignalevent")
													.addClasspathResource("/process/sign/流程范围信号事件.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("外部流程,流程ID: " + deploy.getId());
	}
	/**
	 * 启动抛出信号事件流程实例
	 *
	 */
	@Test
	public void startThrowglobalsignalevent() {
		String processDefinitionKey = "throwglobalsignalevent";
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("throwglobalsignalevent", "抛出信号事件流程");
		runtimeService.startProcessInstanceByKey(processDefinitionKey,variables);
	}
	/**
	 * 启动捕获全局事件流程实例
	 *
	 */
	@Test
	public void startCatchglobalevent() {
		String processDefinitionKey = "catchglobalevent";
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("catchglobalevent", "捕获全局事件流程");
		runtimeService.startProcessInstanceByKey(processDefinitionKey,variables);
	}
	/**
	 * 启动流程范围信号事件实例
	 *
	 */
	@Test
	public void startProcesssignalevent() {
		String processDefinitionKey = "processsignalevent";
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("processsignalevent", "流程范围信号事件");
		runtimeService.startProcessInstanceByKey(processDefinitionKey,variables);
	}
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "30006";
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("updateRules", "修改规则");
		taskService.complete(taskId,variables);
	}

}
