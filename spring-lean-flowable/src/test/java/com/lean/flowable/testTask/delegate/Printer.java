package com.lean.flowable.testTask.delegate;

import org.flowable.engine.delegate.DelegateExecution;

public class Printer {

	public void printMessage(DelegateExecution execution,String myVar) {
		System.out.println("==============================调用方法表达式（method expression）=============================");
		System.out.println("myVar: " + myVar);

	}

}
