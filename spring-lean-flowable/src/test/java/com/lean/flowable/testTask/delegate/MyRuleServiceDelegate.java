package com.lean.flowable.testTask.delegate;

import java.util.HashMap;
import java.util.Map;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.common.engine.api.delegate.Expression;
import org.flowable.common.engine.impl.el.VariableContainerWrapper;
import org.flowable.engine.delegate.BusinessRuleTaskDelegate;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.DelegateHelper;

public class MyRuleServiceDelegate implements BusinessRuleTaskDelegate {


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public void addRuleVariableInputIdExpression(Expression inputId) {

		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("order", "MyRuleServiceDelegate");
		VariableContainerWrapper wrapper = new VariableContainerWrapper(variables);

		inputId.setValue("wrapper", wrapper);

	}

	public void addRuleIdExpression(Expression inputId) {

	}

	public void setExclude(boolean exclude) {
		exclude = false;

	}

	public void setResultVariable(String resultVariableName) {
		resultVariableName = "resultVariableName";

	}

	public void execute(DelegateExecution execution) {
		FlowElement currentFlowElement = DelegateHelper.getFlowElement(execution);
		System.out.println("=========业务规则任务,当前节点:" + currentFlowElement.getName());
	}

}
