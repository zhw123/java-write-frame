package com.lean.flowable.testTask.test;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import com.lean.flowable.config.BaseConfiguation;


/**
 *Java接收任务--测试
 *
 */
public class ReceiveTaskTest extends BaseConfiguation{

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("receiveprocess")
													.name("receiveprocess")
													.addClasspathResource("process/task/Java接收任务.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例
	 *
	 */
	@Test
	public void start() {
		String processDefinitionKey = "receiveprocess";
		Map<String, Object> variables = new HashMap<String, Object>();
		runtimeService.startProcessInstanceByKey(processDefinitionKey,variables);
	}

	/**
	 * 触发流程穿过接收任务继续执行
	 */
	@Test
	public void trigger() {
		String executionId = "175002";
		runtimeService.trigger(executionId);
	}

	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "177502";
		taskService.complete(taskId);
	}
}
