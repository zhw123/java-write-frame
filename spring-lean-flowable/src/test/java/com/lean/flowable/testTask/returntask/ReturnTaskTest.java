package com.lean.flowable.testTask.returntask;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReturnTaskTest extends BaseConfiguation {

    /**
     * 部署流程
     */
    @Test
    void testDeploy() throws Exception {
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("/process/task/串行的回退.bpmn20.xml")
                .addClasspathResource("/process/task/子流程回退.bpmn20.xml")
                .addClasspathResource("/process/task/并行的回退.bpmn20.xml")
                .deploy();
        System.out.println("deploy.getId() = " + deploy.getId());
        System.out.println("deploy.getName() = " + deploy.getName());
        System.out.println("部署开始的时间：" + new Date());
    }
    /**
     * 启动流程实例
     */
    @Test
    void startProcess(){
        ProcessInstance processInstance = runtimeService
                .startProcessInstanceById("myProcess:1:4");
        System.out.println("processInstance.getId() = " + processInstance.getId());
    }

    //通过user1完成任务，直到user3完成任务，到user4来处理任务。

    /**
     * 完成任务
     */
    @Test
    void completeTask(){
        Task task = taskService.createTaskQuery()
                .processDefinitionId("myProcess:1:4")
                .taskAssignee("user1")
                .singleResult();
        taskService.complete(task.getId());
    }

    /**
     * 回退操作
     * 用户任务4回退到用户任务3的操作
     */
    @Test
    void rollbackTask1(){
        // 当前的Task对应的用户任务的Id
        List<String> currentActivityIds = new ArrayList<>();
        currentActivityIds.add("usertask4");
        // 需要回退的目标节点的用户任务Id
        String newActivityId = "usertask3";
        // 回退操作
        runtimeService.createChangeActivityStateBuilder()
                .processInstanceId("2501")
                .moveActivityIdsToSingleActivityId(currentActivityIds,newActivityId)
                .changeState();

    }


    //通过user3来完成任务继续到user4处理，然后我们可以测试回退到user1处。

    /**
     * 回退操作
     */
    @Test
    void rollbackTask2(){
        // 当前的Task对应的用户任务的Id
        List<String> currentActivityIds = new ArrayList<>();
        currentActivityIds.add("usertask4");
        // 需要回退的目标节点的用户任务Id
        String newActivityId = "usertask1";
        // 回退操作
        runtimeService.createChangeActivityStateBuilder()
                .processInstanceId("2501")
                .moveActivityIdsToSingleActivityId(currentActivityIds,newActivityId)
                .changeState();
    }


    //2.并行的回退

    //尝试从    业务副总      处回退到    用户审批01    处


    /**
     * 回退操作
     *   业务副总驳回到到用户审批处  那么行政审批的也应该要返回
     */
    @Test
    void rollbackTask(){
        // 当前的Task对应的用户任务的Id
        List<String> currentActivityIds = new ArrayList<>();
        currentActivityIds.add("usertask4"); // 行政副总
        currentActivityIds.add("usertask3"); // 业务副总
        // 需要回退的目标节点的用户任务Id
        String newActivityId = "usertask1"; // 用户审批01
        // 回退操作
        runtimeService.createChangeActivityStateBuilder()
                .processInstanceId("22501")
                .moveActivityIdsToSingleActivityId(currentActivityIds,newActivityId)
                .changeState();
    }


    //行政副总的 并行分支执行完成了，然后在 业务副总处审批要驳回的处理
    @Test
    void rollbackTask3(){
        // 当前的Task对应的用户任务的Id
        List<String> currentActivityIds = new ArrayList<>();
        currentActivityIds.add("usertask4"); // 行政副总
        //currentActivityIds.add("usertask3"); // 业务副总
        // 需要回退的目标节点的用户任务Id
        String newActivityId = "usertask1"; // 用户审批01
        // 回退操作
        runtimeService.createChangeActivityStateBuilder()
                .processInstanceId("22501")
                .moveActivityIdsToSingleActivityId(currentActivityIds,newActivityId)
                .changeState();
    }


    //3.子流程回退

    //从子流程回退到主流程

    /**
     * 回退操作
     *   从子流程回退到主流程操作
     */
    @Test
    void rollbackMainTask4(){

        // 回退操作
        runtimeService.createChangeActivityStateBuilder()
                .processInstanceId("2501")
                .moveActivityIdTo("usertask2","usertask1")
                .changeState();
    }


    /**
     * 回退操作
     *   从子流程回退到主流程操作：moveExecutionToActivityId不关心当前的节点
     */
    @Test
    void rollbackMainTask5(){

        // 回退操作
        runtimeService.createChangeActivityStateBuilder()
                .processInstanceId("2501")
                .moveExecutionToActivityId("5003","usertask1")
                .changeState();
    }


    /**
     * 回退操作
     *  从主流程回退到子流程操作
     */
    @Test
    void rollbackSubTask6(){

        // 回退操作
        runtimeService.createChangeActivityStateBuilder()
                .processInstanceId("2501")
                .moveActivityIdTo("usertask3","usertask2")
                .changeState();
    }

}
