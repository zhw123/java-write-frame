package com.lean.flowable.testTask.exception;

public class BussinessException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 8782353401282034612L;

	public BussinessException() {
		super();
	}

	public BussinessException(String message) {
		super(message);
		System.out.println("=====抛出业务逻辑异常" + message);
	}
}
