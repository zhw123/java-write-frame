package com.lean.flowable.testTask.test;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import com.lean.flowable.config.BaseConfiguation;


/**
 *Shell任务--测试
 *
 */
public class ShellTaskTest extends BaseConfiguation{

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("shellprocess")
													.name("shellprocess")
													.addClasspathResource("process/task/Shell任务.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例
	 *
	 */
	@Test
	public void start() {
		String processDefinitionKey = "shellprocess";


        Map<String, Object> vars = new HashMap<String, Object>();

		runtimeService.startProcessInstanceByKey(processDefinitionKey,vars);
	}

	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "157507";
		taskService.complete(taskId);
	}
}
