package com.lean.flowable.testTask.multilinstance;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.engine.delegate.DelegateExecution;

public class MulitiInstanceCompleteCondition {
	public boolean completeTask(DelegateExecution execution) {
		FlowElement flowElement = execution.getCurrentFlowElement();
		System.out.println("总的任务数量：" + execution.getVariable("nrOfInstances") + "当前获取的任务数量："
				+ execution.getVariable("nrOfActiveInstances") + " - " + "已经完成的会签任务数量："
				+ execution.getVariable("nrOfCompletedInstances"));

		System.out.println("=======【当前节点：】========" + flowElement.getName());
		// isComplete为true,直接完成
		String isComplete = execution.getVariable("isComplete").toString();

		if ("true".equals(isComplete)) {
			return true;
		}
		return false;
	}
}
