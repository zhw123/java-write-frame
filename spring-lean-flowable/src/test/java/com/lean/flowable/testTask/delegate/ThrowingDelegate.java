package com.lean.flowable.testTask.delegate;

import com.lean.flowable.testTask.exception.BussinessException;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ThrowingDelegate implements JavaDelegate {
	private static final Logger logger = LoggerFactory.getLogger(ThrowingDelegate.class);

	public void execute(DelegateExecution execution) {
		logger.info("Entered throwing delegate");
		Boolean localError = (Boolean) execution.getVariable("localError");

		if (localError) {
			logger.info("Throwing local error");
			throw new BussinessException("localError");
		}
	}

}
