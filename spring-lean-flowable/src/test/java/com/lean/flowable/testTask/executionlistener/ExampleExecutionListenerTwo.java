package com.lean.flowable.testTask.executionlistener;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.DelegateHelper;
import org.flowable.engine.delegate.ExecutionListener;

public class ExampleExecutionListenerTwo implements ExecutionListener {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public void notify(DelegateExecution execution) {
		FlowElement currentFlowElement = DelegateHelper.getFlowElement(execution);
        System.out.println("========================【执行器，当前节点】：" + currentFlowElement.getName()+ "=========================");

		execution.setVariable("variableSetInExecutionListener", "secondValue");
        execution.setVariable("eventNameReceived", execution.getEventName());

	}

}
