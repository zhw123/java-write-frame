package com.lean.flowable.listener;

import java.io.Serializable;

import org.flowable.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;

/**
 *
 * 表达式 处理数据
 *
 * 任务多实例四个内置变量 属性 解释 nrOfInstances 一共有多少个实例 nrOfCompletedInstances 已经完成的实例个数 nrOfActiveInstances 未完成的实例个数
 * <multiInstanceLoopCharacteristics isSequential="false" flowable:collection="assigneeList" flowable:elementVariable=
 * "assignee"> <completionCondition>${multiInstance.accessCondition(execution)}</completionCondition>
 * </multiInstanceLoopCharacteristics>
 */
@Component(value = "multiInstanceTwo")
public class MultiInstanceTwoCompleteTask implements Serializable {

    private static final double scale = 0.5;

    public boolean accessCondition(DelegateExecution execution) {
        Object nrOfInstances = execution.getVariable("nrOfInstances");// 实例总数
        Object nrOfActiveInstances = execution.getVariable("nrOfActiveInstances");// 未完成的实例
        Object nrOfCompletedInstances = execution.getVariable("nrOfCompletedInstances");// 已完成实例
        System.out.println("总实例数量" + Integer.parseInt(nrOfCompletedInstances.toString()));
        System.out.println("未完成的实例" + Integer.parseInt(nrOfActiveInstances.toString()));
        System.out.println("已完成实例" + Integer.parseInt(nrOfCompletedInstances.toString()));
        if (Float.parseFloat(nrOfCompletedInstances.toString()) / Float.parseFloat(nrOfInstances.toString()) > 0.5) {
            return true;// 如果完成的比例高于50%就返回ture,代表会签结束，没有完成的任务就自动结束了
        } else {
            return false;// 如果完成的比例不高于50%就返回false,还需要继续会签
        }
    }
}
