package com.lean.flowable.controller;

import com.lean.flowable.base.ResponseResult;
import com.lean.flowable.entity.FlowUserModel;
import com.lean.flowable.service.FlowableUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户相关
 */
@RequestMapping(value = "/flow-user")
@RestController
public class FlowableUserController {

    @Autowired
    private FlowableUserService flowableUserService;

    /**
     * 查看用户表详情
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getTableBaseData")
    public ResponseResult getTableBaseData() {
        return flowableUserService.getTableBaseData();
    }

    /**
     * 添加用户
     *  http://localhost:8834/flow-user/insertUser?id=1&password=123456&firstName=a&lastName=b&displayName=ab&email=123@123.com
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/insertUser")
    public ResponseResult insertUser(FlowUserModel flowUserModel) {
        return flowableUserService.insertUser(flowUserModel);
    }

    /**
     * 修改用户密码 可以修改其他用户属性
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateUserPassword")
    public ResponseResult updateUserPassword(String userId, String password) {
        return flowableUserService.updateUserPassword(userId, password);
    }

    /**
     * 删除用户
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deleteUser")
    public ResponseResult deleteUser(String userId) {
        return flowableUserService.deleteUser(userId);
    }

    /**
     * 查询用户基本信息
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getUserDetail")
    public ResponseResult getUserDetail(String userId) {
        return flowableUserService.getUserDetail(userId);
    }

    /**
     * 查询用户列表
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/findUserList")
    public ResponseResult getUserDetail(String firstName, String lastName, String displayName, Integer pageNum, Integer pageSize) {
        return flowableUserService.findUserList(firstName, lastName, displayName, pageNum, pageSize);
    }

    /**
     * 添加组
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/insertGroup")
    public ResponseResult insertGroup(String groupId, String groupName) {
        return flowableUserService.insertGroup(groupId, groupName);
    }

    /**
     * 组添加用户
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/addGroupUser")
    public ResponseResult addGroupUser(String groupId, String userId) {
        return flowableUserService.addGroupUser(groupId, userId);
    }

    /**
     * 删除组中的成员
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deleteMembership")
    public ResponseResult deleteMembership(String groupId, String userId) {
        return flowableUserService.deleteMembership(groupId, userId);
    }

    /**
     * 删除组
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deleteGroup")
    public ResponseResult deleteGroup(String groupId) {
        return flowableUserService.deleteGroup(groupId);
    }

    /**
     * 根据 id 查询组信息
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getGroupDetailById")
    public ResponseResult getGroupDetailById(String groupId) {
        return flowableUserService.getGroupDetailById(groupId);
    }

    /**
     * 根据 name 查询组信息
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getGroupDetailByGroupName")
    public ResponseResult getGroupDetailByGroupName(String groupName) {
        return flowableUserService.getGroupDetailByGroupName(groupName);
    }

    /**
     * 根据 成员 查询组信息
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getGroupListByUserName")
    public ResponseResult getGroupListByUserName(String userName, Integer pageNum, Integer pageSize) {
        return flowableUserService.getGroupListByUserName(userName, pageNum, pageSize);
    }

}
