package com.lean.flowable.controller;

import com.lean.flowable.base.ResponseResult;
import com.lean.flowable.service.FlowInstanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 工作流流程实例管理
 */
@Slf4j
@RestController
@RequestMapping("/flowable-instance")
public class FlowInstanceController {

    @Autowired
    private FlowInstanceService flowInstanceService;

    /**
     * 启动流程
     *
     * @param key
     * @param userId 启动人
     * @return
     */
    @RequestMapping(value = "/startProcessByKey")
    @ResponseBody
    public ResponseResult startProcessByName(@RequestParam String userId, @RequestParam String key, @RequestBody(required = false) Map<String, Object> variables) {
        return flowInstanceService.startProcessByName(key, userId, variables);
    }

    /**
     * 启动流程
     *
     * @param processDefinitionId
     * @param variables
     * @return
     */
    @RequestMapping(value = "/startById")
    @ResponseBody
    public ResponseResult startById(@RequestParam String userId,
                                    @RequestParam String processDefinitionId,
                                    @RequestBody(required = false) Map<String, Object> variables) {
        return flowInstanceService.startProcessById(userId, processDefinitionId, variables);
    }

    /**
     * 流程实例
     *  一个流程实例 包含任务
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/findMyProcessList")
    public ResponseResult findMyProcessList(String userId, Integer pageNum, Integer pageSize) {
        return flowInstanceService.findMyProcessList(userId, pageNum, pageSize);
    }

    /**
     * 流程历史流转记录
     *
     * @param processInstanceId
     * @param deployId
     * @return
     */
    @GetMapping(value = "/flowRecord")
    public ResponseResult flowRecord(String processInstanceId, String deployId) {
        return flowInstanceService.flowRecord(processInstanceId, deployId);
    }

    /**
     * 获取流程变量
     *
     * @param taskId 流程任务Id
     * @return
     */
    @GetMapping(value = "/processVariables")
    public ResponseResult processVariables(String taskId) {
        return flowInstanceService.processVariables(taskId);
    }

    /**
     * 激活或挂起流程实例
     *
     * @param state 1:激活,2:挂起
     * @param processInstanceId 流程实例ID
     * @return
     */
    @PostMapping(value = "/updateState")
    public ResponseResult updateState(@RequestParam Integer state, @RequestParam String processInstanceId) {
        flowInstanceService.updateState(state, processInstanceId);
        return ResponseResult.success("ok");
    }

    /**
     * 结束流程实例
     */
    @PostMapping(value = "/stopProcessInstance")
    public ResponseResult stopProcessInstance(@RequestParam String processInstanceId) {
        flowInstanceService.stopProcessInstance(processInstanceId);
        return ResponseResult.success("ok");
    }

    /**
     * 删除流程实例
     *
     * @param processInstanceId 流程实例ID
     * @param deleteReason 删除原因
     * @return
     */
    @DeleteMapping(value = "/delete")
    public ResponseResult delete(@RequestParam String processInstanceId,
                                 @RequestParam(required = false) String deleteReason) {
        flowInstanceService.delete(processInstanceId, deleteReason);
        return ResponseResult.success();
    }

    // ---------------流程启动后产生流程实例才生效-----------------------//

    /**
     * 查询指定流程所有启动的实例列表
     * http://localhost:8834/flowable-define/findProcessInstanceList?processDefinitionKey=leaveApproval&pageSize=1&pageNum=10
     *
     * @param processDefinitionKey 流程定义key
     * @return
     */
    @GetMapping("/findProcessInstanceList")
    @ResponseBody
    public ResponseResult findProcessInstanceList(String processDefinitionKey, Integer pageSize, Integer pageNum) {
        return ResponseResult.success(flowInstanceService.findProcessInstanceList(processDefinitionKey, pageSize, pageNum));
    }

    /**
     * 生成流程图 生成当前流程节点的流程图 生成流程图 http://localhost:8834/flowable-define/processDiagram?processInstanceId=81a5e653-536c-11ed-9fdb-0a0027000004
     *
     * @param processInstanceId 流程实例ID
     */
    @RequestMapping(value = "processDiagram")
    public void genProcessDiagram(HttpServletResponse httpServletResponse, String processInstanceId) throws Exception {
        flowInstanceService.getProcessDiagram(httpServletResponse, processInstanceId);
    }

    /**
     * 获取流程执行过程
     *
     * @param processInstanceId 流程实例编号
     * @param executionId 任务执行编号
     */
    @RequestMapping("/flowViewer")
    public ResponseResult getFlowViewer(@RequestParam String processInstanceId, @RequestParam String executionId) {
        return ResponseResult.success(flowInstanceService.getFlowViewer(processInstanceId, executionId));
    }

}
