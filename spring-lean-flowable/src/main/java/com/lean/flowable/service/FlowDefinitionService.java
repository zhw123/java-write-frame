package com.lean.flowable.service;

import com.lean.flowable.base.CustomProcessDiagramGenerator;
import com.lean.flowable.base.ResponseResult;
import com.lean.flowable.entity.DeploymentModel;
import com.lean.flowable.entity.FlowProcModel;
import com.lean.flowable.entity.FlowViewerModel;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.flowable.bpmn.constants.BpmnXMLConstants;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentQuery;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.repository.ProcessDefinitionQuery;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.image.ProcessDiagramGenerator;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipInputStream;

@Service
public class FlowDefinitionService extends BaseService {

    /**
     * 获取所有的流程定义
     *
     * @return
     */
    public List<FlowProcModel> findProcessList(String name, Integer pageNum, Integer pageSize) {
        final ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        if (StringUtils.isNotEmpty(name)) {
            processDefinitionQuery.processDefinitionNameLike(name);
        }
        processDefinitionQuery.orderByProcessDefinitionKey().asc();
        List<ProcessDefinition> processDefinitionList = processDefinitionQuery.listPage(pageSize * (pageNum - 1), pageSize);

        List<FlowProcModel> dataList = new ArrayList<>();
        for (ProcessDefinition processDefinition : processDefinitionList) {
            String deploymentId = processDefinition.getDeploymentId();
            Deployment deployment = repositoryService.createDeploymentQuery().deploymentId(deploymentId).singleResult();
            FlowProcModel flowProcModel = new FlowProcModel();
            BeanUtils.copyProperties(processDefinition, flowProcModel);
            flowProcModel.setDeploymentId(deploymentId);
            flowProcModel.setProcessDefinitionId(processDefinition.getId());
            // 流程定义时间
            flowProcModel.setDeploymentTime(deployment.getDeploymentTime());
            flowProcModel.setDeployName(deployment.getName());
            dataList.add(flowProcModel);
        }
        return dataList;
    }

    /**
     * 获取流程定义具体信息
     *
     * @param processDefinitionId
     * @return
     */
    public FlowProcModel findProcessDetailById(String processDefinitionId) {
        ProcessDefinition definition = repositoryService.createProcessDefinitionQuery().processDefinitionId(processDefinitionId).singleResult();
        Deployment deployment = repositoryService.createDeploymentQuery().deploymentId(definition.getDeploymentId()).singleResult();
        FlowProcModel flowProcModel = new FlowProcModel();
        BeanUtils.copyProperties(definition, flowProcModel);
        flowProcModel.setDeploymentId(deployment.getId());
        flowProcModel.setProcessDefinitionId(definition.getId());
        // 流程定义时间
        flowProcModel.setDeploymentTime(deployment.getDeploymentTime());
        return flowProcModel;
    }

    /**
     * 激活流程定义
     *
     * @param deployId
     * @return
     */
    public void updateProcessState(Integer state, String deployId) {
        ProcessDefinition procDef = repositoryService.createProcessDefinitionQuery().deploymentId(deployId).singleResult();
        // 激活
        if (state == 1) {
            repositoryService.activateProcessDefinitionById(procDef.getId(), true, null);
        }
        // 挂起
        if (state == 2) {
            repositoryService.suspendProcessDefinitionById(procDef.getId(), true, null);
        }
    }

    /**
     * 删除流程定义
     *
     * @param deploymentId
     * @return
     */
    public void deleteDeployment(String deploymentId) {
        // true 允许级联删除 ,不设置会导致数据库外键关联异常
        repositoryService.deleteDeployment(deploymentId, true);
    }

    /**
     * 部署压缩包内的流程资源— 资源包括：bpmn、png、drl、form等等 流程引擎：内部使用迭代器方式遍历压缩包中文件并读取成响应的文件流
     *
     * @param file
     * @param name 流程模板文件名字
     * @param tenantId 业务系统标识
     * @param category 流程模板文件类别
     * @return
     */
    public Deployment deployByZip(String name, String category, String tenantId, MultipartFile file) {
        Deployment deployment = null;
        try (ZipInputStream zipIn = new ZipInputStream(file.getInputStream(), Charset.forName("UTF-8"))) {
            deployment = repositoryService.createDeployment().addZipInputStream(zipIn).name(name).category(category).tenantId(tenantId).deploy();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deployment;
    }

    /**
     * 导入流程文件
     *
     * @param name
     * @param category
     * @param in
     */
    public void importFile(String name, String category, InputStream in) {
        Deployment deploy = repositoryService.createDeployment().addInputStream(name + BPMN_FILE_SUFFIX, in).name(name).category(category).deploy();
        ProcessDefinition definition = repositoryService.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();
        repositoryService.setProcessDefinitionCategory(definition.getId(), category);

    }

    /**
     * 根据流程部署name，查询流程部署信息（最新）
     *
     * @param deploymentName 流程部署name
     * @return
     */
    public List<DeploymentModel> deployListByName(String deploymentName,Integer pageNum,Integer pageSize) {
        DeploymentQuery deploymentQuery = repositoryService.createDeploymentQuery();
        if(StringUtils.isNotEmpty(deploymentName)){
            deploymentQuery.deploymentName(deploymentName);
        }
        List<DeploymentModel> deploymentModelList=new ArrayList<>();
        List<Deployment> deploymentList = deploymentQuery.orderByDeploymentTime().desc().listPage(pageSize * (pageNum - 1), pageSize);
        if(!CollectionUtils.isEmpty(deploymentList)){
            deploymentList.stream().forEach(deployment -> {
                DeploymentModel deploymentModel=new DeploymentModel();
                BeanUtils.copyProperties(deployment,deploymentModel);
                deploymentModelList.add(deploymentModel);
            });

        }
        return deploymentModelList;
    }

    /**
     * 读取xml文件
     *
     * @param deployId 流程模型id
     * @param processDefinitionId 流程定义id
     * @return
     * @throws IOException
     */
    public ResponseResult readXml(String deployId,String processDefinitionId) throws IOException {
        ProcessDefinition definition = repositoryService.createProcessDefinitionQuery().deploymentId(deployId).processDefinitionId(processDefinitionId).singleResult();
        InputStream inputStream = repositoryService.getResourceAsStream(definition.getDeploymentId(), definition.getResourceName());
        String xml = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
        return ResponseResult.success(xml);
    }



}
