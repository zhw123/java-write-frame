package com.lean.flowable.service;

import com.lean.flowable.base.CustomProcessDiagramGenerator;
import com.lean.flowable.base.FlowComment;
import com.lean.flowable.base.ProcessConstants;
import com.lean.flowable.base.ResponseResult;
import com.lean.flowable.entity.FlowCommentModel;
import com.lean.flowable.entity.FlowTaskDModel;
import com.lean.flowable.entity.FlowViewerModel;
import com.lean.flowable.utils.FlowableUtils;
import com.lean.flowable.utils.ModelUtils;
import liquibase.repackaged.org.apache.commons.collections4.CollectionUtils;
import liquibase.util.ObjectUtil;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.flowable.bpmn.model.*;
import org.flowable.bpmn.model.EndEvent;
import org.flowable.bpmn.model.Process;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.common.engine.impl.identity.Authentication;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.history.HistoricProcessInstanceQuery;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.repository.ProcessDefinitionQuery;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.engine.task.Comment;
import org.flowable.identitylink.api.history.HistoricIdentityLink;
import org.flowable.image.ProcessDiagramGenerator;
import org.flowable.task.api.Task;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FlowInstanceService extends BaseService {


    /**
     * 启动流程
     * @param key
     * @return
     */
    public ResponseResult startProcessByName(String key, String userId, Map<String, Object> variables) {
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionKey(key).latestVersion().singleResult();
        if (Objects.nonNull(processDefinition) && processDefinition.isSuspended()) {
            return ResponseResult.fail("流程已被挂起,请先激活流程");
        }
        //注意一点:这个方法最终使用一个ThreadLocal类型的变量进行存储，也就是与当前的线程绑定，所以流程实例启动完毕之后，需要设置为null，防止多线程的时候出问题。
        Authentication.setAuthenticatedUserId(userId);
        ProcessInstance processInstance = null;
        if(variables!=null && variables.size() > 0){
            processInstance = runtimeService.startProcessInstanceByKey(key, variables);
        }else {
            processInstance = runtimeService.startProcessInstanceByKey(key);
        }
        Authentication.setAuthenticatedUserId(null);
        return ResponseResult.success(processInstance.getProcessInstanceId());
    }

    /**
     *启动流程
     * @param processDefinitionId
     * @param variables
     * @return
     */
    public ResponseResult startProcessById(String userId, String processDefinitionId, Map<String, Object> variables) {
        ProcessDefinition processDefinition =
                repositoryService.createProcessDefinitionQuery().processDefinitionId(processDefinitionId).latestVersion().singleResult();
        if (Objects.nonNull(processDefinition) && processDefinition.isSuspended()) {
            return ResponseResult.fail("流程已被挂起,请先激活流程");
        }
        //设置流程发起人
        identityService.setAuthenticatedUserId(userId);
        //开启流程
        ProcessInstance processInstance = null;
        if(variables!=null && variables.size() > 0){
            processInstance = runtimeService.startProcessInstanceById(processDefinitionId, variables);
        }else {
            processInstance = runtimeService.startProcessInstanceById(processDefinitionId);
        }
        return ResponseResult.success(processInstance.getProcessInstanceId());
    }


    /**
     * 终止流程
     * 1、基于流程实例id获取工作流的model信息。
     * 2、遍历获取end节点id；
     * 3、直接调用  moveExecutionsToSingleActivityId
     * @param processInstanceId
     */
    public void stopProcessInstance(String processInstanceId) {
        // 获取流程实例
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(processInstanceId)
                .singleResult();
        if (processInstance == null) {
            throw new RuntimeException("流程实例不存在，请确认！");
        }
        // 获取流程定义信息
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId(processInstance.getProcessDefinitionId())
                .singleResult();

        // 获取所有节点信息
        BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinition.getId());
        EndEvent endEvent = ModelUtils.getEndEvent(bpmnModel);
        // 终止流程
        List<Execution> executions = runtimeService.createExecutionQuery().parentId(processInstanceId).list();
        List<String> executionIds = executions.stream().map(Execution::getId).collect(Collectors.toList());
        // 变更流程为已结束状态
        runtimeService.createChangeActivityStateBuilder()
                .processInstanceId(processInstanceId)
                .moveExecutionsToSingleActivityId(executionIds, endEvent.getId())
                .changeState();
    }

    /**
     * 激活或挂起流程实例
     * @param state 状态
     * @param processInstanceId 流程实例ID
     */
    public void updateState(Integer state, String processInstanceId) {
        // 激活
        if (state == 1) {
            runtimeService.activateProcessInstanceById(processInstanceId);
        }
        // 挂起
        if (state == 2) {
            runtimeService.suspendProcessInstanceById(processInstanceId);
        }
    }

    /**
     * 删除流程实例ID
     * @param processInstanceId 流程实例ID
     * @param deleteReason 删除原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void delete(String processInstanceId, String deleteReason) {
        // 查询历史数据
        HistoricProcessInstance historicProcessInstance = getHistoricProcessInstanceById(processInstanceId);
        if (historicProcessInstance.getEndTime() != null) {
            historyService.deleteHistoricProcessInstance(historicProcessInstance.getId());
            return;
        }
        // 删除流程实例
        runtimeService.deleteProcessInstance(processInstanceId, deleteReason);
        // 删除历史流程实例
        historyService.deleteHistoricProcessInstance(processInstanceId);
    }

    /**
     * 根据实例ID查询历史实例数据
     * @param processInstanceId
     * @return
     */
    public HistoricProcessInstance getHistoricProcessInstanceById(String processInstanceId) {
        HistoricProcessInstance historicProcessInstance =
            historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        if (Objects.isNull(historicProcessInstance)) {
            throw new FlowableObjectNotFoundException("流程实例不存在: " + processInstanceId);
        }
        return historicProcessInstance;
    }

    /**
     * userId 发起的流程
     * @param userId
     * @param pageNum
     * @param pageSize
     * @return
     */
    public ResponseResult findMyProcessList(String userId, Integer pageNum, Integer pageSize) {
        Map<String, Object> resultMap = new HashMap<>();
        HistoricProcessInstanceQuery historicProcessInstanceQuery =
            historyService.createHistoricProcessInstanceQuery().startedBy(userId).orderByProcessInstanceStartTime().desc();
        List<HistoricProcessInstance> processInstanceList = historicProcessInstanceQuery.listPage(pageSize * (pageNum - 1), pageSize);
        resultMap.put("total", historicProcessInstanceQuery.count());
        List<FlowTaskDModel> flowList = new ArrayList<>();
        for (HistoricProcessInstance hisIns : processInstanceList) {
            FlowTaskDModel flowTask = new FlowTaskDModel();
            flowTask.setCreateTime(hisIns.getStartTime());
            flowTask.setFinishTime(hisIns.getEndTime());
            flowTask.setProcInsId(hisIns.getId());
            flowTask.setStartUserId(hisIns.getStartUserId());
            // 计算耗时
            if (Objects.nonNull(hisIns.getEndTime())) {
                long time = hisIns.getEndTime().getTime() - hisIns.getStartTime().getTime();
                flowTask.setDuration(getDate(time));
            } else {
                long time = System.currentTimeMillis() - hisIns.getStartTime().getTime();
                flowTask.setDuration(getDate(time));
            }
            // 流程定义信息
            ProcessDefinition pd = repositoryService.createProcessDefinitionQuery().processDefinitionId(hisIns.getProcessDefinitionId()).singleResult();
            flowTask.setDeployId(pd.getDeploymentId());
            flowTask.setProcDefName(pd.getName());
            flowTask.setProcDefVersion(pd.getVersion());
            flowTask.setCategory(pd.getCategory());
            flowTask.setProcDefVersion(pd.getVersion());
            // 当前所处流程 todo: 本地启动放开以下注释
            List<Task> taskList = taskService.createTaskQuery().processInstanceId(hisIns.getId()).list();
            if (CollectionUtils.isNotEmpty(taskList)) {
                flowTask.setTaskId(taskList.get(0).getId());
            } else {
                List<HistoricTaskInstance> historicTaskInstance =
                    historyService.createHistoricTaskInstanceQuery().processInstanceId(hisIns.getId()).orderByHistoricTaskInstanceEndTime().desc().list();
                flowTask.setTaskId(historicTaskInstance.get(0).getId());
            }
            flowList.add(flowTask);
        }
        resultMap.put("data", flowList);
        return ResponseResult.success(resultMap);
    }

    /**
     * 流程历史流转记录
     * @param processInstanceId 流程实例Id
     * @param deployId 流程部署id
     * @return
     */
    public ResponseResult flowRecord(String processInstanceId, String deployId) {
        Map<String, Object> map = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(processInstanceId)) {
            List<HistoricActivityInstance> list =
                historyService.createHistoricActivityInstanceQuery().processInstanceId(processInstanceId).orderByHistoricActivityInstanceStartTime().desc().list();
            List<FlowTaskDModel> hisFlowList = new ArrayList<>();
            for (HistoricActivityInstance histIns : list) {
                if (StringUtils.isNotBlank(histIns.getTaskId())) {
                    FlowTaskDModel flowTask = new FlowTaskDModel();
                    flowTask.setTaskId(histIns.getTaskId());
                    flowTask.setTaskName(histIns.getActivityName());
                    flowTask.setCreateTime(histIns.getStartTime());
                    flowTask.setFinishTime(histIns.getEndTime());
                    if (StringUtils.isNotBlank(histIns.getAssignee())) {
                        flowTask.setAssigneeId(Long.parseLong(histIns.getAssignee()));
                        flowTask.setAssigneeName(histIns.getAssignee());
                        flowTask.setDeptName(histIns.getAssignee());
                    }
                    // 展示审批人员
                    List<HistoricIdentityLink> linksForTask = historyService.getHistoricIdentityLinksForTask(histIns.getTaskId());
                    StringBuilder stringBuilder = new StringBuilder();
                    for (HistoricIdentityLink identityLink : linksForTask) {
                        // 获选人,候选组/角色(多个)
                        if ("candidate".equals(identityLink.getType())) {
                            if (StringUtils.isNotBlank(identityLink.getUserId())) {
                                stringBuilder.append(identityLink.getUserId()).append(",");
                            }
                            if (StringUtils.isNotBlank(identityLink.getGroupId())) {
                                stringBuilder.append(identityLink.getUserId()).append(",");
                            }
                        }
                    }
                    if (StringUtils.isNotBlank(stringBuilder)) {
                        flowTask.setCandidate(stringBuilder.substring(0, stringBuilder.length() - 1));
                    }
                    flowTask.setDuration(
                        histIns.getDurationInMillis() == null || histIns.getDurationInMillis() == 0 ? null : getDate(histIns.getDurationInMillis()));
                    // 获取意见评论内容
                    List<Comment> commentList = taskService.getProcessInstanceComments(histIns.getProcessInstanceId());
                    commentList.forEach(comment -> {
                        if (histIns.getTaskId().equals(comment.getTaskId())) {
                            flowTask.setComment(FlowCommentModel.builder().type(comment.getType()).comment(comment.getFullMessage()).build());
                        }
                    });
                    hisFlowList.add(flowTask);
                }
            }
            map.put("flowList", hisFlowList);
        }
        return ResponseResult.success(map);
    }

    /**
     * 获取流程变量
     * @param taskId
     * @return
     */
    public ResponseResult processVariables(String taskId) {
        // 流程变量
        HistoricTaskInstance historicTaskInstance =
            historyService.createHistoricTaskInstanceQuery().includeProcessVariables().finished().taskId(taskId).singleResult();
        if (Objects.nonNull(historicTaskInstance)) {
            return ResponseResult.success(historicTaskInstance.getProcessVariables());
        } else {
            Map<String, Object> variables = taskService.getVariables(taskId);
            return ResponseResult.success(variables);
        }
    }

    /**
     * 查询指定流程所有启动的实例列表
     * @param processDefinitionKey
     * @param pageSize
     * @param pageNum
     */
    public Map<String, Object> findProcessInstanceList(String processDefinitionKey, Integer pageSize, Integer pageNum) {
        Map<String, Object> resultMap = new HashMap<>();
        HistoricProcessInstanceQuery historicProcessInstanceQuery =
                historyService.createHistoricProcessInstanceQuery().processDefinitionKey(processDefinitionKey).orderByProcessInstanceStartTime().desc();
        List<HistoricProcessInstance> processInstanceList = historicProcessInstanceQuery.listPage(pageSize * (pageNum - 1), pageSize);
        resultMap.put("total", historicProcessInstanceQuery.count());
        List<FlowTaskDModel> flowList = new ArrayList<>();
        for (HistoricProcessInstance hisIns : processInstanceList) {
            FlowTaskDModel flowTask = new FlowTaskDModel();
            flowTask.setCreateTime(hisIns.getStartTime());
            flowTask.setFinishTime(hisIns.getEndTime());
            flowTask.setProcInsId(hisIns.getId());
            // 计算耗时
            if (Objects.nonNull(hisIns.getEndTime())) {
                long time = hisIns.getEndTime().getTime() - hisIns.getStartTime().getTime();
                flowTask.setDuration(getDate(time));
            } else {
                long time = System.currentTimeMillis() - hisIns.getStartTime().getTime();
                flowTask.setDuration(getDate(time));
            }
            // 流程定义信息
            ProcessDefinition pd = repositoryService.createProcessDefinitionQuery().processDefinitionId(hisIns.getProcessDefinitionId()).singleResult();
            flowTask.setDeployId(pd.getDeploymentId());
            flowTask.setProcDefName(pd.getName());
            flowTask.setProcDefVersion(pd.getVersion());
            flowTask.setCategory(pd.getCategory());
            flowTask.setProcDefVersion(pd.getVersion());
            // 当前所处流程 todo: 本地启动放开以下注释
            List<Task> taskList = taskService.createTaskQuery().processInstanceId(hisIns.getId()).list();
            if (CollectionUtils.isNotEmpty(taskList)) {
                flowTask.setTaskId(taskList.get(0).getId());
            } else {
                List<HistoricTaskInstance> historicTaskInstance =
                        historyService.createHistoricTaskInstanceQuery().processInstanceId(hisIns.getId()).orderByHistoricTaskInstanceEndTime().desc().list();
                flowTask.setTaskId(historicTaskInstance.get(0).getId());
            }
            flowList.add(flowTask);
        }
        resultMap.put("data", flowList);
        return resultMap;
    }

    /**
     * 生成流程图
     * @param httpServletResponse
     * @param processId
     * @throws Exception
     */
    public void getProcessDiagram(HttpServletResponse httpServletResponse, String processId) throws Exception {
        String processDefinitionId;
        // 获取当前的流程实例
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processId).singleResult();
        // 如果流程已经结束，则得到结束节点
        if (Objects.isNull(processInstance)) {
            HistoricProcessInstance pi = historyService.createHistoricProcessInstanceQuery().processInstanceId(processId).singleResult();

            processDefinitionId = pi.getProcessDefinitionId();
        } else {// 如果流程没有结束，则取当前活动节点
            // 根据流程实例ID获得当前处于活动状态的ActivityId合集
            ProcessInstance pi = runtimeService.createProcessInstanceQuery().processInstanceId(processId).singleResult();
            processDefinitionId = pi.getProcessDefinitionId();
        }
        // 获得活动的节点
        List<HistoricActivityInstance> highLightedFlowList =
                historyService.createHistoricActivityInstanceQuery().processInstanceId(processId).orderByHistoricActivityInstanceStartTime().asc().list();

        List<String> highLightedFlows = new ArrayList<>();
        List<String> highLightedNodes = new ArrayList<>();

        // 获取所有节点信息，暂不考虑子流程情况
        Process process = repositoryService.getBpmnModel(processDefinitionId).getProcesses().get(0);
        // 获取全部节点列表，包含子节点
        Collection<FlowElement> allElements = FlowableUtils.getAllElements(process.getFlowElements(), null);

        //历史节点数据清洗
//        List<String> lastHistoricTaskInstanceList = FlowableUtils.historicTaskInstanceClean(allElements, highLightedFlowList);
//        highLightedNodes.addAll(lastHistoricTaskInstanceList);
        // 高亮线
        for (HistoricActivityInstance tempActivity : highLightedFlowList) {
            if ("sequenceFlow".equals(tempActivity.getActivityType())) {
                // 高亮线
                highLightedFlows.add(tempActivity.getActivityId());
            } else {
                // 高亮节点
                highLightedNodes.add(tempActivity.getActivityId());
            }
        }
        // 获取流程图
        BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinitionId);
        ProcessEngineConfiguration configuration = processEngine.getProcessEngineConfiguration();
        // 获取自定义图片生成器
        ProcessDiagramGenerator diagramGenerator = new CustomProcessDiagramGenerator();
        InputStream in = diagramGenerator.generateDiagram(bpmnModel, "png", highLightedNodes, highLightedFlows, configuration.getActivityFontName(),
                configuration.getLabelFontName(), configuration.getAnnotationFontName(), configuration.getClassLoader(), 1.0, true);
        OutputStream out = null;
        byte[] buf = new byte[1024];
        int legth = 0;
        try {
            out = httpServletResponse.getOutputStream();
            while ((legth = in.read(buf)) != -1) {
                out.write(buf, 0, legth);
            }
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * 获取流程执行过程
     *
     * @param processInstanceId 流程实例id
     * @return
     */
    public ResponseResult getFlowViewer(String processInstanceId, String executionId) {
        List<FlowViewerModel> flowViewerList = new ArrayList<>();
        FlowViewerModel flowViewerDto;
        // 获取任务开始节点(临时处理方式)
        List<HistoricActivityInstance> startNodeList =
                historyService.createHistoricActivityInstanceQuery().processInstanceId(processInstanceId).orderByHistoricActivityInstanceStartTime().asc().listPage(0, 3);
        for (HistoricActivityInstance startInstance : startNodeList) {
            if (!"sequenceFlow".equals(startInstance.getActivityType())) {
                flowViewerDto = new FlowViewerModel();
                if (!"sequenceFlow".equals(startInstance.getActivityType())) {
                    flowViewerDto.setKey(startInstance.getActivityId());
                    // 根据流程节点处理时间校验改节点是否已完成
                    flowViewerDto.setCompleted(!Objects.isNull(startInstance.getEndTime()));
                    flowViewerList.add(flowViewerDto);
                }
            }
        }
        // 历史节点
        List<HistoricActivityInstance> hisActIns =
                historyService.createHistoricActivityInstanceQuery().executionId(executionId).orderByHistoricActivityInstanceStartTime().asc().list();
        for (HistoricActivityInstance activityInstance : hisActIns) {
            if (!"sequenceFlow".equals(activityInstance.getActivityType())) {
                flowViewerDto = new FlowViewerModel();
                flowViewerDto.setKey(activityInstance.getActivityId());
                // 根据流程节点处理时间校验改节点是否已完成
                flowViewerDto.setCompleted(!Objects.isNull(activityInstance.getEndTime()));
                flowViewerList.add(flowViewerDto);
            }
        }
        return ResponseResult.success(flowViewerList);
    }

    /**
     * 流程完成时间处理
     *
     * @param ms
     * @return
     */
    private String getDate(long ms) {
        long day = ms / (24 * 60 * 60 * 1000);
        long hour = (ms / (60 * 60 * 1000) - day * 24);
        long minute = ((ms / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long second = (ms / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - minute * 60);
        if (day > 0) {
            return day + "天" + hour + "小时" + minute + "分钟";
        }
        if (hour > 0) {
            return hour + "小时" + minute + "分钟";
        }
        if (minute > 0) {
            return minute + "分钟";
        }
        if (second > 0) {
            return second + "秒";
        } else {
            return 0 + "秒";
        }
    }
}
