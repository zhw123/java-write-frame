package com.lean.flowable.service;

import java.util.*;
import java.util.stream.Collectors;

import com.lean.flowable.base.FlowComment;
import com.lean.flowable.utils.FlowableUtils;
import com.lean.flowable.utils.ModelUtils;
import liquibase.pro.packaged.S;
import org.apache.commons.lang3.ObjectUtils;
import org.assertj.core.util.Lists;
import org.flowable.bpmn.model.*;
import org.flowable.bpmn.model.Process;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.impl.persistence.entity.ExecutionEntity;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.engine.task.Comment;
import org.flowable.task.api.Task;
import org.flowable.task.api.TaskQuery;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.flowable.task.api.history.HistoricTaskInstanceQuery;
import org.springframework.stereotype.Service;

import com.lean.flowable.base.ResponseResult;
import com.lean.flowable.entity.FlowTaskDModel;

import liquibase.repackaged.org.apache.commons.collections4.CollectionUtils;

@Service
public class FlowTaskService extends BaseService {

    /**
     * 为流程任务设置变量。 如果变量尚未存在，则将在任务中创建该变量。
     *
     * @param taskId 任务的id，不能为null.
     * @param variableName 变量键名.
     * @param variableValue 变量键值.
     * @throws
     */
    public void setVariableLocal(String taskId, String variableName, Object variableValue) {
        taskService.setVariableLocal(taskId, variableName, variableValue);
    }

    /**
     * 为流程任务设置多对变量。 如果变量尚未存在，则将在任务中创建该变量。
     *
     * @param taskId 任务的id，不能为null.
     * @param variables 多对变量键值对.
     * @throws
     */
    public void setVariablesLocal(String taskId, Map<String, ? extends Object> variables) {
        taskService.setVariablesLocal(taskId, variables);
    }

    /**
     * 申领任务。
     *
     * @param taskId 任务的id，不能为null.
     * @param userId 签收人标识.
     * @return
     * @throws Exception
     */
    public void claim(String taskId, String userId) {
        taskService.claim(taskId, userId);
    }

    /**
     * 任务释放
     *
     * @param taskId 任务的id，不能为null.
     * @return
     * @throws Exception
     */
    public void unclaim(String taskId) {
        taskService.unclaim(taskId);
    }

    /**
     * 执行任务，并设置任务变量。
     *
     * @param taskId 任务的id，不能为null.
     * @param variables 任务变量.
     * @param localScope 存储范围。如果为true，则提供的变量将存储在任务本地（当任务结束后，再也取不到这个值）， 而不是流程实例范围（默认是存放在流程实例中）。
     * @return
     * @throws
     */
    public void complete(String taskId, Map<String, Object> variables, boolean localScope) {
        Task finishTask = taskService.createTaskQuery().taskId(taskId).singleResult();
        if (variables != null) {
            taskService.complete(taskId, variables, localScope);
        } else {
            taskService.complete(taskId, null, localScope);
        }
    }

    /**
     * 任务移交：将任务的所有权转移给其他用户。
     *
     * @param taskId 任务的id，不能为null.
     * @param userId 接受所有权的人.
     */

    public void setAssignee(String taskId, String userId) {
        taskService.setAssignee(taskId, userId);
    }

    /**
     * 任务委派
     *
     * @param taskId 任务的id，不能为null.
     * @param userId 被委派人ID.
     */
    public void delegate(String taskId, String userId) {
        taskService.delegateTask(taskId, userId);
    }

    /**
     * 委派任务完成，归还委派人
     *
     * @param taskId 任务的id，不能为null.
     */
    public void resolveTask(String taskId) {
        taskService.resolveTask(taskId);
    }

    /**
     * 更改任务拥有者
     *
     * @param taskId 任务的id，不能为null.
     * @param userId 任务拥有者.
     */
    public void setOwner(String taskId, String userId) {
        taskService.setOwner(taskId, userId);
    }

    /**
     * 删除任务
     *
     * @param taskId 任务的id，不能为null.
     */
    public void delete(String taskId) {
        taskService.deleteTask(taskId);
    }

    /**
     * 删除任务，附带删除理由
     *
     * @param taskId 任务的id，不能为null.
     * @param reason 删除理由.
     */
    public void deleteWithReason(String taskId, String reason) {
        taskService.deleteTask(taskId, reason);
    }

    /**
     * 为任务添加任务处理人。
     *
     * @param taskId 任务的id，不能为null.
     * @param userId 任务处理人ID.
     * @throws Exception
     */
    public void addCandidateUser(String taskId, String userId) {
        taskService.addCandidateUser(taskId, userId);
    }

    /**
     * 为流程任务 和/或 流程实例添加注释。
     *
     * @param taskId 流程任务ID.
     * @param processInstanceId 流程实例ID.
     * @param message 注释信息
     * @return
     * @throws Exception
     */
    public Comment addComment(String taskId, String processInstanceId, String message) {
        return taskService.addComment(taskId, processInstanceId, message);
    }

    /**
     * 查询与任务相关的注释信息。
     *
     * @param taskId 流程任务ID.
     * @return
     * @throws Exception
     */
    public List<Comment> getTaskComments(String taskId) {
        return taskService.getTaskComments(taskId);
    }

    /**
     * 任务撤回
     *
     * @param processInstanceId 流程实例ID.
     * @param currentActivityId 当前活动任务ID.
     * @param newActivityId 撤回到达的任务ID.
     */
    void withdraw(String processInstanceId, String currentActivityId, String newActivityId) {
        runtimeService.createChangeActivityStateBuilder().processInstanceId(processInstanceId).moveActivityIdTo(currentActivityId, newActivityId).changeState();
    }


    /**
     * 根据候选组查询任务
     *
     * @param userGroup
     * @return
     */
    public ResponseResult findTaskListByUserGroup(String userGroup) {
        List<Task> taskList = taskService.createTaskQuery().taskCandidateGroup(userGroup).list();
        List<FlowTaskDModel> flowList = new ArrayList<>();
        for (Task task : taskList) {
            FlowTaskDModel flowTask = new FlowTaskDModel();
            // 当前流程信息
            flowTask.setTaskId(task.getId());
            flowTask.setTaskDefKey(task.getTaskDefinitionKey());
            flowTask.setCreateTime(task.getCreateTime());
            flowTask.setProcDefId(task.getProcessDefinitionId());
            flowTask.setExecutionId(task.getExecutionId());
            flowTask.setTaskName(task.getName());
            // 流程定义信息
            ProcessDefinition pd = repositoryService.createProcessDefinitionQuery().processDefinitionId(task.getProcessDefinitionId()).singleResult();
            flowTask.setDeployId(pd.getDeploymentId());
            flowTask.setProcDefName(pd.getName());
            flowTask.setProcDefVersion(pd.getVersion());
            flowTask.setProcInsId(task.getProcessInstanceId());
            // 流程发起人信息
            HistoricProcessInstance historicProcessInstance =
                historyService.createHistoricProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
            // TODO 查询流程发起人 基本信息
            flowTask.setStartUserId("1");
            flowTask.setStartUserName("校长");
            flowTask.setStartDeptName("部门");
            flowList.add(flowTask);
        }
        return ResponseResult.success(flowList);
    }

    /**
     * 根据候选人查询任务
     * @param userId
     * @return
     */
    public ResponseResult findTaskListByUser(String userId) {
        List<Task> taskList = taskService.createTaskQuery().taskCandidateUser(userId).list();
        List<FlowTaskDModel> flowList = new ArrayList<>();
        for (Task task : taskList) {
            FlowTaskDModel flowTask = new FlowTaskDModel();
            // 当前流程信息
            flowTask.setTaskId(task.getId());
            flowTask.setTaskDefKey(task.getTaskDefinitionKey());
            flowTask.setCreateTime(task.getCreateTime());
            flowTask.setProcDefId(task.getProcessDefinitionId());
            flowTask.setExecutionId(task.getExecutionId());
            flowTask.setTaskName(task.getName());
            // 流程定义信息
            ProcessDefinition pd = repositoryService.createProcessDefinitionQuery().processDefinitionId(task.getProcessDefinitionId()).singleResult();
            flowTask.setDeployId(pd.getDeploymentId());
            flowTask.setProcDefName(pd.getName());
            flowTask.setProcDefVersion(pd.getVersion());
            flowTask.setProcInsId(task.getProcessInstanceId());
            // 流程发起人信息
            HistoricProcessInstance historicProcessInstance =
                historyService.createHistoricProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
            // TODO 查询流程发起人 基本信息
            flowTask.setStartUserId("1");
            flowTask.setStartUserName("校长");
            flowTask.setStartDeptName("部门");
            flowList.add(flowTask);
        }
        return ResponseResult.success(flowList);
    }

    /**
     * 根据用户查询任务
     *
     * @param assignee
     * @return
     */
    public ResponseResult findTaskListByAssignee(String assignee) {
        List<Task> taskList = taskService.createTaskQuery().taskAssignee(assignee).list();
        List<FlowTaskDModel> flowList = new ArrayList<>();
        for (Task task : taskList) {
            FlowTaskDModel flowTask = new FlowTaskDModel();
            // 当前流程信息
            flowTask.setTaskId(task.getId());
            flowTask.setTaskDefKey(task.getTaskDefinitionKey());
            flowTask.setCreateTime(task.getCreateTime());
            flowTask.setProcDefId(task.getProcessDefinitionId());
            flowTask.setExecutionId(task.getExecutionId());
            flowTask.setTaskName(task.getName());
            // 流程定义信息
            ProcessDefinition pd = repositoryService.createProcessDefinitionQuery().processDefinitionId(task.getProcessDefinitionId()).singleResult();
            flowTask.setDeployId(pd.getDeploymentId());
            flowTask.setProcDefName(pd.getName());
            flowTask.setProcDefVersion(pd.getVersion());
            flowTask.setProcInsId(task.getProcessInstanceId());
            // 流程发起人信息
            HistoricProcessInstance historicProcessInstance =
                historyService.createHistoricProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
            // TODO 查询流程发起人 基本信息
            flowTask.setStartUserId("1");
            flowTask.setStartUserName("校长");
            flowTask.setStartDeptName("部门");
            flowList.add(flowTask);
        }
        return ResponseResult.success(flowList);
    }


    /**
     * 代办任务列表
     *
     * @param pageNum  当前页码
     * @param pageSize 每页条数
     * @return
     */
    public ResponseResult todoList(String userId,Integer pageNum, Integer pageSize) {
        TaskQuery taskQuery = taskService.createTaskQuery()
                .active()
                .includeProcessVariables()
                .taskAssignee(userId)
                .orderByTaskCreateTime().desc();
        List<Task> taskList = taskQuery.listPage(pageSize * (pageNum - 1), pageSize);
        List<FlowTaskDModel> flowList = new ArrayList<>();
        for (Task task : taskList) {
            FlowTaskDModel flowTask = new FlowTaskDModel();
            // 当前流程信息
            flowTask.setTaskId(task.getId());
            flowTask.setTaskDefKey(task.getTaskDefinitionKey());
            flowTask.setCreateTime(task.getCreateTime());
            flowTask.setProcDefId(task.getProcessDefinitionId());
            flowTask.setExecutionId(task.getExecutionId());
            flowTask.setTaskName(task.getName());
            // 流程定义信息
            ProcessDefinition pd = repositoryService.createProcessDefinitionQuery()
                    .processDefinitionId(task.getProcessDefinitionId())
                    .singleResult();
            flowTask.setDeployId(pd.getDeploymentId());
            flowTask.setProcDefName(pd.getName());
            flowTask.setProcDefVersion(pd.getVersion());
            flowTask.setProcInsId(task.getProcessInstanceId());
            // 流程发起人信息
            HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                    .processInstanceId(task.getProcessInstanceId())
                    .singleResult();
            String startUserId = historicProcessInstance.getStartUserId();
            // TODO 查询流程发起人 基本信息
            flowTask.setStartUserId("1");
            flowTask.setStartUserName("校长");
            flowTask.setStartDeptName("部门");
            flowList.add(flowTask);
        }
        return ResponseResult.success(flowList);
    }


    /**
     * 已办任务列表
     *
     * @param pageNum  当前页码
     * @param pageSize 每页条数
     * @return
     */
    public ResponseResult finishedList(String userId,Integer pageNum, Integer pageSize) {
        HistoricTaskInstanceQuery taskInstanceQuery = historyService.createHistoricTaskInstanceQuery()
                .includeProcessVariables()
                .finished()
                .taskAssignee(userId)
                .orderByHistoricTaskInstanceEndTime()
                .desc();
        List<HistoricTaskInstance> historicTaskInstanceList = taskInstanceQuery.listPage(pageSize * (pageNum - 1), pageSize);
        List<FlowTaskDModel> hisTaskList = Lists.newArrayList();
        for (HistoricTaskInstance histTask : historicTaskInstanceList) {
            FlowTaskDModel flowTask = new FlowTaskDModel();
            // 当前流程信息
            flowTask.setTaskId(histTask.getId());
            // 审批人员信息
            flowTask.setCreateTime(histTask.getCreateTime());
            flowTask.setFinishTime(histTask.getEndTime());
            flowTask.setDuration(getDate(histTask.getDurationInMillis()));
            flowTask.setProcDefId(histTask.getProcessDefinitionId());
            flowTask.setTaskDefKey(histTask.getTaskDefinitionKey());
            flowTask.setTaskName(histTask.getName());

            // 流程定义信息
            ProcessDefinition pd = repositoryService.createProcessDefinitionQuery()
                    .processDefinitionId(histTask.getProcessDefinitionId())
                    .singleResult();
            flowTask.setDeployId(pd.getDeploymentId());
            flowTask.setProcDefName(pd.getName());
            flowTask.setProcDefVersion(pd.getVersion());
            flowTask.setProcInsId(histTask.getProcessInstanceId());
            flowTask.setHisProcInsId(histTask.getProcessInstanceId());

            // 流程发起人信息
            HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                    .processInstanceId(histTask.getProcessInstanceId())
                    .singleResult();
            String startUserId = historicProcessInstance.getStartUserId();
            // TODO 查询流程发起人 基本信息
            flowTask.setStartUserId("1");
            flowTask.setStartUserName("校长");
            flowTask.setStartDeptName("部门");
            hisTaskList.add(flowTask);
        }
        return ResponseResult.success(hisTaskList);
    }

    /**
     * 根据流程实例id 获取 正处于运行状态的任务
     *
     * @param processInstanceId
     * @return
     */
    public ResponseResult getActiveTask(String processInstanceId) {
        List<FlowTaskDModel> flowTaskDModels = new ArrayList<>();
        // select distinct RES.* from ACT_RU_TASK RES WHERE RES.PROC_INST_ID_ = ? and RES.SUSPENSION_STATE_ = 1 order by
        // RES.ID_ asc
        List<Task> taskList = taskService.createTaskQuery().processInstanceId(processInstanceId).active().list();
        for (Task task : taskList) {
            FlowTaskDModel flowTask = new FlowTaskDModel();
            // 当前流程信息
            flowTask.setTaskId(task.getId());
            flowTask.setTaskDefKey(task.getTaskDefinitionKey());
            flowTask.setCreateTime(task.getCreateTime());
            flowTask.setProcDefId(task.getProcessDefinitionId());
            flowTask.setExecutionId(task.getExecutionId());
            flowTask.setTaskName(task.getName());
            // 流程定义信息
            ProcessDefinition pd = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId(task.getProcessDefinitionId()).singleResult();
            flowTask.setDeployId(pd.getDeploymentId());
            flowTask.setProcDefName(pd.getName());
            flowTask.setProcDefVersion(pd.getVersion());
            flowTask.setProcInsId(task.getProcessInstanceId());
            // 流程发起人信息
            HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                .processInstanceId(task.getProcessInstanceId()).singleResult();
            // TODO 查询流程发起人 基本信息
            flowTask.setStartUserId("1");
            flowTask.setStartUserName("校长");
            flowTask.setStartDeptName("部门");
            flowTaskDModels.add(flowTask);
        }
        return ResponseResult.success(flowTaskDModels);
    }

    /**
     * 根据流程Id获取所有taskId任务Id
     *
     * @param processInstanceId
     * @return
     */
    public ResponseResult getTaskList(String processInstanceId) {
        List<FlowTaskDModel> flowTaskDModels = new ArrayList<>();
        // select distinct RES.* from ACT_RU_TASK RES WHERE RES.PROC_INST_ID_ = ? and RES.SUSPENSION_STATE_ = 1 order by RES.ID_ asc
        List<Task> taskList =
            taskService.createTaskQuery().processInstanceId(processInstanceId).orderByTaskCreateTime().desc().list();
        for (Task task : taskList) {
            FlowTaskDModel flowTask = new FlowTaskDModel();
            // 当前流程信息
            flowTask.setTaskId(task.getId());
            flowTask.setTaskDefKey(task.getTaskDefinitionKey());
            flowTask.setCreateTime(task.getCreateTime());
            flowTask.setProcDefId(task.getProcessDefinitionId());
            flowTask.setExecutionId(task.getExecutionId());
            flowTask.setTaskName(task.getName());
            // 流程定义信息
            ProcessDefinition pd = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId(task.getProcessDefinitionId()).singleResult();
            flowTask.setDeployId(pd.getDeploymentId());
            flowTask.setProcDefName(pd.getName());
            flowTask.setProcDefVersion(pd.getVersion());
            flowTask.setProcInsId(task.getProcessInstanceId());
            // 流程发起人信息
            HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                .processInstanceId(task.getProcessInstanceId()).singleResult();
            // TODO 查询流程发起人 基本信息
            flowTask.setStartUserId("1");
            flowTask.setStartUserName("校长");
            flowTask.setStartDeptName("部门");
            flowTaskDModels.add(flowTask);
        }
        return ResponseResult.success(flowTaskDModels);
    }

    /**
     * 根据任务Id获取下一个task任务
     *
     * @param taskId 任务Id
     * @return
     */
    public FlowTaskDModel getCurrentTaskDetail(String taskId) {
        // 获取当前任务信息
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if (task != null) {
            FlowTaskDModel flowTask = new FlowTaskDModel();
            // 当前流程信息
            flowTask.setTaskId(task.getId());
            flowTask.setTaskDefKey(task.getTaskDefinitionKey());
            flowTask.setCreateTime(task.getCreateTime());
            flowTask.setProcDefId(task.getProcessDefinitionId());
            flowTask.setExecutionId(task.getExecutionId());
            flowTask.setTaskName(task.getName());
            flowTask.setProcInsId(task.getProcessInstanceId());
            flowTask.setAssigneeName(task.getAssignee());
            return flowTask;
        }
        return null;
    }


    /**
     *  获取所有可回退的节点
     * @param taskId
     * @return
     */
    public List<UserTask> findReturnTaskList(String taskId) {
        // 当前任务 task
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        // 获取流程定义信息
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionId(task.getProcessDefinitionId()).singleResult();
        // 获取所有节点信息，暂不考虑子流程情况
        Process process = repositoryService.getBpmnModel(processDefinition.getId()).getProcesses().get(0);
        Collection<FlowElement> flowElements = process.getFlowElements();
        // 获取当前任务节点元素
        UserTask source = null;
        if (flowElements != null) {
            for (FlowElement flowElement : flowElements) {
                // 类型为用户节点
                if (flowElement.getId().equals(task.getTaskDefinitionKey())) {
                    source = (UserTask) flowElement;
                }
            }
        }
        // 获取节点的所有路线
        List<List<UserTask>> roads = FlowableUtils.findRoad(source, null, null, null);
        // 可回退的节点列表
        List<UserTask> userTaskList = new ArrayList<>();
        for (List<UserTask> road : roads) {
            if (userTaskList.size() == 0) {
                // 还没有可回退节点直接添加
                userTaskList = road;
            } else {
                // 如果已有回退节点，则比对取交集部分
                userTaskList.retainAll(road);
            }
        }
        return userTaskList;
    }


    /**
     *
     * @param taskId 当前节点
     * @return
     */
    public FlowTaskDModel getBeforeTaskId(String taskId) {
        FlowTaskDModel flowTask = new FlowTaskDModel();
        // 获取当前节点任务
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        // 获取历史任务节点
        List<HistoricTaskInstance> historicTaskInstanceList = historyService.createHistoricTaskInstanceQuery()
            .processInstanceId(task.getProcessInstanceId()).orderByHistoricTaskInstanceStartTime().asc().list();
        if (CollectionUtils.isNotEmpty(historicTaskInstanceList)) {
            List<String> list = historicTaskInstanceList.stream()
                .map(historicTaskInstance -> historicTaskInstance.getId()).collect(Collectors.toList());
            if(list.contains(taskId)){
                int indexOf = list.indexOf(taskId);
                if (indexOf == 0) {
                    return null;
                }
                HistoricTaskInstance his = historicTaskInstanceList.get(indexOf - 1);
                // 当前流程信息
                flowTask.setTaskId(his.getId());
                // 审批人员信息
                flowTask.setCreateTime(his.getCreateTime());
                flowTask.setFinishTime(his.getEndTime());
                flowTask.setDuration(getDate(his.getDurationInMillis()));
                flowTask.setProcDefId(his.getProcessDefinitionId());
                flowTask.setTaskDefKey(his.getTaskDefinitionKey());
                flowTask.setTaskName(his.getName());

                // 流程定义信息
                ProcessDefinition pd = repositoryService.createProcessDefinitionQuery()
                        .processDefinitionId(his.getProcessDefinitionId()).singleResult();
                flowTask.setDeployId(pd.getDeploymentId());
                flowTask.setProcDefName(pd.getName());
                flowTask.setProcDefVersion(pd.getVersion());
                flowTask.setProcInsId(his.getProcessInstanceId());
                flowTask.setHisProcInsId(his.getProcessInstanceId());

                // 流程发起人信息
                HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                        .processInstanceId(his.getProcessInstanceId()).singleResult();
                String startUserId = historicProcessInstance.getStartUserId();
                flowTask.setStartUserId(startUserId);
            }

        }
        return flowTask;
    }

    /**
     * 流程完成时间处理
     *
     * @param ms
     * @return
     */
    private String getDate(long ms) {

        long day = ms / (24 * 60 * 60 * 1000);
        long hour = (ms / (60 * 60 * 1000) - day * 24);
        long minute = ((ms / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long second = (ms / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - minute * 60);

        if (day > 0) {
            return day + "天" + hour + "小时" + minute + "分钟";
        }
        if (hour > 0) {
            return hour + "小时" + minute + "分钟";
        }
        if (minute > 0) {
            return minute + "分钟";
        }
        if (second > 0) {
            return second + "秒";
        } else {
            return 0 + "秒";
        }
    }

    /**
     * 回退或指向指定流程
     * @param proInstanceId 需要驳回的流程实例id(当前发起节点的流程实例id)
     * @param targetKey   目标节点的key  为act_hi_taskinst 中 TASK_DEF_KEY_
     * @param currTaskKeys 驳回发起的当前节点key 为  act_ru_task 中TASK_DEF_KEY_ 字段的值
     */
    public void changeActivityState(String proInstanceId, String targetKey, List<String> currTaskKeys) {
        runtimeService.createChangeActivityStateBuilder().processInstanceId(proInstanceId)
            .moveActivityIdsToSingleActivityId(currTaskKeys, targetKey).changeState();
    }

    /**
     * 终止任务
     * @param taskId
     * @param comment
     * @return
     */
    public ResponseResult stopTask(String taskId,String comment){
        // 当前任务 task
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if (ObjectUtils.isEmpty(task)) {
            throw new RuntimeException("获取任务信息异常！");
        }
        if (task.isSuspended()) {
            throw new RuntimeException("任务处于挂起状态");
        }
        // 获取流程实例
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(task.getProcessInstanceId())
                .singleResult();
        if (processInstance == null) {
            throw new RuntimeException("流程实例不存在，请确认！");
        }
        // 获取流程定义信息
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId(task.getProcessDefinitionId())
                .singleResult();

        // 添加审批意见
        taskService.addComment(taskId, task.getProcessInstanceId(), FlowComment.REJECT.getType(), comment);
        // 获取所有节点信息
        BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinition.getId());
        EndEvent endEvent = ModelUtils.getEndEvent(bpmnModel);
        // 终止流程
        List<Execution> executions = runtimeService.createExecutionQuery().parentId(task.getProcessInstanceId()).list();
        List<String> executionIds = executions.stream().map(Execution::getId).collect(Collectors.toList());
        runtimeService.createChangeActivityStateBuilder()
                .processInstanceId(task.getProcessInstanceId())
                .moveExecutionsToSingleActivityId(executionIds, endEvent.getId())
                .changeState();
        return ResponseResult.success("ok");
    }

}
