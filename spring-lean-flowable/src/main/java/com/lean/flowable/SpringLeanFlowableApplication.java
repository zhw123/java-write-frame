package com.lean.flowable;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * https://www.cnblogs.com/xfeiyun/p/16185713.html
 */
@MapperScan("com.lean.flowable.mapper") // 扫描的mapper
@SpringBootApplication()
public class SpringLeanFlowableApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanFlowableApplication.class, args);
    }

}
