package com.lean.flowable.base;

import java.io.Serializable;
import lombok.Data;

/**
 * 返回结果集
 */
@Data
public class ResponseResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private boolean success = true;

    private String statusCode = "200";

    private String msg;

    private T data;

    private ResponseResult() {}

    /**
     * 请求成功返回
     */
    public static <T> ResponseResult<T> success() {
        return ResponseResult.success("200",null);
    }

    /**
     * 请求成功返回
     */
    public static <T> ResponseResult<T> success(T t) {
        return ResponseResult.success("200", t);
    }

    /**
     * 请求成功返回
     */
    public static <T> ResponseResult<T> success(String code, T t) {
        return gettResultDataSet(true, code, "success", t);
    }

    /**
     * 请求失败返回
     */
    public static <T> ResponseResult<T> fail(String msg) {
        return ResponseResult.fail("400", msg);
    }

    /**
     * 请求失败返回
     */
    public static <T> ResponseResult<T> fail(String code, String msg) {
        return gettResultDataSet(false, code, msg, null);
    }

    private static <T> ResponseResult<T> gettResultDataSet(boolean state, String code, String msg, T t) {
        ResponseResult<T> rs = new ResponseResult<>();
        rs.setSuccess(state);
        rs.setStatusCode(code);
        rs.setMsg(msg);
        rs.setData(t);
        return rs;
    }
}
