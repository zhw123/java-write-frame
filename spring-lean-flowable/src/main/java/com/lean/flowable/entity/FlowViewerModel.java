package com.lean.flowable.entity;

import java.io.Serializable;

import lombok.Data;


@Data
public class FlowViewerModel implements Serializable {

    /**
     * 流程key
     */
    private String key;

    /**
     * 是否完成(已经审批)
     */
    private boolean completed;
}
