package com.lean.flowable.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class FlowGroupModel implements Serializable {

    private String id;

    private String name;

    private String type;
}
