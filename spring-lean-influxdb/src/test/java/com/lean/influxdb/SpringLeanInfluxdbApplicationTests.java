package com.lean.influxdb;

import com.lean.influxdb.entity.LogInfo;
import com.lean.influxdb.service.InfluxDBService;
import org.influxdb.dto.Point;
import org.influxdb.dto.QueryResult;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class SpringLeanInfluxdbApplicationTests {

    @Test
    void contextLoads() {
    }


    @Autowired
    private InfluxDBService influxDBService;

    @Test
    public void test() {
        LogInfo logInfo = LogInfo.builder()
                .level("1")
                .module("log")
                .deviceId("1")
                .msg("消息")
                .createTime(new Date())
                .build();
        Point point = Point.measurementByPOJO(logInfo.getClass())
                .addFieldsFromPOJO(logInfo)
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .build();
        influxDBService.insertPoint(point);
    }

    @Test
    public void test1() {
        Integer pageSize = 15;
        Integer pageNum = 1;
        // InfluxDB支持分页查询,因此可以设置分页查询条件
        String pageQuery = " LIMIT " + pageSize + " OFFSET " + ((pageNum - 1) * pageSize);
        // 此处查询所有内容,如果
        String queryCmd = "SELECT * FROM student ORDER BY time DESC " + pageQuery;
        QueryResult queryResult = influxDBService.query(queryCmd);
        List<Map<String, Object>> maps = influxDBService.queryResultProcess(queryResult);
        maps.stream().forEach(s->{
            s.forEach((k,v)->{
                System.out.printf("k",k);
                System.out.printf("v",v);
            });
        });
        List<QueryResult.Result> resultList = queryResult.getResults();
        System.out.println(resultList);

    }

    @Test
    public void testSave() {
        String measurement = "student";

        Map<String, String> tags = new HashMap<>();
        tags.put("code", "1");
        tags.put("studentNums", "1");

        Map<String, Object> fields = new HashMap<>();
        fields.put("name", "张三");
        fields.put("age", "11");
        influxDBService.insert(measurement, tags, fields);
    }

    @Test
    public void testGetdata() {
        String command = "select * from student";

        QueryResult queryResult = influxDBService.query(command);
        List<Map<String, Object>> result = influxDBService.queryResultProcess(queryResult);
        for (Map map : result) {
            System.out.println("time:" + map.get("time")
                    + " code:" + map.get("code")
                    + " studentNums:" + map.get("studentNums")
                    + " name:" + map.get("name")
                    + " age:" + map.get("age"));
        }
    }

}
