package com.lean.event.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class SpringLeanEventApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanEventApplication.class, args);
    }

}
