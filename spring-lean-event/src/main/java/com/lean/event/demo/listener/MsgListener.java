package com.lean.event.demo.listener;

import com.lean.event.demo.events.MsgEvent;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
@Slf4j
@Component
public class MsgListener {

    //异步注解 需要开启@EnableAsync异步
    @Async(value = "taskAsyncPool")
    @SneakyThrows
    @EventListener(MsgEvent.class)
    public void sendMsg(MsgEvent event) {
        String messageId = event.getMessageId();
        long start = System.currentTimeMillis();
        log.info("开发发送短信");
        log.info("开发发送邮件");
        long end = System.currentTimeMillis();
        log.info("{}：发送短信、邮件耗时：({})毫秒", messageId, (end - start));
    }
}
