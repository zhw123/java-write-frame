package com.lean.event.demo.service;

import com.lean.event.demo.events.MsgEvent;
import com.lean.event.demo.events.MessageEvent;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
@Slf4j
@Service
@Data
public class MessageService {
    /** 注入ApplicationContext用来发布事件 */
    private final ApplicationContext applicationContext;
    public String testMessage(String messageId) {
        long start = System.currentTimeMillis();
        // （同步处理）
        applicationContext.publishEvent(new MessageEvent(this, messageId));
        // （异步处理）
        applicationContext.publishEvent(new MsgEvent(messageId));
        long end = System.currentTimeMillis();
        log.info("任务全部完成，总耗时：({})毫秒", end - start);
        return "购买成功";
    }
}

