package com.lean.mvc.tx.transaction.interceptor;


import com.lean.mvc.tx.transaction.TransactionDefinition;

/**
 * @author zhangdd on 2022/2/26
 */
public interface TransactionAttribute extends TransactionDefinition {

    boolean rollbackOn(Throwable ex);
}
