package com.lean.mvc.jdbc.core;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author zhangdd on 2022/2/12
 */
public interface PreparedStatementSetter {

    void setValues(PreparedStatement ps) throws SQLException;
}
