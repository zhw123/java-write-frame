package com.lean.mvc.beans.factory;

import com.lean.mvc.beans.BeansException;
import com.lean.mvc.beans.factory.config.AutowireCapableBeanFactory;
import com.lean.mvc.beans.factory.config.BeanDefinition;
import com.lean.mvc.beans.factory.config.BeanPostProcessor;
import com.lean.mvc.beans.factory.config.ConfigurableBeanFactory;

/**
 * Configuration interface to be implemented by most listable bean factories.
 * In addition to {@link ConfigurableBeanFactory}, it provides facilities to
 * analyze and modify bean definitions, and to pre-instantiate singletons.
 *
 * 
 * 
 * 
 *
 * 
 *
 */
public interface ConfigurableListableBeanFactory extends ListableBeanFactory, AutowireCapableBeanFactory, ConfigurableBeanFactory {

    BeanDefinition getBeanDefinition(String beanName) throws BeansException;

    void preInstantiateSingletons() throws BeansException;

}
