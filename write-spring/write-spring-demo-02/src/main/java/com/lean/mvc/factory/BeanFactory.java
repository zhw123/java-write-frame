package com.lean.mvc.factory;

import com.lean.mvc.BeansException;


public interface BeanFactory {

    Object getBean(String name) throws BeansException;

}
