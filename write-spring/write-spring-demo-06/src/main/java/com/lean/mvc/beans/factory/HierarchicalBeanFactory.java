package com.lean.mvc.beans.factory;

/**
 * Sub-interface implemented by bean factories that can be part
 * of a hierarchy.
 */
public interface HierarchicalBeanFactory extends BeanFactory {
}
