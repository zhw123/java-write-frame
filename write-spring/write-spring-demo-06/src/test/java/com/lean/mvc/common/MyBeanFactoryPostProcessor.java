package com.lean.mvc.common;

import com.lean.mvc.beans.BeansException;
import com.lean.mvc.beans.PropertyValue;
import com.lean.mvc.beans.PropertyValues;
import com.lean.mvc.beans.factory.ConfigurableListableBeanFactory;
import com.lean.mvc.beans.factory.config.BeanDefinition;
import com.lean.mvc.beans.factory.config.BeanFactoryPostProcessor;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("userService");
        PropertyValues propertyValues = beanDefinition.getPropertyValues();

        propertyValues.addPropertyValue(new PropertyValue("company", "改为：字节跳动"));
    }

}
