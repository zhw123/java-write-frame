package com.lean.mvc.factory.support;

import com.lean.mvc.BeansException;
import com.lean.mvc.factory.config.BeanDefinition;

import java.lang.reflect.Constructor;

/**
 *
 * <p>
 * Bean 实例化策略
 */
public interface InstantiationStrategy {

    Object instantiate(BeanDefinition beanDefinition, String beanName, Constructor ctor, Object[] args) throws BeansException;

}
