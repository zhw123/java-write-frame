package com.lean.execl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringExeclApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringExeclApplication.class, args);
    }

}
