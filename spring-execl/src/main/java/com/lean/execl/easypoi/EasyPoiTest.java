package com.lean.execl.easypoi;

import cn.afterturn.easypoi.cache.manager.POICacheManager;
import cn.afterturn.easypoi.entity.ImageEntity;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.ExcelXorHtmlUtil;
import cn.afterturn.easypoi.excel.entity.ExcelToHtmlParams;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.afterturn.easypoi.word.WordExportUtil;
import cn.afterturn.easypoi.word.entity.WordImageEntity;
import com.lean.execl.model.MsgClient;
import com.lean.execl.model.MsgClientGroup;
import com.lean.execl.model.Student;
import com.lean.execl.model.Teacher;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * EasyPoi 是对poi封装的一个工具库，封装好了一些常见的Excel操作
 * 最基本的导入导出
 * 图片的导入导出
 * 多表数据的导入导出
 * 大批量数据的导入导出
 * 模板的导出
 *官网地址：http://easypoi.mydoc.io/#category_50222
 */
public class EasyPoiTest {

    public static void main(String[] args) {
        imageWordExport("D:/excel/image.docx");
    }

    public void test() throws Exception{
        //        模拟数据
        List<Teacher> list = new ArrayList<>();
        list.add(new Teacher(1,"李老师","hhh.jpg",1,null));
        list.add(new Teacher(2,"李老师","hhh.jpg",1,null));
        list.add(new Teacher(3,"李老师","hhh.jpg",1,null));
        list.add(new Teacher(4,"李老师","hhh.jpg",1,null));
        list.add(new Teacher(5,"李老师","hhh.jpg",1,null));
        list.add(new Teacher(6,"李老师","hhh.jpg",1,null));
        /**
         * 导出参数对象
         * 参数1 标题
         * 参数2 表的名字
         */
        ExportParams exportParams = new ExportParams("所有老师数据","teacher");
        /**
         * exportExcel 导出Excel文件
         * 参数1 导出参数对象
         * 参数2 要导出的实体类的类对象
         * 参数3 要导出的数据 需要一个集合  数据库查询出来的老师对象的集合
         *
         * 返回值就是封装好的文件对象
         */
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, Teacher.class, list);

        workbook.write(new FileOutputStream("/Users/lubingyang/Desktop/teachers.xls"));
    }

    public void test4() throws IOException {
        List<Student> students = new ArrayList<>();
        students.add(new Student("hh",1));
        students.add(new Student("hh",2));
//        模拟数据
        List<Teacher> list = new ArrayList<>();
        list.add(new Teacher(1,"李老师","/Users/lubingyang/Desktop/hhh.jpg",1,students));
        list.add(new Teacher(6,"李老师","/Users/lubingyang/Desktop/hhh.jpg",1,students));
        /**
         * 导出参数对象
         * 参数1 标题
         * 参数2 表的名字
         */
        ExportParams exportParams = new ExportParams("所有老师数据","teacher");
        /**
         * exportExcel 导出Excel文件
         * 参数1 导出参数对象
         * 参数2 要导出的实体类的类对象
         * 参数3 要导出的数据 需要一个集合  数据库查询出来的老师对象的集合
         *
         * 返回值就是封装好的文件对象
         */
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, Teacher.class, list);

        workbook.write(new FileOutputStream("/Users/lubingyang/Desktop/teachers.xls"));

    }

    public void test5() throws Exception {
        FileInputStream inputStream = new FileInputStream("/Users/lubingyang/Desktop/teachers.xls");
        /**
         * ImportParams 导入参数对象
         * 定义标题栏和表头数据
         */
        ImportParams importParams = new ImportParams();
        importParams.setTitleRows(1);
        importParams.setHeadRows(1);
        /**
         * importExcel 导入方法
         * 参数1 流 读取要导入的文件
         * 参数2 要导入的实体类的类对象  上师对象的类对象
         * 参数3 导入参数对象
         *
         * 返回值 导入数据 直接封装为集合对象
         */
        List<Teacher> teachers = ExcelImportUtil.importExcel(inputStream, Teacher.class, importParams);

        for (Teacher teacher : teachers) {
            System.out.println(teacher);
        }
    }

    /**
     * Excel大数据导出
     * 推荐使用 阿里开源的 EasyExcel 官方介绍可以将内存控制在kb
     * @throws Exception
     */
    public void bigDataExport() throws Exception {

        List<MsgClient> list = new ArrayList<MsgClient>();
        Workbook workbook = null;
        Date start = new Date();
        ExportParams params = new ExportParams("大数据测试", "测试");
        for (int i = 0; i < 1000000; i++) {  //一百万数据量
            MsgClient client = new MsgClient();
            client.setBirthday(new Date());
            client.setClientName("小明" + i);
            client.setClientPhone("18797" + i);
            client.setCreateBy("JueYue");
            client.setId("1" + i);
            client.setRemark("测试" + i);
            MsgClientGroup group = new MsgClientGroup();
            group.setGroupName("测试" + i);
            client.setGroup(group);
            list.add(client);
            if(list.size() == 10000){
                workbook = ExcelExportUtil.exportBigExcel(params, MsgClient.class, list);
                list.clear();
            }
        }
        ExcelExportUtil.closeExportBigExcel();
        System.out.println(new Date().getTime() - start.getTime());
        File savefile = new File("D:/excel/");
        if (!savefile.exists()) {
            savefile.mkdirs();
        }
        FileOutputStream fos = new FileOutputStream("D:/excel/ExcelExportBigData.bigDataExport.xlsx");
        workbook.write(fos);
        fos.close();
    }

//    public void test10() throws IOException {
//        Date start = new Date();
////        查询数据库 用户表总条数
//        Integer userCount = userDao.selectCount(null);
////        计算总页数
//        Integer pageCount = userCount / 200000 + 1;
//
//        List<CmfzUser> users = null;
//        Workbook workbook = null;
//        ExportParams params = new ExportParams("大数据测试", "测试");
//
////        查询测试 页数  每次查询20w条数据
//        for (int i = 1; i <= pageCount; i++) {
//            System.out.println(i);
//            users = userDao.selectPage(new Page<>(i, 200000), null).getRecords();
////            通过 EasyPoi 的大数据导出方法 导出
//            workbook = ExcelExportUtil.exportBigExcel(params, CmfzUser.class, users);
//            users.clear();
//        }
//        Date end = new Date();
//        System.out.println(new Date().getTime() - start.getTime());
//        workbook.write(new FileOutputStream("/Users/lubingyang/Desktop/hhhh.xlsx"));
//
//    }

    /**
     * Excel 的Html预览 07 版本EXCEL预览
     */
    public void toHtmlOf07Base(HttpServletResponse response) throws Exception {
        ExcelToHtmlParams params = new ExcelToHtmlParams(WorkbookFactory.create(POICacheManager.getFile("exceltohtml/testExportTitleExcel.xlsx")));
        response.getOutputStream().write(ExcelXorHtmlUtil.excelToHtml(params).getBytes());
    }
    /**
     * Excel 的Html预览 03 版本EXCEL预览
     */
    public void toHtmlOf03Img(HttpServletResponse response) throws Exception {
        ExcelToHtmlParams params = new ExcelToHtmlParams(WorkbookFactory.create(POICacheManager.getFile("exceltohtml/exporttemp_img.xls")),true,"yes");
        response.getOutputStream().write(ExcelXorHtmlUtil.excelToHtml(params).getBytes());
    }

    /**
     * html转Excel更神奇的导出
     * @throws Exception
     */
    public void htmlToExcelByStr() throws Exception {
        StringBuilder html = new StringBuilder();
        Scanner s = new Scanner(getClass().getResourceAsStream("/html/sample.html"), "utf-8");
        while (s.hasNext()) {
            html.append(s.nextLine());
        }
        s.close();
        Workbook workbook = ExcelXorHtmlUtil.htmlToExcel(html.toString(), ExcelType.XSSF);
        File savefile = new File("D:\\home\\lemur");
        if (!savefile.exists()) {
            savefile.mkdirs();
        }
        FileOutputStream fos = new FileOutputStream("D:\\home\\lemur\\htmlToExcelByStr.xlsx");
        workbook.write(fos);
        fos.close();
        workbook = ExcelXorHtmlUtil.htmlToExcel(html.toString(), ExcelType.HSSF);
        fos = new FileOutputStream("D:\\home\\lemur\\htmlToExcelByStr.xls");
        workbook.write(fos);
        fos.close();
    }

    /**
     * html转Excel更神奇的导出
     * @throws Exception
     */
    public void htmlToExcelByIs() throws Exception {
        Workbook workbook = ExcelXorHtmlUtil.htmlToExcel(getClass().getResourceAsStream("/html/sample.html"), ExcelType.XSSF);
        File savefile = new File("D:\\home\\lemur");
        if (!savefile.exists()) {
            savefile.mkdirs();
        }
        FileOutputStream fos = new FileOutputStream("D:\\home\\lemur\\htmlToExcelByIs.xlsx");
        workbook.write(fos);
        fos.close();
        workbook = ExcelXorHtmlUtil.htmlToExcel(getClass().getResourceAsStream("/html/sample.html"), ExcelType.HSSF);
        fos = new FileOutputStream("D:\\home\\lemur\\htmlToExcelByIs.xls");
        workbook.write(fos);
        fos.close();
    }

    /**
     * Excel 模板---基本导出
     * 空格分割
     * 三目运算 {{test ? obj:obj2}}
     * n: 表示 这个cell是数值类型 {{n:}}
     * le: 代表长度{{le:()}} 在if/else 运用{{le:() > 8 ? obj1 : obj2}}
     * fd: 格式化时间 {{fd:(obj;yyyy-MM-dd)}}
     * fn: 格式化数字 {{fn:(obj;###.00)}}
     * fe: 遍历数据,创建row
     * !fe: 遍历数据不创建row
     * $fe: 下移插入,把当前行,下面的行全部下移.size()行,然后插入
     * #fe: 横向遍历
     * v_fe: 横向遍历值
     * !if: 删除当前列 {{!if:(test)}}
     * 单引号表示常量值 '' 比如'1' 那么输出的就是 1
     * &NULL& 空格
     * ]] 换行符 多行遍历导出
     * sum： 统计数据
     *
     * fe的写法 fe标志 冒号 list数据 单个元素数据（默认t，可以不写） 第一个元素
     * {{$fe: maplist t t.id }}
     *
     * @throws Exception
     */
    public void execlTemplateToExport() throws Exception {
        TemplateExportParams params = new TemplateExportParams(
                "WEB-INF/doc/专项支出用款申请书_map.xls");
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("date", "2014-12-25");
        map.put("money", 2000000.00);
        map.put("upperMoney", "贰佰万");
        map.put("company", "执笔潜行科技有限公司");
        map.put("bureau", "财政局");
        map.put("person", "JueYue");
        map.put("phone", "1879740****");
        List<Map<String, String>> listMap = new ArrayList<Map<String, String>>();
        for (int i = 0; i < 4; i++) {
            Map<String, String> lm = new HashMap<String, String>();
            lm.put("id", i + 1 + "");
            lm.put("zijin", i * 10000 + "");
            lm.put("bianma", "A001");
            lm.put("mingcheng", "设计");
            lm.put("xiangmumingcheng", "EasyPoi " + i + "期");
            lm.put("quancheng", "开源项目");
            lm.put("sqje", i * 10000 + "");
            lm.put("hdje", i * 10000 + "");

            listMap.add(lm);
        }
        map.put("maplist", listMap);

        Workbook workbook = ExcelExportUtil.exportExcel(params, map);
        File savefile = new File("D:/excel/");
        if (!savefile.exists()) {
            savefile.mkdirs();
        }
        FileOutputStream fos = new FileOutputStream("D:/excel/专项支出用款申请书_map.xls");
        workbook.write(fos);
        fos.close();
    }

    /**
     * Excel 模板---使用注解
     * @throws Exception
     */
    public void execlTemplateToExportOne() throws Exception {
        TemplateExportParams params = new TemplateExportParams(
                "doc/exportTemp_image.xls", true);
        Map<String, Object> map = new HashMap<String, Object>();
        // sheet 2
        map.put("month", 10);
        Map<String, Object> temp;
        for (int i = 1; i < 8; i++) {
            temp = new HashMap<String, Object>();
            temp.put("per", i * 10);
            temp.put("mon", i * 1000);
            temp.put("summon", i * 10000);
            ImageEntity image = new ImageEntity();
            image.setHeight(200);
            image.setWidth(500);
            image.setUrl("imgs/company/baidu.png");
            temp.put("image", image);
            map.put("i" + i, temp);
        }
        Workbook book = ExcelExportUtil.exportExcel(params, map);
        File savefile = new File("D:/excel/");
        if (!savefile.exists()) {
            savefile.mkdirs();
        }
        FileOutputStream fos = new FileOutputStream("D:/excel/exportTemp_image.xls");
        book.write(fos);
        fos.close();

    }


    /**
     * word模板-简单导出包含图片
     */
    public static void imageWordExport(String pathUrl) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("department", "Easypoi");
        map.put("person", "JueYue");
        map.put("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        WordImageEntity image = new WordImageEntity();
        image.setHeight(200);
        image.setWidth(500);
        image.setUrl("cn/afterturn/easypoi/test/word/img/testCode.png");
        image.setType(WordImageEntity.URL);
        map.put("testCode", image);
        try {
            XWPFDocument doc = WordExportUtil.exportWord07("Image.docx", map);
            FileOutputStream fos = new FileOutputStream(pathUrl);
            doc.write(fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  word模板-简单导出没有图片和Excel
     */
    public void SimpleWordExport() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("department", "Easypoi");
        map.put("person", "JueYue");
        map.put("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        map.put("me","JueYue");
        map.put("date", "2015-01-03");
        try {
            XWPFDocument doc = WordExportUtil.exportWord07("Simple.docx", map);
            FileOutputStream fos = new FileOutputStream("D:/excel/simple.docx");
            doc.write(fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
