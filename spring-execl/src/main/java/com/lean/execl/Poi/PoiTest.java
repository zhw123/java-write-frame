package com.lean.execl.Poi;

import com.lean.execl.model.User;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Excel中的概念	Poi对应的对象
 * Excel 文件	    HSSFWorkbook（xls 2003版本）  XSSFWorkbook（xlsx 2007版本以上） SXSSFWorkbook：是从POI 3.8版本开始，提供了一种基于XSSF的低内存占用的API,是加强版的XSSFWorkbook
 * Excel 的工作表	HSSFSheet
 * Excel 的行	    HSSFRow
 * Excel 中的单元格	HSSFCell
 * Excel 字体	    HSSFFont
 * Excel 单元格样式	HSSFCellStyle
 * Excel 颜色	    HSSFColor
 * 合并单元格	        CellRangeAddress
 * <p>
 * 大批量数据：问题 内存溢出  选择POI对应的SXSSFWorkbook工具类
 * <p>
 * poi读取Excel有两种模式，
 * 一种是用户模式，一种是sax事件驱动模式。用户模式是通过一次性的将所有数据加载到内存中再去解析每个单元格内容。当Excel数据量比较大时当数据量过大时会出现内存溢出的问题。
 * 事件模式：它逐行扫描文档，一边扫描一边解析。由于应用程序只是在读取时检查数据，因此不需要将数据存储在内存中。
 */
public class PoiTest {

    public static final String EXCEL03_EXTENSION = ".xls";
    //excel2007扩展名
    public static final String EXCEL07_EXTENSION = ".xlsx";

    public static void main(String[] args) throws Exception {

        writeBigDataToExecl("e:/sxssf.xlsx");

    }


    /**
     * 基础 导出数据
     * 流程：execl文件---》读流取文件---》poi解析流获取数据--》入库
     *
     * @param path
     * @throws Exception
     */
    public static void writeBaseExecl(String path) throws Exception {
//        0.准备数据
        User user = new User();
        user.setUserId(1);
        user.setUserName("hhhh");
        user.setUserSex("男");
        String[] titles = {"编号", "名字", "性别"};
        /**
         * 先写入 标题栏数据
         */
//        1.创建文件对象   创建HSSFWorkbook只能够写出为xls格式的Excel
//        要写出 xlsx 需要创建为 XSSFWorkbook 两种Api基本使用方式一样
        Workbook workbook;
        if(path.endsWith(EXCEL03_EXTENSION)){
            workbook = new HSSFWorkbook();
        } else {
            workbook = new XSSFWorkbook();
        }

//        2.创建表对象
        Sheet sheet = workbook.createSheet("users");

//        3.创建标题栏（第一行）  参数为行下标  行下标从0开始
        Row titleRow = sheet.createRow(0);

//        4.在标题栏中写入数据
        for (int i = 0; i < titles.length; i++) {
//            创建单元格
            Cell cell = titleRow.createCell(i);
            cell.setCellValue(titles[i]);
        }
        /**
         * 写入用户数据
         */
//       5 创建行 如果是用户数据的集合 需要遍历
        Row row = sheet.createRow(1);

//       6 将用户数据写入到行中
        row.createCell(0).setCellValue(user.getUserId());
        row.createCell(1).setCellValue(user.getUserName());
        row.createCell(2).setCellValue(user.getUserSex());

//        文件保存到本地 参数为要写出的位置
        workbook.write(new FileOutputStream("/Users/k/Desktop/hhh.xls"));
    }


    /**
     * 基础 导人数据
     * 流程：execl文件---》读流取文件---》poi解析流获取数据--》入库
     *
     * @param path
     * @throws Exception
     */
    public static void readBaseExecl(String path) throws Exception {
//        1.通过流读取Excel文件
        FileInputStream inputStream = new FileInputStream(path);
//        2.通过poi解析流 HSSFWorkbook 处理流得到的对象中 就封装了Excel文件所有的数据
        Workbook workbook=null;
        if(path.endsWith(EXCEL03_EXTENSION)){
            workbook = new HSSFWorkbook(inputStream);
        }else {
            workbook = new XSSFWorkbook(inputStream);
        }

//        3.从文件中获取表对象  getSheetAt通过下标获取
        Sheet sheet = workbook.getSheetAt(0);
//        4.从表中获取到行数据  从第二行开始 到 最后一行  getLastRowNum() 获取最后一行的下标
        int lastRowNum = sheet.getLastRowNum();

        for (int i = 1; i <= lastRowNum; i++) {
//            通过下标获取行
            Row row = sheet.getRow(i);
//            从行中获取数据

            /**
             * getNumericCellValue() 获取数字
             * getStringCellValue 获取String
             */
            double id = row.getCell(0).getNumericCellValue();
            String name = row.getCell(1).getStringCellValue();
            String sex = row.getCell(2).getStringCellValue();

//            封装到对象中
            User user = new User();
            user.setUserId((int) id);
            user.setUserName(name);
            user.setUserSex(sex);

//            将对象添加数据库
            System.out.println(user);
        }
    }

    /**
     * 大数据读取
     *
     * @param pathUrl
     * @throws Exception
     */
    public static void readBigDataToExecl(String pathUrl) throws Exception {
        ExcelReaderUtil.readExcel(pathUrl);
    }

    /**
     * 大数据导入-- 自动刷新
     *
     * @param pathUrl
     * @throws Exception
     */
    public static void writeBigDataToExecl(String pathUrl) throws Exception {
        long start = System.currentTimeMillis();
        //自动刷新
        SXSSFWorkbook wb = new SXSSFWorkbook(10000); // 在内存中保留10000行，超过的行将刷新到磁盘
        Sheet sh = wb.createSheet();
        Row firstRow = sh.createRow(0);
        for (int i = 0; i < 20; i++) {
            Cell cell = firstRow.createCell(i);
            cell.setCellValue("列" + i);
        }
        System.out.println("准备完表头时间：" + (System.currentTimeMillis() - start));
        for (int rownum = 0; rownum < 1000000; rownum++) {
            Row row = sh.createRow(rownum + 1);
            for (int cellnum = 0; cellnum < 20; cellnum++) {
                //输入内容区域
                Cell cell = row.createCell(cellnum);
                cell.setCellValue("列内容为" + "1");
            }
        }
        System.out.println("准备完表内容时间：" + (System.currentTimeMillis() - start));
        FileOutputStream out = new FileOutputStream(pathUrl);
        wb.write(out);
        out.close();
        System.out.println("输入完成时间：" + (System.currentTimeMillis() - start));
        System.out.println("finish");
    }

    /**
     * 大数据导入-- 手动刷新
     *
     * @param pathUrl
     * @throws Exception
     */
    public void writeBigDataToExeclCloseFlush(String pathUrl) throws Exception {
        SXSSFWorkbook wb = new SXSSFWorkbook(-1); // 关闭自动刷新
        Sheet sh = wb.createSheet();
        for (int rownum = 0; rownum < 1000; rownum++) {
            Row row = sh.createRow(rownum);
            for (int cellnum = 0; cellnum < 10; cellnum++) {
                Cell cell = row.createCell(cellnum);
                String address = new CellReference(cell).formatAsString();
                cell.setCellValue(address);
            }
            // 手动控制如何将行刷新到磁盘
            if (rownum % 100 == 0) {
                ((SXSSFSheet) sh).flushRows(100); // 保留最后 100 行并刷新所有其他行
                //((SXSSFSheet)sh).flushRows() 是 ((SXSSFSheet)sh).flushRows(0) 的快捷方式，
                // 此方法刷新所有行
            }
        }
        FileOutputStream out = new FileOutputStream(pathUrl);
        wb.write(out);
        out.close();
        // dispose of temporary files backing this workbook on disk
        wb.dispose();
    }


}
