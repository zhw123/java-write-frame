package com.lean.execl.csv;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class JavaCSVUtil {

    /**
     * 读取CSV
     */
    public static List<String[]> readCSV(String csvFilePath,String charSetName) {
        CsvReader reader = null;
        // 用来保存数据
        List<String[]> csvFileList = new ArrayList<>();
        try {
            // 创建CSV读对象 例如:CsvReader(文件路径，分隔符，编码格式);
            reader = new CsvReader(csvFilePath, ',', Charset.forName(charSetName));
            // 读取表头
            reader.readHeaders();
            String[] headArray = reader.getHeaders();//获取标题
            //如果需要表头，开放下面这句
            //csvFileList.add(headArray);
            // 逐行读入数据
            while (reader.readRecord()) {
                //System.out.println(reader.getRawRecord());
                csvFileList.add(reader.getValues());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (null != reader) {
                reader.close();
            }
        }
        return csvFileList;
    }

    /**
     * 写CSV
     * @param csvFilePath 写入路径
     * @param csvHeaders  表头
     * @param contentList 内容
     */
    public static void writeCSV(String csvFilePath, String[] csvHeaders, List<String[]> contentList) {
        log.info("--------CSV文件开始写入--------");
        try {
            // 创建CSV写对象 例如:CsvWriter(文件路径，分隔符，编码格式);
            CsvWriter csvWriter = new CsvWriter(csvFilePath, ',', Charset.forName("UTF-8"));
            csvWriter.writeRecord(csvHeaders);
            // 写内容
            for (int i = 0; i < contentList.size(); i++) {
                csvWriter.writeRecord(contentList.get(i));
            }
            csvWriter.close();
            log.info("--------CSV文件已经写入，行数：{}--------",contentList.size()+1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        //读
//        String csvFilePath="F://test_two.csv";
//        String charSetName="GB2312";
//        List<String[]> csvFileList = JavaCSVUtil.readCSV(csvFilePath,charSetName);
//        // 遍历读取的CSV文件
//        for (int row = 0; row < csvFileList.size(); row++) {
//            String[] cells = csvFileList.get(row);
//            if(cells != null && cells.length > 0){
//                for(int index=0; index < cells.length; index ++){
//                    System.out.print(cells[index]+"  ");
//                }
//                System.out.println();
//            }
//        }

        //写
//        String csvFilePath = "F://StemQ.csv";
//        String[] csvHeaders = { "编号", "姓名", "年龄" };
//        List<String[]> contentList = new ArrayList<>();
//        String[] str1 = { "1", "小明", "18" };
//        String[] str2 = { "2", "小红", "19" };
//        String[] str3 = { "3", "小芳", "20" };
//
//        contentList.add(str1);
//        contentList.add(str2);
//        contentList.add(str3);
//
//        JavaCSVUtil.writeCSV(csvFilePath,csvHeaders,contentList);
    }
}

