package com.lean.execl.easyexcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.builder.ExcelReaderBuilder;
import com.alibaba.excel.read.builder.ExcelReaderSheetBuilder;
import com.lean.execl.model.DemoData;

import java.util.ArrayList;
import java.util.List;

public class EasyexcelTest {

    public void test01() {
        // 有个很重要的点 DemoDataListener 不能被spring管理，要每次读取excel都要new,然后里面用到spring可以构造方法传进去
        // 获得工作簿对象
        /*
         EasyExcel.read()参数：
        pathName  		文件路径；"d:\\学员信息表.xlsx"
        	head			每行数据对应的实体；Student.class
        	readListener	读监听器，每读一样就会调用一次该监听器的invoke方法
         */
        ExcelReaderBuilder excelReaderBuilder = EasyExcel.read("学员信息表.xlsx", DemoData.class, new ExcelListener());
        // 获取一个工作表
        ExcelReaderSheetBuilder sheet = excelReaderBuilder.sheet();
        // 读取工作表内容:sheet方法参数：工作表的顺序号（从0开始）或者工作表的名字，不传默认为0
        sheet.doRead();

    }

    public static void test() {
        // 实现excel写的操作
        // 1 设置写入文件夹地址和excel文件名称 没有的话会自动创建
        String filename = "/Users/tusxxw/write.xlsx";
        // 2 调用easyexcel里面的方法实现写操作
        // write方法两个参数：第一个参数文件路径名称，第二个参数实体类class
        EasyExcel.write(filename, DemoData.class).sheet("学生列表").doWrite(getData());
        // 实现excel读操作
        // EasyExcel.read(filename,DemoData.class,new ExcelListener()).sheet().doRead();
    }

    // 创建方法返回list集合
    private static List<DemoData> getData() {
        List<DemoData> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DemoData data = new DemoData();
            data.setSno(i);
            data.setSname("lucy" + i);
            list.add(data);
        }
        return list;
    }

    public static void main(String[] args) {
        testExport01();
    }

    public static void testExport01() {
        String fileName = "d:\\文件名称" + System.currentTimeMillis() + ".xlsx";
        EasyExcel.write(fileName)
            // 这里放入动态头
            .head(head()).sheet("模板")
            // 当然这里数据也可以用 List<List<String>> 去传入
            .doWrite(getData1());
    }

    public static List<List<String>> getData1() {
        List<List<String>> total = new ArrayList<>();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            list.add("我是第" + i + "列!");
        }
        total.add(list);

        return total;

    }

    private static List<List<String>> head() {
        List<List<String>> list = new ArrayList<List<String>>();
        List<String> head0 = new ArrayList<>();
        head0.add("Id");

        List<String> head1 = new ArrayList<>();
        head1.add("名称");

        List<String> head2 = new ArrayList<>();
        head2.add("尺寸");

        list.add(head0);
        list.add(head1);
        list.add(head2);

        List<String> productHead = new ArrayList<>();
        productHead.add("aaa");
        productHead.add("bbb");
        productHead.add("ccc");

        List<String> head3 = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            for (String s : productHead) {
                head3 = new ArrayList<>();
                head3.add("子产品" + i);
                head3.add(s);
                list.add(head3);
            }
        }

        return list;
    }

}
