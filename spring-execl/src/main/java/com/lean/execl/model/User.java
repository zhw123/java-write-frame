package com.lean.execl.model;

import lombok.Data;

/**
 * @Author zhw
 * @Description TODO
 * @Date $ $
 * @Param $
 * @return $
 **/
@Data
public class User {
    private int userId;
    private String userName;
    private String userSex;

}
