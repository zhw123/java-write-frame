package com.lean.execl.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Teacher {
    /**
     * 老师的主键
     */
    private Integer teacherId;
    /**
     * 名字
     */
    @Excel(name = "老师名字")
    private String teacherName;
    /**
     * 头像图片地址
     * type:2 该类型为图片 imageType:1 默认可以不填写，表示从file读取。
     * 字段类型是字符串类型，可以用相对路径也可以用绝对路径，绝对路径优先以此获取。
     */
    @Excel(name = "老师图片",type = 2,width = 40,height = 40,imageType = 1)
    private String teacherImage;
    /**
     * 老师的状态 0代表正常 1代表删除
     */
    @Excel(name = "老师状态")
    private Integer teacherStatus;

    @ExcelCollection(name = "学生数据")
    private List<Student> students;

    //savePath 写图片保存文件夹的绝对路径 图片导入
//    @Excel(name = "图片",type = 2,width = 40,height = 40,imageType = 1,savePath = "e://")
//    private String guruImage;

}

