//package com.lean.acitivity;
//
//import org.activiti.engine.*;
//import org.activiti.engine.history.HistoricActivityInstance;
//import org.activiti.engine.repository.Deployment;
//import org.activiti.engine.repository.ProcessDefinition;
//import org.activiti.engine.runtime.ProcessInstance;
//import org.activiti.engine.task.Comment;
//import org.activiti.engine.task.Task;
//import org.apache.commons.io.IOUtils;
//import org.junit.Ignore;
//import org.junit.Test;
//
//import java.io.FileOutputStream;
//import java.io.InputStream;
//import java.util.List;
////https://zhengjianfeng.cn/?p=162
////https://blog.csdn.net/chinese_cai/article/details/103792322
////https://blog.csdn.net/qq_40925189/category_11009323.html
//public class ActivitiTest {
//    //1.开始部署流程定义
//    @Ignore
//    @Test
//    public void testDeploy(){
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取RepositoryService对象
//        RepositoryService repositoryService = processEngine.getRepositoryService();
//        //进行部署
//        Deployment deployment = repositoryService.createDeployment()
//                .addClasspathResource("bpmn/leave.bpmn20.xml")
//                .name("请假流程")
//                .deploy();
//        //输出部署的一些信息
//        System.out.println("流程部署ID:"+deployment.getId());
//        System.out.println("流程部署名称:"+deployment.getName());
//    }
//
//    //2.启动流程示例
//    @Ignore @Test
//    public void testStartProcess(){
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取RuntimeService对象
//        RuntimeService runtimeService = processEngine.getRuntimeService();
//        //根据流程定义的key启动流程实例,这个key是在定义bpmn的时候设置的
//        ProcessInstance instance = runtimeService.
//                startProcessInstanceByKey("leaveProcess");
//        //获取流程实例的相关信息
//        System.out.println("流程定义的id = " + instance.getProcessDefinitionId());
//        System.out.println("流程实例的id = " + instance.getId());
//    }
//
//    //3.任务查询
//    //流程启动后，各个任务的负责人就可以查询自己当前需要处理的任务，查询出来的任务都是该用户的待办任务。
//    @Ignore @Test
//    public void testSelectTodoTaskList(){
//        //任务负责人
//        String assignee = "李四";
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取TaskService
//        TaskService taskService = processEngine.getTaskService();
//        //获取任务集合
//        List<Task> taskList = taskService.createTaskQuery()
//                .processDefinitionKey("leaveProcess")
//                .taskAssignee(assignee)
//                .list();
//        //遍历任务列表
//        for(Task task:taskList){
//            System.out.println("流程定义id = " + task.getProcessDefinitionId());
//            System.out.println("流程实例id = " + task.getProcessInstanceId());
//            System.out.println("任务id = " + task.getId());
//            System.out.println("任务名称 = " + task.getName());
//        }
//    }
//
//    //4.任务处理
//    @Ignore @Test
//    public void testCompleteTask(){
//        //任务负责人
//        String assignee = "李四";
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取TaskService
//        TaskService taskService = processEngine.getTaskService();
//        //获取任务集合
//        List<Task> taskList = taskService.createTaskQuery()
//                .processDefinitionKey("leaveProcess")
//                .taskAssignee(assignee)
//                .list();
//        //遍历任务列表
//        for(Task task:taskList){
//            taskService.complete(task.getId());
//        }
//    }
//
//    //5.添加审批意见
//    @Ignore @Test
//    public void testAddComment(){
//        //任务负责人
//        String assignee = "王五";
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取TaskService
//        TaskService taskService = processEngine.getTaskService();
//        //获取任务集合
//        List<Task> taskList = taskService.createTaskQuery()
//                .processDefinitionKey("leaveProcess")
//                .taskAssignee(assignee)
//                .list();
//        //遍历任务列表
//        for(Task task:taskList){
//            //在任务执行之前任务添加批注信息
//            taskService.addComment(task.getId(),task.getProcessInstanceId(),task.getName()+"审批通过");
//            taskService.complete(task.getId());
//        }
//    }
//
//    //6.查看历史审批
//    @Ignore @Test
//    public void testSelectHistoryTask(){
//        //流程实例ID
//        String processInstanceId = "2501";
//        //任务审核人
//        String taskAssignee = "王五";
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取historyService
//        HistoryService historyService = processEngine.getHistoryService();
//        //获取taskService
//        TaskService taskService = processEngine.getTaskService();
//        //获取历史审核信息
//        List<HistoricActivityInstance> list = historyService
//                .createHistoricActivityInstanceQuery()
//                .activityType("userTask")//只获取用户任务
//                .processInstanceId(processInstanceId)
//                .taskAssignee(taskAssignee)
//                .finished()
//                .list();
//        for(HistoricActivityInstance instance:list){
//            System.out.println("任务名称:"+instance.getActivityName());
//            System.out.println("任务开始时间:"+instance.getStartTime());
//            System.out.println("任务结束时间:"+instance.getEndTime());
//            System.out.println("任务耗时:"+instance.getDurationInMillis());
//            //获取审核批注信息
//            List<Comment> taskComments = taskService.getTaskComments(instance.getTaskId());
//            if(taskComments.size()>0){
//                System.out.println("审批批注:"+taskComments.get(0).getFullMessage());
//            }
//        }
//    }
//
//
//
//
//
//    //---------------------//
//
//    //1.流程定义查询
//    //查询流程相关信息，包含流程定义，流程部署，流程定义版本
//    @Ignore @Test
//    public void testDefinitionQuery(){
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取仓库服务
//        RepositoryService repositoryService = processEngine.getRepositoryService();
//        //获取流程定义集合
//        List<ProcessDefinition> processDefinitionList = repositoryService
//                .createProcessDefinitionQuery()
//                .processDefinitionKey("leaveProcess")
//                .list();
//        //遍历集合
//        for (ProcessDefinition definition:processDefinitionList){
//            System.out.println("流程定义ID:"+definition.getId());
//            System.out.println("流程定义名称:"+definition.getName());
//            System.out.println("流程定义key:"+definition.getKey());
//            System.out.println("流程定义版本:"+definition.getVersion());
//            System.out.println("流程部署ID:"+definition.getDeploymentId());
//            System.out.println("====================");
//        }
//    }
//
//    //2.流程资源下载
//    //现在我们的流程资源文件已经上传到数据库了，如果其他用户想要查看这些资源文件，可以从数据库中把资源文件下载到本地。
//    @Ignore @Test
//    public void testDownloadResource() throws Exception {
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取仓库服务
//        RepositoryService repositoryService = processEngine.getRepositoryService();
//        //获取流程定义集合
//        List<ProcessDefinition> list = repositoryService
//                .createProcessDefinitionQuery()
//                .processDefinitionKey("leaveProcess")
//                .orderByProcessDefinitionVersion()//按照版本排序
//                .desc()//降序
//                .list();
//        //获取最新那个
//        ProcessDefinition definition =list.get(0);
//        //获取部署ID
//        String deploymentId = definition.getDeploymentId();
//        //获取bpmn的输入流
//        InputStream bpmnInput = repositoryService.getResourceAsStream(
//                deploymentId,
//                definition.getResourceName());
//        //获取png的输入流
//        InputStream pngInput = repositoryService.getResourceAsStream(
//                deploymentId,
//                definition.getDiagramResourceName());
//        //设置bpmn输入
//        FileOutputStream bpmnOutPut = new FileOutputStream("D:/leave.bpmn");
//        //设置png输入
//        FileOutputStream pngOutPut = new FileOutputStream("D:/leave.png");
//        IOUtils.copy(bpmnInput,bpmnOutPut);
//        IOUtils.copy(pngInput,pngOutPut);
//    }
//
//
//    //3.流程定义删除
//    //根据部署Id删除对应的流程定义
//    @Ignore @Test
//    public void testDeleteDeploy(){
//        //流程部署Id
//        String deploymentId = "10001";
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取仓库服务
//        RepositoryService repositoryService = processEngine.getRepositoryService();
//        //删除流程定义，如果该流程定义已有流程实例启动则删除时出错
//        repositoryService.deleteDeployment(deploymentId);
//        //设置true 级联删除流程定义，即使该流程有流程实例启动也可以删除，设置为false非级别删除方式，如果流程
//        //repositoryService.deleteDeployment(deploymentId,true);
//    }
//
//
//    //5. BusinessKey（业务标识）
//    @Ignore @Test
//    public void testGetBusinessKey(){
//        //任务负责人
//        String assignee = "李四";
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取TaskService
//        TaskService taskService = processEngine.getTaskService();
//        //获取RuntimeService
//        RuntimeService runtimeService = processEngine.getRuntimeService();
//        //获取任务集合
//        List<Task> taskList = taskService.createTaskQuery()
//                .processDefinitionKey("leaveProcess")
//                .taskAssignee(assignee)
//                .list();
//        //遍历任务列表
//        for(Task task:taskList){
//            System.out.println("流程定义id = " + task.getProcessDefinitionId());
//            System.out.println("流程实例id = " + task.getProcessInstanceId());
//            System.out.println("任务id = " + task.getId());
//            System.out.println("任务名称 = " + task.getName());
//            //根据任务上的流程实例Id查询出对应的流程实例对象，从流程实例对象中获取BusinessKey
//            ProcessInstance instance = runtimeService
//                    .createProcessInstanceQuery()
//                    .processInstanceId(task.getProcessInstanceId())
//                    .singleResult();
//            System.out.println("业务key:"+instance.getBusinessKey());
//            System.out.println("===================");
//        }
//    }
//
//
//    //6.流程定义/实例的挂起/激活
//    @Ignore @Test
//    public void testSuspendAllProcessInstance(){
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取RepositoryService
//        RepositoryService repositoryService = processEngine.getRepositoryService();
//        //获取流程定义对象
//        ProcessDefinition processDefinition = repositoryService
//                .createProcessDefinitionQuery()
//                .processDefinitionKey("leaveProcess")
//                .singleResult();
//        boolean suspended = processDefinition.isSuspended();
//        //输出流程定义状态
//        System.out.println("流程定义状态:"+(suspended ?"已挂起":"已激活"));
//        String processDefinitionId = processDefinition.getId();
//        if(suspended){
//            //如果是挂起，可以执行激活操作 ,参数1 ：流程定义id ，参数2：是否激活流程实例，参数3：激活时间
//            repositoryService.activateProcessDefinitionById(processDefinitionId,true,null);
//            System.out.println("流程ID:"+processDefinitionId+",已激活");
//        }else{
//            //如果是激活，可以执行挂起操作 ,参数1 ：流程定义id ，参数2：是否暂停流程实例，参数3：激活时间
//            repositoryService.suspendProcessDefinitionById(processDefinitionId,true,null);
//            System.out.println("流程ID:"+processDefinitionId+",已挂起");
//        }
//    }
//
//    //查询待办任务的状态,如果是【已挂起】,前台则不允许点击【任务处理】按钮
//    @Ignore @Test
//    public void testSuspendStatus(){
//        //任务负责人
//        String assignee = "李四";
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取TaskService
//        TaskService taskService = processEngine.getTaskService();
//        //获取任务集合
//        List<Task> taskList = taskService.createTaskQuery()
//                .processDefinitionKey("leaveProcess")
//                .taskAssignee(assignee)
//                .list();
//        //遍历任务列表
//        for(Task task:taskList){
//            System.out.println("流程定义id = " + task.getProcessDefinitionId());
//            System.out.println("流程实例id = " + task.getProcessInstanceId());
//            System.out.println("任务id = " + task.getId());
//            System.out.println("任务名称 = " + task.getName());
//            System.out.println("任务状态:"+(task.isSuspended()?"已挂起":"已激活"));
//            System.out.println("===================");
//        }
//    }
//
//    //单个流程实例挂起场景
//    @Ignore @Test
//    public void testQueryProcessInstance(){
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取RepositoryService
//        RuntimeService runtimeService = processEngine.getRuntimeService();
//        List<ProcessInstance> processInstanceList = runtimeService
//                .createProcessInstanceQuery()
//                .processDefinitionKey("leaveProcess")
//                .list();
//        for(ProcessInstance processInstance:processInstanceList){
//            System.out.println("流程实例Id:"+processInstance.getId()+",状态:"+(processInstance.isSuspended()?"已挂起":"已激活"));
//        }
//    }
//
//    //挂起某个流程实例
//    @Ignore @Test
//    public void testSuspendSingleProcessInstance(){
//        //流程实例Id
//        String processInstanceId = "2501";
//        //创建ProcessEngine对象
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        //获取RepositoryService
//        RuntimeService runtimeService = processEngine.getRuntimeService();
//        //根据流程实例Id获取流程实例对象
//        ProcessInstance processInstance = runtimeService
//                .createProcessInstanceQuery()
//                .processInstanceId(processInstanceId)
//                .singleResult();
//        //状态
//        boolean suspended = processInstance.isSuspended();
//        System.out.println("流程实例ID:"+processInstanceId+",状态:"+ (suspended?"已挂起":"已激活"));
//        if(suspended){
//            runtimeService.activateProcessInstanceById(processInstanceId);
//            System.out.println("流程实例ID:"+processInstanceId+",状态修改为已激活");
//        }else{
//            runtimeService.suspendProcessInstanceById(processInstanceId);
//            System.out.println("流程实例ID:"+processInstanceId+",状态修改为已挂起");
//        }
//    }
//
//
//}
