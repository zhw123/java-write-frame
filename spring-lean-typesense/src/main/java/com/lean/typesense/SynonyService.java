package com.lean.typesense;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.typesense.api.Client;
import org.typesense.model.SearchSynonym;
import org.typesense.model.SearchSynonymSchema;
import org.typesense.model.SearchSynonymsResponse;

/**
 * 同义词功能允许您定义应被视为等效的搜索词。例如：当您定义sneakeras的同义词时，除了包含单词 的记录之外，shoe搜索sneaker现在将返回包含该单词的所有记录。 Typesense 支持两种类型的同义词： 单向同义词：将单词iphoneand定义android为 的单向同义词smart
 * phone将导致搜索smart phone返回包含iphone或android/或两者的文档。
 * 多向同义词：定义单词blazer,coat和jacket作为多向同义词将导致搜索这些单词中的任何一个（例如：coat）返回至少包含同义词集中单词之一的文档（例如：带有blazerorcoat或jacketare的记录回到）。
 */
@Service
public class SynonyService {

    @Autowired
    private Client client;

    /**
     * 创建或更新同义词 -多路同义词
     */
    public SearchSynonym createOrUpdateSynony(String collectName) throws Exception {
        SearchSynonymSchema synonym = new SearchSynonymSchema();
        synonym.addSynonymsItem("blazer")
                .addSynonymsItem("coat")
                .addSynonymsItem("jacket");
        return client.collections(collectName).synonyms().upsert("coat-synonyms", synonym);
    }

    /**
     * 创建或更新同义词 -单向同义词
     */
    public SearchSynonym createOrUpdateSimpleSynony(String collectName) throws Exception {
        SearchSynonymSchema synonym = new SearchSynonymSchema();
        synonym.addSynonymsItem("iphone").addSynonymsItem("android");
        synonym.root("smart phone");
        return  client.collections(collectName).synonyms().upsert("smart-phone-synonyms", synonym);
    }

    /**
     * 检索同义词
     */
    public SearchSynonym get(String collectName) throws Exception {
        return client.collections(collectName).synonyms("coat-synonyms").retrieve();

    }

    /**
     * 列出所有同义词
     */
    public SearchSynonymsResponse list(String collectName) throws Exception {
        return client.collections(collectName).synonyms().retrieve();

    }

    /**
     * 删除同义词
     */
    public SearchSynonym del(String collectName) throws Exception {
        return client.collections(collectName).synonyms("coat-synonyms").delete();

    }

}
