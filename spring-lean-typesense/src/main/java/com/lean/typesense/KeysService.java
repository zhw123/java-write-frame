package com.lean.typesense;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.typesense.api.Client;
import org.typesense.model.ApiKey;
import org.typesense.model.ApiKeySchema;
import org.typesense.model.ApiKeysResponse;

import java.util.ArrayList;
import java.util.List;

@Service
public class KeysService {
    @Autowired
    private Client client;

    /**
     * 创建 API 密钥 -管理员密钥
     * @return
     * @throws Exception
     */
    public ApiKey createAdminKey() throws Exception {
        ApiKeySchema apiKeySchema = new ApiKeySchema();
        List<String> actionValues = new ArrayList<>();
        List<String> collectionValues = new ArrayList<>();

        actionValues.add("*");
        collectionValues.add("*");

        apiKeySchema.description("Admin Key").actions(actionValues).collections(collectionValues);
       return client.keys().create(apiKeySchema);
    }

    //收集行动
//    collections:create	允许创建集合。
//    collections:delete	允许删除集合。
//    collections:get	允许检索集合架构。
//    collections:list	允许检索所有集合架构。
//    collections:*	允许各种与集合相关的操作。
//   记录行动
//    documents:search	仅允许搜索请求。
//    documents:get	允许获取单个文档。
//    documents:create	Allows creating documents.
//    documents:upsert	Allows upserting documents.
//    documents:update	Allows updating documents.
//    documents:delete	Allows deletion of documents.
//    documents:import	Allows import of documents in bulk.
//    documents:export	Allows export of documents in bulk.
//    documents:*	Allows all document operations.
//    别名操作
//        aliases:list	允许获取所有别名。
//        aliases:get	允许检索单个别名
//        aliases:create	允许创建别名。
//        aliases:delete	允许删除别名。
//        aliases:*	允许所有别名操作。
//   同义词动作
//        synonyms:list	允许获取所有同义词。
//        synonyms:get	允许检索单个同义词
//        synonyms:create	允许创建同义词。
//        synonyms:delete	允许删除同义词。
//        synonyms:*	允许所有同义词操作。
//   覆盖操作
//        overrides:list	允许获取所有覆盖。
//        overrides:get	允许检索单个覆盖
//        overrides:create	允许创建覆盖。
//        overrides:delete	允许删除覆盖。
//        overrides:*	允许所有覆盖操作。
//    按键动作
//        keys:list	允许获取所有键的元数据
//        keys:get	允许获取单个密钥的元数据
//        keys:create	允许创建 API 密钥。
//        keys:delete	允许删除 API 密钥。
//        keys:*	允许所有 API 密钥相关操作。
//    其他动作
//        metrics.json:list	允许访问指标端点。
//        debug:list	允许访问端点/debug。
//        *	允许所有操作。

    /**
     * 创建 API 密钥 -仅限搜索 API 密钥
     * @return 通过将actions范围设置为["documents:search"]和collections范围为["collectName"]，我们可以生成一个仅允许对companies集合进行搜索的密钥。
     * @throws Exception
     */
    public ApiKey createSearchKey(String collectName) throws Exception {
        ApiKeySchema apiKeySchema = new ApiKeySchema();
        List<String> actionValues = new ArrayList<>();
        List<String> collectionValues = new ArrayList<>();
        actionValues.add("documents:search");
        collectionValues.add(collectName);
        apiKeySchema.description("Search only Key").actions(actionValues).collections(collectionValues);
        return client.keys().create(apiKeySchema);
    }

    /**
     * 创建 API 密钥 - 正则
     * @throws Exception
     */
    public ApiKey createRexgSearchKey(String collectPrefixName) throws Exception {
        ApiKeySchema apiKeySchema = new ApiKeySchema();
        List<String> actionValues = new ArrayList<>();
        List<String> collectionValues = new ArrayList<>();
        actionValues.add("documents:search");
        collectionValues.add(collectPrefixName+".*");
        apiKeySchema.description("Search only Key").actions(actionValues).collections(collectionValues);
        return client.keys().create(apiKeySchema);
    }

    /**
     * 检索 API 密钥
     * @param id
     * @return
     * @throws Exception
     */
    public ApiKey  getKey(Long id) throws Exception {
        return client.keys(id).retrieve();
    }

    /**
     * 检索所有密钥  有关元数据
     * @return
     * @throws Exception
     */
    public ApiKeysResponse listKey() throws Exception {
       return client.keys().retrieve();
    }

    /**
     * 删除 API 密钥
     * @param id
     * @return
     * @throws Exception
     */
    public ApiKey  delKey(Long id) throws Exception {
        return client.keys(id).delete();
    }




}
