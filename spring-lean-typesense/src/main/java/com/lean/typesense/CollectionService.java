package com.lean.typesense;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.typesense.api.Client;
import org.typesense.api.FieldTypes;
import org.typesense.model.*;
/**
 * 集合相当于 库
 */
@Service
public class CollectionService {

    @Autowired
    private Client client;

    /**
     * 创建集合--预定义
     * @param name
     * @return
     */
    public CollectionResponse createCollection(String name) throws Exception {
        CollectionSchema collectionSchema = new CollectionSchema();
        collectionSchema.name(name)
                .addFieldsItem(new Field().name("company_name").type(FieldTypes.STRING))
                .addFieldsItem(new Field().name("num_employees").type(FieldTypes.INT32))
                .addFieldsItem(new Field().name("country").type(FieldTypes.STRING).facet(true));
        return client.collections().create(collectionSchema);
    }


    /**
     * 创建集合--自动检测字段的类型
     * @param name
     * @return
     */
    public CollectionResponse createAutoCollection(String name) throws Exception {
        CollectionSchema collectionSchema = new CollectionSchema();
        collectionSchema.name(name)
                .addFieldsItem(new Field().name(".*").type(FieldTypes.AUTO));
        return client.collections().create(collectionSchema);
    }


    /**
     * 检索集合
     * @param name
     * @return
     */
    public CollectionResponse getColection(String name) throws Exception {
        return client.collections(name).retrieve();
    }


    /**
     * 列出所有集合
     * @return
     */
    public CollectionResponse[] listColection() throws Exception {
        return client.collections().retrieve();
    }


    /**
     * 删除一个集合
     * @return
     */
    public CollectionResponse delColection(String name) throws Exception {
        return client.collections(name).delete();
    }


    /**
     * 更新或更改集合 Typesense 支持就地向集合架构添加或删除字段。
     * @return
     */
    public CollectionUpdateSchema updateFieldColection(String name,String fileName) throws Exception {
        CollectionUpdateSchema updateSchema = new CollectionUpdateSchema();
        updateSchema.addFieldsItem(new Field().name(fileName).drop(true))
                .addFieldsItem(new Field().name(fileName).type(FieldTypes.STRING));
        return client.collections(name).update(updateSchema);
    }


    /**
     * 更新或更改集合 Typesense 支持就地向集合架构添加或删除字段。
     * @return
     */
    public CollectionUpdateSchema dropFiledColection(String name,String fileName) throws Exception {
        CollectionUpdateSchema updateSchema = new CollectionUpdateSchema();
        updateSchema.addFieldsItem(new Field().name(fileName).drop(true));
        return client.collections(name).update(updateSchema);
    }


    /**
     * 更新或更改集合 Typesense 支持就地向集合架构添加或删除字段。
     * @return
     */
    public CollectionUpdateSchema insertFieldColection(String name,String fileName) throws Exception {
        CollectionUpdateSchema updateSchema = new CollectionUpdateSchema();
        updateSchema.addFieldsItem(new Field().name(fileName).type(FieldTypes.STRING));
        return client.collections(name).update(updateSchema);
    }

    //别名
    public CollectionAlias createOrUpdateCollectionAlias(String aliasName,String collectionName) throws Exception {
        CollectionAliasSchema collectionAlias = new CollectionAliasSchema();
        collectionAlias.collectionName(collectionName);
        return client.aliases().upsert(aliasName, collectionAlias);
    }

    //检索别名
    public CollectionAlias getCollectionAlias(String aliasName) throws Exception {
       return client.aliases(aliasName).retrieve();
    }

    //列出所有别名
    public CollectionAliasesResponse listCollectionAlias() throws Exception {
       return client.aliases().retrieve();
    }

    //删除别名
    public CollectionAlias delCollectionAlias(String aliasName) throws Exception {
       return  client.aliases(aliasName).delete();
    }

}
