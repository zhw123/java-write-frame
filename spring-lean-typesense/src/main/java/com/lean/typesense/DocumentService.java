package com.lean.typesense;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.typesense.api.Client;
import org.typesense.model.DeleteDocumentsParameters;
import org.typesense.model.ExportDocumentsParameters;
import org.typesense.model.ImportDocumentsParameters;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 文件-- 数据库 列
 */
@Service
public class DocumentService {

    @Autowired
    private Client client;

    /**
     * 插入数据
     * @param collectionName
     * @param document
     * @return
     * @throws Exception
     */
    public Map<String, Object> insert(String collectionName,HashMap<String, Object> document) throws Exception {
        return client.collections(collectionName).documents().create(document);
    }

    /**
     * 更新插入单个文档
     * @param collectionName
     * @param document
     * @return
     * @throws Exception
     */
    public Map<String, Object> update(String collectionName,HashMap<String, Object> document) throws Exception {
        return client.collections(collectionName).documents().upsert(document);
    }



    /**
     * 批量
     * @param collectionName
     * @param operateName  create:创建一个新文档。如果具有相同 id 的文档已存在，则失败
     *                     upsert:创建新文档或更新现有文档（如果已存在具有相同 ID 的文档）。需要发送整个文件。对于部分更新，请使用update以下操作。
     *                     update: 更新现有文档。如果给定 id 的文档不存在，则失败。您可以发送仅包含要更新的字段的部分文档。
     *                     emplace: 创建新文档或更新现有文档（如果已存在具有相同 ID 的文档）。您可以发送整个文档或部分文档进行更新。
     * @param batchSize 默认情况下，Typesense 一次将 40 个文档摄取到 Typesense 中 - 每摄取 40 个文档后，
     *                  Typesense 就会为搜索请求队列提供服务，然后再切换回导入。要增加该值，请使用该batch_size参数。
     * @return
     * @throws Exception
     */
    public String batchDoing(String collectionName,String operateName,Integer batchSize,ArrayList<HashMap<String, Object>> documentList) throws Exception {
        ImportDocumentsParameters importDocumentsParameters = new ImportDocumentsParameters();
        importDocumentsParameters.action(operateName);
        importDocumentsParameters.batchSize(batchSize);
        // IMPORTANT: Be sure to increase connectionTimeout to at least 5 minutes or more for imports,
        //  when instantiating the client
        return client.collections(collectionName).documents().import_(documentList, importDocumentsParameters);
    }

    /**
     * 处理脏数据
     * @param collectionName
     * @param dirtyValues coerce_or_reject: 尝试将字段的值强制转换为先前推断的类型。如果强制失败，则直接拒绝写入并显示错误消息。
     *                    coerce_or_drop: 尝试将字段的值强制转换为先前推断的类型。如果强制失败，则删除特定字段并为文档的其余部分建立索引。
     *                    drop: 删除特定字段并对文档的其余部分建立索引。
     *                    reject: 彻底拒绝该文件。
     *                    默认行为
     *                      .*如果模式中定义了通配符 ( ) 字段，或者模式包含任何带有正则表达式的字段名称（例如名为 的字段.*_name），
     *                      则默认行为为coerce_or_reject。否则，默认行为是reject（这确保了与旧版 Typesense 版本的向后兼容性）。
     * @return
     * @throws Exception
     */
    public String dealDirtyValues(String collectionName,String operateName,ImportDocumentsParameters.DirtyValuesEnum dirtyValues,
                                  HashMap<String, Object> hmap) throws Exception {
        ImportDocumentsParameters queryParameters = new ImportDocumentsParameters();
        queryParameters.dirtyValues(dirtyValues);
        queryParameters.action(operateName);
        return client.collections(collectionName).documents().create(hmap,queryParameters);
    }

    /**
     * 导入 JSONL 文件
     *
     * @param collectionName
     * @param filePath
     * @return
     * @throws Exception
     */
    public String importJson(String collectionName, String filePath) throws Exception {
        File myObj = new File(filePath);
        ImportDocumentsParameters queryParameters = new ImportDocumentsParameters();
        Scanner myReader = new Scanner(myObj);
        StringBuilder data = new StringBuilder();
        while (myReader.hasNextLine()) {
            data.append(myReader.nextLine()).append("\n");
        }
        return client.collections(collectionName).documents().import_(data.toString(), queryParameters);
    }


    /**
     * 检索文档
     * @param collectionName
     * @param docId 文档id
     * @return
     * @throws Exception
     */
    public Map<String, Object> getDocument(String collectionName, String docId) throws Exception {
        return client.collections(collectionName).documents(docId).retrieve();
    }

    /**
     * 更新文档
     * @param collectionName
     * @param docId 文档id
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateDocument(String collectionName, String docId,Map<String, Object> document) throws Exception {
        return client.collections(collectionName).documents(docId).update(document);
    }

    /**
     * 删除文档
     * @param collectionName
     * @param docId 文档id
     * @return
     * @throws Exception
     */
    public Map<String, Object> delDocument(String collectionName, String docId) throws Exception {
        return client.collections(collectionName).documents(docId).delete();
    }

    /**
     * 查询删除
     * @param collectionName
     * @param query 查询条件
     * @return
     * @throws Exception
     */
    public Map<String, Object> queryAndDelDocument(String collectionName, String query) throws Exception {
        DeleteDocumentsParameters deleteDocumentsParameters = new DeleteDocumentsParameters();
        deleteDocumentsParameters.filterBy(query);
        return client.collections(collectionName).documents().delete(deleteDocumentsParameters);
    }

    /**
     * 以 JSONL 格式导出集合中的文档。
     * @param collectionName
     * @param includeFields 字段
     * @return
     * @throws Exception
     */
    public String exportDocument(String collectionName, String includeFields) throws Exception {
        ExportDocumentsParameters exportDocumentsParameters = new ExportDocumentsParameters();
        exportDocumentsParameters.setIncludeFields(includeFields);
        return client.collections(collectionName).documents().export(exportDocumentsParameters);
    }










}
