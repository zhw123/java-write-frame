package com.lean.typesense;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.typesense.api.Client;
import org.typesense.model.*;

/**
 * 搜索排除文档或指定文档
 */
@Service
public class CurationService {

    @Autowired
    private Client client;

    // 包含或排除文档
    // 使用条件将idincludes为 的文档分别放置在第一和第二位置。422 54
    // 使用条件从结果中excludes删除具有 id 的文档。287
    // Typesense 允许您使用两者includes或excludes仅使用其中之一进行管理。
    public SearchOverride includeOrExcludesItem(String collectionName) throws Exception {
        SearchOverrideSchema searchOverrideSchema = new SearchOverrideSchema();
        searchOverrideSchema.addIncludesItem(new SearchOverrideInclude().id("422").position(1));
        searchOverrideSchema.addIncludesItem(new SearchOverrideInclude().id("54").position(2));
        searchOverrideSchema.addExcludesItem(new SearchOverrideExclude().id("287"));
        searchOverrideSchema.rule(new SearchOverrideRule().query("apple").match(SearchOverrideRule.MatchEnum.EXACT));
        return client.collections(collectionName).overrides().upsert("customize-apple", searchOverrideSchema);
    }

    // 如果有人搜索sony ericsson phone，查询将被重写为phone，并 sony ericsson直接应用品牌过滤器。
    // 如果您不想从查询中删除匹配的标记，请设置remove_matched_tokens为false。默认情况下，该参数设置为true。
    public SearchOverride includeOrExcludesItem2(String collectionName) throws Exception {
        SearchOverrideSchema searchOverrideSchema = new SearchOverrideSchema();
        searchOverrideSchema.rule(new SearchOverrideRule().query("{brand} phone")
                .match(SearchOverrideRule.MatchEnum.CONTAINS));
        return client.collections(collectionName).overrides().upsert("brand-filter", searchOverrideSchema);
    }

    /**
     * 列出与给定集合关联的所有覆盖
     *
     * @throws Exception
     */
    public SearchOverridesResponse list(String collectionName) throws Exception {
        return client.collections(collectionName).overrides().retrieve();

    }

    /**
     * 检索覆盖
     *
     * @throws Exception
     */
    public SearchOverride get(String collectionName) throws Exception {
        return client.collections(collectionName).overrides("customize-apple").retrieve();
    }

    /**
     * 删除覆盖
     *
     * @throws Exception
     */
    public SearchOverride del(String collectionName) throws Exception {
        return client.collections(collectionName).overrides("customize-apple").delete();
    }

}
