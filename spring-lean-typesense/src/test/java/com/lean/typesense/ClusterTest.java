package com.lean.typesense;

import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public class ClusterTest extends TypesenseApplicationTests {

    @Autowired
    private ClusterOperationsService clusterOperationsService;

    @Test
    public void create() throws Exception {
        Map<String, String> map = clusterOperationsService.create("/tmp/typesense-data-snapshot");
        System.out.println(JSONUtil.toJsonStr(map));
    }

    @Test
    public void db() throws Exception {
        Map<String, String> map = clusterOperationsService.db();
        System.out.println(JSONUtil.toJsonStr(map));
    }

    @Test
    public void vote() throws Exception {
        Map<String, String> map = clusterOperationsService.vote();
        System.out.println(JSONUtil.toJsonStr(map));
    }

    @Test
    public void toggleSlowRequestLog(){
        HttpResponse response =clusterOperationsService.toggleSlowRequestLog();
        System.out.println(response);
    }

    @Test
    public void metrics() throws Exception {
        Map<String, String> map = clusterOperationsService.metrics();
        System.out.println(JSONUtil.toJsonStr(map));
    }

    @Test
    public void apiStats(){
        HttpResponse response = clusterOperationsService.apiStats();
        System.out.println(response);
    }

    @Test
    public void health() throws Exception {
        Map<String, Object> map = clusterOperationsService.health();
        System.out.println(JSONUtil.toJsonStr(map));
    }

}
