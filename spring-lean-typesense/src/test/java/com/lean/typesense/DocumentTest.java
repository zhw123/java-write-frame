package com.lean.typesense;

import cn.hutool.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.typesense.model.ImportDocumentsParameters;
import org.junit.Test;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 表
 */
public class DocumentTest extends TypesenseApplicationTests {

    @Autowired
    private DocumentService documentService;

    @Test
    public void insert() throws Exception {
        HashMap<String, Object> document = new HashMap<>();
        document.put("id", "124");
        document.put("company_name", "Stark Industries");
        document.put("num_employees", 5215);
        document.put("country", "USA");
        Map<String, Object> companies = documentService.insert("companies", document);
        System.out.println(JSONUtil.toJsonStr(companies));
    }

    @Test
    public void update() throws Exception {
        HashMap<String, Object> document = new HashMap<>();
        document.put("id", "124");
        document.put("company_name", "Stark Industries");
        document.put("num_employees", 5215);
        document.put("country", "USA");
        Map<String, Object> companies = documentService.update("companies", document);
        System.out.println(JSONUtil.toJsonStr(companies));
    }

    @Test
    public void batchDoing() throws Exception {
        HashMap<String, Object> document1 = new HashMap<>();
        ArrayList<HashMap<String, Object>> documentList = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            document1.put("company_name", "Stark Industries" + i);
            document1.put("num_employees", 5215 + i);
            document1.put("country", "USA" + i);
            documentList.add(document1);
        }
        String batchDoing = documentService.batchDoing("companies", "create", 40, documentList);
        System.out.println(batchDoing);
    }

    @Test
    public void dealDirtyValues() throws Exception {
        String[] authors = {"shakspeare", "william"};
        HashMap<String, Object> hmap = new HashMap<>();
        hmap.put("title", 111);
        hmap.put("authors", authors);
        hmap.put("publication_year", 1666);
        hmap.put("ratings_count", 124);
        hmap.put("average_rating", 3.2);
        hmap.put("id", "2");
        String dirtyValues = documentService.dealDirtyValues("books", "upsert", ImportDocumentsParameters.DirtyValuesEnum.COERCE_OR_REJECT, hmap);
        System.out.println(dirtyValues);
    }

    @Test
    public void importJson() throws Exception {
        documentService.importJson("companies", "");
    }

    @Test
    public void getDocument() throws Exception {
        Map<String, Object> document = documentService.getDocument("companies", "124");
        System.out.println(JSONUtil.toJsonStr(document));
    }

    @Test
    public void updateDocument() throws Exception {
        HashMap<String, Object> document = new HashMap<>();
        document.put("company_name", "Stark Industries");
        document.put("num_employees", 5500);
        Map<String, Object> companies = documentService.updateDocument("companies", "124", document);
        System.out.println(JSONUtil.toJsonStr(companies));
    }

    @Test
    public void delDocument() throws Exception {
        Map<String, Object> companies = documentService.delDocument("companies", "46");
        System.out.println(JSONUtil.toJsonStr(companies));
    }

    @Test
    public void queryAndDelDocument() throws Exception {
        documentService.queryAndDelDocument("companies", "num_employees:>100");
    }

    @Test
    public void exportDocument() throws Exception {
        String companies = documentService.exportDocument("books", "id,publication_year,authors");
        System.out.println(companies);
    }

}
