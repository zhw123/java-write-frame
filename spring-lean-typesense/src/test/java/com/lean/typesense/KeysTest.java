package com.lean.typesense;

import cn.hutool.json.JSONUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.typesense.model.ApiKey;
import org.typesense.model.ApiKeysResponse;

public class KeysTest extends TypesenseApplicationTests {

    @Autowired
    private KeysService keysService;

    @Test
    public void createAdminKey() throws Exception {
        keysService.createAdminKey();
    }

    @Test
    public void createSearchKey() throws Exception {
        keysService.createSearchKey("companies");
    }

    @Test
    public void createRexgSearchKey() throws Exception {
        ApiKey serviceRexgSearchKey = keysService.createRexgSearchKey("org_");
        System.out.println(JSONUtil.toJsonStr(serviceRexgSearchKey));
    }

    @Test
    public void getKey() throws Exception {
        ApiKey key = keysService.getKey(1L);
        System.out.println(JSONUtil.toJsonStr(key));
    }

    @Test
    public void listKey() throws Exception {
        ApiKeysResponse apiKeysResponse = keysService.listKey();
        System.out.println(JSONUtil.toJsonStr(apiKeysResponse));
    }

    @Test
    public void delKey() throws Exception {
        keysService.delKey(1L);
    }
}
