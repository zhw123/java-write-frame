package com.lean.rocketmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanRocketmqDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanRocketmqDemoApplication.class, args);
    }

}
