package com.rocketmq.service.consumer;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.UtilAll;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.stereotype.Service;

/**
 * 通用消费者
 */
@Service
@RocketMQMessageListener(consumerGroup = "common-customer-group", topic = "common_topic", messageModel = MessageModel.CLUSTERING)
public class CommonConsumerListener implements RocketMQListener<MessageExt> , RocketMQPushConsumerLifecycleListener {
    @Override
    public void onMessage(MessageExt messageExt) {
        System.out.println("通用消费者-----------------"+messageExt.toString());
        //消费者处理时抛出异常时就会自动重试
//        throw new RuntimeException("消费者处理时抛出异常时就会自动重试");
    }

    //自定义消息 开始消费的位置
    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {
        // set consumer consume message from now
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_TIMESTAMP);
        consumer.setConsumeTimestamp(UtilAll.timeMillisToHumanString3(System.currentTimeMillis()));
    }
}
