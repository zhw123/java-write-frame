package com.rocketmq.service.consumer;

import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * consumerGroup: 消费组
 * topic：主题
 * selectorExpression： 过滤表达式： tag/指明了消息过滤使用SQL92方式
 * messageModel:消息模式  集群clustering(每条消息只能有一个消费者进行消费)、广播broadcasting(广播消息，所有订阅者都能收到消息)
 */
@Service
@RocketMQMessageListener(consumerGroup = "common-customer-sql-group", topic = "common_topic",selectorType = SelectorType.SQL92 ,selectorExpression = "type='user' or a <7", messageModel = MessageModel.CLUSTERING)
public class SqlConsumerListener implements RocketMQListener<MessageExt> {
    @Override
    public void onMessage(MessageExt message) {
        System.out.println("消费消息："+new String(message.getBody()));
        System.out.println("消费消息："+message.getProperties());
    }
}

