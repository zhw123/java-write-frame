package com.rocketmq.service.controller;


import com.alibaba.fastjson.JSON;
import com.rocketmq.service.tx.UserModel;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


/**
 * 发送事务消息
 */
@RestController
public class RocketMQTransationController {

    private static final Logger log = LoggerFactory.getLogger("RocketMQTransationController");

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    private static final String Topic = "RLT_TEST_TOPIC";
    private static final String Tag = "transTag";
    private static final String Tx_Charge_Group = "tx-group";

    /**
     * 发送事务消息
     */
    @RequestMapping("/sendTransationMessage")
    @ResponseBody
    public TransactionSendResult sendTransationMessage() {
        UserModel userModel=new UserModel();
        userModel.setUserId("1");
        userModel.setChargeAmount(100);
        //发送事务消息:采用的是sendMessageInTransaction方法，返回结果为TransactionSendResult对象，该对象中包含了事务发送的状态、本地事务执行的状态等
        //事务表中的id
        String transactionId = UUID.randomUUID().toString().replace("-", "");
        log.info("【发送半消息】transactionId={}", transactionId);
        TransactionSendResult result = rocketMQTemplate.sendMessageInTransaction(Tx_Charge_Group, Topic + ":" + Tag
                , MessageBuilder.withPayload(userModel).setHeader(RocketMQHeaders.TRANSACTION_ID, transactionId)
                        .build(), null);
        //发送状态
        String sendStatus = result.getSendStatus().name();
        //本地事务执行状态
        String localState = result.getLocalTransactionState().name();
        log.info("【发送半消息】sendResult={},【发送状态】sendStatus={},【本地事务执行状态】localState={}", JSON.toJSONString(userModel), sendStatus, localState);
        return result;
    }




    /**
     * 步骤四
     * 功能：消费者异常mq回调此接口手动补偿数据
     * @param productOrderNoLog 订单商品记录
     */
//    @RequestMapping("/roBackProduct")
//    public Boolean roBackProduct(@RequestBody ProductOrderNoLog productOrderNoLog){
//        log.info("roBackProduct:{}",productOrderNoLog.getOrderNo());
//        return productService.roBackProduct(productOrderNoLog);
//    }

}

