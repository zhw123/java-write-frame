package com.rocketmq.service.tx;


public class MQTransactionLog {
    private String transactionId;
    private String log;

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getLog() {
        return log;
    }
}
