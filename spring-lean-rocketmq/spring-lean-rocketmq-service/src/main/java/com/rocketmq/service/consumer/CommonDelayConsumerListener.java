package com.rocketmq.service.consumer;

import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * 延迟消息
 */
@Service
@RocketMQMessageListener(consumerGroup = "common-customer-delay-group", topic = "common_delay_topic", messageModel = MessageModel.CLUSTERING)
public class CommonDelayConsumerListener implements RocketMQListener<MessageExt> {
    @Override
    public void onMessage(MessageExt messageExt) {
        System.out.println("延迟消息-----------------" + messageExt.toString());
        //消费者处理时抛出异常时就会自动重试
//        throw new RuntimeException("消费者处理时抛出异常时就会自动重试");
    }
}
