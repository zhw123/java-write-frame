package com.rocketmq.service.consumer;

import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * 顺序信息的三种方式：异步
 * 并发消费模式（ConsumeMode.CONCURRENTLY）
 * ConsumeMode.ORDERLY:顺序消费
 */
@Service
@RocketMQMessageListener(consumerGroup = "common-customer-sync-orderly-group", topic = "sync_test_topic_orderly" ,consumeMode = ConsumeMode.ORDERLY, messageModel = MessageModel.CLUSTERING)
public class CommonSyncOrderConsumerListener implements RocketMQListener<MessageExt> {
    @Override
    public void onMessage(MessageExt messageExt) {
        System.out.println("顺序信息的三种方式：异步-----------------"+messageExt.toString());
    }
}
