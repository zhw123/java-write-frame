package com.rocketmq.service.consumer;

import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * 异步消费
 */
@Service
@RocketMQMessageListener(consumerGroup = "common-customer-async-group", topic = "common_async_topic", messageModel = MessageModel.CLUSTERING)
public class CommonConsumerAsyncMessageListener implements RocketMQListener<MessageExt> {
    @Override
    public void onMessage(MessageExt messageExt) {
        System.out.println("异步消费-----------------"+messageExt.toString());
        //消费者处理时抛出异常时就会自动重试
//        throw new RuntimeException("消费者处理时抛出异常时就会自动重试");
    }
}
