package com.lean.rocketmq.product.controller;


import cn.hutool.core.lang.Dict;
import com.lean.rocketmq.product.config.ProducerSchedule;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * http://localhost:8082/mqProducer/send?msg=hello
 * http://localhost:8082/mqProducer/push?msg=hello
 */
@RestController
@RequestMapping("/mqProducer")
public class MQProducerController {
    public static final Logger LOGGER = LoggerFactory.getLogger(MQProducerController.class);


    @Autowired
    private ProducerSchedule producerSchedule;

    @Autowired
    DefaultMQProducer defaultMQProducer;

    /**
     * 发送简单的MQ消息
     *
     * @param msg
     * @return
     */
    @GetMapping("/send")
    public String send(String msg) throws Exception {
        if (StringUtils.isEmpty(msg)) {
            return Dict.create().set("result", "success").toString();
        }
        LOGGER.info("发送MQ消息内容：" + msg);
        Message sendMsg = new Message("UserTopic", "UserTag", msg.getBytes());
        // 默认3秒超时
        SendResult sendResult = defaultMQProducer.send(sendMsg);
        LOGGER.info("消息发送响应：" + sendResult.toString());
        return Dict.create().set("result", sendResult).toString();
    }

    /**
     * 延迟消息队列
     * @throws Exception
     */
    @GetMapping("/push")
    public void pushMessageToMQ(String msg) throws Exception {
        producerSchedule.send("Topic", msg);
    }

}
