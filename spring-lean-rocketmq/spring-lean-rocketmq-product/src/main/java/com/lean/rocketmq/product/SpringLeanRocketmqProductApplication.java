package com.lean.rocketmq.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanRocketmqProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanRocketmqProductApplication.class, args);
    }

}
