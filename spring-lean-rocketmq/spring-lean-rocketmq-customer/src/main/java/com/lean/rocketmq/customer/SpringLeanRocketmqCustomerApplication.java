package com.lean.rocketmq.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanRocketmqCustomerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanRocketmqCustomerApplication.class, args);
    }

}
