package com.lean.websocketlog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsocketInfomationApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebsocketInfomationApplication.class, args);
    }

}
