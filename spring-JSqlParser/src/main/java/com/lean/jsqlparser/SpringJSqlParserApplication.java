package com.lean.jsqlparser;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan("com.lean.jsqlparser.dao")
@SpringBootApplication
public class SpringJSqlParserApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringJSqlParserApplication.class, args);
    }

}
