package com.lean.jsqlparser.dao;

import com.lean.jsqlparser.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public interface UserMapper {

    /**
     * 批量插入
     * @param list
     * @return
     */
    Integer insertBacth(List<User> list);
}
