package com.lean.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanKafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanKafkaApplication.class, args);
    }

}
