package com.lean.kafka.config;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.utils.Utils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 自定义分区
 */
public class PartitionPolicy implements Partitioner {

    private final ConcurrentMap<String, AtomicInteger> concurrentMap = new ConcurrentHashMap();

    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        List<PartitionInfo> partitions = cluster.partitionsForTopic(topic);
        int size = partitions.size();
        if (keyBytes == null) {
            int nextValue = this.nextValue(topic);
            List<PartitionInfo> partitionInfoList = cluster.availablePartitionsForTopic(topic);
            if (partitionInfoList.size() > 0) {
                return (partitionInfoList.get((Utils.toPositive(nextValue) % partitionInfoList.size()))).partition();
            } else {
                return Utils.toPositive(nextValue) % size;
            }
        } else {
            return Utils.toPositive(Utils.murmur2(keyBytes)) % size;
        }
    }


    private int nextValue(String topic) {
        AtomicInteger value = this.concurrentMap.get(topic);
        if (null == value) {
            value = new AtomicInteger(ThreadLocalRandom.current().nextInt());
            AtomicInteger currentCounter = this.concurrentMap.putIfAbsent(topic, value);
            if (currentCounter != null) {
                value = currentCounter;
            }
        }
        return value.getAndIncrement();
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> map) {

    }
}


