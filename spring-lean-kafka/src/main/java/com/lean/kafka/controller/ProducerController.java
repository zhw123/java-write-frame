package com.lean.kafka.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lean.kafka.entity.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

// https://blog.csdn.net/qq1309664161/category_11026945.html
@RestController
public class ProducerController {

    private Logger log = LoggerFactory.getLogger(ProducerController.class);

    @Autowired
    KafkaTemplate kafkaTemplate;

    @RequestMapping("/test")
    public String data() {
        String topicName = "firstTopic";
        String msg = "2023年01月05日兔年快乐！！！";
        // 通过kafka发送出去
        kafkaTemplate.send(topicName, msg);
        log.info("kafka成功发送消息给：" + topicName + "，内容为：" + msg);
        return "ok";
    }

    @RequestMapping("/test2")
    public String data2() {
        String topicName = "test-offset";
        String msg = "修改偏移量";
        // 通过kafka发送出去
        kafkaTemplate.send(topicName, msg);
        log.info("kafka成功发送消息给：" + topicName + "，内容为：" + msg);
        return "ok";
    }

    @GetMapping("/orderTest")
    public void sendMessage() {
        // 发送 1000 个消息
        try {
            for (int i = 0; i < 1000; i++) {
                long orderId = i + 1;
                String orderNum = UUID.randomUUID().toString();
                sendMessage(orderId, orderNum, LocalDateTime.now());
            }
            TimeUnit.MINUTES.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/batchMessage")
    public void batchMessage() {
        // 发送 1000 个消息
        for (int i = 0; i < 1000; i++) {
            kafkaTemplate.send("batch-topic",i+"");
        }
    }

    public void sendMessage(long orderId, String orderNum, LocalDateTime createTime) {
        // 构建一个订单类
        Order order = Order.builder().orderId(orderId).orderNum(orderNum).createTime(createTime).build();

        // 发送消息，订单类的 json 作为消息体
        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send("orderTopic", JSONObject.toJSONString(order));

        // 监听回调
        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onFailure(Throwable throwable) {
                log.info("发送失败");
            }

            @Override
            public void onSuccess(SendResult<String, String> result) {
                log.info("发送成功");
            }
        });
    }
}
