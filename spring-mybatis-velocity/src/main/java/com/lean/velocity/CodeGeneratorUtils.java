package com.lean.velocity;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.*;

/**
 * @Author zhw
 * @Description TODO
 * @Date $ $
 * @Param $
 * @return $
 **/
public class CodeGeneratorUtils {

    private static String[] scanner(String tip) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入" + tip + "：");

        String str = sc.next();
        if (str.equals("all")) {
            return null;
        } else {
            return str.split(",");
        }
    }

    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();
        String parent = "com.lean.velocity";
        String rPath = "com.lean.velocity.common.ResultVo";
        String module="/spring-mybatis-velocity";

        System.out.println("后端代码生成路径：" + parent);
        System.out.println("响应体位置：" + rPath + "\n");

        // 全局配置
        GlobalConfig gc = new GlobalConfig();

        String oPath = System.getProperty("user.dir");
        gc.setOutputDir(oPath +module+"/src/main/java");
        gc.setAuthor("zhw");
        // 当代码生成完之后是否打开代码所在的文件夹
        gc.setOpen(false);
        // 是否覆盖原来生成的
        gc.setFileOverride(false);
        // 自定义Service包名
        gc.setServiceName("%sService");
        // 生成resultMap
        gc.setBaseResultMap(true);
        // XML中生成基础列
        gc.setBaseColumnList(true);
        // 指定生成日期类型
        gc.setDateType(DateType.ONLY_DATE);
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://10.0.0.2:3306/zhw-crm?useUnicode=true&characterEncoding=utf8&useSSL=true&serverTimezone=UTC&useSSL=false");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("dxm123654");
        dsc.setDbType(DbType.MYSQL);
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setController("controller");
        pc.setEntity("model");
        pc.setMapper("dao");
        pc.setService("service");
        pc.setServiceImpl("service.impl");
        pc.setXml("mapper");
        pc.setParent(parent);
        mpg.setPackageInfo(pc);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        // 设置字段和表名的是否把下划线完成驼峰命名规则
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        // 是否启动lombok
        strategy.setEntityLombokModel(true);
        // rest注解：@RestController
        strategy.setRestControllerStyle(true);
        // 字段自动注解：@TableField
        strategy.setEntityTableFieldAnnotationEnable(true);
        // 逻辑删除
        strategy.setLogicDeleteFieldName("is_del");
        // 要设置生成哪些表 不设置就是生成所有的表
        strategy.setInclude(scanner("表名，多个表英文逗号分割，生成所有表请输入 \"all\""));
        strategy.setControllerMappingHyphenStyle(true);
        mpg.setStrategy(strategy);
        /**
         自定义配置
         */
        //配置自定义属性注入
        InjectionConfig cfg = new InjectionConfig() {
            //.vm模板中，通过${cfg.abc}获取属性
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<>();
//                map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-mp");
                map.put("abc", "自定义属性描述");
                map.put("rPath", rPath);
                this.setMap(map);
            }
        };

        TemplateConfig tc = new TemplateConfig();
        // 后端模板
        tc.setController("/templates/generator/controller.java.vm");
        tc.setService("/templates/generator/service.java.vm");
        tc.setServiceImpl("/templates/generator/serviceImpl.java.vm");
        tc.setMapper("/templates/generator/mapper.java.vm");
        // 前端模板 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig("/templates/generator/list.html.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return System.getProperty("user.dir") +module+ "/src/main/resources/templates/" + tableInfo.getEntityPath()
                        + "/list.html";
            }
        });
        focList.add(new FileOutConfig("/templates/generator/add.html.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return System.getProperty("user.dir") + module+"/src/main/resources/templates/" + tableInfo.getEntityPath()
                        + "/add.html";
            }
        });
        focList.add(new FileOutConfig("/templates/generator/edit.html.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return System.getProperty("user.dir") + module+"/src/main/resources/templates/" + tableInfo.getEntityPath()
                        + "/edit.html";
            }
        });
        cfg.setFileOutConfigList(focList);

        mpg.setCfg(cfg);
        mpg.setTemplate(tc);

        mpg.execute();

    }
}
