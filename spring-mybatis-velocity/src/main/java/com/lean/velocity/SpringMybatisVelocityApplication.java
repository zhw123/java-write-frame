package com.lean.velocity;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.lean.velocity.mapper")
@SpringBootApplication
public class SpringMybatisVelocityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMybatisVelocityApplication.class, args);
    }

}
