package com.lean.velocity.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Setter
@Getter
@Accessors(fluent = true, chain = true)
@ToString
public class ResultVo<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    // 0:success 1:failure
    private int code = 0;
    // 信息
    private String msg = "success";
    // 总记录数 200
    private long count;
    private T data;

    /**
     * 查询通用
     **/
    public ResultVo<T> data(T data, long count) {
        this.data = data;
        this.count = count;
        return this;
    }

    /**
     * 更新
     **/
    public ResultVo<T> update() {
        this.msg = "修改成功！";
        return this;
    }

    /**
     * 删除
     **/
    public ResultVo<T> delete() {
        this.msg = "删除成功！";
        return this;
    }

    /**
     * 添加
     **/
    public ResultVo<T> add() {
        this.msg = "添加成功！";
        return this;
    }

    /**
     * 成功信息
     **/
    public ResultVo<T> ok(String msg) {
        this.msg = msg;
        return this;
    }

    /**
     * 成功数据
     **/
    public ResultVo<T> data(T data) {
        this.data = data;
        return this;
    }

    /**
     * 错误信息
     **/
    public ResultVo<T> error(String msg) {
        this.code = 1;
        this.msg = msg;
        return this;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
