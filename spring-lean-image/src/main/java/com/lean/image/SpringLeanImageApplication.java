package com.lean.image;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanImageApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanImageApplication.class, args);
    }

}
