package com.lean.image.constants;

/**
 * Description 图片类型
 * ProjectName imagetool
 */
public class ImageType {

    public static String IMAGE_TYPE_PNG = "png";

    public static String IMAGE_TYPE_JPG = "jpg";

    public static String IMAGE_TYPE_WBPP = "webpp";

    public static String IMAGE_TYPE_BMP = "bmp";

    public static String IMAGE_TYPE_GIF = "gif";
}
