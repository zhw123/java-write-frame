package com.lean.image.constants;

/**
 * 常见图片尺寸
 * 数组为宽，高单位为PX
 * @author 小帅丶
 **/
public class ImageSizeConts {
    //一寸 小学报名照片、初级会计职称考试
    public static final int [] ONE_INCH={295,413};
    //大一寸 中国签证、普通话水平测试
    public static final int [] ONE_INCH_BIG={390,567};
    //二寸
    public static final int [] TWO_INCH={413,579};
    //小二寸、加拿大签证、澳大利亚签证、新加坡签证
    public static final int [] TWO_INCH_SMALL={413,531};
    //社保证
    public static final int [] SOCIAL_SECURITY_INCH={236,295};
    //日本签证
    public static final int [] JAPAN_VISA_INCH={531,531};
    //美国签证
    public static final int [] USA_VISA_INCH={600,600};
    //小一寸 驾照、驾驶证
    public static final int [] ONE_INCH_SMALL={267,378};
    //五寸
    public static final int [] FIVE_INCH={1050,1499};
    //大二寸
    public static final int [] TWO_INCH_BIG={413,626};
    //学籍照片
    public static final int [] STUDENT_STATUS_INCH={307,378};
    //学信网
    public static final int [] XUEXINWANG_INCH={480,640};
    //身份证、医保证(300dpi)
    public static final int [] IDCARD_INCH={480,640};
    //泰国签证、越南签证
    public static final int [] THAILAND_VIETNAM_VISA_INCH={472,708};
    //马来西亚签证
    public static final int [] MALAYSIA_VISA_INCH={413,590};
    //全国计算机等级考试、全国高等教育自学考试
    public static final int [] EXAMINATION_INCH={144,192};
    //全护士执业资格考试
    public static final int [] NURSE_INCH={160,210};
    //教师资格证
    public static final int [] TEACHER_INCH={360,480};

}
