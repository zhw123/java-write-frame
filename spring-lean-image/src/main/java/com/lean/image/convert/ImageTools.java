package com.lean.image.convert;

import lombok.SneakyThrows;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import sun.nio.ch.IOUtil;

import javax.swing.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ImageTools {

    private static String rootPath = System.getProperty("user.dir");

    /**
     * 创建文件路径
     * @param type 文件类型
     * @param suffix 文件后缀
     * @return
     */
    public static List<String> createFilePath(String type, String suffix) {
        List<String> res = new ArrayList();

        SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd");
        long timestamp = new Date().getTime();
        String date = f.format(timestamp);
        int randomNum = (int) (Math.random() * 900) + 100;

        String baseUrl = "/contentfile/" + type;
        baseUrl += String.format("/%s/%d%d", date, timestamp, randomNum);

        if (type != null && type.equals("image")) {
            String artwork = baseUrl + "/artwork" + suffix;
            String img1000 = baseUrl + "/1000" + suffix;
            String img400 = baseUrl + "/400" + suffix;
            String img100 = baseUrl + "/100" + suffix;
            res.add(artwork);
            res.add(img1000);
            res.add(img400);
            res.add(img100);
        } else {
            String artwork = baseUrl + suffix;
            res.add(artwork);
        }
        return res;
    }

    /**
     * 创建固定分辨率图片
     * @param pathList
     * @return
     */
    public static List<String> createImages(List<String> pathList,FileInputStream is) {
        List<String> resList = new ArrayList();
        try {
            String baseUrl = "e://files";
            ImageIcon img = new ImageIcon(baseUrl + pathList.get(0));
            int width = img.getIconWidth();
            int height = img.getIconHeight();
            resList.add(pathList.get(0));
            if (width >= 1000) {
                OutputStream os = new FileOutputStream(baseUrl + pathList.get(1));
                Thumbnails.of(is).size(1000, 1000 * height / width).toOutputStream(os);
                resList.add(pathList.get(1));
            }
            if (width >= 400) {
                OutputStream os = new FileOutputStream(baseUrl + pathList.get(2));
                Thumbnails.of(is).size(400, 400 * height / width).toOutputStream(os);
                resList.add(pathList.get(2));
            }
            if (width >= 100) {
                OutputStream os = new FileOutputStream(baseUrl + pathList.get(3));
                //按照比例进行缩小和放大
//                Thumbnails.of(fromPic).scale(0.2f).toFile(toPic);//按比例缩小
//                Thumbnails.of(fromPic).scale(2f);//按比例放大
                //图片尺寸不变，压缩图片文件大小outputQuality实现,参数1为最高质量
//                Thumbnails.of(fromPic).scale(1f).outputQuality(0.25f).toFile(toPic);
                Thumbnails.of(is).size(100, 100 * height / width).toOutputStream(os);
                resList.add(pathList.get(3));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resList;
    }

    public static void main(String[] args) throws Exception {
        File file = new File("C:\\Users\\Lenovo\\Pictures\\0a0c81de227940342b7943e0f8dd2e76.jpg");
        String fileName = file.getName();
        String suffix = fileName != null ? fileName.substring(fileName.lastIndexOf(".")).toLowerCase() : "";
        List<String> pathList = createFilePath("image",suffix);
        FileInputStream fileInputStream = new FileInputStream(file);
        List<String> images = createImages(pathList,fileInputStream);
        for (String image : images) {
            System.out.println(image);
        }
    }

}
