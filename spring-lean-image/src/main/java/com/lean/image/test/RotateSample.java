package com.lean.image.test;


import com.lean.image.util.RotateImageUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * @ClassName: RotateSample
 * @description: 图片旋转示例代码
 **/
public class RotateSample {
    public static void main(String[] args) throws  Exception {
        long start = System.currentTimeMillis();
        BufferedImage src = ImageIO.read(new File("E:\\testimg\\glassess.png"));//原图片本地路径
        BufferedImage des = RotateImageUtil.rotateImage(src,20);//旋转的角度
        ImageIO.write(des, "png", new File("E:\\testimg\\glassess2.png"));//旋转后保存的图片
        long end = System.currentTimeMillis();
        System.out.println("开始时间:" + start+ "; 结束时间:" + end+ "; 总共用时:" + (end - start) + "(ms)");
        //给图片加框测试代码
        int height = 160;
        int left = 817;
        int rotation = 13;
        int top = 180;
        int width = 159;
        String sourcePath = "F:\\testimg\\demo-card-1.jpg";
        String targetPath = "F:\\testimg\\0815.jpg";
        RotateImageUtil.addFrameImage(sourcePath, targetPath, left, top, width, height, rotation, Color.BLUE,2);
    }
}
