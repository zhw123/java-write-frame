package com.lean.image.test;

import com.lean.image.constants.ImageType;
import com.lean.image.util.ImageLosslessUtil;
import com.lean.image.util.MergeImageFontUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Description
 * ProjectName imagetool
 */

public class FontImgMergeSample {
    /** 字体 */
    private static String FONT_FAMILY = "方正粗黑宋简体";
    /** 字体大小 */
    private static Integer FONT_SIZE = 88;
    /** 颜色 */
    private static Color FONT_COLOR = new Color(255,255,255);

    public static void main(String[] args) throws Exception{

        long startTime = System.currentTimeMillis();
        //原图本地路径、保存图的本地路径
        String sourcePath = "F:\\testfile\\testimg\\banner.jpg";
        String targetPath = "F:\\testfile\\testimg\\mergebanner.jpg";

        BufferedImage image = ImageIO.read(new File(sourcePath));
        //文本
        String content = "小帅测试代码";

        Graphics2D g = (Graphics2D) image.getGraphics();
        //填充文字 从左往右
        MergeImageFontUtil.setContentToImgLR(content, g, 100, 200,FONT_FAMILY,FONT_SIZE,FONT_COLOR);

        ImageLosslessUtil.saveLossLessImageTwo(image, targetPath, ImageType.IMAGE_TYPE_JPG);

        System.out.println("总耗时"+(System.currentTimeMillis()-startTime));
    }
}
