package com.lean.image.xsexception;

/**
 * Description 自定义异常
 * ProjectName imagetool
 */
public class FileNumberException extends Throwable {
    public FileNumberException() {
    }

    public FileNumberException(String message) {
        super(message);
    }
}
