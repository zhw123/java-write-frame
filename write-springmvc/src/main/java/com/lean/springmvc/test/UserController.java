package com.lean.springmvc.test;

import com.lean.springmvc.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.List;
@Controller
public class UserController {

    @AutoWired
    private UserService userService;

    @RequestMapping("/test")
    public void findUser(HttpServletRequest request, HttpServletResponse response,String name) throws Exception{
        //处理相应的中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        List<User> users = userService.findUsers(name);
        PrintWriter out = response.getWriter();
        out.println("<h1>SpringMvc控制器" + name + "</h1>");

    }

    /**
     * 返回到一个新的页面
     * @param request
     * @param response
     * @param name
     * @return
     */
    @RequestMapping("/test1")
    public String findUser1(HttpServletRequest request, HttpServletResponse response, String name){
        response.setContentType("text/html;charset=utf-8");//处理响应的中文乱码
        String userMassage = userService.getUserMessage(name);
        request.setAttribute("userMassage",userMassage);
        //转发到user.jsp
        return "forward:/user.jsp";
    }

    /**
     * 返回一个json数据
     */
    @RequestMapping("/test2")
    @RequestBody
    public List<User> findUser2(HttpServletRequest request, HttpServletResponse response, String name){
        List<User> user = userService.findUsers(name);
        return user;
    }
}
