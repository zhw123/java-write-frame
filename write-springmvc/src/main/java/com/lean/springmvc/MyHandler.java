package com.lean.springmvc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.Method;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MyHandler {
    private String url;
    private Object controller;
    private Method method;
}
