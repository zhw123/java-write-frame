package com.lean.springmvc;


import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;

/**
 * 解析springMVC.xml
 */
public class XmlParser {
    public static String getBasePackage(String xml){
        SAXReader saxReader=new SAXReader();
        InputStream inputStream = XmlParser.class.getClassLoader().getResourceAsStream(xml);
        //xml文档对象
        try {
            Document document = saxReader.read(inputStream);
            //获取到根节点
            Element rootElement = document.getRootElement();
            //找到扫描包的配置节点component-scan
            Element componentScan = rootElement.element("component-scan");
            //去取出来属性为base-package的值
            Attribute attribute = componentScan.attribute("base-package");
            String basePackage = attribute.getText();
            return basePackage;//包得到的com.bruce.service返回
        } catch (DocumentException e) {

        }
        return "";
    }



}
