package com.lean.springmvc.annotation;

import java.lang.annotation.*;

/**
 * 自定义注解
 */
@Target(ElementType.PARAMETER)//元注解。PARAMETER表示该这个自定义注解的作用范围是在参数上
@Retention(RetentionPolicy.RUNTIME)//表示运行时这个注解起作用
@Documented//生成文档注释，可以不用加
public @interface RequestParam {
    String value() default "";//添加service注解的参数value
}
