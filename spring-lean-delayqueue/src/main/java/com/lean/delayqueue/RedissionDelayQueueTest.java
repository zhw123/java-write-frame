package com.lean.delayqueue;

import org.redisson.Redisson;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.concurrent.TimeUnit;

/**
 * 延迟队列 方案四
 */
public class RedissionDelayQueueTest {

    public static void main(String[] args) throws Exception {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://10.0.0.21:6379")
                .setPassword("xxxx")
                .setDatabase(0)
                //心跳检测，定时与redis连接，可以防止一段时间过后，与redis的连接断开
                .setPingConnectionInterval(2000);;
        RedissonClient redissonClient = Redisson.create(config);
        /**
         * 红包目标队列
         */
        RBlockingQueue<RedPacketMessage> blockingRedPacketQueue = redissonClient.getBlockingQueue("redPacketDelayQueue");
        /**
         * 定时任务将到期的元素转移到目标队列
         */
        RDelayedQueue<RedPacketMessage> delayedRedPacketQueue = redissonClient.getDelayedQueue(blockingRedPacketQueue);
        /**
         * 延时信息入队列
         */
        // 经常会出现首条消息消费不了的情况，加一个初始为0的消息。
        delayedRedPacketQueue.offer(new RedPacketMessage(0), 1, TimeUnit.SECONDS);
        // 下面是真正的业务
        delayedRedPacketQueue.offer(new RedPacketMessage(5), 5, TimeUnit.SECONDS);
        delayedRedPacketQueue.offer(new RedPacketMessage(10), 10, TimeUnit.SECONDS);
        delayedRedPacketQueue.offer(new RedPacketMessage(30), 30, TimeUnit.SECONDS);
        delayedRedPacketQueue.offer(new RedPacketMessage(50), 50, TimeUnit.SECONDS);
        System.out.println("红包ID:");
        while (true) {
            /**
             * 取出失效红包
             */
            RedPacketMessage redPacket = blockingRedPacketQueue.take();
            System.out.printf("红包ID:%s过期失效", redPacket.getRedPacketId());
            /**
             * 处理相关业务逻辑：记录相关信息并退还剩余红包金额
             */
        }

    }

}
