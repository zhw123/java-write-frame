package com.lean.delayqueue;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;

/**
 * 延迟队列
 * 方法三：
 * 使用redis的Keyspace Notifications，中文翻译就是键空间机制，
 * 就是利用该机制可以在key失效之后，提供一个回调，实际上是redis会给客户端发送一个消息。是需要redis版本2.8以上。
 * 在redis.conf中，加入一条配置
 * notify-keyspace-events Ex
 *
 * 缺点：
 *      ps:redis的pub/sub机制存在一个硬伤，官网内容如下
 *
 *      原:Because Redis Pub/Sub is fire and forget currently there is no way to use this feature if your application demands
 *      reliable notification of events, that is, if your Pub/Sub client disconnects, and reconnects later, all the events delivered
 *      during the time the client was disconnected are lost.
 *
 *      翻: Redis的发布/订阅目前是即发即弃(fire and forget)模式的，因此无法实现事件的可靠通知。也就是说，如果发布/订阅的客户端断链之后又重连，
 *      则在客户端断链期间的所有事件都丢失了。因此，该方案不是太推荐。当然，如果你对可靠性要求不高，可以使用。
 *
 * 优缺点
 *      优点:
 *          (1)由于使用Redis作为消息通道，消息都存储在Redis中。如果发送程序或者任务处理程序挂了，重启之后，还有重新处理数据的可能性。
 *          (2)做集群扩展相当方便
 *          (3)时间准确度高
 *      缺点:
 *          (1)需要额外进行redis维护
 *
 */
public class RedisDelayQueueNotifyTest {


    private static final String ADDR = "127.0.0.1";
    private static final int PORT = 6379;
    private static JedisPool jedis = new JedisPool(ADDR, PORT);
    private static RedisSub sub = new RedisSub();

    public static void init() {
        new Thread(new Runnable() {
            public void run() {
                jedis.getResource().subscribe(sub, "__keyevent@0__:expired");
            }
        }).start();
    }

    public static void main(String[] args) throws InterruptedException {
        init();
        for(int i =0;i<10;i++){
            String orderId = "OID000000"+i;
            jedis.getResource().setex(orderId, 3, orderId);
            System.out.println(System.currentTimeMillis()+"ms:"+orderId+"订单生成");
        }
    }

    static class RedisSub extends JedisPubSub {
        @Override
        public void onMessage(String channel, String message) {
            System.out.println(System.currentTimeMillis()+"ms:"+message+"订单取消");
        }
    }

}
