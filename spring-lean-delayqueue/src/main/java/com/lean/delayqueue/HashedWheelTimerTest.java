package com.lean.delayqueue;

import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.Timer;
import io.netty.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * 时间轮算法 思路 先上一张时间轮的图(640.png)
 *
 * 时间轮算法可以类比于时钟，如上图箭头（指针）按某一个方向按固定频率轮动，每一次跳动称为一个 tick。这样可以看出定时轮由个3个重要的属性参数，
 * ticksPerWheel（一轮的tick数），tickDuration（一个tick的持续时间）以及
 * timeUnit（时间单位），例如当ticksPerWheel=60，tickDuration=1，timeUnit=秒，这就和现实中的始终的秒针走动完全类似了。
 *
 * 如果当前指针指在1上面，我有一个任务需要4秒以后执行，那么这个执行的线程回调或者消息将会被放在5上。那如果需要在20秒之后执行怎么办，由于这个环形结构槽数只到8，
 * 如果要20秒，指针需要多转2圈。位置是在2圈之后的5上面（20 % 8 + 1）
 *
 *
 *优缺点
 * 优点：效率高，任务触发时间延迟时间比delayQueue低，代码复杂度比delayQueue低。
 *
 * 缺点：
 *
 * (1)服务器重启后，数据全部消失，怕宕机
 * (2)集群扩展相当麻烦
 * (3)因为内存条件限制的原因，比如下单未付款的订单数太多，那么很容易就出现OOM异常
 *
 *
 */
public class HashedWheelTimerTest {
    static class MyTimerTask implements TimerTask {
        boolean flag;

        public MyTimerTask(boolean flag) {
            this.flag = flag;
        }

        public void run(Timeout timeout) throws Exception {
            // TODO Auto-generated method stub
            System.out.println("要去数据库删除订单了。。。。");
            this.flag = false;
        }
    }

    public static void main(String[] argv) {
        MyTimerTask timerTask = new MyTimerTask(true);
        Timer timer = new HashedWheelTimer();
        timer.newTimeout(timerTask, 5, TimeUnit.SECONDS);
        int i = 1;
        while (timerTask.flag) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println(i + "秒过去了");
            i++;
        }
    }
}
