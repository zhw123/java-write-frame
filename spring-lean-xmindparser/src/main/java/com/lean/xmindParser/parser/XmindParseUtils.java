package com.lean.xmindParser.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.alibaba.fastjson.JSON;

import com.lean.xmindParser.entity.XmindRoot;
import org.dom4j.*;
import org.json.JSONObject;
import org.json.XML;

/**
 *解析xmind工具类
 */
public class XmindParseUtils {
    public static final String xmindZenJson = "content.json";
    public static final String xmindLegacyContent = "content.xml";
    public static final String xmindLegacyComments = "comments.xml";

    public static String parseText(String xmindFile) throws Exception {
        String res = ZipUtils.extract(xmindFile);
        String content = null;
        //兼容新旧版本
        if (isXmindZen(res, xmindFile)) {
            content = getXmindZenContent(xmindFile,res);
        } else {
            content = getXmindLegacyContent(xmindFile,res);
        }
        File dir = new File(res);
        deleteDir(dir);
        XmindRoot XmindRoot = JSON.parseObject(content, XmindRoot.class);
       return(JSON.toJSONString(XmindRoot,false));
    }

    public static Object parseObject(String xmindFile) throws Exception {
        String content = parseText(xmindFile);
        XmindRoot XmindRoot = JSON.parseObject(content, XmindRoot.class);
        return XmindRoot;
    }


    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            //递归删除目录中的子目录下
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }


    /**
     * @return
     */
    public static String getXmindZenContent(String xmindFile,String extractFileDir) throws Exception {
        List<String> keys = new ArrayList<>();
        keys.add(xmindZenJson);
        Map<String, String> map = ZipUtils.getContents(keys, xmindFile,extractFileDir);
        String content = map.get(xmindZenJson);
        content = XmindZenUtils.getContent(content);
        return content;
    }

    /**
     * @return
     */
    public static String getXmindLegacyContent(String xmindFile,String extractFileDir) throws Exception {
        List<String> keys = new ArrayList<>();
        keys.add(xmindLegacyContent);
        keys.add(xmindLegacyComments);
        Map<String, String> map = ZipUtils.getContents(keys, xmindFile,extractFileDir);

        String contentXml = map.get(xmindLegacyContent);
        String commentsXml = map.get(xmindLegacyComments);
        String xmlContent = getContent(contentXml, commentsXml);

        return xmlContent;
    }

    private static boolean isXmindZen(String res, String xmindFile){
        //解压
        File parent = new File(res);
        if (parent.isDirectory()) {
            String[] files = parent.list(new ZipUtils.FileFilter());
            for (int i = 0; i < Objects.requireNonNull(files).length; i++) {
                if (files[i].equals(xmindZenJson)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getContent(String xmlContent, String xmlComments) throws Exception {
        //删除content.xml里面不能识别的字符串
        xmlContent = xmlContent.replace("xmlns=\"urn:xmind:xmap:xmlns:content:2.0\"", "");
        xmlContent = xmlContent.replace("xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"", "");
        //删除<topic>节点
        xmlContent = xmlContent.replace("<topics type=\"attached\">", "");
        xmlContent = xmlContent.replace("</topics>", "");

        //去除title中svg:width属性
        xmlContent = xmlContent.replaceAll("<title svg:width=\"[0-9]*\">", "<title>");

        Document document = DocumentHelper.parseText(xmlContent);// 读取XML文件,获得document对象
        Element root = document.getRootElement();
        List<Node> topics = root.selectNodes("//topic");

        if (xmlComments != null) {
            //删除comments.xml里面不能识别的字符串
            xmlComments = xmlComments.replace("xmlns=\"urn:xmind:xmap:xmlns:comments:2.0\"", "");
            Document commentDocument = DocumentHelper.parseText(xmlComments);
            List<Node> commentsList = commentDocument.selectNodes("//comment");
            for (Node topic : topics) {
                for (Node commentNode : commentsList) {
                    Element commentElement = (Element) commentNode;
                    Element topicElement = (Element) topic;
                    if (topicElement.attribute("id").getValue().equals(commentElement.attribute("object-id").getValue())) {
                        Element comment = topicElement.addElement("comments");
                        comment.addAttribute("creationTime", commentElement.attribute("time").getValue());
                        comment.addAttribute("author", commentElement.attribute("author").getValue());
                        comment.addAttribute("content", commentElement.element("content").getText());
                    }
                }

            }
        }
        Node rootTopic = root.selectSingleNode("/xmap-content/sheet/topic");
        rootTopic.setName("rootTopic");
        List<Node> topicList = rootTopic.selectNodes("//topic");
        for (Node node : topicList) {
            node.setName("attached");
        }
        Element sheet = root.elements("sheet").get(0);
        String res = sheet.asXML();
        JSONObject xmlJSONObj = XML.toJSONObject(res);
        JSONObject jsonObject = xmlJSONObj.getJSONObject("sheet");
        //设置缩进
        return jsonObject.toString(4);
    }
}
