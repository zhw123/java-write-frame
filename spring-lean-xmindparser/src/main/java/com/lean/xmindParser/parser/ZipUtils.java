package com.lean.xmindParser.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.examples.Expander;

/**
 *
 * @Classname ZipUtil
 * @Description zip解压工具
 */
public class ZipUtils {

    private static final String currentPath = System.getProperty("user.dir");

    public static Map<String,String> getContents(List<String> subFileNames, String fileName,String extractFileDir) throws Exception {
        String destFilePath =extractFileDir;
        Map<String,String> map = new HashMap<>();
        File destFile = new File(destFilePath);
        if (destFile.isDirectory()) {
            String[] res = destFile.list(new FileFilter());
            for (int i = 0; i < Objects.requireNonNull(res).length; i++) {
                if (subFileNames.contains(res[i])) {
                    String s = destFilePath + File.separator + res[i];
                    String content = getFileContent(s);
                    map.put(res[i], content);
                }
            }
        }
        return map;
    }

    public static String extract(String fileName) throws IOException, ArchiveException {
        File file = new File(fileName);
        Expander expander = new Expander();
        String destFileName =currentPath +File.separator+ "XMind"+System.currentTimeMillis();
        expander.expand(file, new File(destFileName));
        return destFileName;
    }

    static class FileFilter implements FilenameFilter {
        @Override
        public boolean accept(File dir, String name) {
            if (name.endsWith(".xml") || name.endsWith(".json")) {
                return true;
            }
            return false;
        }
    }

    public static String getFileContent(String fileName) throws IOException {
        File file;
        try {
            file = new File(fileName);
        } catch (Exception e) {
            throw new RuntimeException("找不到该文件");
        }
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReder = new BufferedReader(fileReader);
        StringBuilder stringBuffer = new StringBuilder();
        while (bufferedReder.ready()) {
            stringBuffer.append(bufferedReder.readLine());
        }
        bufferedReder.close();
        fileReader.close();
        return stringBuffer.toString();
    }
}
