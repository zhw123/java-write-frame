package com.lean.xmindParser;

import com.lean.xmindParser.parser.XmindParseUtils;

/**
 *
 * @Classname Example
 * @Description 测试例子
 */
public class Example {
    public static void main(String[] args) throws Exception {
        String fileName = "E:\\spring-lean-xmindparser\\doc\\高等数学·上.xmind";
        String res = XmindParseUtils.parseText(fileName);
        System.out.println(res);
    }
}
