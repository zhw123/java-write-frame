package com.lean.xmindParser.entity;
import lombok.Data;
import java.util.List;

@Data
public class TopicText {
    private String id;
    private String title;
    private Notes notes;
    private List<Comments> comments;
    private Children children;
    private List<String> labels;
}
