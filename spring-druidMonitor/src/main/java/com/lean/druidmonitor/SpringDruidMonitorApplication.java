package com.lean.druidmonitor;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan("com.lean.druidmonitor.dao")
@SpringBootApplication
public class SpringDruidMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDruidMonitorApplication.class, args);
    }

}
