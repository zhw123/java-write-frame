package com.lean.mongodb.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * @ClassName: AutoIncKey
 * @Description: 用于标识需要自增的Field
 * @Author: zhanghongwei
 * @Date: 2022/6/21 20:37
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AutoIncKey {}
