package com.lean.mongodb.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @ClassName: IncInfo
 * @Description: d的自增进度需要一个数据表存储(只是存储当前id号数据，可以不用数据库而用其他形式)
 * @Author: zhanghongwei
 * @Date: 2022/6/21 20:37
 */
@Document(collection = "inc")
@Data
public class IncInfo {

    @Id
    private String id;// 主键

    @Field
    private String collName;// 需要自增id的集合名称(这里设置为MyDomain)

    @Field
    private Integer incId;// 当前自增id值

}
