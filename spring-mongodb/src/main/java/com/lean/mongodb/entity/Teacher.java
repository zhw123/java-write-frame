package com.lean.mongodb.entity;

import com.lean.mongodb.annotation.AutoIncKey;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * @ClassName: Teacher
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/6/21 20:39
 */
@Data
public class Teacher {
    @Id
    @AutoIncKey
    private Integer id=0;

    private String username;
}
