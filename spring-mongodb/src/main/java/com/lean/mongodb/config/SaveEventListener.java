package com.lean.mongodb.config;

import java.lang.reflect.Field;

import com.lean.mongodb.annotation.AutoIncKey;
import com.lean.mongodb.entity.IncInfo;
import com.lean.mongodb.entity.Teacher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

/**
 * @ClassName: SaveEventListener
 * @Description: 自定义id 从1开始自增
 * @Author: zhanghongwei
 * @Date: 2022/6/21 20:38
 */
@Component
public class SaveEventListener extends AbstractMongoEventListener<Object> {
    private static final Logger logger = LoggerFactory.getLogger(SaveEventListener.class);
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void onBeforeConvert(BeforeConvertEvent<Object> event) {
        logger.info(event.getSource().toString());
        Teacher teacher = (Teacher) event.getSource();
        if (teacher != null) {
            ReflectionUtils.doWithFields(teacher.getClass(), new ReflectionUtils.FieldCallback() {
                public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                    ReflectionUtils.makeAccessible(field);
                    // 如果字段添加了我们自定义的AutoIncKey注解
                    if (field.isAnnotationPresent(AutoIncKey.class)) {
                        // 设置自增ID
                        field.set(teacher, getNextId(teacher.getClass().getSimpleName()));
                    }
                }
            });
        }
    }
    private Integer getNextId(String collName) {
        Query query = new Query(Criteria.where("collName").is(collName));
        Update update = new Update();
        update.inc("incId", 1);
        FindAndModifyOptions options = new FindAndModifyOptions();
        options.upsert(true);
        options.returnNew(true);
        IncInfo inc = mongoTemplate.findAndModify(query, update, options, IncInfo.class);
        return inc.getIncId();
    }
}
