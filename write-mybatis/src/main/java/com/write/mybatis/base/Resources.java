package com.write.mybatis.base;

import java.io.InputStream;

public class Resources {

    /**
     * 用于加载 .xml文件，获取一个流对象
     *
     * @param xmlPath
     * @return
     */
    public static InputStream getResourceAsStream(String xmlPath) {
        return Resources.class.getClassLoader().getResourceAsStream(xmlPath);
    }
}
