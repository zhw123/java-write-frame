package com.write.mybatis.base;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;

public class DefaultSqlSession implements SqlSession {

    private Configuration cfg;

    private Connection con;

    public void setCfg(Configuration cfg) {
        this.cfg = cfg;
    }

    public Connection getConnection() {
        try {
            con = DataSourceUtil.getConnection(cfg);
            return con;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    // 开始会话
    public void begin() {
        try {
            con.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // 提交会话
    public void commit() {
        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // 回滚会话
    public void rollback() {
        try {
            con.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 动态代理 生成需要的Mapper
     *
     * @param daoClass
     * @param <T>
     * @return
     */
    @Override
    public <T> T getMapper(Class<T> daoClass) {
        Connection con = getConnection();
        // 动态代理生成对象
        T daoProxy = (T)Proxy.newProxyInstance(daoClass.getClassLoader(), new Class[] {daoClass}, new MapperProxyFactory(con, cfg.getMapperMap()));
        return daoProxy;
    }

    /**
     * 释放资源
     */
    @Override
    public void close() {
        try {
            System.out.println(con);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
