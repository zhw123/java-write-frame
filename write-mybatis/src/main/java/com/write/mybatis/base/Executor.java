package com.write.mybatis.base;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Executor {

    /**
     * 查询语句执行器，同时解析结果并返回
     *
     * @param mapper
     * @param con
     * @param <E>
     * @return
     */
    public <E> List<E> selectList(Mapper mapper, Connection con, Object[] args) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            // 读取mapper数据
            String queryString = mapper.getQueryString(); // 获取sql查询语句
            String resultType = mapper.getResultType(); // 全限定类名
            Class typeClass = Class.forName(resultType);
            // 获取PrepareStatement对象
            statement = con.prepareStatement(queryString);
            if (args != null) {
                for (int i = 0; i < args.length; i++) {// 替换占位符的参数
                    statement.setObject(i + 1, args[i]);
                }
            }
            // 执行sql语句
            resultSet = statement.executeQuery();
            // 解析数据
            List<E> list = new ArrayList<>();
            while (resultSet.next()) {
                // 通过反射，创建结果实体对象
                E obj = (E)typeClass.newInstance();
                // 获取结果集合的元数据
                ResultSetMetaData metaData = resultSet.getMetaData();
                int count = metaData.getColumnCount(); // 获取数据列数
                // 循环遍历每列数据(下标从1开始)
                for (int i = 1; i <= count; i++) {
                    // 获取指定列的名字
                    String name = metaData.getColumnName(i);
                    // 根据列名获取对应列的值
                    Object columnValue = resultSet.getObject(name);
                    // java内省机制，通过反射获取javabean的setting方法并通过method执行
                    PropertyDescriptor pd = new PropertyDescriptor(name, typeClass);
                    Method method = pd.getWriteMethod();
                    method.invoke(obj, columnValue);
                }
                // 数据添加入list
                list.add(obj);
            }
            // 返回解析结果
            return list;
        } catch (Exception e) {
            throw new RuntimeException();
        } finally {
            // 释放资源
            release(statement, resultSet);
        }
    }

    public int insert(Mapper mapper, Connection con, Object[] args) {
        PreparedStatement statement = null;
        try {
            String useGeneratedKeys = mapper.getUseGeneratedKeys();
            if ("true".equalsIgnoreCase(useGeneratedKeys)) {
                statement = con.prepareStatement(mapper.getQueryString(), Statement.RETURN_GENERATED_KEYS);// 根据sql得到预编译语句
            } else {
                statement = con.prepareStatement(mapper.getQueryString());// 根据sql得到预编译语句
            }
            Class<?> clazz = args[0].getClass();
            Field[] fields = clazz.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                fields[i].setAccessible(true);
                statement.setObject(i + 1, fields[i].get(args[0]));
            }
            int executeUpdate = statement.executeUpdate();
            if ("true".equalsIgnoreCase(useGeneratedKeys)) {
                // 返回自增ID
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    Field f = clazz.getDeclaredField(mapper.getKeyColumn());
                    f.setAccessible(true);
                    f.set(args[0], generatedKeys.getInt(1));
                }
            }
            return executeUpdate;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 释放资源
            release(statement,null);
        }
        return 0;
    }

    public int update(Mapper mapper, Connection con, Object[] args) {
        PreparedStatement statement = null;
        try {
            String mapperQueryString = mapper.getQueryString();
            statement = con.prepareStatement(parse(mapperQueryString));// 根据sql得到预编译语句
            for (int i = 0; i < args.length; i++) {
                statement.setObject(i + 1, args[i]);
            }
            return statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 释放资源
            release(statement,null);
        }
        return 0;
    }

    public int delete(Mapper mapper, Connection con, Object[] args) {
        PreparedStatement statement = null;
        try {
            String mapperQueryString = mapper.getQueryString();
             statement = con.prepareStatement(parse(mapperQueryString));// 根据sql得到预编译语句
            for (int i = 0; i < args.length; i++) {
                statement.setObject(i + 1, args[i]);
            }
            return statement.executeUpdate();
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 释放资源
            release(statement,null);
        }
        return 0;
    }

    /**
     * 释放资源
     *
     * @param ps
     * @param rs
     */
    private void release(PreparedStatement ps, ResultSet rs) {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        String parse = parse("update t_user set username = #{username} where id = #{id}");
        System.out.println(parse);
    }

    /**
     * #{} 替换成 ？
     * @param text
     * @return
     */
    public static String parse(String text) {
        String openToken = "#{";
        String closeToken = "}";
        if (text == null || text.isEmpty()) {
            return "";
        }
        int start = text.indexOf(openToken);
        if (start == -1) {
            return text;
        }
        // 将text划分为 字符数组
        char[] src = text.toCharArray();
        int offset = 0;
        final StringBuilder builder = new StringBuilder();
        do {
            if (start > 0 && src[start - 1] == '\\') {
                builder.append(src, offset, start - offset - 1).append(openToken);
                offset = start + openToken.length();
            } else {
                builder.append(src, offset, start - offset);
                offset = start + openToken.length();// 定位到参数的开始位置
                int end = text.indexOf(closeToken, offset);
                while (end > -1) {
                    if (end > offset && src[end - 1] == '\\') {
                        offset = end + closeToken.length();
                        end = text.indexOf(closeToken, offset);
                    } else {
                        break;
                    }
                }
                if (end == -1) {
                    builder.append(src, start, src.length - start);
                    offset = src.length;
                } else {
                    builder.append("?");
                    offset = end + closeToken.length();
                }
            }
            start = text.indexOf(openToken, offset);
        } while (start > -1);
        if (offset < src.length) {
            builder.append(src, offset, src.length - offset);
        }
        return builder.toString();
    }
}
