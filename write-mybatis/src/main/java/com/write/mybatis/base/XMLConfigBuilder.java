package com.write.mybatis.base;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XMLConfigBuilder {

    /**
     * 解析主配置文件，把里面的内容填充到 DefaultSqlSession 所需要的地方
     *
     * @param session
     * @param input
     */
    public static void loadConfiguration(DefaultSqlSession session, InputStream input) {
        try {
            // 定义封装连接信息的配置对象（mybatis的配置对象）
            Configuration cfg = new Configuration();
            // 获取 SAXReader 对象
            SAXReader reader = new SAXReader();
            // 根据字节输入流获取 Document 对象
            Document document = reader.read(input);
            // 获取根节点
            Element root = document.getRootElement();
            // 使用 xpath 中选择指定节点的方式，遍历获取所有 property 节点
            List<Element> propertyElements = root.selectNodes("//property");
            for (Element propertyElement : propertyElements) {
                // 判断节点是连接数据库的哪部分信息，取出 name 属性的值
                String name = propertyElement.attributeValue("name");
                if ("driver".equals(name)) {
                    // 表示驱动
                    String driver = propertyElement.attributeValue("value");
                    cfg.setDriver(driver);
                }
                if ("url".equals(name)) {
                    // 表示连接字符串
                    String url = propertyElement.attributeValue("value");
                    cfg.setUrl(url);
                }
                if ("username".equals(name)) {
                    // 表示用户名
                    String username = propertyElement.attributeValue("value");
                    cfg.setUsername(username);
                }
                if ("password".equals(name)) {
                    // 表示密码
                    String password = propertyElement.attributeValue("value");
                    cfg.setPassword(password);
                }
            }

            List mappersData = root.elements("mapper");// 获得mapper标签集合
            List<String> mapperPaths = new ArrayList<>();
            for (Object element : mappersData) {// 遍历标签得到里面的dao.xml文件的映射路径
                Element mapper = (Element)element;
                mapperPaths.add(mapper.getTextTrim());// 存储路径
            }

            System.out.println(mapperPaths);

            // 取出 mappers中的所有 mapper 标签，判断他们使用了 resource 还是 class 属性
            List<Element> mapperElements = root.selectNodes("//mappers/mapper");
            // 遍历集合
            for (Element mapperElement : mapperElements) {
                // 判断 mapperElement 使用的是哪个属性
                Attribute attribute = mapperElement.attribute("resource");
                if (attribute != null) {
                    // System.out.println("使用的是 XML");
                    String mapperPath = attribute.getValue();
                    // 把映射配置文件的内容获取出来，封装成一个 map，给 configuration 中的 mappers 赋值
                    Map<String, Mapper> mappers = loadMapperConfiguration(mapperPath);
                    cfg.setMapperMap(mappers);
                } else {
                    // System.out.println("使用的是注解");
                    String daoClassPath = mapperElement.attributeValue("class");
                    // 根据 daoClassPath 获取封装的必要信息，给 configuration 中的 mappers 赋值
                    Map<String, Mapper> mappers = loadMapperAnnotation(daoClassPath);
                    cfg.setMapperMap(mappers);
                }
            }
            // 把配置对象传递给 DefaultSqlSession
            session.setCfg(cfg);
        } catch (DocumentException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 根据 注释传入的参数，解析 XML，并且封装到 Map 中
     *
     * @param daoClassPath
     * @return
     */
    private static Map<String, Mapper> loadMapperAnnotation(String daoClassPath) throws ClassNotFoundException {
        // 定义返回值对象
        Map<String, Mapper> mappers = new HashMap<String, Mapper>();
        // 得到 dao 接口的字节码对象
        Class daoClass = Class.forName(daoClassPath);
        // 得到 dao 接口中的方法数组
        Method[] methods = daoClass.getMethods();
        // 遍历 Method 数组
        for (Method method : methods) {
            // 取出每一个方法，判断是否有 select 注解
            if (method.isAnnotationPresent(Select.class)) {
                // 创建 Mapper 对象
                Mapper mapper = new Mapper();
                // 取出注解的 value 属性值
                Select selectAnno = method.getAnnotation(Select.class);
                String queryString = selectAnno.value();
                mapper.setQueryString(queryString);
                // 获取当前方法的返回值，还要求必须带有泛型信息
                Type type = method.getGenericReturnType();// List<User>
                // 判断 type 是不是参数化的类型
                if (type instanceof ParameterizedType) {
                    // 强转
                    ParameterizedType ptype = (ParameterizedType)type;
                    // 得到参数化类型中的实际类型参数
                    Type[] types = ptype.getActualTypeArguments();
                    // 取出第一个
                    Class domainClass = (Class)types[0];
                    // 获取 domainClass 的类名
                    String resultType = domainClass.getName();
                    // 给 Mapper 赋值
                    mapper.setResultType(resultType);
                }
                // 组装 key 的信息
                // 获取方法的名称
                String methodName = method.getName();
                String className = method.getDeclaringClass().getName();
                String key = className + "." + methodName;
                // 给 map 赋值
                mappers.put(key, mapper);
            }
        }
        return mappers;
    }

    /**
     * 根据 xml文件传入的参数，解析 XML，并且封装到 Map 中
     *
     * @param mapperPath
     * @return
     */
    private static Map<String, Mapper> loadMapperConfiguration(String mapperPath) {
        InputStream in = null;
        try {
            // 定义返回值对象
            Map<String, Mapper> mappers = new HashMap();
            // 根据路径获取字节输入流
            in = Resources.getResourceAsStream(mapperPath);
            // 根据字节输入流获取 Document 对象
            SAXReader reader = new SAXReader();
            Document document = reader.read(in);
            // 获取根节点,获取根节点的 namespace 属性取值
            Element root = document.getRootElement();
            String namespace = root.attributeValue("namespace");// 是组成 map 中 key 的部分
            // 获取所有的 select 节点
            List<Element> selectElements = root.selectNodes("//select");
            List<Element> insertElements = root.selectNodes("//insert");
            List<Element> updateElements = root.selectNodes("//update");
            List<Element> deleteElements = root.selectNodes("//delete");
            List<Element> allElements= new ArrayList<>();
            allElements.addAll(selectElements);
            allElements.addAll(insertElements);
            allElements.addAll(updateElements);
            allElements.addAll(deleteElements);
            // 遍历 select 节点集合
            String paramType = null;
            for (Element selectElement : allElements) {
                String type = selectElement.getName();// 获取到标签的名字，也就是SQL语句的类型
                String id = selectElement.attributeValue("id");
                String useGeneratedKeys = selectElement.attributeValue("useGeneratedKeys");
                String keyProperty = selectElement.attributeValue("keyProperty");
                String keyColumn = selectElement.attributeValue("keyColumn");
                String resultType = selectElement.attributeValue("resultType");
                if (selectElement.attribute("parameterType") != null) {
                    paramType = selectElement.attribute("parameterType").getValue();// 获取返回值类型
                }
                // 取出文本内容 组成 map 中的SQL语句
                String querySql = selectElement.getText();
                // 创建 Key
                String key = namespace + "." + id;
                if(mappers.containsKey(key)){
                    throw new RuntimeException("xml存在同一个key");
                }
                // 创建 Value
                Mapper mapper = new Mapper();
                mapper.setType(type);
                mapper.setParamType(paramType);
                mapper.setQueryString(querySql.trim());
                mapper.setResultType(resultType);
                mapper.setUseGeneratedKeys(useGeneratedKeys);
                mapper.setKeyColumn(keyColumn);
                mapper.setKeyProperty(keyProperty);
                // 把 key 和 value 存入 mappers 中
                mappers.put(key, mapper);
            }
            return mappers;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
