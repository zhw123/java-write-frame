package com.lean.look;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanLookApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanLookApplication.class, args);
    }

}
