package com.lean.geneate.layui.engine.impl;

import com.lean.geneate.layui.bean.GlobleConfig;
import com.lean.geneate.layui.factory.ClassInfoFactory;
import com.lean.geneate.layui.intercept.CustomEngine;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.util.Set;

/**
 * ******************************
 * description:  CustomEngineImpl 基于自定义拦截接口的方法调用
 *               基于org.reflections进行全量文件接口扫描
 * ******************************
 */
public final class CustomEngineImpl {

    /***
     * 扫描全包获取 实现CustomEngine接口的类
     */
    private static Set<Class<? extends CustomEngine>> toDos () {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage(""))
                .filterInputsBy(input -> {
                    assert input != null;
                    return input.endsWith(".class");
                }));

        return reflections.getSubTypesOf(CustomEngine.class);
    }

    public static void handleCustom() {
        Set<Class<? extends CustomEngine>> classes = toDos();
        for (Class<? extends CustomEngine> aClass : classes) {
            // 基于配置项检测是否需要启用自定义实现类
            if("*;".equals(GlobleConfig.getGlobleConfig().getCustomHandleInclude()) ||
                    GlobleConfig.getGlobleConfig().getCustomHandleIncludeMap().containsKey(aClass.getSimpleName())) {
                try {
                    // 基于反射构建对象 - 调用handle方法
                    CustomEngine engine = aClass.newInstance();
                    engine.handle(GlobleConfig.getGlobleConfig(), ClassInfoFactory.getClassInfoList());
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
