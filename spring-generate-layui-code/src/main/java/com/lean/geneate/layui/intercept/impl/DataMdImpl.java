package com.lean.geneate.layui.intercept.impl;

import com.lean.geneate.layui.bean.ClassInfo;
import com.lean.geneate.layui.bean.ConfigurationInfo;
import com.lean.geneate.layui.intercept.CustomEngine;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ******************************
 * description:  表结构Impl
 * ******************************
 */
public class DataMdImpl implements CustomEngine {

    @Override
    public void handle(ConfigurationInfo config, List<ClassInfo> classInfos) {
        Map<String, Object> params = new HashMap<>();
        params.put("config"    , config);
        params.put("classInfos", classInfos);

        // 构建文件地址
        String filePath = config.getProjectPath() + SRC_MAIN_RESOURCE + SPACER + "DataSource.md";

        try {
            this.execute(params, "code-generator/markdown-file/DataMd.ftl", filePath);
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }

        logger.info("=== Data Sql Md Part Build Complete.=== ");
    }

    private static final String SPACER = File.separator;

    private static final String SRC_MAIN_RESOURCE = SPACER + "src" + SPACER + "main" + SPACER + "resources" + SPACER;

    private static Logger logger = LoggerFactory.getLogger(DataMdImpl.class);
}
