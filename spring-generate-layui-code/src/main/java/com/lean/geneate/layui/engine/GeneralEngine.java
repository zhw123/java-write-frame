package com.lean.geneate.layui.engine;

import com.lean.geneate.layui.bean.ClassInfo;

/**
 * ******************************
 * description:  具体涉及GeneralEngine
 * ******************************
 */
public interface GeneralEngine {

    /***
     * 执行器执行
     */
    void execute();

    /**
     * 生成固有的文件
     */
    void genFix();

    /**
     * 生成控制层 GeneralEngine
     */
    void genController(ClassInfo classInfo);

    /**
     * 生成业务层(
     */
    void genService(ClassInfo classInfo);

    /**
     * 生成持久层类
     */
    void genRepositoryClass(ClassInfo classInfo);

    /**
     * 生成持久层xml
     */
    void genRepositoryXml(ClassInfo classInfo);

    /**
     * 生成实体
     */
    void genEntity(ClassInfo classInfo);

    /**
     * 生成配置文件(包括pom.xml,application.yml,application.properties)
     */
    void genConfig();
}
