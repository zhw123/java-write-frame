/*
 Navicat Premium Data Transfer

 Source Server         : 81.69.24.201
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : 81.69.24.201:3306
 Source Schema         : logo_cms

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 23/12/2022 15:46:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for crawler_article
-- ----------------------------
DROP TABLE IF EXISTS `crawler_article`;
CREATE TABLE `crawler_article`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `source_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '原文地址',
  `publish_date` datetime NULL DEFAULT NULL COMMENT '发布时间',
  `author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '爬取文章' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of crawler_article
-- ----------------------------

-- ----------------------------
-- Table structure for logo_cms_adver
-- ----------------------------
DROP TABLE IF EXISTS `logo_cms_adver`;
CREATE TABLE `logo_cms_adver`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `adver_anme` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `adver_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `adver_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` int(1) NULL DEFAULT 1 COMMENT '1-首页 2-导航页 3-内页',
  `sort` int(1) NULL DEFAULT 0,
  `start_time` datetime NULL DEFAULT NULL,
  `end_time` datetime NULL DEFAULT NULL,
  `is_delete` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '广告' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logo_cms_adver
-- ----------------------------
INSERT INTO `logo_cms_adver` VALUES (1, 'logo设计_商标设计_logo在线生成_logo免费制作_标志社', 'http://www.biaozhishe.com', 'http://logo-file.biaozhishe.com/file/img/one.jpg', 1, 1, '2020-11-05 23:33:55', '2022-03-03 23:34:01', 0);
INSERT INTO `logo_cms_adver` VALUES (2, 'logo设计_商标设计_logo在线生成_logo免费制作_标志社', 'http://www.biaozhishe.com', 'http://logo-file.biaozhishe.com/file/img/two.jpg', 2, 1, '2020-11-05 23:33:55', '2022-03-03 23:34:01', 0);
INSERT INTO `logo_cms_adver` VALUES (3, 'logo设计_商标设计_logo在线生成_logo免费制作_标志社', 'http://www.biaozhishe.com', 'http://logo-file.biaozhishe.com/file/img/one.jpg', 2, 2, '2020-11-05 23:33:55', '2022-03-03 23:34:01', 0);
INSERT INTO `logo_cms_adver` VALUES (4, 'logo设计_商标设计_logo在线生成_logo免费制作_标志社', 'http://www.biaozhishe.com', 'http://logo-file.biaozhishe.com/file/img/first.jpg', 3, 1, '2020-11-05 23:33:55', '2022-03-03 23:34:01', 0);
INSERT INTO `logo_cms_adver` VALUES (5, 'logo设计_商标设计_logo在线生成_logo免费制作_标志社', 'http://www.biaozhishe.com', 'http://logo-file.biaozhishe.com/file/img/two.jpg', 3, 2, '2020-11-05 23:33:55', '2022-03-03 23:34:01', 0);
INSERT INTO `logo_cms_adver` VALUES (6, 'logo设计_商标设计_logo在线生成_logo免费制作_标志社', 'http://www.biaozhishe.com', 'http://logo-file.biaozhishe.com/file/img/two.jpg', 3, 3, '2020-11-05 23:33:55', '2022-03-03 23:34:01', 0);
INSERT INTO `logo_cms_adver` VALUES (7, 'logo设计_商标设计_logo在线生成_logo免费制作_标志社', 'http://www.biaozhishe.com', 'http://logo-file.biaozhishe.com/file/img/two.jpg', 3, 4, '2020-11-05 23:33:55', '2022-03-03 23:34:01', 0);
INSERT INTO `logo_cms_adver` VALUES (8, 'logo设计_商标设计_logo在线生成_logo免费制作_标志社', 'http://www.biaozhishe.com', 'http://logo-file.biaozhishe.com/file/img/one.jpg', 5, 5, '2020-11-05 23:33:55', '2022-03-03 23:34:01', 0);
INSERT INTO `logo_cms_adver` VALUES (9, 'logo设计_商标设计_logo在线生成_logo免费制作_标志社', 'http://www.biaozhishe.com', 'http://logo-file.biaozhishe.com/file/img/two.jpg', 1, 2, '2020-11-05 23:33:55', '2022-03-03 23:34:01', 0);

-- ----------------------------
-- Table structure for logo_cms_boke
-- ----------------------------
DROP TABLE IF EXISTS `logo_cms_boke`;
CREATE TABLE `logo_cms_boke`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '描述(简介)',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `key_words` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关键字',
  `title_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题图',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `read_num` int(32) NULL DEFAULT 0 COMMENT '点击量',
  `is_top` int(1) NULL DEFAULT 0 COMMENT '是否置顶0否 1是',
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '开始时间',
  `end_time` timestamp NULL DEFAULT NULL COMMENT '结束时间',
  `is_push` int(1) NULL DEFAULT 0 COMMENT '是否推送百度0否1是',
  `nav_id` int(11) NULL DEFAULT NULL COMMENT '导航id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `title`(`title`, `is_top`, `start_time`) USING BTREE,
  INDEX `is_top`(`is_top`, `nav_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '博客表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logo_cms_boke
-- ----------------------------
INSERT INTO `logo_cms_boke` VALUES (1, 'Word中如何手动制作目录 手动目录的2种方法', 'Word中如何手动制作目录 手动目录的2种方法', ' <p>\r\n	&nbsp;</p>\r\n<p>\r\n	<img alt=\"Word中如何手动制作目录\" src=\"/uploads/allimg/c200330/15U55T63U550-91209.gif\"></p>\r\n<p>\r\n	通常情况下，我们用Word制作目录，都是根据内容自动生成出来的。不过根据不同的需求，也有许多人希望能够手动制作目录，不要自动生成的。</p>\r\n<p>\r\n	当然，各有各的优势，自动生成目录不用我们操心，软件会自动给我们生成完美的目录，不过限制也很多，比如：样式方面，就不能随心所欲！手动目录的话就比较灵活一些，就是在制作的过程中比较麻烦。今天，易老师主要讲解一下手动制作目录两种方法！<br>\r\n	&nbsp;</p>\r\n<h2>\r\n	方法一、直接插入</h2>\r\n<p>\r\n	直接【引用】-【目录】-【手动目录】即可。<br>\r\n	&nbsp;</p>\r\n<h2>\r\n	方法二、纯手工制作目录</h2>\r\n<p>\r\n	步骤一、先看一下自己目录的级别哈，这里给大家演示，我准备了2级，按住Ctrl然后选中所有的2级内容，缩一下。然后就是添加页数，也就是本章或本节的内容所在的页面是多少页。</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	步骤二、每一个标题与页数之间按一下【Tab】键。</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	步骤三、选中所有标题，【段落】-【制表位】，这个位置自己看一下，需要设置多少合适，也就是页数最终会显示在标尺的哪个地方。然后，右对齐，选中一种前导符。</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', NULL, 'http://cyword.rywxw.com/uploads/allimg/c200330/15U55TA46210-143O0.gif', '2020-11-04 21:02:07', NULL, '2020-11-07 00:42:47', NULL, 51, 1, '2020-11-04 21:02:07', NULL, 0, 2);
INSERT INTO `logo_cms_boke` VALUES (2, '如何在Word2010中输入商标、版权符号', '在不同领域，不同用户有不同的需求。有些用户在编辑Word文档时，偶尔会需要输入一些特殊的符号，诸如商标符号、注册符号、版权符号。介于这些符号不是一般的常用符号，所以一些...', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	在不同领域，不同用户有不同的需求。有些用户在编辑Word文档时，偶尔会需要输入一些特殊的符号，诸如商标符号、注册符号、版权符号。介于这些符号不是一般的常用符号，所以一些用户在真正需要它的时候不知道怎么输入它，下面小编就教教大家如何攻破这个“疑难杂症”。<br>\r\n	<br>\r\n	方法一、利用快捷键<br>\r\n	<br>\r\n	首先在Word文档中将光标定位到需要插入特殊符号的位置上。如果要输入注册符号，则按键盘上的【Ctrl+Ait+R】组合键即可输入注册符号；如果要输入商标符号，则按键盘上的【Ctrl+Ait+T】组合键即可输入商标符号；如果要输入版权符号，则按键盘上的【Ctrl+Ait+C】组合键即可输入版权所有符号。<br>\r\n	<br>\r\n	<img alt=\"利用快捷键\" height=\"507\" src=\"/uploads/allimg/c200330/15U55TC2K30-154P9.jpg\" width=\"600\"><br>\r\n	<br>\r\n	方法二、插入特殊字符<br>\r\n	<br>\r\n	1.在Word文档中将光标定位到需要插入特殊符号的位置上，单击“插入”-“符号”按钮，在弹出的下拉框中选择“其他符号”命令。<br>\r\n	<br>\r\n	<img alt=\"选择“其他符号”命令\" height=\"488\" src=\"/uploads/allimg/c200330/15U55TCXD0-162433.jpg\" width=\"600\"><br>\r\n	<br>\r\n	2.在弹出的“符号”对话框中，选择“特殊符号”选项卡，然后在“字符”的列表框中选择需要的符号，这里我们选择注册符号，单击“插入”按钮即可将选择的符号插入到文档中，单击“关闭”按钮关闭对话框。<br>\r\n	<br>\r\n	<img alt=\"插入符号\" height=\"511\" src=\"/uploads/allimg/c200330/15U55TD44960-1G062.jpg\" width=\"600\"><br>\r\n	<br>\r\n	方法三、自动更正<br>\r\n	<br>\r\n	1.在Word文档中将光标定位到需要插入特殊符号的位置上。要输入注册符号，则在文档中依次输入“(r)“，输入完成后，Word会自动更正为注册符号；要输入商标符号，则在文档中依次输入“(tm)\"；输入版权符号，则在文档中依次输入“(c)\"。提示：“(r)\"、“(tm)\"、“(c)\"全部为半角状态下的输入，字符必须按照从左到右的次序依次输入。<br>\r\n	<br>\r\n	<img alt=\"自动更正\" height=\"506\" src=\"/uploads/allimg/c200330/15U55TEE60-1V4J.jpg\" width=\"600\"></p>\r\n<p>\r\n	&nbsp;</p>', NULL, 'http://cyword.rywxw.com/uploads/200330/1-2003301J031R1.gif', '2020-11-04 21:11:59', NULL, '2020-11-07 08:58:10', NULL, 75, 1, '2020-11-04 21:11:59', NULL, 0, 2);
INSERT INTO `logo_cms_boke` VALUES (3, '将Word2013文档设为最终版本', '我们做好了一份文档，并不希望别人进行更改，出了加密的方法还有什么方法呢？文档加密，总是给人一种不友好的感觉，下面，我教大家另外一种方法，将文档标记为最终状态，让人...', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	我们做好了一份文档，并不希望别人进行更改，出了加密的方法还有什么方法呢？文档加密，总是给人一种不友好的感觉，下面，我教大家另外一种方法，将文档标记为最终状态，让人不能编辑。<br>\r\n	<br>\r\n	①单击文件--信息--保护文档--标记为最终状态。<br>\r\n	<br>\r\n	<img alt=\"标记\" border=\"0\" height=\"535\" src=\"/uploads/allimg/c200330/15U55T22242P-1L530.jpg\" width=\"600\"><br>\r\n	<br>\r\n	②提示文档先被标记为终稿，然后保存，单击确定按钮。<br>\r\n	<br>\r\n	<img alt=\"保存\" border=\"0\" height=\"533\" src=\"/uploads/allimg/c200330/15U55T2339230-1S051.jpg\" width=\"600\"><br>\r\n	<br>\r\n	③然后是保存文档，选取好保存路径，以及保存名称，单击确定按钮。<br>\r\n	<br>\r\n	<img alt=\"保存文件\" border=\"0\" height=\"458\" src=\"/uploads/allimg/c200330/15U55T242030-192493.jpg\" width=\"600\"><br>\r\n	<br>\r\n	④再次出现提示文档升级到最新的文件格式，依然是单击确定。<br>\r\n	<br>\r\n	<img alt=\"升级\" border=\"0\" height=\"532\" src=\"/uploads/allimg/c200330/15U55T24J330-20G16.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑤文档已经被标记为最终状态，确定关闭对话框。<br>\r\n	<br>\r\n	<img alt=\"标记最终状态\" border=\"0\" height=\"170\" src=\"/uploads/allimg/c200330/15U55T2620030-212N8.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑥我们重新打开文档，最上面可以看到提示，文档已经标记为最终状态。<br>\r\n	<br>\r\n	<img alt=\"已经标记完成\" border=\"0\" height=\"533\" src=\"/uploads/allimg/c200330/15U55T2H2640-221W1.jpg\" width=\"600\"><br>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', NULL, 'http://cyword.rywxw.com/uploads/200330/1-2003301J031R1.gif', '2020-11-04 21:12:07', NULL, '2020-11-07 00:42:34', NULL, 5, 1, '2020-11-04 21:12:07', NULL, 0, 2);
INSERT INTO `logo_cms_boke` VALUES (4, '利用表格法在Word2013中制作联合文件头', '一些机关、政府经常会向下级部门发放一些公文，通常来说，这些公文的文件头都是由几个机构组成，所以叫做联合公文。这个联合公文的文件头可以用双行合一的方法制作，也可以用...', '\r\n\r\n\r\n<p class=\"ql-long-20093186\"><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\">三角结构被认为是最稳定和安全的。这种</span><span class=\"ql-size-12 ql-font-microsoftyahei ql-author-21828059\">形状</span><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\">通常与力量，能量，灵感，转变和动力相关。但是，由于图形的角度和位置在空间中的组合发生了变化，因此可以不同地感知这种形式。例如，向上或向右指向的角度表示向前移动，向下或向左指向被动。</span></p>\r\n<p class=\"ql-long-20093186\"><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\">三角形通常</span><span class=\"ql-size-12 ql-font-microsoftyahei ql-author-21828059\">表示专注</span><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\">于男性或青年观众（用于建筑，汽车，法律和科学行业）的创新，勇敢，充满活力的公司使用。让我们分析这些知名品牌的象征意义：</span></p>\r\n<ul>\r\n<li class=\"\"><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\">达美航空：飞机机翼；</span><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://logo-file.biaozhishe.com/file/boke/1604161191643.png\" alt=\"达美航空\" width=\"600\"></span></li>\r\n<li class=\"\"><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\">Google Play：播放符号；</span><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://logo-file.biaozhishe.com/file/boke/1604161226355.png\" alt=\"Google Play\" width=\"600\"></span></li>\r\n<li class=\"\"><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\">阿迪达斯：一座山，即克服困难并实现目标；</span><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://logo-file.biaozhishe.com/file/boke/1604161304619.png\" alt=\"阿迪达斯\" width=\"600\"></span></li>\r\n<li class=\"\"><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\">Doritos：三角形炸薯条-公司定位为充满活力，勇气和灵感的基本产品。</span><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://logo-file.biaozhishe.com/file/boke/1604161326070.png\" alt=\"Doritos\" width=\"600\"></span></li>\r\n<li class=\"\"><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\">FedEx：在字母“ E”和“ X”之间的负数空格中作为箭头一部分的三角形；这象征着速度和可靠性。</span><span class=\"ql-author-20093186 ql-size-12 ql-font-microsoftyahei\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://logo-file.biaozhishe.com/file/boke/1604161350261.png\" alt=\"FedEx\" width=\"600\"></span></li>\r\n</ul>\r\n<p class=\"ql-long-20093186\"><span class=\"ql-author-20093186\">想了解更多logo设计技巧吗？参见《<a title=\"企业品牌\" href=\"http://www.biaozhishe.com/tag/7\">企业品牌</a>》</span></p>\r\n', NULL, 'http://cyword.rywxw.com/uploads/200330/1-2003301J031R1.gif', '2020-11-04 21:12:56', NULL, '2020-11-06 01:40:36', NULL, 10, 1, '2020-11-04 21:12:56', NULL, 0, 6);
INSERT INTO `logo_cms_boke` VALUES (5, 'GIF动画教程-制作圣诞节PPT图表页.5：全民', '圣诞节PPT主题第五期，继续前面的讲解，这一课是制作图表页面。这个图表，其实，使用多个六边形拼凑而成的，然...', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	一些机关、政府经常会向下级部门发放一些公文，通常来说，这些公文的文件头都是由几个机构组成，所以叫做联合公文。这个联合公文的文件头可以用双行合一的方法制作，也可以用公式编辑器来完成。而我们本次要介绍的是另一种简单方法--表格法。<br>\r\n	<br>\r\n	①启动Word2013，先插入一个表格，单击菜单栏--插入--表格，选择行列数。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"494\" src=\"/uploads/allimg/c200330/15U55T1B6010-105D3.jpg\" width=\"600\"><br>\r\n	<br>\r\n	②插入之后，选中整个表格，单击布局选项卡中的对齐方式，将其设置为居中对齐。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"492\" src=\"/uploads/allimg/c200330/15U55T1JUP-1124I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	③调整行高，输入文字，设置文字的颜色、字号、大小等等。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T1P9350-1214I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	④选中旁边的列，右击，合并单元格，将其整合为一个。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"490\" src=\"/uploads/allimg/c200330/15U55T193b60-134249.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑤输入与之前一样格式的文字，例如输入文件两字。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2010320-143643.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑥下面再次选中整个表格，点击工具栏上的边框按钮，选择无边框。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"496\" src=\"/uploads/allimg/c200330/15U55T20P510-151D2.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑦设置完成，大家如果觉得不紧凑，可以自行调整行距间距。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2134930-161Q1.jpg\" width=\"600\"><br>\r\n	<br>\r\n	相关阅读<br>\r\n	<br>\r\n	Word2003排版技巧：如何实现双行合一</p>\r\n<p>\r\n	&nbsp;</p>', NULL, 'http://cyword.rywxw.com/uploads/200330/1-2003301J031R1.gif', '2020-11-04 21:13:11', NULL, '2020-11-06 01:39:01', NULL, 0, 1, '2020-11-04 21:13:11', NULL, 0, 6);
INSERT INTO `logo_cms_boke` VALUES (6, 'PPT设计一组简约UI图标风格', '图标和LOGO都可以用来表达我们所想要表达的言语，在制作PPT模板的过程中，用图标来表达可能比文字更加醒目，比如...', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	一些机关、政府经常会向下级部门发放一些公文，通常来说，这些公文的文件头都是由几个机构组成，所以叫做联合公文。这个联合公文的文件头可以用双行合一的方法制作，也可以用公式编辑器来完成。而我们本次要介绍的是另一种简单方法--表格法。<br>\r\n	<br>\r\n	①启动Word2013，先插入一个表格，单击菜单栏--插入--表格，选择行列数。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"494\" src=\"/uploads/allimg/c200330/15U55T1B6010-105D3.jpg\" width=\"600\"><br>\r\n	<br>\r\n	②插入之后，选中整个表格，单击布局选项卡中的对齐方式，将其设置为居中对齐。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"492\" src=\"/uploads/allimg/c200330/15U55T1JUP-1124I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	③调整行高，输入文字，设置文字的颜色、字号、大小等等。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T1P9350-1214I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	④选中旁边的列，右击，合并单元格，将其整合为一个。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"490\" src=\"/uploads/allimg/c200330/15U55T193b60-134249.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑤输入与之前一样格式的文字，例如输入文件两字。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2010320-143643.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑥下面再次选中整个表格，点击工具栏上的边框按钮，选择无边框。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"496\" src=\"/uploads/allimg/c200330/15U55T20P510-151D2.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑦设置完成，大家如果觉得不紧凑，可以自行调整行距间距。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2134930-161Q1.jpg\" width=\"600\"><br>\r\n	<br>\r\n	相关阅读<br>\r\n	<br>\r\n	Word2003排版技巧：如何实现双行合一</p>\r\n<p>\r\n	&nbsp;</p>', NULL, 'http://cyword.rywxw.com/uploads/200330/1-2003301J031R1.gif', '2020-11-04 21:13:30', NULL, '2020-11-06 01:39:03', NULL, 5, 1, '2020-11-04 21:13:30', NULL, 0, 6);
INSERT INTO `logo_cms_boke` VALUES (7, '设置文字进入声音：星辰玩PPT第二十二招', '悦耳的风铃声，总能让我们回想起童年的味道，挂在门窗上的风铃，伴随着风声舞蹈，那清脆的声音，大家还记得吗...', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	一些机关、政府经常会向下级部门发放一些公文，通常来说，这些公文的文件头都是由几个机构组成，所以叫做联合公文。这个联合公文的文件头可以用双行合一的方法制作，也可以用公式编辑器来完成。而我们本次要介绍的是另一种简单方法--表格法。<br>\r\n	<br>\r\n	①启动Word2013，先插入一个表格，单击菜单栏--插入--表格，选择行列数。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"494\" src=\"/uploads/allimg/c200330/15U55T1B6010-105D3.jpg\" width=\"600\"><br>\r\n	<br>\r\n	②插入之后，选中整个表格，单击布局选项卡中的对齐方式，将其设置为居中对齐。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"492\" src=\"/uploads/allimg/c200330/15U55T1JUP-1124I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	③调整行高，输入文字，设置文字的颜色、字号、大小等等。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T1P9350-1214I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	④选中旁边的列，右击，合并单元格，将其整合为一个。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"490\" src=\"/uploads/allimg/c200330/15U55T193b60-134249.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑤输入与之前一样格式的文字，例如输入文件两字。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2010320-143643.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑥下面再次选中整个表格，点击工具栏上的边框按钮，选择无边框。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"496\" src=\"/uploads/allimg/c200330/15U55T20P510-151D2.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑦设置完成，大家如果觉得不紧凑，可以自行调整行距间距。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2134930-161Q1.jpg\" width=\"600\"><br>\r\n	<br>\r\n	相关阅读<br>\r\n	<br>\r\n	Word2003排版技巧：如何实现双行合一</p>\r\n<p>\r\n	&nbsp;</p>', NULL, 'http://cyword.rywxw.com/uploads/200330/1-2003301J031R1.gif', '2020-11-04 21:13:43', NULL, '2020-11-07 00:28:19', NULL, 5, 1, '2020-11-04 21:13:43', NULL, 0, 2);
INSERT INTO `logo_cms_boke` VALUES (8, '解决Word2013禁用Web服务器访问的故障', '有人留言，说是Word弹出禁用对Web服务器的访问，求解决办法。一般的用户基本不会发生这类现象，不过我还是有必要写一篇教程分享一下我曾经的处理方法，你们感受一下。 ①我们打...', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	一些机关、政府经常会向下级部门发放一些公文，通常来说，这些公文的文件头都是由几个机构组成，所以叫做联合公文。这个联合公文的文件头可以用双行合一的方法制作，也可以用公式编辑器来完成。而我们本次要介绍的是另一种简单方法--表格法。<br>\r\n	<br>\r\n	①启动Word2013，先插入一个表格，单击菜单栏--插入--表格，选择行列数。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"494\" src=\"/uploads/allimg/c200330/15U55T1B6010-105D3.jpg\" width=\"600\"><br>\r\n	<br>\r\n	②插入之后，选中整个表格，单击布局选项卡中的对齐方式，将其设置为居中对齐。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"492\" src=\"/uploads/allimg/c200330/15U55T1JUP-1124I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	③调整行高，输入文字，设置文字的颜色、字号、大小等等。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T1P9350-1214I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	④选中旁边的列，右击，合并单元格，将其整合为一个。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"490\" src=\"/uploads/allimg/c200330/15U55T193b60-134249.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑤输入与之前一样格式的文字，例如输入文件两字。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2010320-143643.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑥下面再次选中整个表格，点击工具栏上的边框按钮，选择无边框。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"496\" src=\"/uploads/allimg/c200330/15U55T20P510-151D2.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑦设置完成，大家如果觉得不紧凑，可以自行调整行距间距。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2134930-161Q1.jpg\" width=\"600\"><br>\r\n	<br>\r\n	相关阅读<br>\r\n	<br>\r\n	Word2003排版技巧：如何实现双行合一</p>\r\n<p>\r\n	&nbsp;</p>', NULL, 'http://cyword.rywxw.com/uploads/200330/1-2003301J031R1.gif', '2020-11-04 21:13:56', NULL, '2020-11-07 00:25:07', NULL, 4, 1, '2020-11-04 21:13:56', NULL, 0, 2);
INSERT INTO `logo_cms_boke` VALUES (9, 'Word2013无法打开低版本文档兼容性问题解', '同事新安装了Word2013，突然发现之前低版本创建的文档由于兼容性问题导致无法打开，下面，我就这个常见故障来分析一番，希望对大家有所帮助。 ①以Word2013为例，启动，单击文件-...', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	一些机关、政府经常会向下级部门发放一些公文，通常来说，这些公文的文件头都是由几个机构组成，所以叫做联合公文。这个联合公文的文件头可以用双行合一的方法制作，也可以用公式编辑器来完成。而我们本次要介绍的是另一种简单方法--表格法。<br>\r\n	<br>\r\n	①启动Word2013，先插入一个表格，单击菜单栏--插入--表格，选择行列数。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"494\" src=\"/uploads/allimg/c200330/15U55T1B6010-105D3.jpg\" width=\"600\"><br>\r\n	<br>\r\n	②插入之后，选中整个表格，单击布局选项卡中的对齐方式，将其设置为居中对齐。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"492\" src=\"/uploads/allimg/c200330/15U55T1JUP-1124I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	③调整行高，输入文字，设置文字的颜色、字号、大小等等。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T1P9350-1214I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	④选中旁边的列，右击，合并单元格，将其整合为一个。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"490\" src=\"/uploads/allimg/c200330/15U55T193b60-134249.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑤输入与之前一样格式的文字，例如输入文件两字。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2010320-143643.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑥下面再次选中整个表格，点击工具栏上的边框按钮，选择无边框。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"496\" src=\"/uploads/allimg/c200330/15U55T20P510-151D2.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑦设置完成，大家如果觉得不紧凑，可以自行调整行距间距。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2134930-161Q1.jpg\" width=\"600\"><br>\r\n	<br>\r\n	相关阅读<br>\r\n	<br>\r\n	Word2003排版技巧：如何实现双行合一</p>\r\n<p>\r\n	&nbsp;</p>', NULL, 'http://cyword.rywxw.com/uploads/200330/1-2003301J014420.gif', '2020-11-04 21:14:08', NULL, '2020-11-07 10:21:03', NULL, 15, 1, '2020-11-04 21:14:08', NULL, 0, 2);
INSERT INTO `logo_cms_boke` VALUES (10, '在Word2013中制作书法字帖，发扬国粹', '时代在飞速发展，我们中华民族的一些传统文化早已被人遗忘......现在绝大多数年轻人手上拿的都是数码产品。书法做为一项国粹，是不能不发扬的。像我的小孩，每天放学回来，不是...', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	一些机关、政府经常会向下级部门发放一些公文，通常来说，这些公文的文件头都是由几个机构组成，所以叫做联合公文。这个联合公文的文件头可以用双行合一的方法制作，也可以用公式编辑器来完成。而我们本次要介绍的是另一种简单方法--表格法。<br>\r\n	<br>\r\n	①启动Word2013，先插入一个表格，单击菜单栏--插入--表格，选择行列数。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"494\" src=\"/uploads/allimg/c200330/15U55T1B6010-105D3.jpg\" width=\"600\"><br>\r\n	<br>\r\n	②插入之后，选中整个表格，单击布局选项卡中的对齐方式，将其设置为居中对齐。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"492\" src=\"/uploads/allimg/c200330/15U55T1JUP-1124I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	③调整行高，输入文字，设置文字的颜色、字号、大小等等。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T1P9350-1214I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	④选中旁边的列，右击，合并单元格，将其整合为一个。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"490\" src=\"/uploads/allimg/c200330/15U55T193b60-134249.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑤输入与之前一样格式的文字，例如输入文件两字。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2010320-143643.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑥下面再次选中整个表格，点击工具栏上的边框按钮，选择无边框。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"496\" src=\"/uploads/allimg/c200330/15U55T20P510-151D2.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑦设置完成，大家如果觉得不紧凑，可以自行调整行距间距。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2134930-161Q1.jpg\" width=\"600\"><br>\r\n	<br>\r\n	相关阅读<br>\r\n	<br>\r\n	Word2003排版技巧：如何实现双行合一</p>\r\n<p>\r\n	&nbsp;</p>', NULL, 'http://cyword.rywxw.com/uploads/200330/1-2003301I9423I.gif', '2020-11-04 21:14:21', NULL, '2020-11-07 00:24:47', NULL, 8, 1, '2020-11-04 21:14:21', NULL, 0, 2);
INSERT INTO `logo_cms_boke` VALUES (11, 'Word2013中设置自动备份', 'word2013给我们带来的不仅仅是视觉上的舒适享受，在很多细节方面也给我们带来了惊喜。对于很多人来说，备份是个麻烦事，经常会因为忘记备份文件，而当文件丢失的时候，而在懊恼...', '圣诞节PPT主\r\n\r\n圣诞节PPT图表页预览', NULL, 'http://cyword.rywxw.com/uploads/200330/1-2003301IJ9E6.gif', '2020-11-04 21:14:34', NULL, '2020-11-07 00:22:32', NULL, 6, 1, '2020-11-04 21:14:34', NULL, 0, 2);
INSERT INTO `logo_cms_boke` VALUES (12, 'Word邮件合并带照片 - 批量生成准考证', '前面易老师有讲过利用Word邮件合并功能批量生成工资条和成绩单的操作，今天，继续邮件合并功能的讲解，这里我们再升一级，邮件合并中带图片照片的生成方法，这里我们准备的案例...', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	一些机关、政府经常会向下级部门发放一些公文，通常来说，这些公文的文件头都是由几个机构组成，所以叫做联合公文。这个联合公文的文件头可以用双行合一的方法制作，也可以用公式编辑器来完成。而我们本次要介绍的是另一种简单方法--表格法。<br>\r\n	<br>\r\n	①启动Word2013，先插入一个表格，单击菜单栏--插入--表格，选择行列数。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"494\" src=\"/uploads/allimg/c200330/15U55T1B6010-105D3.jpg\" width=\"600\"><br>\r\n	<br>\r\n	②插入之后，选中整个表格，单击布局选项卡中的对齐方式，将其设置为居中对齐。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"492\" src=\"/uploads/allimg/c200330/15U55T1JUP-1124I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	③调整行高，输入文字，设置文字的颜色、字号、大小等等。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T1P9350-1214I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	④选中旁边的列，右击，合并单元格，将其整合为一个。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"490\" src=\"/uploads/allimg/c200330/15U55T193b60-134249.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑤输入与之前一样格式的文字，例如输入文件两字。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2010320-143643.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑥下面再次选中整个表格，点击工具栏上的边框按钮，选择无边框。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"496\" src=\"/uploads/allimg/c200330/15U55T20P510-151D2.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑦设置完成，大家如果觉得不紧凑，可以自行调整行距间距。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2134930-161Q1.jpg\" width=\"600\"><br>\r\n	<br>\r\n	相关阅读<br>\r\n	<br>\r\n	Word2003排版技巧：如何实现双行合一</p>\r\n<p>\r\n	&nbsp;</p>', NULL, 'http://cyword.rywxw.com/uploads/allimg/c200330/15U55T6252Y0-R154.jpg', '2020-11-04 21:14:47', NULL, '2020-11-08 13:03:26', NULL, 15, 1, '2020-11-04 21:14:47', NULL, 0, 2);
INSERT INTO `logo_cms_boke` VALUES (13, '金山WPS 2012个人版免费下载', '金山WPS 2010正式版终于出来了，果真秉承了WPS以往的作风，体积小、安装快速、兼容微软Microsoft Office，一点也不比微...', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	一些机关、政府经常会向下级部门发放一些公文，通常来说，这些公文的文件头都是由几个机构组成，所以叫做联合公文。这个联合公文的文件头可以用双行合一的方法制作，也可以用公式编辑器来完成。而我们本次要介绍的是另一种简单方法--表格法。<br>\r\n	<br>\r\n	①启动Word2013，先插入一个表格，单击菜单栏--插入--表格，选择行列数。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"494\" src=\"/uploads/allimg/c200330/15U55T1B6010-105D3.jpg\" width=\"600\"><br>\r\n	<br>\r\n	②插入之后，选中整个表格，单击布局选项卡中的对齐方式，将其设置为居中对齐。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"492\" src=\"/uploads/allimg/c200330/15U55T1JUP-1124I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	③调整行高，输入文字，设置文字的颜色、字号、大小等等。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T1P9350-1214I.jpg\" width=\"600\"><br>\r\n	<br>\r\n	④选中旁边的列，右击，合并单元格，将其整合为一个。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"490\" src=\"/uploads/allimg/c200330/15U55T193b60-134249.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑤输入与之前一样格式的文字，例如输入文件两字。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2010320-143643.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑥下面再次选中整个表格，点击工具栏上的边框按钮，选择无边框。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"496\" src=\"/uploads/allimg/c200330/15U55T20P510-151D2.jpg\" width=\"600\"><br>\r\n	<br>\r\n	⑦设置完成，大家如果觉得不紧凑，可以自行调整行距间距。<br>\r\n	<br>\r\n	<img alt=\"利用表格法在Word2013中制作联合文件头\" border=\"0\" height=\"491\" src=\"/uploads/allimg/c200330/15U55T2134930-161Q1.jpg\" width=\"600\"><br>\r\n	<br>\r\n	相关阅读<br>\r\n	<br>\r\n	Word2003排版技巧：如何实现双行合一</p>\r\n<p>\r\n	&nbsp;</p>', NULL, 'http://cyword.rywxw.com/uploads/allimg/c200330/15U55RJL560-54193.gif', '2020-11-04 21:14:55', NULL, '2020-11-07 00:39:00', NULL, 18, 1, '2020-11-04 21:14:55', NULL, 0, 5);

-- ----------------------------
-- Table structure for logo_cms_boke_comment
-- ----------------------------
DROP TABLE IF EXISTS `logo_cms_boke_comment`;
CREATE TABLE `logo_cms_boke_comment`  (
  `comment_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT 0,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_qq` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `boke_id` bigint(20) NOT NULL,
  `reply_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `reply_user_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `reply_user_qq` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `comment_msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `creat_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_del` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of logo_cms_boke_comment
-- ----------------------------
INSERT INTO `logo_cms_boke_comment` VALUES (8, 0, '喝醉的咕咕鸟', 'http://q1.qlogo.cn/g?b=qq&nk=731727335&s=100', '731727335', 2, '0', NULL, NULL, '<img src=\"/images/tza_thumb.gif\" title=\"[可爱]\">', '2020-11-06 18:39:23', 0);
INSERT INTO `logo_cms_boke_comment` VALUES (9, 0, '喝醉的咕咕鸟', 'http://q1.qlogo.cn/g?b=qq&nk=731727335&s=100', '731727335', 1, '0', NULL, NULL, '<span style=\"color: rgb(102, 102, 102); font-family: 微软雅黑; font-size: 12px; text-indent: 20px;\">有什么新鲜事想告诉大家?</span><img src=\"/images/tootha_thumb.gif\" title=\"嘻嘻]\">', '2020-11-06 21:46:39', 1);
INSERT INTO `logo_cms_boke_comment` VALUES (10, 0, '喝醉的咕咕鸟', 'http://q1.qlogo.cn/g?b=qq&nk=731727335&s=100', '731727335', 1, '0', NULL, NULL, '<span style=\"color: rgb(102, 102, 102); font-family: 微软雅黑; font-size: 12px; text-indent: 20px;\">有什么新鲜事想告诉大家?</span><img src=\"/images/ldln_thumb.gif\" title=\"[懒得理你]\">', '2020-11-06 21:46:50', 0);
INSERT INTO `logo_cms_boke_comment` VALUES (11, 0, '匿名', 'http://q1.qlogo.cn/g?b=qq&nk=13592540006&s=100', '13592540006', 2, '0', NULL, NULL, '你好呀', '2020-11-06 21:56:51', 0);
INSERT INTO `logo_cms_boke_comment` VALUES (12, 0, '喝醉的咕咕鸟', 'http://q1.qlogo.cn/g?b=qq&nk=731727335&s=100', '731727335', 12, '0', NULL, NULL, '小飞是的大逗比 ，毛毛虫 渣渣峰<img src=\"/images/laugh.gif\" title=\"[哈哈]\"><img src=\"/images/tootha_thumb.gif\" title=\"嘻嘻]\"><img src=\"/images/tootha_thumb.gif\" title=\"嘻嘻]\">', '2020-11-07 00:37:00', 0);
INSERT INTO `logo_cms_boke_comment` VALUES (13, 0, '喝醉的咕咕鸟', 'http://q1.qlogo.cn/g?b=qq&nk=731727335&s=100', '731727335', 2, '0', NULL, NULL, '哈喽 小逗逼们<img src=\"/images/sk_thumb.gif\" title=\"[思考]\">', '2020-11-07 00:37:49', 0);
INSERT INTO `logo_cms_boke_comment` VALUES (14, 0, '喝醉的咕咕鸟', 'http://q1.qlogo.cn/g?b=qq&nk=731727335&s=100', '731727335', 2, '0', NULL, NULL, '你好<img src=\"/images/cza_thumb.gif\" title=\"[馋嘴]\">', '2020-11-07 00:41:25', 0);
INSERT INTO `logo_cms_boke_comment` VALUES (15, 0, '喝醉的咕咕鸟', 'http://q1.qlogo.cn/g?b=qq&nk=731727335&s=100', '731727335', 9, '0', NULL, NULL, '你就是个**，**，***<img src=\"/images/88_thumb.gif\" title=\"[拜拜]\">', '2020-11-07 10:20:03', 0);

-- ----------------------------
-- Table structure for logo_cms_boke_label
-- ----------------------------
DROP TABLE IF EXISTS `logo_cms_boke_label`;
CREATE TABLE `logo_cms_boke_label`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `boke_type_id` int(32) NOT NULL COMMENT '子分类id',
  `blog_label_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `blog_label_alias_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `blog_label_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_delete` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `boke_type_id`(`boke_type_id`, `id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '博客标签' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logo_cms_boke_label
-- ----------------------------
INSERT INTO `logo_cms_boke_label` VALUES (1, 2, 'Word宏案例', 'Wordhonganli', 'Word宏案例', 0);
INSERT INTO `logo_cms_boke_label` VALUES (2, 2, 'Word技巧大全', 'Wordjiqiaodaquan', 'Word技巧大全', 0);
INSERT INTO `logo_cms_boke_label` VALUES (3, 2, 'Word制作表格', 'Wordzhizuobiaoge', 'Word制作表格', 0);
INSERT INTO `logo_cms_boke_label` VALUES (4, 2, 'Word排版布局', 'Wordpaibanbuju', 'Word排版布局', 0);
INSERT INTO `logo_cms_boke_label` VALUES (5, 2, 'Word2010教程', 'Word2010jiaocheng', 'Word2010教程', 0);
INSERT INTO `logo_cms_boke_label` VALUES (6, 2, 'Word2013教程', 'Word2013jiaocheng', 'Word2013教程', 0);
INSERT INTO `logo_cms_boke_label` VALUES (7, 2, 'Word模板下载', 'Wordmobanxiazai', 'Word模板下载', 0);
INSERT INTO `logo_cms_boke_label` VALUES (8, 3, 'Excel函数不求人', 'Excelhanshubuqiuren', 'Excel函数不求人', 0);
INSERT INTO `logo_cms_boke_label` VALUES (9, 3, '全民学Excel', 'quanminxueExcel', '全民学Excel', 0);
INSERT INTO `logo_cms_boke_label` VALUES (10, 3, '玩转Excel数据', 'wanzhuanExcelshuju', '玩转Excel数据', 0);
INSERT INTO `logo_cms_boke_label` VALUES (11, 3, 'Excel图表制作', 'Exceltubiaozhizuo', 'Excel图表制作', 0);
INSERT INTO `logo_cms_boke_label` VALUES (12, 3, 'ExcelVBA实例', 'ExcelVBAshili', 'ExcelVBA实例', 0);
INSERT INTO `logo_cms_boke_label` VALUES (13, 3, 'Excel技巧大全', 'Exceljiqiaodaquan', 'Excel技巧大全', 0);
INSERT INTO `logo_cms_boke_label` VALUES (14, 3, 'Excel2010教程', 'Excel2010jiaocheng', 'Excel2010教程', 0);
INSERT INTO `logo_cms_boke_label` VALUES (15, 3, 'Excel表格模板下载', 'Excelbiaogemobanxiazai', 'Excel表格模板下载', 0);
INSERT INTO `logo_cms_boke_label` VALUES (16, 5, 'WPS下载', 'WPSxiazai', 'WPS下载', 0);
INSERT INTO `logo_cms_boke_label` VALUES (17, 5, 'WPS表格教程', 'WPSbiaogejiaocheng', 'WPS表格教程', 0);
INSERT INTO `logo_cms_boke_label` VALUES (18, 5, 'WPS文字教程', 'WPSwenzijiaocheng', 'WPS文字教程', 0);
INSERT INTO `logo_cms_boke_label` VALUES (19, 5, 'WPS演示教程', 'WPSyanshijiaocheng', 'WPS演示教程', 0);
INSERT INTO `logo_cms_boke_label` VALUES (20, 4, '全民学PPT', 'quanminxuePPT', '全民学PPT', 0);
INSERT INTO `logo_cms_boke_label` VALUES (21, 4, 'PPT技巧大全', 'PPTjiqiaodaquan', 'PPT技巧大全', 0);
INSERT INTO `logo_cms_boke_label` VALUES (22, 4, 'PPT字体之美', 'PPTzitizhimei', 'PPT字体之美', 0);
INSERT INTO `logo_cms_boke_label` VALUES (23, 4, '菜鸟PPT动画之旅', 'cainiaoPPTdonghuazhilv', '菜鸟PPT动画之旅', 0);
INSERT INTO `logo_cms_boke_label` VALUES (24, 4, 'PowerPoint基础教程', 'PowerPointjichujiaocheng', 'PowerPoint基础教程', 0);
INSERT INTO `logo_cms_boke_label` VALUES (25, 4, 'PowerPoint制作教程', 'PowerPointzhizuojiaocheng', 'PowerPoint制作教程', 0);
INSERT INTO `logo_cms_boke_label` VALUES (26, 4, 'PowerPoint2013教程', 'PowerPoint2013jiaocheng', 'PowerPoint2013教程', 0);

-- ----------------------------
-- Table structure for logo_cms_boke_lable_middle
-- ----------------------------
DROP TABLE IF EXISTS `logo_cms_boke_lable_middle`;
CREATE TABLE `logo_cms_boke_lable_middle`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `boke_id` int(32) NULL DEFAULT NULL COMMENT 'boke id',
  `label_id` int(32) NULL DEFAULT NULL COMMENT '标签id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `boke_id`(`boke_id`, `label_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '博客标签中间表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logo_cms_boke_lable_middle
-- ----------------------------
INSERT INTO `logo_cms_boke_lable_middle` VALUES (1, 1, 1);
INSERT INTO `logo_cms_boke_lable_middle` VALUES (2, 2, 1);
INSERT INTO `logo_cms_boke_lable_middle` VALUES (3, 3, 1);
INSERT INTO `logo_cms_boke_lable_middle` VALUES (4, 4, 1);

-- ----------------------------
-- Table structure for logo_cms_friendship_links
-- ----------------------------
DROP TABLE IF EXISTS `logo_cms_friendship_links`;
CREATE TABLE `logo_cms_friendship_links`  (
  `id` int(32) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `friend_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '友情链接名称',
  `friend_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '点击路径',
  `sort` int(3) NULL DEFAULT 0 COMMENT '排序',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_del` int(1) NULL DEFAULT 0 COMMENT '删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_german2_ci COMMENT = '友情链接' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logo_cms_friendship_links
-- ----------------------------
INSERT INTO `logo_cms_friendship_links` VALUES (1, 'a', 'http://baidu.com', 0, '2020-11-07 23:10:58', 0);

-- ----------------------------
-- Table structure for logo_cms_tag
-- ----------------------------
DROP TABLE IF EXISTS `logo_cms_tag`;
CREATE TABLE `logo_cms_tag`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tag_aslia_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `web_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `web_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `web_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_delete` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '标签' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logo_cms_tag
-- ----------------------------
INSERT INTO `logo_cms_tag` VALUES (1, '底色', 'dise', '底色', '底色', '底色', 0);
INSERT INTO `logo_cms_tag` VALUES (2, '使用', 'shiyong', '使用', '使用', '使用', 0);
INSERT INTO `logo_cms_tag` VALUES (3, '去除', 'quchu', '去除', '去除', '去除', 0);
INSERT INTO `logo_cms_tag` VALUES (4, '金山', 'jinshan', '金山', '金山', '金山', 0);
INSERT INTO `logo_cms_tag` VALUES (5, '圆心', 'yuanxin', '圆心', '圆心', '圆心', 0);
INSERT INTO `logo_cms_tag` VALUES (6, '画圆', 'huayuan', '画圆', '画圆', '画圆', 0);
INSERT INTO `logo_cms_tag` VALUES (7, '颜色', 'yanse', '颜色', '颜色', '颜色', 0);
INSERT INTO `logo_cms_tag` VALUES (8, '相册', 'xiangce', '相册', '相册', '相册', 0);
INSERT INTO `logo_cms_tag` VALUES (9, '下载', 'xiazai', '下载', '下载', '下载', 0);
INSERT INTO `logo_cms_tag` VALUES (10, '图案', 'tuan', '图案', '图案', '图案', 0);
INSERT INTO `logo_cms_tag` VALUES (11, '2012', '2012', '2012', '2012', '2012', 0);
INSERT INTO `logo_cms_tag` VALUES (12, '国庆', 'guoqing', '国庆', '国庆', '国庆', 0);
INSERT INTO `logo_cms_tag` VALUES (13, '裁剪', 'caijian', '裁剪', '裁剪', '裁剪', 0);
INSERT INTO `logo_cms_tag` VALUES (14, '修改', 'xiugai', '修改', '修改', '修改', 0);
INSERT INTO `logo_cms_tag` VALUES (15, '某点', 'moudian', '某点', '某点', '某点', 0);
INSERT INTO `logo_cms_tag` VALUES (16, '更改', 'genggai', '更改', '更改', '更改', 0);
INSERT INTO `logo_cms_tag` VALUES (17, '次数', 'cishu', '次数', '次数', '次数', 0);
INSERT INTO `logo_cms_tag` VALUES (18, '2011', '2011', '2011', '2011', '2011', 0);
INSERT INTO `logo_cms_tag` VALUES (19, '增加', 'zengjia', '增加', '增加', '增加', 0);
INSERT INTO `logo_cms_tag` VALUES (20, '批量', 'piliang', '批量', '批量', '批量', 0);
INSERT INTO `logo_cms_tag` VALUES (21, '动画', 'donghua', '动画', '动画', '动画', 0);
INSERT INTO `logo_cms_tag` VALUES (22, '快乐', 'kuaile', '快乐', '快乐', '快乐', 0);
INSERT INTO `logo_cms_tag` VALUES (23, 'logo设计', 'logosheji', 'logo设计', 'logo设计', 'logo设计', 0);

-- ----------------------------
-- Table structure for logo_cms_tag_article_middle
-- ----------------------------
DROP TABLE IF EXISTS `logo_cms_tag_article_middle`;
CREATE TABLE `logo_cms_tag_article_middle`  (
  `tag_id` int(32) NOT NULL,
  `article_id` int(32) NOT NULL,
  INDEX `tag_id`(`tag_id`, `article_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logo_cms_tag_article_middle
-- ----------------------------
INSERT INTO `logo_cms_tag_article_middle` VALUES (1, 1);
INSERT INTO `logo_cms_tag_article_middle` VALUES (1, 2);

-- ----------------------------
-- Table structure for logo_head_line
-- ----------------------------
DROP TABLE IF EXISTS `logo_head_line`;
CREATE TABLE `logo_head_line`  (
  `head_line_id` int(32) NOT NULL AUTO_INCREMENT,
  `head_line_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `head_line_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `head_line_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `head_line_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `priority` int(1) NULL DEFAULT NULL,
  `type_num` int(32) NULL DEFAULT 1 COMMENT '1-轮番图 2-中部轮番图',
  `status` int(1) NULL DEFAULT 0 COMMENT '1-生效',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`head_line_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logo_head_line
-- ----------------------------
INSERT INTO `logo_head_line` VALUES (2, '如何在Word2010中输入商标、版权符号', '在不同领域，不同用户有不同的需求。有些用户...', 'http://test-sc.biaozhishe.com/wordjiaocheng/detail/12.html', 'http://cyword.rywxw.com/uploads/allimg/c200330/15U55TF2c10-193Z9.jpg', 20, 1, 1, '2020-11-04 22:02:14', '2020-11-07 00:35:45');
INSERT INTO `logo_head_line` VALUES (3, '利用表格法在Word2013中制作联合文件头', '一些机关、政府经常会向下级部门发放一些公文...', 'http://test-sc.biaozhishe.com/wordjiaocheng/detail/12.html', 'http://cyword.rywxw.com/uploads/200330/1-2003301I444191.jpg', 30, 1, 1, '2020-11-04 22:02:46', '2020-11-07 00:35:45');
INSERT INTO `logo_head_line` VALUES (4, '将Word2013文档设为最终版本', '我们做好了一份文档，并不希望别人进行更改，...', 'http://test-sc.biaozhishe.com/wordjiaocheng/detail/12.html', 'http://cyword.rywxw.com/uploads/200330/1-2003301I501459.gif', 40, 1, 1, '2020-11-04 22:03:59', '2020-11-07 00:35:45');
INSERT INTO `logo_head_line` VALUES (5, '解决Word2013禁用Web服务器访问的故障', '解决Word2013禁用Web服务器访问的故障', 'http://test-sc.biaozhishe.com/wordjiaocheng/detail/12.html', 'http://cyword.rywxw.com/uploads/200330/1-2003301J031R1.gif', 10, 2, 1, '2020-11-04 22:04:42', '2020-11-07 00:35:45');
INSERT INTO `logo_head_line` VALUES (6, 'Word2013无法打开低版本文档兼容性问题解', 'Word2013无法打开低版本文档兼容性问题解', 'http://test-sc.biaozhishe.com/wordjiaocheng/detail/12.html', 'http://cyword.rywxw.com/uploads/200330/1-2003301J014420.gif', 20, 2, 1, '2020-11-04 22:05:14', '2020-11-07 00:35:45');
INSERT INTO `logo_head_line` VALUES (7, '在Word2013中制作书法字帖，发扬国粹', '在Word2013中制作书法字帖，发扬国粹', 'http://test-sc.biaozhishe.com/wordjiaocheng/detail/12.html', 'http://cyword.rywxw.com/uploads/200330/1-2003301I9423I.gif', 30, 2, 1, '2020-11-04 22:06:15', '2020-11-07 00:35:45');

-- ----------------------------
-- Table structure for logo_nav
-- ----------------------------
DROP TABLE IF EXISTS `logo_nav`;
CREATE TABLE `logo_nav`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type_aslia_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `web_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `web_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `web_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_delete` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '网站导航' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logo_nav
-- ----------------------------
INSERT INTO `logo_nav` VALUES (1, '网站首页', 'wangzhanshouye', '/', '网站首页', '网站首页', '网站首页', 0);
INSERT INTO `logo_nav` VALUES (2, 'Word教程', 'wordjiaocheng', '/nav/wordjiaocheng/index.html', 'Word教程', 'Word教程', 'Word教程', 0);
INSERT INTO `logo_nav` VALUES (3, 'Excel教程', 'Exceljiaocheng', '/nav/Exceljiaocheng/index.html', 'Excel教程', 'Excel教程', 'Excel教程', 0);
INSERT INTO `logo_nav` VALUES (4, 'PPT教程', 'PPTjiaocheng', '/nav/PPTjiaocheng/index.html', 'PPT教程', 'PPT教程', 'PPT教程', 0);
INSERT INTO `logo_nav` VALUES (5, 'WPS教程', 'WPSjiaocheng', '/nav/WPSjiaocheng/index.html', 'WPS教程', 'WPS教程', 'WPS教程', 0);
INSERT INTO `logo_nav` VALUES (6, '办公软件', 'bangongruanjian', '/nav/bangongruanjian/index.html', '办公软件', '办公软件', '办公软件', 0);
INSERT INTO `logo_nav` VALUES (7, '电脑基础', 'diannaojichu', '/nav/diannaojichu/index.html', '电脑基础', '电脑基础', '电脑基础', 0);
INSERT INTO `logo_nav` VALUES (8, '热点新闻', 'redianxinwen', '/nav/redianxinwen/index.html', '热点新闻', '热点新闻', '热点新闻', 0);
INSERT INTO `logo_nav` VALUES (9, '实时动态', 'shishidongtai', '/nav/shishidongtai/index.html', '实时动态', '实时动态', '实时动态', 0);

SET FOREIGN_KEY_CHECKS = 1;
