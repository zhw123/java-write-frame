package com.lean.generate.vue;

import com.alibaba.fastjson.JSON;
import com.lean.generate.vue.service.SysGeneratorService;
import com.lean.generate.vue.utils.PageUtils;
import com.lean.generate.vue.utils.Query;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.io.FileOutputStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringGenerateVueApplicationTest {

    @Autowired
    private SysGeneratorService sysGeneratorService;


    /**
     * 获取所有的表数据
     */
    @Test
    public void findTables(){
        PageUtils pageUtil = sysGeneratorService.queryList(new Query(1,10));
        System.out.println(JSON.toJSONString(pageUtil));
    }

    /**
     * 生成代码
     */
    @Test
    public void generatorCode() throws Exception {
        String tables="";
        byte[] data = sysGeneratorService.generatorCode(tables.split(","));
        IOUtils.write(data, new FileOutputStream("generate.zip"));
    }
}
