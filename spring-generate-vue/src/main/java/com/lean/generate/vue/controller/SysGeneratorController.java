package com.lean.generate.vue.controller;
import com.lean.generate.vue.service.SysGeneratorService;
import com.lean.generate.vue.utils.PageUtils;
import com.lean.generate.vue.utils.Query;
import com.lean.generate.vue.utils.R;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 代码生成器
 */
@Controller
@RequestMapping("/sys/generator")
public class SysGeneratorController {
	@Autowired
	private SysGeneratorService sysGeneratorService;

	/**
	 * 列表
	 */
	@ResponseBody
	@RequestMapping("/list")
	public R list(Integer page,Integer limit){
		PageUtils pageUtil = sysGeneratorService.queryList(new Query(page,limit));
		return ResultDataSet.success().put("page", pageUtil);
	}
	/**
	 * 生成代码
	 */
	@RequestMapping("/code")
	public void code(String tables, HttpServletResponse response) throws IOException{
		byte[] data = sysGeneratorService.generatorCode(tables.split(","));
		response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=\"generate.zip\"");
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(data, response.getOutputStream());
	}
}
