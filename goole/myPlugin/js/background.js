// setInterval(() => {
//     alert('background')
// }, 5000);

$.ajax({
    url: "https://www.douban.com/j/search",
    type: "GET",
    data: {
        q: '电影',
        start: 10,
        cat: 1002,
    },
    success: function (res) {
        console.log(res)
    },
    error: function (e) {
        console.log(e.responseText)
    }
})
//background.js
//接收消息
chrome.runtime.onMessage.addListener(async (req, sender, sendResponse) => {
    console.log('我是background，我接收了来自 content.js的消息：', req.info)
    sendResponse('哈哈哈')
    const tabId = await getCurrentTabId()
    // 在背景页面发送消息，需要当前 tabID
    chrome.tabs.sendMessage(tabId, '我是background，我在发送消息', function (res) {
        console.log('background：', res)
    });
})
/**
 * 获取当前 tab ID
 */
function getCurrentTabId() {
    return new Promise((resolve, reject) => {
        chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
            resolve(tabs.length ? tabs[0].id : null)
        });
    })
}
//background.js
/**
 * 通信函数
 */
function backFun () {
    console.log('arguments：', arguments)
    const allViews = chrome.extension.getViews()
    console.log('chrome.extension.getViews()：', allViews)
}
