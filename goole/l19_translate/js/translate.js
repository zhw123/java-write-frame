var menuItem = {
    "id": "bdtranslate",
    "title": "百度翻译",
    "contexts": ["selection"]
}
chrome.contextMenus.create(menuItem);

chrome.contextMenus.onClicked.addListener(function (clickData) {
    if (clickData.menuItemId == 'bdtranslate' && clickData.selectionText) {
        //以下内容，从百度翻译API的index.html中复制粘贴
        //需要将其中的appid,key,query修改为自己的id,key和查询内容
        var appid = '20220327001144201';//自己的APPID,官网可查
        var key = 'E_NCDdjbBUP8gqzFPdYJ';//自己的KEY,官网可查
        var salt = (new Date).getTime();
        var query = clickData.selectionText;//右键选中的内容
        // 多个query可以用\n连接  如 query='apple\norange\nbanana\npear'
        var from = 'zh';//中文
        var to = 'en';//英文
        var str1 = appid + query + salt + key;
        var sign = MD5(str1);
        //由于插件内的js内容不允许使用ajax，以下内容为从index.html中的ajax部分复制并修改得到
        var url = 'http://api.fanyi.baidu.com/api/trans/vip/translate';
        url += "?q="+query;
        url += "&appid="+appid;
        url += "&salt="+salt;
        url += "&from="+from;
        url += "&to="+to;
        url += "&sign="+sign;
        //使用fetch获取的json结果需要先转换为string再转换为json
        fetch(url)
        .then(reponse => reponse.text())
        .then(text =>JSON.parse(text))
        .then(translateRsult => {
            chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
                //result通过查询浏览器的开发者模式得知
                chrome.tabs.sendMessage(tabs[0].id, { todo: "translate", result: translateRsult.trans_result[0].dst })
            })
        })  
    }
})