let openPage = document.getElementById("openback");
openPage.addEventListener("click", () => {
    window.open(chrome.extension.getURL("back.html"));
});

let getTitle = document.getElementById("getbacktitle");
getTitle.addEventListener("click", () => {
    let bg = chrome.extension.getBackgroundPage();
    alert(bg.document.title);
});

let setTitle = document.getElementById("setbacktitle");
setTitle.addEventListener("click", () => {
    let title = prompt("请输入后天页新标题", "新标题");
    let bg = chrome.extension.getBackgroundPage();
    bg.document.title = title;
    alert(bg.document.title);
});
let callBack = document.getElementById("callbackjs");
callBack.addEventListener("click", () => {
    let bg = chrome.extension.getBackgroundPage();
    bg.backjs();
});
let showbadge = document.getElementById("showbadge");
showbadge.addEventListener("click", () => {
    chrome.browserAction.setBadgeText({text: "奥特曼"});
    chrome.browserAction.setBadgeBackgroundColor({color: [0, 255, 0, 255]});
});

let hidebadge = document.getElementById("hidebadge");
hidebadge.addEventListener("click", () => {
    chrome.browserAction.setBadgeText({text: ""});
    chrome.browserAction.setBadgeBackgroundColor({color: [0, 0, 0, 0]});
});

let opt = {
    type: "basic",
    title: "是兄弟就来砍我!",
    message: "我是渣渣辉，是兄弟就来砍我~",
    iconUrl: "img/icons.png",
};
chrome.notifications.create("", opt);
