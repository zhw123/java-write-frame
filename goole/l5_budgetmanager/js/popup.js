$(function(){
    //在第一次运行时，如果总金额为0，也不会留空，而是有值显示
    chrome.storage.sync.get('total',function(budget){
        $('#total').text(budget.total);
    });
    $('#add').click(function(){
        //1、从浏览器获取存储的金额
        chrome.storage.sync.get('total',function(budget){
            var totalAmount = 0;
            if(budget.total){
                totalAmount = parseFloat(budget.total);
            }
            //2、将本次金额加到总金额中
            var amount = $('#amount').val();
            if(amount){
                totalAmount += parseFloat(amount);
                chrome.storage.sync.set({'total':totalAmount});
            }
            //3、更新显示ui
            $('#total').text(totalAmount);
            $('#amount').val('');
        })
    })
})