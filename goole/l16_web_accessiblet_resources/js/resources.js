var menuItem = {
    "id":"resources",
    "title":"设置背景图",
    "contexts":["all"]
}
chrome.contextMenus.create(menuItem);

chrome.contextMenus.onClicked.addListener(function(clickData){
    var imgurl = chrome.extension.getURL("img/背景.png");
    chrome.tabs.executeScript(null,{code:"document.body.style.backgroundImage = 'url(\""+imgurl+"\")';\
    document.body.style.backgroundRepeat = 'repeat';"});
})

