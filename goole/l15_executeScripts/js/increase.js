var menuItem = {
    "id":"increase",
    "title":"增大字体",
    "contexts":["all"]
}
chrome.contextMenus.create(menuItem);

chrome.contextMenus.onClicked.addListener(function(clickData){
    chrome.tabs.executeScript(null,{code:"var old = window.getComputedStyle(document.body).fontSize;\
    var index = old.indexOf('p');\
    var size = parseInt(old.substring(0,index));\
    var newSize = size + 10+'px';\
    document.body.style.fontSize = newSize;"});
})
