package com.lean.minio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringFileMinioApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringFileMinioApplication.class, args);
    }

}
