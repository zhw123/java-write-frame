CREATE TABLE `t_user` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `username` varchar(255) DEFAULT NULL,
                          `birthday` varchar(255) DEFAULT NULL,
                          `sex` varchar(255) DEFAULT NULL,
                          `address` varchar(255) DEFAULT NULL,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
