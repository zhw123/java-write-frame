package com.writeframe.cas;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName: WriteCasTest
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/4/29 14:02
 */
public class WriteCasTest {

    public static void main(String[] args) {
       final WriteCas writeCas=new WriteCas();
        for(int i=0;i<100;i++){
            new Thread(new Runnable() {
                public void run() {
                    AtomicInteger atomicInteger=new AtomicInteger(0);
                    boolean flag = false;
                    while(!flag){
                        atomicInteger.incrementAndGet();
                        //如果没有成功 进行自旋 知道设置成功为止
                        int innerValue = writeCas.getInnerValue();
                        int num = (int) (Math.random() * 101);
                        System.out.println(Thread.currentThread().getName()+"内存中的值:"+innerValue);
                        System.out.println(Thread.currentThread().getName()+"预修改的值:"+num);
                        flag = writeCas.CompareAndSet(innerValue, num);
                        if(flag){
                            System.out.println(Thread.currentThread().getName()+"------------->循环"+atomicInteger.get()+"遍获取锁成功");
                        }else {
                            System.out.println(Thread.currentThread().getName()+"------------->获取锁失败");
                        }
                    }
                }
            }).start();
        }
    }

}
