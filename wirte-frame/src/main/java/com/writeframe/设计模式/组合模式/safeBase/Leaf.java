package com.writeframe.设计模式.组合模式.safeBase;
/**
 * @ClassName Leaf
 * @Description 组合对象，叶子节点
 */
public class Leaf extends Component {

    public Leaf(String name) {
        super(name);
    }

    @Override
    public String operation() {
        return this.name;
    }
}
