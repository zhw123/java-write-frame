package com.writeframe.设计模式.组合模式.safeBase;

/**
 * @ClassName Component
 * @Description 组合对象，顶层抽象组件，根节点
 * A.顶层抽象组件
 */
public abstract class Component {
    protected String name;

    public Component(String name) {
        this.name = name;
    }

    public abstract String operation();
}
