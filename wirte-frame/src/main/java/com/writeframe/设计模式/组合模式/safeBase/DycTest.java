package com.writeframe.设计模式.组合模式.safeBase;

/**
 *安全写法：
 * 　　顶层抽象组件，只定义公共方法，调用时就不能使用多态的方式，只能子类自己new自己。
 * 　　避免叶子节点类实例，错误调用。
 * 组合模式一定是树形结构吗？
 * 　　答：不一定。只是强调整体与部分的关系。它是针对整体与部分，寻找共同点，进行公共逻辑抽取的思维方式。
 *
 *
 */
public class DycTest {
    public static void main(String[] args) {
        //由于Composite对抽象组件Component的功能，进行扩展
        //就不能使用多态的方式，调用子类的方法
        // 来一个根节点
        Composite root = new Composite("root");
        // 来一个树枝节点
        Composite branchA = new Composite("---branchA");
        Composite branchB = new Composite("------branchB");
        // 来一个叶子节点
        Component leafA = new Leaf("------leafA");
        Component leafB = new Leaf("---------leafB");
        Component leafC = new Leaf("---leafC");

        root.addChild(branchA);
        root.addChild(leafC);
        branchA.addChild(leafA);
        branchA.addChild(branchB);
        branchB.addChild(leafB);

        String result = root.operation();
        System.out.println(result);

    }
}
