package com.writeframe.设计模式.组合模式.staticBase;

/**
 * 透明写法:
 *      抽象组件中定义几个空方法，作为公共组件，Leaf实现类，不需要使用到这几个空方法。
 *
 *透明写法：
 *
 * 　　顶层抽象组件会定义所有的方法，包括叶子节点不需要的方法。只是作空方法声明。
 *     对于叶子节点，很容易错误调用，所以，违背最少知道原则。
 *
 */
public class CompositeTest {

    public static void main(String[] args) {
        // 来一个根节点
        Component root = new Composite("root");
        // 来一个树枝节点
        Component branchA = new Composite("---branchA");
        Component branchB = new Composite("------branchB");
        // 来一个叶子节点
        Component leafA = new Leaf("------leafA");
        Component leafB = new Leaf("---------leafB");
        Component leafC = new Leaf("---leafC");

        root.addChild(branchA);
        root.addChild(leafC);
        branchA.addChild(leafA);
        branchA.addChild(branchB);
        branchB.addChild(leafB);

        String result = root.operation();
        System.out.println(result);

    }

}
