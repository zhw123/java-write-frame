package com.writeframe.设计模式.适配器模式.classadapter;


/**
 * @ClassName PowerAdapter
 * @Description 电源适配器
 * 说明：
 * 　　适配器类中，继承适配原角色，实现目标接口
 * 　　实现接口的功能，调用原角色的功能，并进行扩展。
 */
public class PowerAdapter extends AC220 implements DC5 {

    @Override
    public int output5V() {
        int adapterInput = super.outputAC220V();
        int adapterOutput = adapterInput / 44;
        System.out.println("使用Adapter输入AC" + adapterInput + "V,输出DC[" + adapterOutput + "]V");
        return adapterOutput;
    }

    /**
     * 说明：
     * 　　由于适配器，继承原角色，并实现目标接口。所以，适配器具有两者的功能。
     * 　　显然，有些违背最少知道原则。
     * 　　因此，提出对象适配器写法。它的实现思路：
     *
     * 弃用继承结构，而采用组合方式。
     * @param args
     */
    public static void main(String[] args) {
        PowerAdapter adapter = new PowerAdapter();
        adapter.output5V();
    }
}
