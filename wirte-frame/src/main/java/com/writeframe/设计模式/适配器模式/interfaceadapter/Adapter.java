package com.writeframe.设计模式.适配器模式.interfaceadapter;

/**
 * @ClassName Adapter
 * @Description 接口适配器,组合引用原角色，也得实现目标对象的功能
 */
public class Adapter implements Target {
    //组合引用
    private Adaptee adaptee;

    public Adapter(Adaptee adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public int request0() {
        return 0;
    }

    @Override
    public int request1() {
        return 0;
    }

    @Override
    public int request2() {
        return 0;
    }

    @Override
    public int request3() {
        return 0;
    }

    @Override
    public int request4() {
        return 0;
    }

    /**
     * 它的使用场景：
     * 　　它是用于解决，需要适配功能很多的情况，并且都与适配原角色相关，如果针对每一个功能都写一个适配器，就会出现很多的类，
     * 造成类臃肿。　　
     * 　　所以，就想到把这多个功能，归类到一个目标对象中，统一创建一个接口适配器，就可以达到一个类适配多个功能的目的。
     * 　　　解决方案：通过接口适配器，只适配需要的接口方法。
     * @param args
     */
    public static void main(String[] args) {
        Target adapter = new Adapter(new Adaptee());
        int result = adapter.request0();
        System.out.println("最终结果："+result);

        result = adapter.request2();
        System.out.println("最终结果："+result);
    }
}
