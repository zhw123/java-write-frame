package com.writeframe.设计模式.适配器模式.testdemo1;


import com.writeframe.设计模式.适配器模式.ResultMsg;
import com.writeframe.设计模式.适配器模式.ResultMsg;

/**
 * 【2】原有登录系统功能
 * @ClassName PassportService
 * @Description 登录，注册服务
 * C.登录注册服务
 */
public class PassportService {

    public ResultMsg register(String username, String password) {
        return new ResultMsg(200, "注册成功", null);
    }

    public ResultMsg login(String username, String password) {
        return null;
    }
}
