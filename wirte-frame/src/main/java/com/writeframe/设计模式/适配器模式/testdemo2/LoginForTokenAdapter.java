package com.writeframe.设计模式.适配器模式.testdemo2;

/**
 * @ClassName LoginForTokenAdapter
 * @Description 内部员工登录，适配器
 */
public class LoginForTokenAdapter extends AbstractAdapter {
    @Override
    public boolean support(Object adapter) {
        return adapter instanceof LoginForTokenAdapter;
    }
}
