package com.writeframe.设计模式.适配器模式.testdemo;

import com.writeframe.设计模式.适配器模式.testdemo1.IPassportForThird;
import com.writeframe.设计模式.适配器模式.testdemo1.PassportForThirdAdapter;

public class Test {

    /**
     * 说明：
     * 　　 功能基本完成。但是，由于适配器内部需要进行各种openAPI接入，导致类的职责过重。
     *      而且，当系统进行需求扩展时，如：需要支持抖音帐号登录。就需要修改适配器原有代码，不符合开闭原则。
     * @param args
     */
    public static void main(String[] args) {
        IPassportForThird service = new PassportForThirdAdapter();
        //给一个openId,不会是qq号,为了保护用户隐私
        service.loginForQQ("owpgnw");
    }

}
