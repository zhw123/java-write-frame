package com.writeframe.设计模式.适配器模式.testdemo2;


import com.writeframe.设计模式.适配器模式.ResultMsg;

/**
 * 说明：
 * 　　如果要扩展抖音帐号登录，需要修改目标接口及实现。并且增加一个适配器实现子类。
 * 　　这只能在一定程度上，实现动态扩展的目的。但并不能够真正地满足开闭原则。
 * @ClassName AbstractAdapter
 * @Description 提取公共代码
 */
public abstract class AbstractAdapter extends PassportService implements ILoginAdapter{

    /**
     * 提供一个老的登录逻辑，先注册，再登录
     * 当使用第三方登录时，不能够得到密码，处理办法是：如果密码为null,约定一个特殊密码。
     * 后台做一个标识，只要得到这个特殊密码，都表示第三方登录，走特殊流程
     * @param username
     * @param password
     * @return
     */
    protected ResultMsg loginForRegister(String username, String password){
        if(null == password){
            password = "THIRD_EMPTY";
        }
        super.register(username,password);
        return super.login(username,password);
    }

    @Override
    public ResultMsg login(String id, Object adapter) {
        return loginForRegister(id,null);
    }
}
