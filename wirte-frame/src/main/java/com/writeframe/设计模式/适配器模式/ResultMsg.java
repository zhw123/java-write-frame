package com.writeframe.设计模式.适配器模式;

/**
 * 【1】原有登录系统功能
 * @ClassName ResultMsg
 * @Description 结果集
 * B.结果集pojo
 */
public class ResultMsg {
    private int code;
    private String msg;
    private Object data;
    public ResultMsg(int code, String msg,String data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
