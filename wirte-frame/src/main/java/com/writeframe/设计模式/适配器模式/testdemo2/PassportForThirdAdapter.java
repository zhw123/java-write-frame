package com.writeframe.设计模式.适配器模式.testdemo2;


import com.writeframe.设计模式.适配器模式.ResultMsg;

/**
 * @ClassName PassportForThirdAdapter
 * @Description 第三方登录适配，中间实现类
 * 【2】适配器中转类
 */
public class PassportForThirdAdapter implements IPassportForThird {
    @Override
    public ResultMsg loginForQQ(String openId) {
        return processLogin(openId,LoginForQQAdapter.class);
    }

    @Override
    public ResultMsg loginForWeChat(String openId) {
        return processLogin(openId,LoginForWeChatAdapter.class);
    }

    @Override
    public ResultMsg loginForToken(String token) {
        return processLogin(token,LoginForTokenAdapter.class);
    }

    @Override
    public ResultMsg loginForTelephone(String phone, String code) {
        return processLogin(phone,LoginForTelephoneAdapter.class);
    }

    private ResultMsg processLogin(String id, Class<? extends ILoginAdapter> clazz){
        try {
            ILoginAdapter adapter = clazz.newInstance();
            if(adapter.support(adapter)){
                return adapter.login(id,adapter);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
