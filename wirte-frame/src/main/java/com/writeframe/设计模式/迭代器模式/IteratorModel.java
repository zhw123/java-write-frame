package com.writeframe.设计模式.迭代器模式;

/**
 * 迭代器模式【Iterator Pattern】,又叫游标模式【Cursor Pattern】,它提供一种顺序访问集合/容器
 * 对象元素的方法，又无须暴露集合内部表示。
 * 本质：
 * 　　抽离集合对象迭代行为到迭代器中，提供一致访问接口。属于行为型模式。
 *
 *迭代器模式在生活中体现
 *          》寄件迭代分发
 *          》刷脸检票进站
 *
 *迭代器模式使用场景
 *          1.访问一个集合对象的内容而无须暴露它的内部表示【遍历集合元素】
 *          2.为遍历不同的集合结构提供一个统一的访问接口
 */
public class IteratorModel {

    public static void main(String[] args) {
        //创建一个聚合容器
        IAggregate<String> aggregate = new ConcreteAggregate<String>();
        //存储元素
        aggregate.add("one");
        aggregate.add("two");
        aggregate.add("three");
        //获取容器对象迭代器
        Iterator<String> iterator = aggregate.iterator();
        //遍历迭代器
        while (iterator.hasNext()) {
            String ele = iterator.next();
            System.out.println(ele);
        }
    }

}
