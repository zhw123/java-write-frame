package com.writeframe.设计模式.迭代器模式;


import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName ConcreteAggregate
 * @Description 聚合对象子类实现
 */
public class ConcreteAggregate<E> implements IAggregate<E> {
    private List<E> list = new ArrayList<E>();

    @Override
    public boolean add(E ele) {
        return this.list.add(ele);
    }

    @Override
    public boolean remove(E ele) {
        return this.list.remove(ele);
    }

    @Override
    public Iterator<E> iterator() {
        return new ConcreteIterator<E>(this.list);
    }
}
