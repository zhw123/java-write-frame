package com.writeframe.设计模式.迭代器模式;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName ConcreteIterator
 * @Description 实现迭代器功能
 */
public class ConcreteIterator<E> implements Iterator<E> {
    private List<E> list = new ArrayList<E>();
    int cursor;

    public ConcreteIterator(List<E> list) {
        this.list = list;
    }

    @Override
    public boolean hasNext() {
        return cursor != list.size();
    }

    @Override
    public E next() {
        return list.get(cursor++);
    }
}
