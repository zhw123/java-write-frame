package com.writeframe.设计模式.迭代器模式;


/**
 * @ClassName IAggregate
 * @Description 顶层聚合接口
 */
public interface IAggregate<E> {
    boolean add(E e);
    boolean remove(E e);
    Iterator<E> iterator();
}
