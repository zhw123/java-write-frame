package com.writeframe.设计模式.享元模式.demo;


/**
 * @ClassName ITicket
 * @Description 火车票，享元接口
 */
public interface ITicket {
    //根据席别区分车票
    void showInfo(String bunk);
}
