package com.writeframe.设计模式.享元模式.demo;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName TicketFactory
 * @Description 车票创建工厂
 *
 * 享元模式，看起来和注册工单例是很像的。然而享元模式的重点，在于系统结构上，而不关心对象创建，是不是单例的。
 */
public class TicketFactory {
    private static Map<String,ITicket> pool = new ConcurrentHashMap<String,ITicket>();
    public static ITicket queryTicket(String from, String to){
        String key = from +"->"+ to;
        if(!pool.containsKey(key)){
            ITicket ticket = new TrainTicket(from,to);
            System.out.println("首次查询，创建对象："+key);
            pool.put(key,ticket);
        }

        return pool.get(key);
    }


    public static void main(String[] args) {
        ITicket ticket = TicketFactory.queryTicket("杭州","遵义");
        ticket.showInfo("硬座");
    }
}
