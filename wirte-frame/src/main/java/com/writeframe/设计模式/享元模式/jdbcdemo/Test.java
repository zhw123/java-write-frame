package com.writeframe.设计模式.享元模式.jdbcdemo;

import java.sql.Connection;

public class Test {
    public static void main(String[] args) {
        ConnectionPool pool = new ConnectionPool();
        Connection conn = pool.getConnection();
        System.out.println(conn);

    }
}
