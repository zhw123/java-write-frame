package com.writeframe.设计模式.享元模式.jdbcdemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Vector;

/**
 * @ClassName ConnectionPool
 */
public class ConnectionPool {
    private Vector<Connection> pool;
    private int poolSize = 100;
    private String url = "jdbc:mysql://localhost:3306/test";
    private String username = "root";
    private String password = "root";
    private String driverClassName = "com.mysql.jdbc.Driver";

    public ConnectionPool() {
        this.pool = new Vector<Connection>(poolSize);
        try {
            Class.forName(driverClassName);
            for (int i = 0; i < poolSize; i++) {
                Connection conn = DriverManager.getConnection(url, username, password);
                pool.add(conn);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized Connection getConnection() {
        if (poolSize > 0) {
            Connection conn = pool.get(0);
            pool.remove(conn);
            return conn;
        }
        return null;
    }

    public synchronized void release(Connection conn) {
        pool.add(conn);
    }
}
