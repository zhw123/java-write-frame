package com.writeframe.设计模式.享元模式.demo;

import java.util.Random;

/**
 * @ClassName TrainTicket
 * @Description 火车票，具体享元实现
 */
public class TrainTicket implements ITicket {
    //列车出发地
    private String from;
    //目的地
    private String to;
    //价格由后台设定，用户无需填写
    private int price;

    public TrainTicket(String from, String to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public void showInfo(String bunk) {
        this.price = new Random().nextInt(500);
        System.out.println(from + "->" + to + "：" + bunk + ",价格是：" + this.price);

    }
}
