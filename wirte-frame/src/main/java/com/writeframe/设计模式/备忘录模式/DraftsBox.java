package com.writeframe.设计模式.备忘录模式;

import java.util.Stack;

/**
 * @ClassName DraftsBox
 * @Description 草稿箱
 */
public class DraftsBox {
    /**后进先出 栈*/
    private final Stack<ArticleMemento> STACK = new Stack<ArticleMemento>();

    /**
     * 压栈，将元素压入栈顶
     * @param memento
     */
    public void addMemento(ArticleMemento memento){
        STACK.push(memento);
    }
    public ArticleMemento getMemento(){
        ArticleMemento articleMemento = STACK.pop();
        return articleMemento;
    }

}
