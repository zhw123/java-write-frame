package com.writeframe.设计模式.访问者模式;

import java.util.Random;

/**
 * @ClassName Engineer
 * @Description 普通开发人员
 */
public class Engineer extends Employee{
    public Engineer(String name) {
        super(name);
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
    //考核：代码量
    public int getCodingLine(){
        return new Random().nextInt(100000);
    }

}
