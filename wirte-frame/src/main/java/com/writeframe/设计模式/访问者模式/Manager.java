package com.writeframe.设计模式.访问者模式;

import java.util.Random;

/**
 * @ClassName Manager
 * @Description 项目经理
 */
public class Manager extends Employee {
    public Manager(String name) {
        super(name);
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
    //考核：每年的新产品研发数量
    public int getProducts(){
        return new Random().nextInt(10);
    }
}
