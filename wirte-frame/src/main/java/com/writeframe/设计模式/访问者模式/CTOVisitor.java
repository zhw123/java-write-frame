package com.writeframe.设计模式.访问者模式;

/**
 * @ClassName CTOVisitor
 * @Description cto考核者
 */
public class CTOVisitor implements IVisitor{

    @Override
    public void visit(Engineer engineer) {
        System.out.println("工程师："+engineer.getName()+" ,编写代码行数："+engineer.getCodingLine());
    }

    @Override
    public void visit(Manager manager) {
        System.out.println("项目经理："+manager.getName()+" ,产品数量："+manager.getProducts());
    }
}
