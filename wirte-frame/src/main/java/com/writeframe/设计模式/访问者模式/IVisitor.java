package com.writeframe.设计模式.访问者模式;

/**
 * @ClassName IVisitor
 * @Description 访问者接口
 */
public interface IVisitor {
    //传参具体的元素
    void visit(Engineer engineer);

    void visit(Manager manager);
}
