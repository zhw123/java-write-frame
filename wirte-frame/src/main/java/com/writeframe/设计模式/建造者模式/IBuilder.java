package com.writeframe.设计模式.建造者模式;

/**
 * @ClassName IBuilder
 * @Description 实例构建接口
 */
public interface IBuilder<T> {
    T build();
}
