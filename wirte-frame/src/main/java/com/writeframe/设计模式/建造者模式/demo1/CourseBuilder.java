package com.writeframe.设计模式.建造者模式.demo1;

import com.writeframe.设计模式.建造者模式.IBuilder;

/**
 *
 * 建造者模式的总结:
 *      jdk源码中StringBuilder
 *              public StringBuilder append(StringBuffer sb) {
 *                  super.append(sb);
 *                  return this;　　//返回this,实现链式编程
 *              }
 */
public class CourseBuilder implements IBuilder<Course> {

    private Course course = new Course();

    public Course build() {
        return course;
    }


/*  public void addName(String name) {
        course.setName(name);
    }

    public void addPpt(String ppt) {
        course.setPpt(ppt);
    }

    public void addVideo(String video) {
        course.setVideo(video);
    }

    public void addNote(String note) {
        course.setNote(note);
    }

    public void addHomework(String homework) {
        course.setHomework(homework);
    }

    public static void main(String[] args) {
        CourseBuilder builder = new CourseBuilder();
        //设置很多步骤
        builder.addName("设计模式");
        builder.addHomework("作业");
        builder.addPpt("ppt课件");
        System.out.println(builder.build());

    }*/

//    优化方案：测试类中使用buidler.xxx进行设值，builder多次出现，且意义不大。为了避免重复，
//            　　能不能只让它出现一次，从而使用链式编程。就像StringBuilder一样。
//            　　xxx.append().append()....


    public CourseBuilder addName(String name) {
        course.setName(name);
        return this;
    }

    public CourseBuilder addPpt(String ppt) {
        course.setPpt(ppt);
        return this;
    }

    public CourseBuilder addVideo(String video) {
        course.setVideo(video);
        return this;
    }

    public CourseBuilder addNote(String note) {
        course.setNote(note);
        return this;
    }

    public CourseBuilder addHomework(String homework) {
        course.setHomework(homework);
        return this;
    }


    public static void main(String[] args) {
        //使用链式编程
        CourseBuilder builder = new CourseBuilder()
                .addName("设计模式")
                .addHomework("作业")
                .addPpt("ppt课件");

        System.out.println(builder.build());

    }

}
