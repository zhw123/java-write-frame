package com.writeframe.设计模式.解释器模式;

/**
 * @ClassName IArithmeticInterpreter
 * @Description 顶层解释器接口
 */
public interface IArithmeticInterpreter {
    int interpret();
}
