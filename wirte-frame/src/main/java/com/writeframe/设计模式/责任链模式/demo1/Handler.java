package com.writeframe.设计模式.责任链模式.demo1;

/**
 * 处理器
 */
public abstract class Handler {
    protected Handler nextHandle;

    public void setNextHandle(Handler nextHandle) {
        this.nextHandle = nextHandle;
    }
    public abstract void handleRequest(String request);
}
