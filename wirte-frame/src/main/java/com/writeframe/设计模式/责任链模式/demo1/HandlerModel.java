package com.writeframe.设计模式.责任链模式.demo1;

public class HandlerModel {
    public static void main(String[] args) {
        Handler handlerA = new ConcreteHandlerA();
        Handler handlerB = new ConcreteHandlerB();
        //设置链关系，先A后B
        handlerA.setNextHandle(handlerB);
        handlerA.handleRequest("requestB");
    }
}
