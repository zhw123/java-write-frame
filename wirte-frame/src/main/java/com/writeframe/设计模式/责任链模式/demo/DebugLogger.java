package com.writeframe.设计模式.责任链模式.demo;

public class DebugLogger  extends AbstractLogger {

    public DebugLogger(int level){
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("Standard Debug::Logger: " + message);
    }
}
