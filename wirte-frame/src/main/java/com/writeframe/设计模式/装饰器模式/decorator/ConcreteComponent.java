package com.writeframe.设计模式.装饰器模式.decorator;

/**
 * @ClassName ConcreteComponent
 * @Description 组件原生具体实现
 */
public class ConcreteComponent extends Component {
    @Override
    public void operation() {
        System.out.println("处理具体的业务逻辑");
    }
}
