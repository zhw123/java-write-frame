package com.writeframe.设计模式.装饰器模式.decorator;

/**
 * @ClassName Component
 * @Description 顶层抽象功能组件
 */
public abstract class Component {
    public abstract void operation();
}
