package com.writeframe.设计模式.装饰器模式.decorator;
/**
 * @ClassName ConcreteDecoratorA
 * @Description 装饰器实现子类A
 */
public class ConcreteDecoratorA extends Decorator {
    public ConcreteDecoratorA(Component component) {
        super(component);
    }

    @Override
    public void operation() {
        //此前添加功能
        operationFirst();
        super.operation();
        operationLast();
    }

    private void operationLast() {
        System.out.println("装饰器A 在调用父类方法之后执行");
    }

    private void operationFirst() {
        System.out.println("装饰器A 在调用父类方法之前执行");
    }
}
