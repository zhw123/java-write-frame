package com.writeframe.设计模式.装饰器模式.demo;
/**
 * @ClassName BaseBattercake
 * @Description 具体化煎饼功能
 */
public class BaseBattercake extends AbstractBattercake {
    @Override
    protected String getMsg() {
        return "这是一个煎饼";
    }

    @Override
    public int getPrice() {
        return 5;
    }
}
