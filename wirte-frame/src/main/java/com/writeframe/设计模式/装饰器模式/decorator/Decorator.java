package com.writeframe.设计模式.装饰器模式.decorator;

/**
 * @ClassName Decorator
 * @Description 装饰器抽象接口
 */
public abstract class Decorator extends Component{
    //持有组件引用
    protected  Component component;

    public Decorator(Component component){
        this.component = component;
    }

    @Override
    public void operation() {
        //转发请求给组件对象，可以在转发前后执行一些附加动作【增强组件】
        component.operation();
    }
}
