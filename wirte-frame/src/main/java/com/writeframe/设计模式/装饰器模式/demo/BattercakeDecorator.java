package com.writeframe.设计模式.装饰器模式.demo;

/**
 * @ClassName BattercakeDecorator
 * @Description 顶层装饰器
 * 说明：
 * 　　这个顶层装饰器很重要，必须要组合引用抽象组件。通过构造器封装。
 * 　　实现方法，调用父类方法。
 */
public abstract class BattercakeDecorator extends AbstractBattercake {
    private AbstractBattercake battercake;

    public BattercakeDecorator(AbstractBattercake battercake) {
        this.battercake = battercake;
    }

    @Override
    protected String getMsg() {
        return this.battercake.getMsg();
    }

    @Override
    public int getPrice() {
        return this.battercake.getPrice();
    }
}
