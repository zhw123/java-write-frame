package com.writeframe.设计模式.装饰器模式.demo;

/**
 * @ClassName SausageDecorator
 * @Description 加香肠装饰器
 */
public class SausageDecorator extends BattercakeDecorator{
    public SausageDecorator(AbstractBattercake battercake) {
        super(battercake);
    }

    @Override
    protected String getMsg() {
        return super.getMsg()+"，再加一根香肠";
    }

    @Override
    public int getPrice() {
        return super.getPrice() + 2;
    }
}
