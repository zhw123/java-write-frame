package com.writeframe.设计模式.观察者模式.demo2;


/**
 * @ClassName MouseEventType
 * @Description 鼠标事件
 */
public interface MouseEventType {
    String ON_CLICK = "click";
}
