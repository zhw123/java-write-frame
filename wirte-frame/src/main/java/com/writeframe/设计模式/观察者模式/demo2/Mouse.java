package com.writeframe.设计模式.观察者模式.demo2;



/**
 * @ClassName Mouse
 * @Description 具体的被观察者
 */
public class Mouse extends EventListener {
    public void click() {
        System.out.println("调用单机方法");
        this.trigger(MouseEventType.ON_CLICK);
    }
}
