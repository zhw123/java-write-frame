package com.writeframe.设计模式.观察者模式.demo1;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * @ClassName GuavaEvent
 * @Description 观察者
 */
public class GuavaEvent {
    //表示观察者回调
    @Subscribe
    public void observer(String str) {
        System.out.println("执行observer方法，传参为：" + str);
    }

    public static void main(String[] args) {
        EventBus eventBus = new EventBus();
        GuavaEvent event = new GuavaEvent();
        eventBus.register(event);

        eventBus.post("tom");
        eventBus.post("tom1");
        eventBus.post("tom1");
    }
}
