package com.writeframe.设计模式.观察者模式.demo;

import java.util.Observable;
import java.util.Observer;

/**
 * @ClassName Teacher
 * @Description 观察者
 */
public class Teacher implements Observer {
    private String name;

    public Teacher(String name) {
        this.name = name;
    }

    @Override
    public void update(Observable ob, Object arg) {
        GPer gper = (GPer) ob;
        Question question = (Question) arg;
        System.out.println("===================");
        System.out.println(name + "老师，你好\n" +
                "，您收到一个来自" + gper.getName() + "的提问，希望你解答，问题内容如下：\n" + question.getContent() +
                "\n提问者：" + question.getUserName());
    }
}
