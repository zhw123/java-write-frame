package com.writeframe.设计模式.观察者模式.demo;

import java.util.Observable;

/**
 * @ClassName GPer
 * @Description 社区生态圈，被观察者
 */
public class GPer extends Observable {
    private String name = "GPer 生态圈";

    public String getName() {
        return name;
    }

    private static final GPer gper = new GPer();

    private GPer() {
    }

    public static GPer getInstance(){
        return gper;
    }

    public void publishQuestion(Question question){
        System.out.println(question.getUserName()+" 在" +this.name +"提交了一个问题");
        //调用jdk api
        setChanged();
        notifyObservers(question);
    }
}
