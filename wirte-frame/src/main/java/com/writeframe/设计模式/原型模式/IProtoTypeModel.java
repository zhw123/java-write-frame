package com.writeframe.设计模式.原型模式;

/**
 * 说明：
 * 　　这里通过调用clone方法，得到一个新的实例。并且成员的值，和原有对象是一样。
 * 　　我们在编码中，经常存在对象转换，如：DO转换DTO,或VO。我们需要把成员的值，也一起设置给新的对象。
 * 　　就可以使用原型模式。
 *    原型模式：创建克隆对象------不通过构造器，进行创建。创建过程不能定制。
 */
public class IProtoTypeModel {
    public static void main(String[] args) {
        ConcretePrototype prototype = new ConcretePrototype();
        prototype.setName("test");
        prototype.setAge(18);
        System.out.println(prototype);
        ConcretePrototype cloneType = prototype.clone();
        System.out.println(cloneType);
    }
}
