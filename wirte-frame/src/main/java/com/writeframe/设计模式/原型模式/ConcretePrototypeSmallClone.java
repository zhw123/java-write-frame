package com.writeframe.设计模式.原型模式;

import java.util.ArrayList;
import java.util.List;

/**
 * 浅克隆: 通过查询Cloneable接口的源码，发现内部并没有定义clone方法，而是使用Object中clone方法。
 * 注意：
 * 　　通过查询Cloneable接口的源码，发现内部并没有定义clone方法，而是使用Object中clone方法。
 * 　　 Object是所有类的父类，所以，clone方法定义在Object中，是一个native方法：
 *     protected native Object clone() throws CloneNotSupportedException;
 *总结：
 *
 * 　　发现两个对象中成员hobbies的内存地址是一样。
 * 　　我们发现，hobbies是一个List类型成员，是引用类型。jdk中clone方法，在克隆对象时，只是把原型对象成员的内存地址进行的复制，而不是复制属性的值。
 * 　　所以，当修改克隆对象属性值，原型对象的属性值也会被改变。
 * *
 */
public class ConcretePrototypeSmallClone implements Cloneable {
    private int age;
    private String name;

    //增加一个引用类型成员
    private List<String> hobbies = new ArrayList<>();


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    @Override
    public ConcretePrototypeSmallClone clone() {
        try {
            //调用jdk中clone()实现
            return (ConcretePrototypeSmallClone) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "ConcretePrototype{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    public static void main(String[] args) {
        ConcretePrototypeSmallClone prototype = new ConcretePrototypeSmallClone();
        prototype.setName("test");
        prototype.setAge(18);
        //既爱书法，也爱美术
        List<String> hobbies = new ArrayList<>();
        hobbies.add("书法");
        hobbies.add("美术");
        prototype.setHobbies(hobbies);
        //System.out.println(prototype);    //调整打印顺序

        ConcretePrototypeSmallClone cloneType = prototype.clone();
        //还是个技术控
        cloneType.getHobbies().add("技术控");

        System.out.println("原型对象："+prototype);
        System.out.println("克隆对象："+cloneType);
        System.out.println(prototype==cloneType);//预期：返回true,实际为false

        System.out.println("原型对象的爱好属性:"+prototype.getHobbies());
        System.out.println("克隆对象的爱好属性:"+cloneType.getHobbies());
        System.out.println(prototype.getHobbies() == cloneType.getHobbies());

    }
}
