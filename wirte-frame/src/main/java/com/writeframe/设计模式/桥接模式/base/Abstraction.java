package com.writeframe.设计模式.桥接模式.base;

/**
 * @ClassName Abstraction
 * @Description 抽象类, 组合引用接口实例
 */
public abstract class Abstraction {
    protected IImplementor mImplementor;

    public Abstraction(IImplementor mImplementor) {
        this.mImplementor = mImplementor;
    }

    public void operation() {
        this.mImplementor.operationImpl();
    }
}
