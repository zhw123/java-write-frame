package com.writeframe.设计模式.桥接模式.base;

/**
 * 定义：
 * 　　Bridge Pattern,也称桥梁模式，接口模式，或柄体（Handle and Body）模式，是将抽象部分与它的具体实现部分分离，使它们可以独立变化的。
 * 　　通过组合的方式建立两个类之间的联系，而不使用继承。
 * 　　属于结构型模式。
 * 桥接模式在生活中的应用场景：
 *
 * 1.连接两个空间的拱桥
 *
 * 2.虚拟网络与真实网络的桥接
 *适用场景
 * 1.在抽象和具体实现之间需要增加更多的灵活性的场景
 * 2.一个类存在两个（或多个）独立变化的维度，而这两个(或多个)维度都需要独立进行扩展。
 * 3.不希望继承，或因为多层继承导致系统类的个数剧增【多用组合引用】
 *
 */
public class Test {
    public static void main(String[] args) {
        RefinedAbstraction abstraction = new RefinedAbstraction(new ConcreteImplementorA());
        abstraction.operation();
    }
}
