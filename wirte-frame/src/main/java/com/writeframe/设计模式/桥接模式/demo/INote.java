package com.writeframe.设计模式.桥接模式.demo;


/**
 * @ClassName INote
 * @Description 笔记接口
 */
public interface INote {
    void edit();
}

