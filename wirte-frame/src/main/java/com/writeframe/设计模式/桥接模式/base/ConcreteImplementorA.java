package com.writeframe.设计模式.桥接模式.base;

/**
 * @ClassName ConcreteImplementorA
 * @Description 实现
 */
public class ConcreteImplementorA implements IImplementor {
    @Override
    public void operationImpl() {
        System.out.println("I'm ConcreteImplementorA");
    }
}
