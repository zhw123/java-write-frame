package com.writeframe.设计模式.模板方法;

abstract class AbstractClass {
    //一些抽象行为，放到子类去实现
    public abstract void PrimitiveOperation1();

    public abstract void PrimitiveOperation2();

    //模板方法，给出逻辑骨架。
    public void TemplateMethod() {
        PrimitiveOperation1();
        PrimitiveOperation2();
        System.out.println("模板方法");
    }
}
