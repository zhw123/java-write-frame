package com.writeframe.设计模式.委派模式;


import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName Leader
 * @Description 部门主管
 */
public class Leader implements IEmployee {
    private Map<String,IEmployee> employeeMap = new HashMap<String,IEmployee>();

    public Leader() {
        employeeMap.put("爬虫",new EmployeeA());
        employeeMap.put("海报图",new EmployeeB());
    }

    @Override
    public void doWork(String task) {
        //结合策略模式
        if(!employeeMap.containsKey(task)){
            //卖一个产品
            System.out.println("这个任务【"+task+"】，超出我的能力范围");
            return;
        }
        employeeMap.get(task).doWork(task);
    }
}
