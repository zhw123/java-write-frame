package com.writeframe.设计模式.委派模式;


/**
 * @ClassName EmployeeA
 * @Description 部门主管下，员工A
 */
public class EmployeeA implements IEmployee {
    private String goodAt = "编程";
    @Override
    public void doWork(String task) {
        System.out.println("我是员工A,我擅长【"+goodAt+"】,现在开始做【"+task+"]工作");
    }
}
