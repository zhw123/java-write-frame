package com.writeframe.设计模式.委派模式;


/**
 * @ClassName EmployeeB
 * @Description 部门主管,下员工B
 */
public class EmployeeB implements IEmployee {
    private String goodAt = "平面设计";
    @Override
    public void doWork(String task) {
        System.out.println("我是员工B,我擅长【"+goodAt+"】,现在开始做【"+task+"]工作");
    }
}

