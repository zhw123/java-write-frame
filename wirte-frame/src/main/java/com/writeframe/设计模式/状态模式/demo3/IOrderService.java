package com.writeframe.设计模式.状态模式.demo3;

import java.util.Map;

/**
 * @ClassName IOrderService
 * @Description 订单服务接口
 */
public interface IOrderService {
    Order create();

    Order pay(int id);

    Order deliver(int id);

    Order receive(int id);

    Map<Integer,Order> getOrders();
}
