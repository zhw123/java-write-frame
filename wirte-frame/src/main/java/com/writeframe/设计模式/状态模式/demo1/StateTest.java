package com.writeframe.设计模式.状态模式.demo1;

public class StateTest {
    public static void main(String[] args) {
        AppContext context = new AppContext();
        //默认为未登录状态
        context.favorite();
        context.comment("你好，你评论得perfect");
    }
}
