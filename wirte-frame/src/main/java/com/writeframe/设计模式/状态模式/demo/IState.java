package com.writeframe.设计模式.状态模式.demo;

/**
 * @ClassName IState
 * @Description 顶层状态接口
 */
public interface IState {
    void handle();
}
