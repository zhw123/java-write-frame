package com.writeframe.设计模式.状态模式.demo1;

/**
 * @ClassName AppContext
 * @Description 上下文
 * @Version 1.0
 */
public class AppContext {
    public static final UserState STATE_LOGIN = new LoginState();
    public static final UserState STATE_UNLOGIN = new UnLoginState();
    private UserState currentState = STATE_UNLOGIN;

    {
        STATE_LOGIN.setContext(this);
        STATE_UNLOGIN.setContext(this);
    }

    public void setState(UserState currentState) {
        this.currentState = currentState;
    }

    public UserState getState() {
        return currentState;
    }

    public void favorite() {
        this.currentState.favorite();
    }

    public void comment(String comment) {
        this.currentState.comment(comment);
    }
}
