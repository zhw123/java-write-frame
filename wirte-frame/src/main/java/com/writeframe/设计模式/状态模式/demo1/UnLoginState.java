package com.writeframe.设计模式.状态模式.demo1;

/**
 * @ClassName UnLoginState
 * @Description 未登录状态
 */
public class UnLoginState extends UserState {
    @Override
    public void favorite() {
        //要想收藏，先进行登录
        this.switchToLogin();
        //调用登录后的功能
        this.context.getState().favorite();
    }

    @Override
    public void comment(String comment) {
        this.switchToLogin();
        this.context.getState().comment(comment);
    }
    private void switchToLogin(){
        System.out.println("跳转到登录页，进行登录");
        //登录完成后，切换为登录状态
        this.context.setState(this.context.STATE_LOGIN);
    }
}
