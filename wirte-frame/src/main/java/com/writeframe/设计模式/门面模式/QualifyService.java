package com.writeframe.设计模式.门面模式;
/**
 * @ClassName QualifyService
 * @Description 库存系统
 */
public class QualifyService {
    public boolean isAvailable(GiftInfo giftInfo){
        System.out.println("校验 " + giftInfo.getName()+" 积分足够，库存足够，允许兑换");
        return true;
    }
}
