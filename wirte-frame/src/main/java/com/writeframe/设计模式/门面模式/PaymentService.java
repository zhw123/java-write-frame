package com.writeframe.设计模式.门面模式;

/**
 * @ClassName PaymentService
 * @Description 支付系统
 */
public class PaymentService {
    public boolean pay(GiftInfo giftInfo){
        System.out.println("扣减 "+giftInfo.getName() + " 积分成功");
        return true;
    }
}
