package com.writeframe.设计模式.门面模式;

/**
 *
 *Facade Pattern,又称外观模式。提供一个统一的接口，用来访问子系统中的一群接口。属于结构型设计模式。
 * 特征：
 * 　　门面模式定义一个高层接口，让子系统更容易使用。
 * 1.门面模式的适用场景
 * 　　子系统越来越复杂，增加门面模式提供简单接口。
 * 　　构建多层系统结构，利用门面对象作为每层的入口，简化层间调用。
 * 2.生活中门面模式运用
 * 　　前台接待员【前台---为公司做导向】
 * 　　包工头
 *
 * 有一个社区商城，它不是直接通过付费进行购买礼品。而是通过积分兑换，当对一个礼品有兴趣时，通常点击商品图片，
 * 进入商品详情页，商品下边提供立即兑换功能。当点击兑换时，弹出表单，获取用户信息【自己填写表单】，如：姓名，电话，邮箱，备注，
 * 然后确定兑换。会判断你的积分是否足够，如果积分足够，就发起一个支付动作，否则无法兑换。
 * 　　支付完成后，进入物流信息记录。发货时间，订单号。。。
 * 　　这样一个购物流程，如果全部由前端来控制，需要调用大量的接口，前端工作量会很大，调用会很复杂，那么，该怎么办呢？
 * 为了提升开发效率，减轻前端开发工作量，后端可以统一封装一个接口，控制整个流程。
 * 系统设计：
 * 　　分析业务bean【礼品GiftInfo】
 * 　　系统构成：支付系统PaymentService----》库存系统QualifyService【校验积分是否足够】-----》物流系统ShippingService
 */
public class facadeModel {
    public static void main(String[] args) {
        QualifyService qualifyService = new QualifyService();
        PaymentService paymentService = new PaymentService();
        ShippingService shippingService = new ShippingService();

        //判断积分是否足够
        GiftInfo giftInfo = new GiftInfo("《spring5 核心原理解析》");
        if (qualifyService.isAvailable(giftInfo)) {
            if (paymentService.pay(giftInfo)) {
                String shippingNO = shippingService.delivery(giftInfo);
                System.out.println("物流系统下单成功，物流单号是：" + shippingNO);
            }
        }
    }
}
