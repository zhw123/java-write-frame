package com.writeframe.设计模式.策略模式;

/**
 * 上下文对象
 */
public class Context {
    private IStrategy strategy;

    public Context(IStrategy strategy) {
        this.strategy = strategy;
    }
    //由构造器中传参实现子类类型，来决定选择哪一种算法
    public void algorithm(){
        this.strategy.algorithm();
    }
}
