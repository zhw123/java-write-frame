package com.writeframe.设计模式.策略模式;

public class ConcreteStrategyA implements IStrategy {
    @Override
    public void algorithm() {
        System.out.println("这是算法A");
    }
}
