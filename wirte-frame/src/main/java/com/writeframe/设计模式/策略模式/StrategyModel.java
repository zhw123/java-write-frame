package com.writeframe.设计模式.策略模式;

/**
 * 策略模式【Strategy Pattern】,又叫政策模式【Policy Pattern】,它是将定义的算法家族，分别封装起来，让它们之间可以相互替换，从而
 * 让算法的变化不会影响到使用算法的用户。
 * 　　可以避免多重分支的if...else...和switch语句。
 * 　　属于行为型模式。
 */
public class StrategyModel {
    public static void main(String[] args) {
        //使用时，客户选择一种策略
        IStrategy strategy = new ConcreteStrategyA();
        //封装到上下文中
        Context context = new Context(strategy);
        //调用策略方法
        context.algorithm();
    }
}
