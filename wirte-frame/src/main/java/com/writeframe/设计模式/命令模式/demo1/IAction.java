package com.writeframe.设计模式.命令模式.demo1;

/**
 * @ClassName IAction
 * @Description 命令接口
 */
public interface IAction {
    void execute();
}
