package com.writeframe.设计模式.命令模式.demo1;

/**
 * @ClassName Player
 * @Description 播放器, 命令接收者
 */
public class Player {
    public void play() {
        System.out.println("正常播放");
    }

    public void speed() {
        System.out.println("拖动滚动条");
    }

    public void stop() {
        System.out.println("停止播放");
    }

    public void pause() {
        System.out.println("暂停播放");
    }
}
