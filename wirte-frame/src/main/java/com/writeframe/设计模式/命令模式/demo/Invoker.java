package com.writeframe.设计模式.命令模式.demo;

/**
 * @ClassName Invoker
 * @Description 调用者，命令发出方
 */
public class Invoker {
    private ICommand command;

    public Invoker(ICommand command) {
        this.command = command;
    }

    public void action(){
        command.execute();
    }
}
