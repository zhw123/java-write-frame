package com.writeframe.设计模式.命令模式.demo1;
/**
 * @ClassName SpeedAction
 * @Description 拖动滚动条播放命令
 */
public class SpeedAction implements IAction {
    private Player player;

    public SpeedAction(Player player) {
        this.player = player;
    }

    @Override
    public void execute() {
        player.speed();
    }
}
