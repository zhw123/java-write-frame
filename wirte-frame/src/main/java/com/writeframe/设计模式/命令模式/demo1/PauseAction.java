package com.writeframe.设计模式.命令模式.demo1;

/**
 * @ClassName PauseAction
 * @Description 暂停播放命令
 */
public class PauseAction implements IAction {
    private Player player;

    public PauseAction(Player player) {
        this.player = player;
    }

    @Override
    public void execute() {
        player.pause();
    }
}
