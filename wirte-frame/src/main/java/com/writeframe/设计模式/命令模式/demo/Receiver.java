package com.writeframe.设计模式.命令模式.demo;

/**
 * @ClassName Receiver
 * @Description 命令接收方
 */
public class Receiver {
    public void action(){
        System.out.println("根据接收到的命令，执行具体操作");
    }
}
