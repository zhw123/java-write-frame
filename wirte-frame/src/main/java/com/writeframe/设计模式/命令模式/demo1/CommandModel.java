package com.writeframe.设计模式.命令模式.demo1;


/**
 * @ClassName Test
 * @Description 测试类
 */
public class CommandModel {
    public static void main(String[] args) {
        Player player = new Player();
        Controller controller = new Controller();
        //遥控器，点击正常播放键，播放器接收到命令，开始播放
        controller.execute(new PlayAction(player));
        System.out.println("==========开始组合命令式执行：==============");
        //多条命令批量执行
        IAction pause = new PauseAction(player);
        IAction play = new PlayAction(player);
        IAction stop = new StopAction(player);
        IAction speed = new SpeedAction(player);

        controller.addAction(pause);
        controller.addAction(play);
        controller.addAction(stop);
        controller.addAction(speed);

        controller.batchExecute();
    }
}
