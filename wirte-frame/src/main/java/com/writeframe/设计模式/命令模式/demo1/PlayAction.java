package com.writeframe.设计模式.命令模式.demo1;

/**
 * @ClassName PlayAction
 * @Description 正常播放命令实现
 */
public class PlayAction implements IAction {
    private Player player;

    public PlayAction(Player player) {
        this.player = player;
    }

    @Override
    public void execute() {
        player.play();
    }
}
