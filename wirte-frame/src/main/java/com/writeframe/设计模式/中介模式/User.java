package com.writeframe.设计模式.中介模式;

/**
 * @ClassName User
 * @Description 用户类
 */
public class User {
    private ChatRoom chatRoom;
    private String name;

    public String getName() {
        return name;
    }

    public User(String name,ChatRoom chatRoom) {
        this.chatRoom = chatRoom;
        this.name = name;
    }

    public void sendMessage(String msg){
        //用户传达信息，通过聊天室
        this.chatRoom.showMsg(this, msg);
    }
}
