package com.writeframe.设计模式.代理模式.dynamicproxy.cglibproxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @ClassName CglibMarriageAgency
 * @Description 婚介所 代理类
 */
public class CglibMarriageAgency implements MethodInterceptor {
    //jdk动态代理 实现一个抽象的接口【接口可以定义任意功能】

    //加强目标对象功能，需要引入目标对象
    private IPerson person;

    public Object getProxy(Class<?> clazz) {
        //根据目标对象，得到代理对象
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(clazz);
        enhancer.setCallback(this);
        return enhancer.create();
    }

    private void doAfter() {
        System.out.println("是不是确认关系，开始交往");
    }

    private void doBefore() {
        System.out.println("开始物色对象");
    }

    @Override
    public Object intercept(Object obj, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        doBefore();
        Object result = methodProxy.invokeSuper(obj, objects);
        doAfter();
        return result;
    }


    public static void main(String[] args) {
        PersonZhang person = (PersonZhang) new CglibMarriageAgency().getProxy(PersonZhang.class);
        person.findLove();
    }

}
