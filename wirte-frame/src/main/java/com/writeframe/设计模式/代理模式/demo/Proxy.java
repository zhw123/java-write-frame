package com.writeframe.设计模式.代理模式.demo;

/**
 * @ClassName Proxy
 * @Description 代理对象
 */
public class Proxy implements ISubject {
    //代理对象 需要对目标对象的功能进行增强
    //即增强目标对象的方法，所以实现同一接口
    //增强目标对象，所以目标对象【基于多态，可以引入顶层接口bean】
    private ISubject subject;

    //客户端要传参目标对象，这里定义构建器传参
    public Proxy(ISubject subject) {
        this.subject = subject;
    }

    //程序中，什么叫功能增强，就是多加一段代码，提供更多功能
    @Override
    public void request() {
        doBefore();
        //方法前增强
        subject.request();
        //方法后增强
        doAfter();
    }

    private void doAfter() {
        System.out.println("方法后面进行增强");
    }

    private void doBefore() {
        System.out.println("方法前面进行增强");
    }

    public static void main(String[] args) {
        RealSubject subject = new RealSubject();
        Proxy proxy = new Proxy(subject);
        proxy.request();
    }
}
