package com.writeframe.设计模式.代理模式.staticproxy;

/**
 * @ClassName Person
 * @Description 程序员 目标对象
 */
public class Person implements IPerson {
    @Override
    public void findLove() {
        System.out.println("程序员，自己没有找到女朋友");
    }
}
