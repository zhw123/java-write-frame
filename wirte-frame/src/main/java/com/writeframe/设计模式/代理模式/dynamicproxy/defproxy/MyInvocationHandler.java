package com.writeframe.设计模式.代理模式.dynamicproxy.defproxy;

import java.lang.reflect.Method;

/**
 * 自定义handler
 */
public interface MyInvocationHandler {
    public Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable;
}
