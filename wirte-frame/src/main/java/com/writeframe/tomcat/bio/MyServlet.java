package com.writeframe.tomcat.bio;

/**
 * @ClassName: MyServlet
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/5/5 15:55
 */
public interface MyServlet {
    void init() throws Exception;
    void service(Request request, Response response) throws Exception;
    void destory();
}
