package com.writeframe.tomcat.bio;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @ClassName: UserServlet
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/5/5 15:56
 */
public class UserServlet extends MyHttpServlet {
    @Override
    public void doGet(Request request, Response response) {
        this.doPost(request,response);
    }
    @Override
    public void doPost(Request request, Response response) {
        try {
            //省略业务调用的代码，tomcat会根据request对象里面的inputStream拿到对应的参数进行业务调用
            //模拟业务层调用后的返回
            OutputStream outputStream=response.outputStream;
            String result=Response.responseData("user handle successful");
            outputStream.write(result.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void init() throws Exception { }
    public void destory() { }

}
