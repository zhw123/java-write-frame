package com.writeframe.tomcat.nio;



import java.io.IOException;

/**
 * @ClassName: HelloWorldServlet
 * @Description: http://localhost:8080/world?data=yes
 * @Author: zhanghongwei
 * @Date: 2022/5/5 15:39
 */
public class HelloWorldServlet extends MyServlet {
    @Override
    public void doGet(MyRequest myRequest, MyResponse myResponse) {
        try{
            myResponse.write("get hello world");
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(MyRequest myRequest, MyResponse myResponse) {
        try{
            myResponse.write("post hello world");
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}

