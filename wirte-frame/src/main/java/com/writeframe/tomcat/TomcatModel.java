package com.writeframe.tomcat;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName: TomcatModel
 * @Description: 客户端和服务器的通信，说到底就是两个数据的传输，客户端发送inputStream给服务器，服务器回复outputStream给客户端。
 * @Author: zhanghongwei
 * @Date: 2022/5/5 13:25
 */
public class TomcatModel {

    //客户端只能识别符合HTTP响应协议的数据 模拟浏览器浏览器响应格式
    public static final String responsebody = "HTTP/1.1 200+\r\n" + "Content-Type：text/html+\r\n" + "\r\n";

    /**
     * http://localhost:8080/hello
     * 收到客户请求
     * BIO模型解决只能连接一次的弊端
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        //开启ServerSocket服务，设置端口号为8080
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("======服务启动成功========");
        //当服务没有关闭时
        while (!serverSocket.isClosed()) {
            //使用socket进行通信
            Socket socket = serverSocket.accept();
//            //对于每个连接，都开启一个线程
//            RequestHandler requestHandler=new TomcatModel.RequestHandler(socket);
//            new Thread(requestHandler).start();

            // 服务器一次只能连接一个客户端
            //收到客户端发出的inputstream
            InputStream inputStream = socket.getInputStream();
            System.out.println("执行客户请求:" + Thread.currentThread());
            System.out.println("收到客户请求");
            //读取inputstream的内容
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));
            String msg = null;
            while ((msg = reader.readLine()) != null) {
                if (msg.length() == 0) break;
                System.out.println(msg);
            }
            //返回outputstream，主体内容是OK
            String resp = "OK";
            OutputStream outputStream = socket.getOutputStream();
            System.out.println(resp);
            outputStream.write(resp.getBytes());
            outputStream.flush();
            outputStream.close();
            socket.close();
        }
    }

   static class RequestHandler implements Runnable {
        public Socket socket;

        public RequestHandler(Socket socket) {
            this.socket = socket;
        }

        //继承Runnable接口，实现run方法
        public void run() {
            InputStream inputStream = null;
            try {
                inputStream = socket.getInputStream();
                System.out.println("执行客户请求" + Thread.currentThread());
                System.out.println("====收到客户端请求====");
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));
                String msg = null;
                while ((msg = reader.readLine()) != null) {
                    if (msg.length() == 0) {
                        break;
                    }
                    System.out.println(msg);
                }
                String resp =responsebody + "OK";
                OutputStream outputStream = socket.getOutputStream();
                System.out.println(resp);
                outputStream.write(resp.getBytes());
                outputStream.flush();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}


