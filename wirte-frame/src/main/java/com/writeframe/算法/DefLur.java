package com.writeframe.算法;

import java.util.LinkedHashMap;
import java.util.Map;

public class DefLur extends LinkedHashMap {

    //最大容量
    private final int maxCapacity;

    private static final float DEFAULT_LOAD_FACTOR = 0.75f;


    public DefLur(int maxCapacity) {
        super(maxCapacity, DEFAULT_LOAD_FACTOR, true);
        this.maxCapacity = maxCapacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry eldest) {
        // 当达到预设缓存上限时删除最老元素
        return this.size()>maxCapacity;
    }
}
