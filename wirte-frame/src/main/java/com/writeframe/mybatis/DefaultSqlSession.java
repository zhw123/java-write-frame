package com.writeframe.mybatis;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * SqlSession接口的实现类
 */
public class DefaultSqlSession implements SqlSession {

    private Configuration cfg;
    private Connection connecion;

    public DefaultSqlSession(Configuration cfg) {
        this.cfg = cfg;
        this.connecion = DataSourceUtil.getConnection(cfg);
    }

    /**
     * 用于创建代理对象
     *
     * @param daoInterfaceClass dao的接口字节码
     * @param <T>
     * @return
     */
    public <T> T getMapper(Class<T> daoInterfaceClass) {

        return (T)Proxy.newProxyInstance(daoInterfaceClass.getClassLoader(), new Class[] {daoInterfaceClass}, new MapperProxy(cfg.getMappers(), connecion));
    }

    /**
     * 用于释放资源
     */
    public void close() {
        if (connecion != null) {
            try {
                connecion.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
