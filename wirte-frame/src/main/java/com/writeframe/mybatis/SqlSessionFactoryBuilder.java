package com.writeframe.mybatis;

import java.io.InputStream;

/*
 * 用于创建sqlSessionFactory对象
 *运行逻辑：
     * 1.解析xml配置文件（包括主配置文件和映射文件）
     *
     * 2.根据xml配置文件创建SqlSessionFactory
     *
     * 3.由SqlSessionFactory 创建 sqlSession对象用来执行mapper接口中定义的方法，并封装到实体类
 */
public class SqlSessionFactoryBuilder {
    /**
     * 更据配置文件字节输入流创建sqlSessionFactory工厂
     *
     * @param in
     * @return
     */
    public SqlSessionFactory build(InputStream in) {
        Configuration cfg = XMLConfigBuilder.loadConfiguration(in);

        return new DefaultSqlSessionFactory(cfg);
    }
}
