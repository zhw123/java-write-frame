package com.writeframe.mybatis;

public interface SqlSession {

    /**
     * 创建dao接口的代理对象
     *
     * @param daoClass
     * @param <T>
     * @return
     */
    <T> T getMapper(Class<T> daoClass);

    /**
     * 释放资源链接
     */
    void close();
}
