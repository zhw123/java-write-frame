package com.writeframe.baselock;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * Semaphore是一个计数信号量，常用于限制可以访问某些资源（物理或逻辑的）线程数目。
 * 非公平：
 *      public Semaphore(int permits);//permits就是允许同时运行的线程数目
 * 公平（获得锁的顺序与线程启动顺序有关）：
 *      public Semaphore(int permits,boolean fair);//permits就是允许同时运行的线程数目
 * 创建一个信号量
 *      Semaphore semaphore = new Semaphore(2);
 * 从信号量中获取一个许可
 *      semaphore.acquire();
 * 释放一个许可(在释放许可之前，必须先获获得许可。)
 *      semaphore.release();
 * 尝试获取一个许可，若获取成功返回true，若获取失败返回false
 *      semaphore.tryAcquire();
 *
 * 信号量通常用于限制线程数，而不是访问某些（物理或逻辑）资源。
 *
 * Semaphore： 信号灯。 适用场景：限制资源，如抢位置、限流等。
 *
 */
public class SemaphoreDemo {
    //假设有10个人在银行办理业务，只有2个工作窗口，代码实现逻辑如下

    // 排队总人数（请求总数）
    public static int clientTotal = 10;

    // 可同时受理业务的窗口数量（同时并发执行的线程数）
    public static int threadTotal = 2;

    /**
     * // 创建具有给定的许可数和非公平的公平设置的 Semaphore。
     * Semaphore(int permits)
     * // 创建具有给定的许可数和给定的公平设置的 Semaphore。
     * Semaphore(int permits, boolean fair)
     *
     * // 从此信号量获取一个许可，在提供一个许可前一直将线程阻塞，否则线程被中断。
     * void acquire()
     * // 从此信号量获取给定数目的许可，在提供这些许可前一直将线程阻塞，或者线程已被中断。
     * void acquire(int permits)
     * // 从此信号量中获取许可，在有可用的许可前将其阻塞。
     * void acquireUninterruptibly()
     * // 从此信号量获取给定数目的许可，在提供这些许可前一直将线程阻塞。
     * void acquireUninterruptibly(int permits)
     * // 返回此信号量中当前可用的许可数。
     * int availablePermits()
     * // 获取并返回立即可用的所有许可。
     * int drainPermits()
     * // 返回一个 collection，包含可能等待获取的线程。
     * protected Collection<Thread> getQueuedThreads()
     * // 返回正在等待获取的线程的估计数目。
     * int getQueueLength()
     * // 查询是否有线程正在等待获取。
     * boolean hasQueuedThreads()
     * // 如果此信号量的公平设置为 true，则返回 true。
     * boolean isFair()
     * // 根据指定的缩减量减小可用许可的数目。
     * protected void reducePermits(int reduction)
     * // 释放一个许可，将其返回给信号量。
     * void release()
     * // 释放给定数目的许可，将其返回到信号量。
     * void release(int permits)
     * // 返回标识此信号量的字符串，以及信号量的状态。
     * String toString()
     * // 仅在调用时此信号量存在一个可用许可，才从信号量获取许可。
     * boolean tryAcquire()
     * // 仅在调用时此信号量中有给定数目的许可时，才从此信号量中获取这些许可。
     * boolean tryAcquire(int permits)
     * // 如果在给定的等待时间内此信号量有可用的所有许可，并且当前线程未被中断，则从此信号量获取给定数目的许可。
     * boolean tryAcquire(int permits, long timeout, TimeUnit unit)
     * // 如果在给定的等待时间内，此信号量有可用的许可并且当前线程未被中断，则从此信号量获取一个许可。
     * boolean tryAcquire(long timeout, TimeUnit unit)
     * @param args
     * @throws Exception
     */

    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(threadTotal);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int i = 0; i < clientTotal; i++) {
            final int count = i;
            executorService.execute(() -> {
                try {
                    //从此信号量获取给定数目的许可，在提供这些许可前一直将线程阻塞，或者线程已被中断。
                    semaphore.acquire(1);
                    resolve(count);
                    // 释放给定数目的许可，将其返回到信号量。
                    semaphore.release(2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        executorService.shutdown();
    }

    private static void resolve(int i) throws InterruptedException {
        Thread.sleep(2000);
    }
}
