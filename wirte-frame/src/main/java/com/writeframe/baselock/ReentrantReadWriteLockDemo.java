package com.writeframe.baselock;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReentrantReadWriteLockDemo {
    // 创建map集合
    private volatile Map<String, Object> map = new HashMap<>();
    // 当一个变量被 volatile 修饰时，任何线程对它的写操作都会立即刷新到主内存中，并且会强制让缓存了该变量的线程的数据清空，
    // 必须从主内存重新读取最新数据。

    // 创建读写锁
    ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();

    // 放数据
    public void put(String key, Object value) {
        // 锁上
        rwLock.writeLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + "：正在执行写操作" + key);
            TimeUnit.MILLISECONDS.sleep(500);
            map.put(key, value);
            System.out.println(Thread.currentThread().getName() + "：写完了" + key);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // 释放锁
            rwLock.writeLock().unlock();
        }
    }

    // 取数据
    public Object get(String key) {
        rwLock.readLock().lock();
        Object result = null;
        // 暂停一会
        try {
            System.out.println(Thread.currentThread().getName() + "：正在执行读操作" + key);
            TimeUnit.MILLISECONDS.sleep(500);
            result = map.get(key);
            System.out.println(Thread.currentThread().getName() + "已经取完了" + key);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // 释放读锁
            rwLock.readLock().unlock();
        }
        return result;
    }

    public static void main(String[] args) {
        ReentrantReadWriteLockDemo writeLockDemo = new ReentrantReadWriteLockDemo();
        // 创建线程往里面放数据
        for (int i = 0; i < 5; i++) {
            int num = 1;
            new Thread(() -> {
                writeLockDemo.put("" + num, num);
            }, String.valueOf(i)).start();
        }
        // 创建线程取数据
        for (int i = 0; i < 5; i++) {
            int num = i;
            new Thread(() -> {
                writeLockDemo.get("" + num);
            }, String.valueOf(i)).start();
        }


        //可重入锁
        ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
        ReentrantReadWriteLock.ReadLock readLock = rwLock.readLock();  //读锁
        ReentrantReadWriteLock.WriteLock writeLock = rwLock.writeLock();  //写锁

        //锁降级
        //可以将写锁降级为读锁：
        //	获取写锁 --> 获取读锁 --> 释放写锁 --> 释放读锁
        //1、获取写锁
        writeLock.lock();
        System.out.println("xiaoxin");
        //2、获取读锁
        readLock.lock();
        //3、释放写锁，这时候其他读线程可以来写咯！
        writeLock.unlock();
        //4、释放读锁
        readLock.unlock();


    }

}
