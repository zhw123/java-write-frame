package com.writeframe.baselock;

/**
 * @Description 分布式锁的接口
 * https://www.cnblogs.com/sx-bj-srr/p/distributedLock.html
 */
abstract  public interface DistributedLock {
    /**
     * 获取锁
     */
    boolean lock();
    /**
     * 解锁
     */
    void unlock();
}
