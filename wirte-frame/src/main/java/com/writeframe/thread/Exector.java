package com.writeframe.thread;

public interface Exector {

    void execute(Runnable runnable);

    void shutdown();

}
