package com.amazon.lean;

//https://blog.51cto.com/zhuxianzhong/3961237
//https://blog.51cto.com/u_15200177/2773437


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AmazonItemComment {

    //建个类把这放进去 Jsoup 的jar很好找吧


    /**
     * 抓取亚马逊商品评论
     *
     * @param args
     */
    public static void main(String[] args) {
        //最好赋值
        Document document;
        List<AmazonModel> plsjList = new ArrayList<AmazonModel>();
        try {
            //每页10条评论 20000*10 差不多够用了
            for (int i = 1; i < 20000; i++) {
                try {
                    //效率高不一定好 会被检测
                    Thread.sleep((int) (Math.random() * 5000));

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //西班牙的站点 其他站点美国啊英国啊意大利啊等等都通用 这个网站是评论的第二页 怎么写成动态的不用多说吧     红色的是ASIN 换个真的替换一下
                document = Jsoup.connect("https://www.amazon.es/TJLMCORP-pintado-autoadhesivo-resistente-ladrillo/product-reviews/B082/ref=cm_cr_arp_d_paging_btm_next_2?ie=UTF8&reviewerType=all_reviews&pageNumber=2").userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36").get();
                // 获取每个产品的全部信息
                Elements cpxx = document.getElementsByClass("a-section review aok-relative");
                if (cpxx.size() != 0) {
                    for (int j = 0; j < cpxx.size(); j++) {
                        AmazonModel plsj = new AmazonModel();
                        Document parse = Jsoup.parse(cpxx.get(i).toString());
                        //星级
                        plsj.setXj(OtherUtil.getTextFromHTML(parse.getElementsByClass("a-icon-alt").toString()).substring(0, 4));
                        //时间
                        plsj.setSj(OtherUtil.getTextFromHTML(parse.getElementsByClass("a-size-base a-color-secondary review-date").toString()));
                        //尺寸
                        plsj.setCc(OtherUtil.getTextFromHTML(parse.getElementsByClass("a-size-mini a-link-normal a-color-secondary").toString()));
                        //评论
                        plsj.setMs(OtherUtil.getTextFromHTML(parse.getElementsByClass("a-size-base review-text review-text-content").toString()));
                        plsjList.add(plsj);
                    }
                } else {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //去除html标签 复用率高
    public static String getTextFromHTML(String htmlStr) {
        Document doc = Jsoup.parse(htmlStr);
        String text = doc.text();
        // remove extra white space
        StringBuilder builder = new StringBuilder(text);
        int index = 0;
        while (builder.length() > index) {
            char tmp = builder.charAt(index);
            if (Character.isSpaceChar(tmp) || Character.isWhitespace(tmp)) {
                builder.setCharAt(index, ' ');
            }
            index++;
        }
        text = builder.toString().replaceAll(" +", " ").trim();
        return text;
    }

    public void test() {
        Document document;
        Document document1;
        List<AmazonModel> plsjList = new ArrayList<AmazonModel>();
        int num = 1;
        try {
            //因为 一页50 个产品 所以。。。。
            for (int m = 1; m < 3; m++) {
                document = Jsoup.connect("https://www.amazon.com/Best-Sellers-Home-Improvement-Wallpaper/zgbs/hi/2242314011/ref=zg_bs_pg_" + m + "?_encoding=UTF8&pg=" + m).userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36").get();
                Elements elementsByClass = document.getElementsByClass("a-list-item");
                for (int i = 0; i < elementsByClass.size(); i++) {
                    Document parse = Jsoup.parse(elementsByClass.get(i).toString());
                    AmazonModel plsj = new AmazonModel();
                    plsj.setPm(OtherUtil.getTextFromHTML(parse.getElementsByClass("zg-badge-text").toString().replace("#", "")));
                    plsj.setXj(OtherUtil.getTextFromHTML(parse.getElementsByClass("a-icon-alt").toString()).substring(0, 4));
                    plsj.setPls(OtherUtil.getTextFromHTML(parse.getElementsByClass("a-size-small a-link-normal").toString()));
                    plsj.setJg(OtherUtil.getTextFromHTML(parse.getElementsByClass("a-size-base a-color-price").toString()));
                    String str = parse.getElementsByClass("a-link-normal a-text-normal").toString();
                    str = OtherUtil.regularexpression("(?<=href=\").*?(?=\")", str).get(0);
                    document1 = Jsoup.connect("https://www.amazon.com/" + str).userAgent("User-Agent:Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50").get();
                    try {
                        Elements elementsByClass1 = document1.getElementsByClass("a-normal a-spacing-micro").select("table").select("tr");
                        for (int k = 0; k < elementsByClass1.size(); k++) {
                            Document parse1 = Jsoup.parse(elementsByClass1.get(k).toString());
                            String temp = OtherUtil.getTextFromHTML(parse1.getElementsByClass("a-size-base a-text-bold").toString());
                            if ("Brand".equals(temp)) {
                                plsj.setBrand(OtherUtil.getTextFromHTML(parse1.getElementsByClass("a-size-base").toString()));
                            }
                        }

                    } catch (NullPointerException e) {

                    }
                    plsj.setDp(OtherUtil.getTextFromHTML(document1.getElementById("bylineInfo").toString()));
                    try {
                        Elements select1 = document1.getElementById("tabular-buybox-container").select("table").select("tr");
                        for (int l = 0; l < select1.size(); l++) {
                            Document parse1 = Jsoup.parse(select1.get(l).toString());
                            String textFromHTML = OtherUtil.getTextFromHTML(parse1.getElementsByClass("a-color-tertiary tabular-buybox-label").toString());
                            if ("Ships from".equals(textFromHTML)) {
                                plsj.setPsfs(OtherUtil.getTextFromHTML(parse1.getElementsByClass("tabular-buybox-text").toString()));

                            }
                        }
                    } catch (NullPointerException e) {

                    }
                    Elements select = document1.getElementById("prodDetails").select("table").select("tr");
                    for (int j = 0; j < select.size(); j++) {
                        String string = select.get(j).toString();
                        if (string.indexOf("Package Dimensions") != -1) {
                            plsj.setCc(OtherUtil.getTextFromHTML(string.replace("Package Dimensions", "")));
                        }
                        if (string.indexOf("ASIN") != -1) {
                            plsj.setAsin(OtherUtil.getTextFromHTML(string.replace("ASIN", "")));
                        }
                        if (string.indexOf("Item Weight") != -1) {
                            plsj.setZl(OtherUtil.getTextFromHTML(string.replace("Item Weight", "")));
                        }

                    }
                    plsjList.add(plsj);
                    System.out.println("第 " + num + " 个");
                    //别嫌慢  亚马逊检测很厉害  验证码 我没研究 不会过
                    try {
                        Thread.sleep((int) (Math.random() * 5000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    num++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//jsoup 实现亚马逊英国站枕头 top100数据抓取
    public void test1() throws Exception{
        Document document = Jsoup.connect("https://www.amazon.co.uk/Best-Sellers-Kitchen-Home-Standard-Pillows/zgbs/kitchen/3229399031/ref=zg_bs_pg_2/258-7153723-2251853?_encoding=UTF8&pg=2").userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36").get();
        Elements elementsByClass = document.getElementsByClass("zg-item-immersion");
        List<AmazonModel> plsjList = new ArrayList<AmazonModel>();

        for (int i = 0; i < elementsByClass.size(); i++) {
            //实体类 自己定义就好
            AmazonModel plsj = new AmazonModel();
            Element element = elementsByClass.get(i);
            //排名
            plsj.setPm(OtherUtil.regularexpression("(?<=#)..", element.toString()).get(0));
            //星级
            List<String> regularexpression1 = OtherUtil.regularexpression("(?<=title=\")... ", element.toString());
            plsj.setXj(regularexpression1.size() == 0 ? "" : regularexpression1.get(0));
            //价格
            List<String> regularexpression = OtherUtil.regularexpression("(\\£).*?(?=\\<)", element.toString());
            plsj.setJg(regularexpression.size() == 0 ? "" : regularexpression.get(0));

            Document documentcp = Jsoup.connect("https://www.amazon.co.uk" + OtherUtil.regularexpression("(?<=href=\").*?(?=\")", element.toString()).get(0)).userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36").get();

            Elements elementsByClass1 = documentcp.getElementsByClass("a-normal a-spacing-micro");

            if (elementsByClass1.size() != 0) {

                Elements elementsByClass2 = elementsByClass1.get(0).getElementsByClass("a-spacing-small");

                for (int j = 0; j < elementsByClass2.size(); j++) {

                    String str = elementsByClass2.get(j).toString();

                    if (str.indexOf("Brand") != -1) {
                        plsj.setBrand(OtherUtil.getTextFromHTML(elementsByClass2.get(j).getElementsByClass("a-span9").toString()));
                    }
                }
            }

            try {
                Element elementById = documentcp.getElementById("merchant-info");
                plsj.setPsfs(OtherUtil.getTextFromHTML(elementById.toString()));
            } catch (Exception e) {

            }
            try {
                Element acrCustomerReviewLink = documentcp.getElementById("acrCustomerReviewLink");
                plsj.setPls(OtherUtil.getTextFromHTML(acrCustomerReviewLink.toString()));
            } catch (Exception e) {

            }

            Elements elementsByClass2 = documentcp.getElementsByClass("a-row a-spacing-top-base");


            List<String> regularexpression2 = OtherUtil.regularexpression("<tr>[\\s\\S]*?</tr>", elementsByClass2.toString());

            for (int k = 0; k < regularexpression2.size(); k++) {

                String str = regularexpression2.get(k);

                if (str.indexOf("ASIN") != -1) {
                    List<String> regularexpression3 = OtherUtil.regularexpression("<td.*?</td>", str);

                    plsj.setAsin(OtherUtil.getTextFromHTML(regularexpression3.get(0)));
                }
                if (str.indexOf("Item Weight") != -1) {
                    List<String> regularexpression3 = OtherUtil.regularexpression("<td.*?</td>", str);

                    plsj.setZl(OtherUtil.getTextFromHTML(regularexpression3.get(0)));
                }
                if (str.indexOf("Colour") != -1) {
                    List<String> regularexpression3 = OtherUtil.regularexpression("<td.*?</td>", str);
                    plsj.setYs(OtherUtil.getTextFromHTML(regularexpression3.get(0)));
                }
                if (str.indexOf("Product Dimensions") != -1) {
                    List<String> regularexpression3 = OtherUtil.regularexpression("<td.*?</td>", str);
                    plsj.setCc(OtherUtil.getTextFromHTML(regularexpression3.get(0)));
                }


            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            plsjList.add(plsj);

            System.out.println(i);

        }
    }
}
