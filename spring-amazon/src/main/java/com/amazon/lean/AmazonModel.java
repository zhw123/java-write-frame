package com.amazon.lean;

/**
 * @Author zhw
 * @Description TODO
 * @Date $ $
 * @Param $
 * @return $
 **/
public class AmazonModel {

    private String xj;
    private String sj;
    private String cc;
    private String ms;


    private String asin;
    /**
     * 品牌
     */
    private String brand;
    /**
     * 店铺
     */
    private String dp;
    /**
     * 配送方式
     */
    private String psfs;
    /**
     * 价格
     */
    private String jg;
    /**
     * 排名
     */
    private String pm;
    /**
     * 评论数
     */
    private String pls;
    /**
     *重量
     */
    private String zl;
    private String ys;

    public String getAsin() {
        return asin;
    }

    public void setAsin(String asin) {
        this.asin = asin;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public String getPsfs() {
        return psfs;
    }

    public void setPsfs(String psfs) {
        this.psfs = psfs;
    }

    public String getJg() {
        return jg;
    }

    public void setJg(String jg) {
        this.jg = jg;
    }

    public String getPm() {
        return pm;
    }

    public void setPm(String pm) {
        this.pm = pm;
    }

    public String getPls() {
        return pls;
    }

    public void setPls(String pls) {
        this.pls = pls;
    }

    public String getZl() {
        return zl;
    }

    public void setZl(String zl) {
        this.zl = zl;
    }

    public String getXj() {
        return xj;
    }

    public void setXj(String xj) {
        this.xj = xj;
    }

    public String getSj() {
        return sj;
    }

    public void setSj(String sj) {
        this.sj = sj;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getMs() {
        return ms;
    }

    public void setMs(String ms) {
        this.ms = ms;
    }

    public void setYs(String ys) {
        this.ys = ys;
    }

    public String getYs() {
        return ys;
    }
}
