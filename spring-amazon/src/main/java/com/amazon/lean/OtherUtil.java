package com.amazon.lean;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author zhw
 * @Description TODO
 * @Date $ $
 * @Param $
 * @return $
 **/
public class OtherUtil {

    public static String getTextFromHTML(String htmlStr) {
        Document doc = Jsoup.parse(htmlStr);
        String text = doc.text();
        // remove extra white space
        StringBuilder builder = new StringBuilder(text);
        int index = 0;
        while (builder.length() > index) {
            char tmp = builder.charAt(index);
            if (Character.isSpaceChar(tmp) || Character.isWhitespace(tmp)) {
                builder.setCharAt(index, ' ');
            }
            index++;
        }
        text = builder.toString().replaceAll(" +", " ").trim();
        return text;
    }

    public static List<String> regularexpression(String pattern, String str) {
        List<String> list = new ArrayList<String>();
        Pattern r = Pattern.compile(pattern);
        Matcher matcher = r.matcher(str);
        if (matcher.find()) {
            //1000应该足够用了
            for (int i = 0; i < 1000; i++) {
                try {
                    list.add(matcher.group(i));
                } catch (IndexOutOfBoundsException e) {
                    return list;
                }
            }

        }
        return list;
    }


}
