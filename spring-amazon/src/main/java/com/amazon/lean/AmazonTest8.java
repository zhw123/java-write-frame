package com.amazon.lean;

import java.util.Objects;

import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 声明
 * 该方法也可能出现验证码问题，详细参照验证码处理篇，本篇不再赘述。如果仅仅是为了测试，可以简单的只修改user-agent，这样就不会出现验证码了。
 *
 * 两种方式介绍
 * 通过ASIN获取商品信息至少有两种方式，第一种是进入商品详情页，第二种是通过搜索得到商品信息。
 *
 * 进入商品详情页
 * 拼接访问链接：https://www.amazon.com/dp/ + asin编码，进入的页面就是商品详情页面。通过不同的标签获取到商品信息。
 * 该方法有个小问题，亚马逊不同的商品信息详情页是不相同的，需要匹配多种情况。优点是详细信息都有，所有数据都可以拿到。
 *
 * 搜索ASIN搜索
 * 拼接搜索结果链接：https://www.amazon.com/s?k= + asin编码，查询出来的结果就是对应asin编码的商品信息，和商品列表中获取到的信息基本上一致。优点是格式统一，方便获取数据；缺点是信息有限，部分数据拿不到，只能拿到列表上有的信息。同时可能出现多个推广商品，需要匹配下ASIN值是否相等。
 *注意，如果不携带中文首页的cookie，打印出来的内容就会是英文。为了显示中文，代码中都是先进入了一次中文首页，然后再携带cookie继续请求。
 *
 */
public class AmazonTest8 {

    public static void main(String[] args) throws Exception {
        printInfo1("B09B52572Z");

        System.out.println("========================");

        printInfo2("B09B52572Z");
    }

    static void printInfo1(String asin) throws Exception {
        CookieStore store = new BasicCookieStore();
        CloseableHttpClient httpclient = HttpClients.custom().setDefaultCookieStore(store).build();
        HttpGet get = new HttpGet("https://www.amazon.com/?language=zh_CN");
        get.addHeader("user-agent",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.163 Safari/535.1");
        get.addHeader("accept-language", "zh-CN,zh;q=0.9,en;q=0.8");
        httpclient.execute(get);

        get = new HttpGet("https://www.amazon.com/dp/" + asin);
        get.addHeader("accept-language", "zh-CN,zh;q=0.9,en;q=0.8");
        get.addHeader("user-agent",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.163 Safari/535.1");
        CloseableHttpResponse rese = httpclient.execute(get);
        String redsa = EntityUtils.toString(rese.getEntity());
        Document doc = Jsoup.parse(redsa);

        String title = doc.getElementById("productTitle").text();
        System.out.println("名称：" + title);

        Element priceEle = doc.getElementById("priceblock_saleprice");
        if(priceEle == null) {
            priceEle = doc.getElementById("mbc-price-1");
            if(doc.getElementById("priceblock_dealprice") != null) {
                System.out.println("秒杀价：" + doc.getElementById("priceblock_dealprice").text());
            }
        }
        if(priceEle != null) {
            String price = priceEle.text();
            System.out.println("价格：" + price);
        }
    }

    static void printInfo2(String asin) throws Exception {
        CookieStore store = new BasicCookieStore();
        CloseableHttpClient httpclient = HttpClients.custom().setDefaultCookieStore(store).build();
        HttpGet get = new HttpGet("https://www.amazon.com/?language=zh_CN");
        get.addHeader("user-agent",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.163 Safari/535.1");
        get.addHeader("accept-language", "zh-CN,zh;q=0.9,en;q=0.8");
        httpclient.execute(get);

        get = new HttpGet("https://www.amazon.com/s?k=" + asin);
        get.addHeader("accept-language", "zh-CN,zh;q=0.9,en;q=0.8");
        get.addHeader("user-agent",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.163 Safari/535.1");
        CloseableHttpResponse rese = httpclient.execute(get);
        String redsa = EntityUtils.toString(rese.getEntity());
        Document doc = Jsoup.parse(redsa);

        Elements goodsEles = doc.getElementsByClass("sg-col-4-of-12 s-result-item s-asin sg-col-4-of-16 sg-col s-widget-spacing-small sg-col-4-of-20");
        Element goodsEle = null;
        for(int i = 0; i < goodsEles.size(); i++) {
            String asins = goodsEles.get(i).attr("data-asin");
            if(Objects.equals(asins, asin)) {
                goodsEle = goodsEles.get(i);
                break;
            }
        }

        String detailUrl = "https://www.amazon.cn" + goodsEle.getElementsByTag("a").first().attr("href");
        System.out.println("商品详情：" + detailUrl);

        String asins = goodsEle.attr("data-asin");
        System.out.println("ASIN：" + asins);

        String uuid = goodsEle.attr("data-uuid");
        System.out.println("UUID：" + uuid);

        String img = goodsEle.getElementsByTag("img").first().attr("src");
        System.out.println("封面图片：" + img);

        String subTitle = goodsEle.getElementsByTag("h2").first().text();
        System.out.println("名称：" + subTitle);

        Element starEle = goodsEle.getElementsByClass("a-icon-alt").first();
        if(starEle != null) {
            String star = starEle.text();
            System.out.println("评分：" + star);

            String count = goodsEle.getElementsByClass("a-section a-spacing-none a-spacing-top-micro").first().getElementsByClass("a-size-base").first().text().replaceAll(",", "");
            System.out.println("评价人数：" + count);
        } else {
            System.out.println("暂无评分");
            System.out.println("评价人数：0");
        }

        String price = goodsEle.getElementsByClass("a-offscreen").first().text().replaceAll(",", "");
        System.out.println("价格：" + price);

    }

}

