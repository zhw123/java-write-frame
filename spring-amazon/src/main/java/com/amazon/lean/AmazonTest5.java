package com.amazon.lean;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 亚马逊国际站获取全部商品分类:
 * 分类的获取
 * 查看网络请求信息，可以很容易站到对应的全部分类链接地址为：https://www.amazon.com/gp/navigation/ajax/generic.html?language=zh_CN&ajaxTemplate=hamburgerMainContent&pageType=Gateway&hmDataAjaxHint=1&navDeviceType=desktop&isSmile=0&isPrime=0&isBackup=false&hashCustomerAndSessionId=4b402a5e056c106b91334a9de902ace2e7c7e999&isExportMode=true&languageCode=zh_CN&environmentVFI=AmazonNavigationCards%2Fdevelopment%40B6059425445-AL2_x86_64&secondLayerTreeName=prm_digital_music_hawkfire%2Bkindle%2Bandroid_appstore%2Belectronics_exports%2Bcomputers_exports%2Bsbd_alexa_smart_home%2Barts_and_crafts_exports%2Bautomotive_exports%2Bbaby_exports%2Bbeauty_and_personal_care_exports%2Bwomens_fashion_exports%2Bmens_fashion_exports%2Bgirls_fashion_exports%2Bboys_fashion_exports%2Bhealth_and_household_exports%2Bhome_and_kitchen_exports%2Bindustrial_and_scientific_exports%2Bluggage_exports%2Bmovies_and_television_exports%2Bpet_supplies_exports%2Bsoftware_exports%2Bsports_and_outdoors_exports%2Btools_home_improvement_exports%2Btoys_games_exports%2Bvideo_games_exports%2Bgiftcards%2Bamazon_live%2BAmazon_Global
 *
 * 需要先进入国际站，然后将语言调整为中文，再打开上面的链接，否则查看到的分类是英文模式。找到对应的全部商品信息部分，进行格式化的解析即可。
 */
public class AmazonTest5 {

    public static void main(String[] args) throws Exception {
        CookieStore store = new BasicCookieStore();
        CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCookieStore(store)
                .build();
        HttpGet get = new HttpGet("https://www.amazon.com/?language=zh_CN");
        get.addHeader("user-agent",
                "Mozilla/5.0(Macintosh;IntelMacOSX10_13_4)AppleWebKit/537.36(KHTML,likeGecko)Chrome/81.0.4044.138Safari/537.36");
        get.addHeader("accept-language", "zh-CN,zh;q=0.9,en;q=0.8");
        httpclient.execute(get);

        String url = "https://www.amazon.com/gp/navigation/ajax/generic.html?language=zh_CN&ajaxTemplate=hamburgerMainContent&pageType=Gateway&hmDataAjaxHint=1&navDeviceType=desktop&isSmile=0&isPrime=0&isBackup=false&hashCustomerAndSessionId=4b402a5e056c106b91334a9de902ace2e7c7e999&isExportMode=true&languageCode=zh_CN&environmentVFI=AmazonNavigationCards%2Fdevelopment%40B6059425445-AL2_x86_64&secondLayerTreeName=prm_digital_music_hawkfire%2Bkindle%2Bandroid_appstore%2Belectronics_exports%2Bcomputers_exports%2Bsbd_alexa_smart_home%2Barts_and_crafts_exports%2Bautomotive_exports%2Bbaby_exports%2Bbeauty_and_personal_care_exports%2Bwomens_fashion_exports%2Bmens_fashion_exports%2Bgirls_fashion_exports%2Bboys_fashion_exports%2Bhealth_and_household_exports%2Bhome_and_kitchen_exports%2Bindustrial_and_scientific_exports%2Bluggage_exports%2Bmovies_and_television_exports%2Bpet_supplies_exports%2Bsoftware_exports%2Bsports_and_outdoors_exports%2Btools_home_improvement_exports%2Btoys_games_exports%2Bvideo_games_exports%2Bgiftcards%2Bamazon_live%2BAmazon_Global";
        get = new HttpGet(url);
        get.addHeader("accept-language", "zh-CN,zh;q=0.9,en;q=0.8");
        get.addHeader("User-Agent",
                "Mozilla/5.0(Macintosh;IntelMacOSX10_13_4)AppleWebKit/537.36(KHTML,likeGecko)Chrome/81.0.4044.138Safari/537.36");
        CloseableHttpResponse rese = httpclient.execute(get);
        String redsa = EntityUtils.toString(rese.getEntity());
        Document doc = Jsoup.parse(redsa);
        Elements uls = doc.getElementsByTag("ul");
        for(int i = 3; i < uls.size()-3; i++) {
            Element ul = uls.get(i);
            Elements lis = ul.getElementsByTag("li");
            String type = lis.get(1).text();
            System.out.println("分类：" + type);
            for(int j = 2; j < lis.size()-1; j++) {
                String smallType = lis.get(j).text();
                String smallUrl = lis.get(j).getElementsByTag("a").first().attr("href");
                System.out.println("\t小类：" + smallType + "(https://www.amazon.com" + smallUrl + ")");
            }
        }

    }

}
