package com.caffeine.lean;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanCaffeineApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanCaffeineApplication.class, args);
    }

}
