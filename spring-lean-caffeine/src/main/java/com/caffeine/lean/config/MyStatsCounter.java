package com.caffeine.lean.config;

import com.github.benmanes.caffeine.cache.RemovalCause;
import com.github.benmanes.caffeine.cache.stats.CacheStats;
import com.github.benmanes.caffeine.cache.stats.StatsCounter;
import lombok.Data;

import java.util.concurrent.atomic.LongAdder;

/**
 * 自定义的缓存状态收集器
 */
@Data
public class MyStatsCounter implements StatsCounter {

    private final LongAdder hitCount = new LongAdder();
    private final LongAdder missCount = new LongAdder();
    private final LongAdder loadSuccessCount = new LongAdder();
    private final LongAdder loadFailureCount = new LongAdder();
    private final LongAdder totalLoadTime = new LongAdder();
    private final LongAdder evictionCount = new LongAdder();
    private final LongAdder evictionWeight = new LongAdder();

    public MyStatsCounter() {
    }

    @Override
    public void recordHits(int i) {
        hitCount.add(i);
        System.out.println("命中次数：" + i);
    }

    @Override
    public void recordMisses(int i) {
        missCount.add(i);
        System.out.println("未命中次数：" + i);
    }

    @Override
    public void recordLoadSuccess(long l) {
        loadSuccessCount.increment();
        totalLoadTime.add(l);
        System.out.println("加载成功次数：" + l);
    }

    @Override
    public void recordLoadFailure(long l) {
        loadFailureCount.increment();
        totalLoadTime.add(l);
        System.out.println("加载失败次数：" + l);
    }

    @Override
    public void recordEviction() {
        evictionCount.increment();
        System.out.println("因为缓存权重限制，执行了一次缓存清除工作");
    }

    @Override
    public void recordEviction(int weight, RemovalCause cause) {
        evictionCount.increment();
        evictionWeight.add(weight);
        System.out.println("因为缓存权重限制，执行了一次缓存清除工作，清除的数据的权重为：" + weight);
    }

    @Override
    public void recordEviction(int weight) {
        evictionCount.increment();
        evictionWeight.add(weight);
        System.out.println("因为缓存权重限制，执行了一次缓存清除工作，清除的数据的权重为：" + weight);
    }

    @Override
    public CacheStats snapshot() {
        return CacheStats.of(
                negativeToMaxValue(hitCount.sum()),
                negativeToMaxValue(missCount.sum()),
                negativeToMaxValue(loadSuccessCount.sum()),
                negativeToMaxValue(loadFailureCount.sum()),
                negativeToMaxValue(totalLoadTime.sum()),
                negativeToMaxValue(evictionCount.sum()),
                negativeToMaxValue(evictionWeight.sum()));
    }
    private static long negativeToMaxValue(long value) {
        return (value >= 0) ? value : Long.MAX_VALUE;
    }
}


