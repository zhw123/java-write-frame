package com.caffeine.lean.demo;

import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @description Caffeine 同步加载
 */
public class CaffenineLoadingCacheDemo {

    public static void main(String[] args) throws InterruptedException {


    LoadingCache<String, String> cache = Caffeine.newBuilder()
                // 基于时间失效,写入之后开始计时失效
                .expireAfterWrite(2000, TimeUnit.MILLISECONDS)
                // 缓存容量
                .maximumSize(5)
                // 可以使用java8函数式接口的方式,这里其实是重写CacheLoader类的load方法
                .build(new MyLoadingCache());

        // 获取一个不存在的kay,让它去调用CacheLoader的load方法
        System.out.println(cache.get("key"));
        // 等待2秒让key失效
        TimeUnit.SECONDS.sleep(2);
        System.out.println(cache.getIfPresent("key"));
        // 批量获取key,让他批量去加载
        Map<String, String> all = cache.getAll(Arrays.asList("key1", "key2", "key3"));
        System.out.println(all);
    }

    static class MyLoadingCache implements CacheLoader {

        @Nullable
        @Override
        public Object load(@NonNull Object key) throws Exception {
            return key + "_" + System.currentTimeMillis();
        }
    }

}