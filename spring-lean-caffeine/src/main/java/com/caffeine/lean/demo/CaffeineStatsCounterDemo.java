package com.caffeine.lean.demo;

import com.caffeine.lean.config.MyStatsCounter;
import com.caffeine.lean.entity.UserInfo;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.stats.CacheStats;
import java.util.concurrent.TimeUnit;

/**
 * Caffeine 监控
 */
public class CaffeineStatsCounterDemo {
    public static void main(String[] args) throws InterruptedException {
        MyStatsCounter myStatsCounter = new MyStatsCounter();
        Cache<String, String> cache = Caffeine.newBuilder()
                // 基于时间失效,写入之后开始计时失效
                .expireAfterWrite(2000, TimeUnit.MILLISECONDS)
                // 缓存容量
                .maximumSize(5)
                .recordStats(()->myStatsCounter)
                .build();
        cache.put("one", "one");
        cache.put("two", "two");
        cache.put("three","three");
        cache.getIfPresent("ww");
        CacheStats stats = myStatsCounter.snapshot();
        Thread.sleep(1000);
        System.out.println(stats.toString());
    }
}