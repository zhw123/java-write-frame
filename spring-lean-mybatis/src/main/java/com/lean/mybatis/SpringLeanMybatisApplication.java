package com.lean.mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanMybatisApplication.class, args);
    }

}
