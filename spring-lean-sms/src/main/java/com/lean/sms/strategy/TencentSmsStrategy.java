package com.lean.sms.strategy;

import cn.hutool.core.map.MapUtil;
import com.lean.sms.properties.SmsProperties;
import com.lean.sms.utils.Constant;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20210111.SmsClient;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20210111.models.SendSmsResponse;
import com.tencentcloudapi.sms.v20210111.models.SendStatus;

import java.util.Map;

/**
 * 腾讯云短信
 *https://cloud.tencent.com/document/api/382/52077
 * @author 
 */
public class TencentSmsStrategy extends SmsStrategy {

    private SmsClient client;

    public TencentSmsStrategy(SmsProperties smsProperties) {
        this.smsProperties = smsProperties;

        try {
            // 实例化一个http选项，可选的
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setReqMethod("POST");
            httpProfile.setConnTimeout(30); // 请求连接超时时间，单位为秒(默认60秒)
            httpProfile.setWriteTimeout(30);  // 设置写入超时时间，单位为秒(默认0秒)
            httpProfile.setReadTimeout(30);  // 设置读取超时时间，单位为秒(默认0秒)
            httpProfile.setEndpoint("sms.tencentcloudapi.com"); // 指定接入地域域名(默认就近接入)
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setSignMethod("HmacSHA256"); // 指定签名算法(默认为HmacSHA256)
            // 自3.1.80版本开始，SDK 支持打印日志。
            clientProfile.setHttpProfile(httpProfile);
            Credential cred = new Credential(smsProperties.getConfig().getAccessKey(), smsProperties.getConfig().getSecretKey());
            this.client = new SmsClient(cred, "ap-shanghai", clientProfile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void send(String mobile, Map<String, String> params) {
        SendSmsRequest request = new SendSmsRequest();
        request.setSmsSdkAppId(smsProperties.getTencent().getAppId());
        request.setSignName(smsProperties.getTencent().getSignName());
        request.setTemplateId(smsProperties.getTencent().getTemplateId());
        // 有参数则设置
        if (MapUtil.isNotEmpty(params)) {
            request.setTemplateParamSet(params.values().toArray(new String[0]));
        }
        //采用 E.164 标准，格式为+[国家或地区码][手机号]，单次请求最多支持200个手机号且要求全为境内手机号或全为境外手机号。
        // 区段 + 手机号
        String[] phoneNumberSet = {"+86" + mobile};
        request.setPhoneNumberSet(phoneNumberSet);
        // 国际、港澳台短信，需要添加SenderId，国内短信填空，默认未开通
        request.setSenderId(smsProperties.getTencent().getSenderId());
        try {
            // 发送短信
            SendSmsResponse response = client.SendSms(request);
            SendStatus sendStatus = response.getSendStatusSet()[0];
            // 发送失败
            if (!Constant.OK.equalsIgnoreCase(sendStatus.getCode())) {
                throw new RuntimeException(sendStatus.getMessage());
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
