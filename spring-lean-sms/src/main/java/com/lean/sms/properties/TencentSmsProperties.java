package com.lean.sms.properties;

import lombok.Data;

@Data
public class TencentSmsProperties {
    /**
     * 短信应用的ID，如：腾讯云等
     */
    private String appId;
    /**
     * 短信签名
     */
    private String signName;
    /**
     * 短信模板
     */
    private String templateId;
    /**
     * 腾讯云国际短信
     */
    private String senderId;
}
