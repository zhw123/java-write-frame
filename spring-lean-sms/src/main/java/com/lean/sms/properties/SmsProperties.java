package com.lean.sms.properties;

import com.lean.sms.enums.SmsPlatformEnum;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;


import lombok.Data;

/**
 * 存储配置项
 *
 * @author 
 */
@Data
@ConfigurationProperties(prefix = "sms")
public class SmsProperties {
    /**
     * 是否开启存储
     */
    private boolean enabled;
    /**
     * 通用配置项
     */
    private SmsConfig config;
    /**
     * 阿里云配置项
     */
    private AliyunSmsProperties aliyun;
    /**
     * 七牛云配置项
     */
    private QiniuSmsProperties qiniu;
    /**
     * 华为云配置项
     */
    private HuaweiSmsProperties huawei;
    /**
     * 腾讯云配置项
     */
    private TencentSmsProperties tencent;

    @Data
    public static class SmsConfig {

        /**
         * AccessKey
         */
        private String accessKey;

        /**
         * SecretKey
         */
        private String secretKey;
        /**
         * 存储类型
         */
        private SmsPlatformEnum type;
    }

    @Bean
    @ConfigurationProperties(prefix = "sms.aliyun")
    public AliyunSmsProperties aliyunStorageProperties() {
        return new AliyunSmsProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "sms.qiniu")
    public QiniuSmsProperties qiniuStorageProperties() {
        return new QiniuSmsProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "sms.huawei")
    public HuaweiSmsProperties huaweiStorageProperties() {
        return new HuaweiSmsProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "sms.tencent")
    public TencentSmsProperties tencentStorageProperties() {
        return new TencentSmsProperties();
    }

}
