package com.lean.sms.properties;

import lombok.Data;

@Data
public class QiniuSmsProperties {
    /**
     * 短信模板
     */
    private String templateId;

}
