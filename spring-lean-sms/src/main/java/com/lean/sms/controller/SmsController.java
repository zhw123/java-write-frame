package com.lean.sms.controller;

import com.lean.sms.model.SmsSendVO;
import com.lean.sms.strategy.SmsStrategy;
import com.lean.sms.utils.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("sms")
@Tag(name = "短信服务")
@AllArgsConstructor
public class SmsController {

    @Autowired
    private SmsStrategy smsStrategy;

    @PostMapping("send")
    @Operation(summary = "发送短信")
    public Result<String> send(@RequestBody SmsSendVO vo) {
        // 短信参数
        Map<String, String> params = new LinkedHashMap<>();
        if (StringUtils.isNotBlank(vo.getParamValue())) {
            params.put(vo.getParamKey(), vo.getParamValue());
        }
        smsStrategy.send(vo.getMobile(),params);
        return Result.ok();
    }
}
