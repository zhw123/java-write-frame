package com.canal.lean.test.entity;

import com.canal.lean.canal.annotation.CanalTable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 管理员表
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@CanalTable("t_admin")//优先这个
//@TableName("t_admin")//CanalTable不存在，才这个
public class Admin {

    private Long id;

    private String userName;

    private String avatar;

    private LocalDateTime createTime;

    private String createName;

    private Date updateTime;

    private String updateName;

    private Integer version;

    private Boolean deleted;

    private Integer tenantId;
}
