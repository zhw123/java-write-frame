package com.canal.lean.test.service;

import com.canal.lean.canal.handler.CanalEntryHandler;
import com.canal.lean.test.entity.Admin;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 模式canal同步mysql测试
 */
@Slf4j
@Service
public class AdminService implements CanalEntryHandler<Admin> {

    @Override
    public void insert(Admin admin) {
        log.info("AdminService.insert---->admin={}", admin);
    }

    @Override
    public void update(Admin before, Admin after) {
        log.info("AdminService.update---->before={}  ,after={}", before, after);
    }

    @Override
    public void delete(Admin admin) {
        log.info("AdminService.delete---->admin={}", admin);
    }
}
