package com.canal.lean;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 启动类
 **/
@EnableScheduling//这里以定时任务触发，当然也可以想canal官方demo一样，用while一直循环获取
@SpringBootApplication
public class SpringLeanCanalApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanCanalApplication.class, args);
    }

}
