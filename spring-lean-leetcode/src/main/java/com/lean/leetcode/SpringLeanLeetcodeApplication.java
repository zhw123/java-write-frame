package com.lean.leetcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanLeetcodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanLeetcodeApplication.class, args);
    }

}
