package com.lean.quartz.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.*;

import com.lean.quartz.base.PageResult;
import com.lean.quartz.base.Result;
import com.lean.quartz.convert.ScheduleJobConvert;
import com.lean.quartz.entity.ScheduleJobEntity;
import com.lean.quartz.query.ScheduleJobQuery;
import com.lean.quartz.service.ScheduleJobService;
import com.lean.quartz.utils.CronUtils;
import com.lean.quartz.vo.ScheduleJobVO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;

/**
 * 定时任务
 *
 * @author
 */
@RestController
@RequestMapping("schedule")
@Tag(name = "定时任务")
@AllArgsConstructor
public class ScheduleJobController {
    private final ScheduleJobService scheduleJobService;

    //http://localhost:8082/schedule/page?page=1&limit=20
    @GetMapping("page")
    @Operation(summary = "分页")
    public Result<PageResult<ScheduleJobVO>> page(@Valid ScheduleJobQuery query) {
        PageResult<ScheduleJobVO> page = scheduleJobService.page(query);
        return Result.ok(page);
    }

    //http://localhost:8082/schedule/1
    @GetMapping("{id}")
    @Operation(summary = "信息")
    public Result<ScheduleJobVO> get(@PathVariable("id") Long id) {
        ScheduleJobEntity entity = scheduleJobService.getById(id);

        return Result.ok(ScheduleJobConvert.INSTANCE.convert(entity));
    }

    //http://localhost:8082/schedule/save
    //{
    //
    //    "jobName": "测试任务2",
    //    "jobGroup": "system",
    //    "beanName": "testTask",
    //    "method": "run",
    //    "params": "{\"id\":\"678\"}",
    //    "cronExpression": "3 * * * * ? *",
    //    "concurrent": 1,
    //    "remark": "",
    //    "misfirePolicy": "1"
    //}
    @PostMapping("/save")
    @Operation(summary = "保存")
    public Result<String> save(@RequestBody ScheduleJobVO vo) {
        if (!CronUtils.isValid(vo.getCronExpression())) {
            return Result.error("操作失败，Cron表达式不正确");
        }
        scheduleJobService.save(vo);
        return Result.ok();
    }

    //http://localhost:8082/schedule/update
    //{
    //    "id": 1,
    //    "jobName": "测试任务",
    //    "jobGroup": "system",
    //    "beanName": "testTask",
    //    "method": "run",
    //    "params": "{\"id\":\"123\"}",
    //    "cronExpression": "2 * * * * ? *",
    //    "status": 1,
    //    "concurrent": 1,
    //    "remark": "",
    //    "misfirePolicy": "1",
    //    "createTime": "2022-09-22 04:32:03"
    //}
    @PostMapping("/update")
    @Operation(summary = "修改")
    public Result<String> update(@RequestBody ScheduleJobVO vo) {
        if (!CronUtils.isValid(vo.getCronExpression())) {
            return Result.error("操作失败，Cron表达式不正确");
        }
        scheduleJobService.update(vo);
        return Result.ok();
    }

    //http://localhost:8082/schedule/delete?idList=[1,2]
    @GetMapping("/del")
    @Operation(summary = "删除")
    public Result<String> delete(@RequestParam("idList") List<Long> idList) {
        scheduleJobService.delete(idList);

        return Result.ok();
    }

    //http://localhost:8082/schedule/run?id=1
    @GetMapping("run")
    @Operation(summary = "立即执行")
    public Result<String> run(@RequestParam("id") String id) {
        scheduleJobService.run(id);

        return Result.ok();
    }


    //http://localhost:8082/schedule/change-status?id=1&status=0
    @GetMapping("change-status")
    @Operation(summary = "修改状态")
    public Result<String> changeStatus(@RequestParam("id") String id,
                                       @RequestParam("status") Integer status) {
        scheduleJobService.changeStatus(id,status);

        return Result.ok();
    }
}
