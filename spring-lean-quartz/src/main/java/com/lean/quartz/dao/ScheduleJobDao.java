package com.lean.quartz.dao;

import org.apache.ibatis.annotations.Mapper;

import com.lean.quartz.entity.ScheduleJobEntity;

/**
 * 定时任务
 *
 * @author
 */
@Mapper
public interface ScheduleJobDao extends BaseDao<ScheduleJobEntity> {

}