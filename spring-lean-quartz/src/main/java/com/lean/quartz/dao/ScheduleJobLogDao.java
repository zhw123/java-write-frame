package com.lean.quartz.dao;

import org.apache.ibatis.annotations.Mapper;

import com.lean.quartz.entity.ScheduleJobLogEntity;

/**
 * 定时任务日志
 *
 * @author
 */
@Mapper
public interface ScheduleJobLogDao extends BaseDao<ScheduleJobLogEntity> {

}