package com.lean.quartz.service;

import com.lean.quartz.base.PageResult;
import com.lean.quartz.entity.ScheduleJobLogEntity;
import com.lean.quartz.query.ScheduleJobLogQuery;
import com.lean.quartz.vo.ScheduleJobLogVO;

/**
 * 定时任务日志
 *
 * @author
 */
public interface ScheduleJobLogService extends BaseService<ScheduleJobLogEntity> {

    PageResult<ScheduleJobLogVO> page(ScheduleJobLogQuery query);

}