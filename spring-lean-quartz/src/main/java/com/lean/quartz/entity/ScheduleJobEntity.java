package com.lean.quartz.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import com.lean.quartz.base.BaseEntity;

import com.lean.quartz.utils.ScheduleStrategy;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 定时任务
 *
 * @author
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("schedule_job")
public class ScheduleJobEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 任务组名
     */
    private String jobGroup;

    /**
     * bean名称
     */
    private String beanName;

    /**
     * 执行方法
     */
    private String method;

    /**
     * 方法参数
     */
    private String params;

    /**
     * cron表达式
     */
    private String cronExpression;

    //计划策略 0=默认,1=立即触发执行,2=触发一次执行,3=不触发立即执行
    private String misfirePolicy;

    /**
     * 状态 0=正常,1=暂停
     */
    private Integer status;

    /**
     * 是否并发 0：禁止 1：允许
     */
    private Integer concurrent;

    /**
     * 备注
     */
    private String remark;

}
