package com.lean.quartz.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

/**
 * 定时任务配置
 *
 * @author
 */
@Configuration
public class ScheduleConfig {

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(DataSource dataSource) {
        // quartz参数
        Properties prop = new Properties();
        // 调度标识名 集群中每一个实例都必须使用相同的名称
        prop.put("org.quartz.scheduler.instanceName", "clusteredScheduler");
        // 调度器实例编号自动生成，每个实例不能不能相同
        prop.put("org.quartz.scheduler.instanceId", "AUTO");

        // 数据库方式 JobStore配置
        prop.put("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
        // 持久化方式配置数据驱动
        prop.put("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.StdJDBCDelegate");
        prop.put("org.quartz.jobStore.dataSource", "quartzDataSource");
        prop.put("org.quartz.jobStore.tablePrefix", "QRTZ_");

        // 线程池配置
        // 实例化ThreadPool时，使用的线程类为SimpleThreadPool（一般使用SimpleThreadPool即可满足几乎所有用户的需求）
        prop.put("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
        // 并发个数,指定线程数，至少为1（无默认值）(一般设置为1-100之间的的整数合适)
        prop.put("org.quartz.threadPool.threadCount", "20");
        // 设置线程的优先级（最大为java.lang.Thread.MAX_PRIORITY 10，最小为Thread.MIN_PRIORITY 1，默认为5）
        prop.put("org.quartz.threadPool.threadPriority", "5");
        // 线程继承初始化线程的上下文类加载器
        prop.put("org.quartz.threadPool.threadsInheritContextClassLoaderOfInitializingThread", "true");

        // 集群配置开启分布式部署，集群
        prop.put("org.quartz.jobStore.isClustered", "true");

        // 分布式节点有效性检查时间间隔，单位：毫秒,默认值是15000
        prop.put("org.quartz.jobStore.useProperties", "false");
        prop.put("org.quartz.jobStore.clusterCheckinInterval", "15000");
        prop.put("org.quartz.jobStore.maxMisfiresToHandleAtATime", "1");
        prop.put("org.quartz.jobStore.txIsolationLevelSerializable", "true");

        // 容许的最大作业延长时间,最大能忍受的触发超时时间，如果超过则认为"失误",不敢再内存中还是数据中都要配置
        prop.put("org.quartz.jobStore.misfireThreshold", "12000");

        prop.put("org.quartz.jobStore.selectWithLockSQL", "SELECT * FROM {0}LOCKS UPDLOCK WHERE LOCK_NAME = ?");

        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        factory.setSchedulerName("clusteredScheduler");
        factory.setDataSource(dataSource);
        factory.setQuartzProperties(prop);
        // 延时启动
        factory.setStartupDelay(10);
        factory.setApplicationContextSchedulerContextKey("applicationContextKey");
        // 启动时更新己存在的Job，这样就不用每次修改targetObject后删除qrtz_job_details表对应记录了
        factory.setOverwriteExistingJobs(true);

        return factory;
    }
}
