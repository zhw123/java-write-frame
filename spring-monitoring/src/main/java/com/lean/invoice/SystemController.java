package com.lean.invoice;

import com.lean.invoice.sysInfo.ServerInfomation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: SystemController
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/6/1 10:28
 */
@RestController
@RequestMapping("system")
public class SystemController {
    @GetMapping("/server")
    public Result getInfo() throws Exception {
        ServerInfomation serverInfomation = new ServerInfomation();
        serverInfomation.getSysInfo();
        return new Result(true, "数据查询成功", serverInfomation);
    }
}
