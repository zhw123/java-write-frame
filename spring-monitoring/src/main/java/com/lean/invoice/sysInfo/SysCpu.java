package com.lean.invoice.sysInfo;

/**
 * CPU相关信息
 */
public class SysCpu {
    /**
     * 核心
     */
    private int cpuNum;
    /**
     * CPU总的使用
     */
    private double total;
    /**
     * CPU系统使用
     */
    private double sys;
    /**
     * CPU用户使用
     */
    private double used;
    /**
     * CPU当前等待
     */
    private double wait;
    /**
     * CPU当前空闲
     */
    private double free;

    public int getCpuNum() {
        return cpuNum;
    }

    public void setCpuNum(int cpuNum) {
        this.cpuNum = cpuNum;
    }

    public double getTotal() {
        return ArithUtils.round(ArithUtils.mul(total, 100), 2);
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getSys() {
        return ArithUtils.round(ArithUtils.mul(sys / total, 100), 2);
    }

    public void setSys(double sys) {
        this.sys = sys;
    }

    public double getUsed() {
        return ArithUtils.round(ArithUtils.mul(used / total, 100), 2);
    }

    public void setUsed(double used) {
        this.used = used;
    }

    public double getWait() {
        return ArithUtils.round(ArithUtils.mul(wait / total, 100), 2);
    }

    public void setWait(double wait) {
        this.wait = wait;
    }

    public double getFree() {
        return ArithUtils.round(ArithUtils.mul(free / total, 100), 2);
    }

    public void setFree(double free) {
        this.free = free;
    }
}
