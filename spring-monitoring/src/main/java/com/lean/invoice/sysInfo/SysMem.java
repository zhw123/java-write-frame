package com.lean.invoice.sysInfo;

/**
 * 內存相关信息
 */
public class SysMem {
    /**
     * 内存总量 (G)
     */
    private double total;
    /**
     * 已用内存 (G)
     */
    private double used;
    /**
     * 剩余内存 (G)
     */
    private double free;

    public double getTotal() {
        return ArithUtils.div(total, (1024 * 1024 * 1024), 2);
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public double getUsed() {
        return ArithUtils.div(used, (1024 * 1024 * 1024), 2);
    }

    public void setUsed(long used) {
        this.used = used;
    }

    public double getFree() {
        return ArithUtils.div(free, (1024 * 1024 * 1024), 2);
    }

    public void setFree(long free) {
        this.free = free;
    }

    public double getUsage() {
        return ArithUtils.mul(ArithUtils.div(used, total, 4), 100);
    }
}
