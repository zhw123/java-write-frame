package com.lean.captcha;

import com.wf.captcha.*;
import com.wf.captcha.base.Captcha;
import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;

@Controller
public class CaptchaController {

    @RequestMapping("/captcha")
    public void captcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // 设置位数
        CaptchaUtil.out(5, request, response);
        // 设置宽、高、位数
//        CaptchaUtil.out(130, 48, 5, request, response);
//
//        // 使用gif验证码
//        GifCaptcha gifCaptcha = new GifCaptcha(130,48,4);
//        CaptchaUtil.out(gifCaptcha, request, response);
//
//        // png类型
//        SpecCaptcha captcha = new SpecCaptcha(130, 48);
//        captcha.text();  // 获取验证码的字符
//        captcha.textChar();  // 获取验证码的字符数组
//
//        // gif类型
//        GifCaptcha captcha = new GifCaptcha(130, 48);
//
//        // 中文类型
//        ChineseCaptcha captcha = new ChineseCaptcha(130, 48);
//
//        // 中文gif类型
//        ChineseGifCaptcha captcha = new ChineseGifCaptcha(130, 48);
//
//        // 算术类型
//        ArithmeticCaptcha captcha = new ArithmeticCaptcha(130, 48);
//        captcha.setLen(3);  // 几位数运算，默认是两位
//        captcha.getArithmeticString();  // 获取运算的公式：3+2=?
//        captcha.text();  // 获取运算的结果：5
//
//        captcha.out(outputStream);  // 输出验证码
    }

    @PostMapping("/login")
    public String login(HttpServletRequest request, String username, String password, String verCode) {
        if (!CaptchaUtil.ver(verCode, request)) {
            CaptchaUtil.clear(request);  // 清除session中的验证码
            return "验证码不正确";
        }
        return "ok";
    }

    @RequestMapping("/captcha2")
    public void captcha2(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // 设置请求头为输出图片类型
        response.setContentType("image/gif");
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);

        // 三个参数分别为宽、高、位数
        SpecCaptcha specCaptcha = new SpecCaptcha(130, 48, 5);
        // 设置字体
        specCaptcha.setFont(new Font("Verdana", Font.PLAIN, 32));  // 有默认字体，可以不用设置
        // 设置类型，纯数字、纯字母、字母数字混合
        //类型	描述
        //TYPE_DEFAULT	数字和字母混合
        //TYPE_ONLY_NUMBER	纯数字
        //TYPE_ONLY_CHAR	纯字母
        //TYPE_ONLY_UPPER	纯大写字母
        //TYPE_ONLY_LOWER	纯小写字母
        //TYPE_NUM_AND_UPPER	数字和大写字母
        //只有SpecCaptcha和GifCaptcha设置才有效果。
        specCaptcha.setCharType(Captcha.TYPE_ONLY_NUMBER);

        // 验证码存入session
        request.getSession().setAttribute("captcha", specCaptcha.text().toLowerCase());

        // 输出图片流
        specCaptcha.out(response.getOutputStream());
        // 输出Base64
//        specCaptcha.toBase64();
//
//        // 如果不想要base64的头部data:image/png;base64,
//        specCaptcha.toBase64("");  // 加一个空的参数即可

        //输出到文件
//        FileOutputStream outputStream = new FileOutputStream(new File("C:/captcha.png"));
//        specCaptcha.out(outputStream);


    }
}
