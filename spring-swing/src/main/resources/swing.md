#Swing

##特点
```
    Swing 组件通常被称为 “轻量级组件”，因为它完全由 Java 语言编写，Java 是不依赖于操作系统的语言，它可以在任何平台上运行，
而依赖于本地平台的组件相应地被称为 “重量级组件”，比如 AWT 组件就是依赖本地平台的窗口系统来决定组件的功能、外观和风格的。

Swing 主要具有以下特点。
    (1) 轻量级组件
    (2) 可插入外观组件

swing组件分类
顶层容器：JFrame、JApplet、JDialog、JWindow共4个
中间容器：JPanel、JScrollPane、JSplitPane、JToolBar
基本控件： ImageIcon（图标），JLabel（标签），ITextField（文本框），JTextArea（文本区域），JScrollPane（可滚动窗格），JScrollBar（滚动条），ICheckBox（复选框），JRadioButton（单选按钮），JComboBox（下拉式列表和组合框)。

```

##Swing包
```
    为了有效地使用 Swing 组件，必须了解 Swing 包的层次结构和继承关系，其中比较重要的类是 Component 类、Container 类和 JComponent 类。下图描述了这些类的层次和继承关系。

    java.lang.Object - > java.awt.Component -> java.awt.Container -> javax.swing.JComponent
```

##常用组件概述

```
组件名称	            定义
JButton	            按钮，按钮可以带一些图片或文字
JCheckBox	        复选框
JComBox	            下拉列表框，可以在下拉显示区域显示多个选项
JFrame	            框架类
JDialog	            对话框
JLabel	            标签组件
JRadioButton	    单选按钮
JList	            能够在用户界面中显示一系列条目的组件
JTextField	        文本框
JPasswordField	    密码框
JTextArea	        文本区域
JOptionPane	        一些面板

 ```

##顶层窗体
``` 
    窗体作为 Swing 应用程序中组件的承载体，处于非常重要的位置。Swing 中常用的窗体包括 JFrame 和 JDialog 窗体，下面我们将会讲解这两个窗体的使用方法。
```
##### 1.JFrame 窗体
```
    JFrame 窗体是一个容器，它是 Swing 程序中各个组件的载体，因此，可以将 JFrame 看作是承载这些 Swing 组件的容器。

在开发应用程序时，可以通过继承 java.swing.JFrame 类来创建一个窗体，然后再在窗体中添加组件，同时为组件设置事件。由于该窗体继承了 JFrame 类，所以它拥有最大化、最小化和关闭按钮。

下面将详细讲解 JFrame 窗体在 Java 应用程序中的使用方法。

JFrame 在程序中的语法格式如下：
    JFrame frame = new JFrame(title);
    Container container = frame.getContentPane();
    ① frame：JFrame 类的对象。
    ② container：Container 类的对象，可以使用 JFrame 对象调用 getContentPane() 方法获取。
    Swing 组件的窗体通常与组件和容器相关，在 JFrame 对象创建完成后，需要调用 getContentPane() 方法将窗体转换为容器，然后在容器中添加组件或设置布局管理器。
    通常这个容器用来包含和显示组件。如果需要将组件添加至容器，那么可以使用来自 Container 类的 add() 方法进行设置，示例代码如下：
    container.add(new JButton("按钮"));    //添加JButton按钮组件
    container.remove(new JButton("按钮")); //移除JButton按钮组件

`
import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame {                       	//定义一个类继承JFrame类

    public void CreateJFrame(String title) {                	//定义一个CreateJFrame()方法

        JFrame frame = new JFrame(title);                     	 //实例化一个JFrame对象
        Container container = frame.getContentPane();          	// 获取一个容器
        container.setBackground(Color.CYAN);                  	//设置容器的背景颜色

        JLabel jl = new JLabel("这是一个JFrame窗体");        	//创建一个JLabel标签
        jl.setHorizontalAlignment(SwingConstants.CENTER);       //使标签上的文字居中
        container.add(jl);                                      // 将标签添加到容器中


        frame.setVisible(true);                                //使窗体可视
        frame.setBounds(400,300,400, 300);  				   //设置窗体显示位置和大小
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);         //设置窗体关闭方式
    }

    public static void main(String args[]) {
        //在主方法中调用createJFrame()方法
        new MyFrame().CreateJFrame("一个JFrame窗体");
    }
}
`
MyFrame 类继承了 JFrame 类，在 CreateJFrame() 方法中实例子化 JFrame 对象

Frame 类中的两种构造方法分别为无参的构造方法与有参的构造方法：

第一种形式的构造方法创建一个初始不可见、没有标题的新窗体。

第二种形式的构造方法在实例化该 JFrame 对象时，创建一个不可见但具有标题的窗体。

我们可以使用 JFrame 对象调用 show() 方法使窗体可见，但是这个方法早已被新版 JDK 弃用，现在通常使用 setVisible(true) 方法使窗体可见。

同时，可以使用 setBounds(x,y,width, height) 方法设置窗体显示位置和大小，其中 x 与 y 为窗体左上角坐标，width 和 height 为窗体的宽和高。

创建窗体后，需要给予窗体一个关闭方式，可以调用 setDefaultCloseOperation() 方法关闭窗体。

Java为关闭窗体提供了多种方式，常用的有4种方式，分别为：DO_NOTHING_ON_CLOSE、DISPOSE_ON_CLOSE、HIDE_ON_CLOSE 和 EXIT_ON_CLOSE，这几种操作实质上是将一个 int 类型的常量封装在 javax.swing.WindowConstants 接口中。

具体含义如下：

    关闭窗体方式	            描述
    EXIT_ON_CLOSE	        退出应用程序，默认窗口关闭
    DO_NOTHING_ON_CLOSE     什么都不做就将窗体关闭
    DISPOSE_ON_CLOSE	    移除窗口的默认窗口关闭操作
    HIDE_ON_CLOSE	        隐藏窗口的默认窗口关闭
```
##### 2.JDialog 窗体
```
JDialog 窗体是 Swing 组件中的对话框，它继承了 AWT 组件中的 java.awt.Dialog 类。

JDialog 窗体的功能是从一个窗体中弹出另一个窗体，就像是在使用浏览器时弹出其他的对话框一样。JDialog 窗体实质上就是另一种类型的窗体，它与 JFrame 窗体类似，在使用 JDialog 窗体时也需要调用 getContentPane() 方法，从而将窗体转换为容器，然后在容器中设置窗体的特性。

在应用程序中创建 JDialog 窗体需要实例化 JDialog 类，通常使用以下几种 JDialog 类的构造方法。

构造方法	                                                描述
public JDialog()	                                    创建一个没有标题和父窗体的对话框
public JDialog(Frame f)	                                创建一个指定父窗体的对话框，但该窗体没有标题
public JDialog(Frame f, boolean model)	                创建一个指定类型的对话框，并指定父窗体，但该窗体没有指定标题
public JDialog(Frame f, String title)	                创建一个指定标题和父窗体的对话框
public JDialog(Frame f, String title, boolean model)	创建一个指定标题、窗体和模式的对话框

`
import javax.swing.*;
import java.awt.*;

class MyJDialog extends JDialog {                           //创建新类继承JDialog类

    public MyJDialog(MyFrame frame) {
        //实例化一个JDialog类对象，指定对话框的父窗体、窗体标题和类型
        super(frame, "JDialog窗体", true);

        Container container = getContentPane();                    //创建一个容器
        container.add(new JLabel("这是一个对话框"));           //在容器中添加标签
        setBounds(120, 120, 150, 100);          //设置对话框窗体大小
    }
}

public class MyFrame extends JFrame {                            //创建新类

    public MyFrame() {
        Container container = getContentPane();                 //创建一个容器
        container.setLayout(null);

        JLabel label = new JLabel("这是一个JFrame窗体");      //在窗体中设置标签
        label.setHorizontalAlignment(SwingConstants.CENTER);    //将标签的文字置于标签中间位置
        container.add(label);

        JButton button = new JButton("点我弹出对话框");         //定义一个按钮
        button.setBounds(10, 10, 150, 20);

        //为按钮添加鼠标单击事件
        button.addActionListener(e -> {
            //使MyJDialog窗体可见
            new MyJDialog(MyFrame.this).setVisible(true);
        });

        container.add(button);                             // 将按钮添加到容器中
        container.setBackground(Color.white);

        setSize(400,300);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    //main方法
    public static void main(String args[]) {
        // 实例化MyJDialog类对象
        new MyFrame();
    }
}
`
本实例为了使对话框在父窗体中弹出，定义了一个 JFrame 窗体，然后在该窗体中添加一个按钮，并为此按钮添加一个鼠标单击监听事件（这儿使用了 lambda 表达式），这里使用 “new MyJDialog().setVisible(true)” 语句使对话框窗体可见，这样就实现了当用户单击该按钮后将弹出对话框的功能。

由于 MyJDialog 类继承了 JDialog 类，所以可以在构造方法中使用 super 关键字调用 JDialog 构造方法。这里使用了 “public JDialog(Frame f, String title, boolean model)” 这种形式的构造方法，同时设置了自定义的 JFrame 窗体及对话框的标题和窗体类型。

从本实例的代码可以看出，JDialog 窗体与 JFrame 窗体形式基本相同，甚至在设置窗体的特性时所调用的方法名称都基本相同，比如设置窗体大小、设置窗体关闭状态等。
```
##应用基本组件
```
软件界面是软件和用户之间的交流平台，而组件则是构成软件界面的基本元素，是软件和用户之间的交流途径。例如，用标签组件来显示相关信息，用单选按钮、复选框、文本框等接受用户的输入信息，
用按钮来提交用户的输入信息等等。
```
####1、标签组件：JLabel
```
用 JLabel 组件显示文本和图像时，不仅可以只显示其中的一者，而且可以同时显示。在通过构造方法创建组件对象时，可以设置标签内容相对于组件的水平显示位置。

JLabel 类提供的所有构造方法如下表所示：

构造方法	                                        描述
JLabel()	                                    创建一个既无文本内容，又无图像内容的标签
JLabel(String text)	                            创建一个具有文本内容的标签
JLabel(String text, int horizontalAlignment)	创建一个具有文本内容并且指定水平显示位置的标签
JLabel(Icon icon)	                            创建一个具有图像内容的标签
JLabel(Icon icon, int horizontalAlignment)	    创建一个具有图像内容并且指定水平显示位置的标签
JLabel(String text, Icon icon, int horizontalAlignment)	创建一个既有文本内容，又有图像内容，并且指定水平显示位置的标签

构造方法中的入口参数 horizontalAlignment 可以从 JLabel 类的静态常量中选择，可选的静态常量如下表所示：

静态变量	        常量值	标签内容显示位置
LEFT	        2	        靠左侧显示
CENTER	        0	        居中显示
RIGHT	        4	        靠右侧显示
JLabel 类中提供了众多的方法，方便程序员设置标签的内容，常用方法有以下几个：
方法	                                        描述
setText(String text)	                    设置要显示的文本
setIcon(Icon icon)	                        设置要显示的图像
setHorizontalAlignment(int alignment)	    设置标签内容在水平方向的对齐方式
setHorizontalTextPosition(int textPosition)	设置文本相对于图像在水平方向的位置
setVerticalAlignment(int alignment)	        设置标签内容在垂直方向的对齐方式
setVerticalTextPosition(int textPosition)	设置文本相对于图像在垂直方向的位置
setIconTextGap(int iconTextGap)	            设置文本和图像之间的距离，单位为像素，默认为 4 像素
setEnabled(boolean enabled)	                设置标签是否可用，设为 false 时表示不可用，默认为可用
setDisabledIcon(Icon disabledIcon)	        设置当标签不可以时显示的图像
下面我们将通过一个例子来进一步了解 JLabel 组件的使用方法。
要求：首先将窗口标题修改为 “添加标签”，然后创建一个带有指定文本的标签对象，并为标签添加一幅图像，设置标签内容的显示位置，最后将标签设置为不可用，并设置标签在不可用的情况下显示另一幅图像。
`
import javax.swing.*;
public class MyFrame extends JFrame {       //创建新类
    JLabel label;

    public MyFrame() {
        //修改窗口标题
        setTitle("添加标签");

        label = new JLabel("我是一个JLabel！", JLabel.CENTER);                               //创建指定文本的标签对象

        label.setIcon(new ImageIcon("C:\\Users\\15269\\Desktop\\自行车.png"));              //为标签添加图像

        label.setHorizontalTextPosition(JLabel.CENTER);                                    //设置文本相对于图像的水平位置

        label.setVerticalTextPosition(JLabel.BOTTOM);                                     //设置文本相对于图像的垂直位置

        label.setEnabled(false);                                                       	  //设置标签为不可用

        label.setDisabledIcon(new ImageIcon("C:\\Users\\15269\\Desktop\\射箭.png"));      //设置标签在不可用的情况下显示的图像

        add(label);  // 将标签添加到窗口中

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(400,300,800,650);
        setVisible(true);
    }

    //main方法
    public static void main(String args[]) {
        new MyFrame();
    }
}
`
```
####2、按钮组件
```
Swing 中的按钮是较为常见的组件，它用于触发特定动作。Swing 提供了很多种按钮，包括提交按钮、复选框按钮、单选按钮等，这些按钮都是从 AbstractButton 类中继承而来的。
```
#####1.普通按钮：JButton
```
Swing 中的普通按钮由 JButton 对象表示，其构造方法主要有如下几种形式：
① public JButton()
② public JButton(String text)
③ public JButton(Icon icon)
④ public JButton(String text, Icon icon)

通过使用上述构造方法，在 Swing 按钮上不仅能显示文本标签，还可以显示图标。上述构造方法中的第一个构造方法可以生成不带任何文字和图标的组件，然后根据需要再使用相应的方法为按钮设置指定的文本和图标，其他构造方法都在初始化时指定了按钮上显示的图标或文字。
`
import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.*;

public class MyFrame extends JFrame {       //创建新类
    JPanel root;
    JButton messageButton,imageButton;

    public MyFrame() throws IOException {
        root = new JPanel();      //定义面板容器
        setContentPane(root);
        setLayout(null);         //设置面板为绝对布局

        //按钮添加文字
        messageButton = new JButton("普通按钮");        //定义显示文本内容的按钮
        messageButton.setBounds(54, 68, 100, 40);      //设置按钮显示位置和大小
        root.add(messageButton);                       //将按钮添加到面板容器中

        //按钮添加图标
        ImageIcon icon = new ImageIcon(ImageIO.read(new File("C:\\Users\\15269\\Desktop\\坏笑.png")));                                             //定义图片对象
        imageButton = new JButton(icon);                //定义显示图片的按钮对象
        imageButton.setBounds(196, 40, 120, 120);       //定义按钮显示位置
        root.add(imageButton);                          //就按钮显示在面板中

        //设置窗口风格
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(400,300,400,300);
        setVisible(true);
    }

    //main方法
    public static void main(String args[]) throws IOException {
        new MyFrame();
    }
}
`
```
#####2.单选按钮：JRadioButton
```
默认情况下的单选按钮显示为一个圆形图标，在该图标旁通常放置一些说明性文字。为了使多个单选按钮表现出某种功能，应用程序一般将多个单选按钮放置在按钮组中，即当用户选中某个单选按钮后，按钮组 中其他按钮将被自动取消选中。

单选按钮是 Swing 组件中 JRadioButton 类的对象，该类是 JToggleButton 的子类，而 JToggleButton 类又是 AbstractButton 类的子类，因此，单选按钮的一系列控制方法都是 AbstractButton 类中的方法。

(1) 单选按钮
可以使用 JRadioButton 类中的构造方法创建单选按钮对象。

JRadioButton 类的常用构造方法主要有以下几种形式：
① public JRadioButton()
② public JRadioButton(Icon icon)
③ public JRadioButton(Icon icon, boolean selected)
④ public JRadioButton(String text)
⑤ public JRadioButton(String text, Icon icon)
⑥ public JRadioButton(String text, Icon icon, boolean selected)

通过观察上述构造方法的形式，我们知道在初始化单选按钮时可以同时设置单选按钮的图标、文字及默认是否被选中等属性。

(2) 按钮组
Swing 存在一个 ButtonGroup 类，该类用于产生按钮组。如果希望将所有的单选按钮放置在按钮组中，那么我们需要实例化一个 JRadioButton 对象，并使用该对象调用 add() 方法添加单选按钮。
`
import javax.swing.*;

public class MyFrame extends JFrame {       
    JPanel root;
    JRadioButton radioButton, radioButton_1, radioButton_2, radioButton_3;

    public MyFrame() {
        root = new JPanel();      //定义面板容器
        setContentPane(root);
        setLayout(null);         //设置面板为绝对布局

        //定义单选按钮
        radioButton = new JRadioButton("学习");
        radioButton_1 = new JRadioButton("打豆豆");
        radioButton_2 = new JRadioButton("运动");
        radioButton_3 = new JRadioButton("旅行");

        //定义按钮显示位置和大小
        radioButton.setBounds(45, 29, 73, 23);
        radioButton_1.setBounds(134, 29, 121, 23);
        radioButton_2.setBounds(45, 74, 73, 23);
        radioButton_3.setBounds(134, 74, 121, 23);

        //将按钮添加到面板中
        root.add(radioButton);
        root.add(radioButton_1);
        root.add(radioButton_2);
        root.add(radioButton_3);

        //定义按钮组控件
        ButtonGroup group = new ButtonGroup();

        //将单选按钮添加到按钮组中
        group.add(radioButton);
        group.add(radioButton_1);
        group.add(radioButton_2);
        group.add(radioButton_3);

        //设置窗口风格
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(400, 300, 271, 161);
        setVisible(true);
    }

    //main方法
    public static void main(String args[]) {
        new MyFrame();
    }
}

`
```
#####3.复选框：JCheckBox
```
Swing 组件中的复选框的使用也非常广泛，一般情况下，它显示为一个方块图标，外加一段描述性文字。与单选按钮不同的是复选框可以进行多选选择，每一个复选框都提供 “选中” 与 “不选中” 两种状态。复选框由 JCheckBox 类的对象表示，它同样继承于 AbstractButton 类，所以复选框组件的属性设置也来源于 AbstractButton 类。

JCheckBox 的常用构造方法如下：
① public JCheckBox()
② public JCheckBox(Icon icon, Boolean checked)
③ public JCheckBox(String text, Boolean checked)

复选框与其他按钮的设置基本相同，除了可以在初始化时设置图标之外，还可以设置复选框的文字域是否被选中。
`
import javax.swing.*;

public class CheckBoxFrame extends JFrame {
    JPanel root;
    JCheckBox checkBox,checkBox_1,checkBox_2,checkBox_3;

    public CheckBoxFrame() {
        root = new JPanel();      //定义面板容器
        setContentPane(root);
        setLayout(null);         //设置面板为绝对布局

        //定义复选框组件
        checkBox = new JCheckBox("C");
        checkBox_1 = new JCheckBox("C++");
        checkBox_2 = new JCheckBox("Java");
        checkBox_3 = new JCheckBox("Python");

        //设置复选框显示位置和大小
        checkBox.setBounds(31, 28, 73, 23);
        checkBox_1.setBounds(132, 28, 73, 23);
        checkBox_2.setBounds(31, 75, 54, 23);
        checkBox_3.setBounds(132, 75, 74, 23);

        //往面板中添加复选框
        root.add(checkBox);
        root.add(checkBox_1);
        root.add(checkBox_2);
        root.add(checkBox_3);

        //设置窗口风格
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(400, 300, 271, 161);
        setVisible(true);
    }

    //main方法
    public static void main(String args[]) {
        new CheckBoxFrame();
    }
}

`

```
####3、文本组件
```
在现实项目开发中，文本组件是使用最为广泛的组件，尤其是文本框与密码框组件，通过文本组件可以很轻松地处理单行文字、多行文字、口令字段。下面我们来看文本组件的定义及其使用。
```
#####1.文本框：JTextField
```
文本框用来显示或编辑一个单行文本，通过 Swing 中的 JTextField 类对象创建，该类继承了 javax.swing.text.JTextComponent 类。

下面列举了一些常用的创建文本框的构造方法。
① public JTextField()
② public JTextField(String text)
③ public JTextField(int fieldwidth)
④ public JTextField(String text, int fieldwidth)
⑤ public JTextField(Document docModel, String text, int fieldWidth)
`
import javax.swing.*;

public class JTextFieldFrame extends JFrame {
    JPanel root;
    JLabel userNameLabel,passWordLabel;
    JTextField userTextField,passWordTextField;
    JButton enterButton,closeButton;

    public JTextFieldFrame() {
        root = new JPanel();      //定义面板容器
        setContentPane(root);
        setLayout(null);         //设置面板为绝对布局

        //用户名标签
        userNameLabel = new JLabel("用户名：");
        userNameLabel.setBounds(52, 33, 54, 15);
        root.add(userNameLabel);

        //用户名文本框
        userTextField = new JTextField(12);
        userTextField.setBounds(116, 30, 139, 21);
        root.add(userTextField);

        //密码标签
        passWordLabel = new JLabel("密 码：");       //定义标签对象
        passWordLabel.setBounds(52, 74, 54, 15);
        root.add(passWordLabel);

        //密码文本框
        passWordTextField = new JTextField(12);
        passWordTextField.setBounds(116, 71, 139, 21);
        root.add(passWordTextField);

        //登录按钮
        enterButton = new JButton("登录");          //定义按钮对象
        enterButton.setBounds(64, 116, 69, 23);
        root.add(enterButton);

        //退出按钮
        closeButton = new JButton("退出");
        closeButton.setBounds(174, 116, 69, 23);
        root.add(closeButton);

        //设置窗口风格
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(400, 300, 340, 256);
        setVisible(true);
    }

    //main方法
    public static void main(String[] args) {
        new JTextFieldFrame();
    }
}

`

```
#####2.密码框：JPasswordField
```
密码框与文本框的定义和用法基本相同，它们俩的区别是密码框使用户输入的字符串以某种符号进行加密。密码框对象是通过 JPasswordField 类 来 创 建 的，JPasswordField 类的构造方法与 JTextField 类的构造方法非常相似，下面列举几个常用的构造方法：
① public JPasswordField()
② public JPasswordFiled(String text)
③ public JPasswordField(int fieldwidth)
④ public JPasswordField(String text, int fieldwidth)
⑤ public JPasswordField(Document docModel, String text, int fieldWidth)
JPasswordField jp = new JPasswordField();
jp.setEchoChar('#');       //设置回显字符

`
import javax.swing.*;

public class JTextFieldFrame extends JFrame {
    JPanel root;
    JLabel userNameLabel,passWordLabel;
    JTextField userTextField;
    JButton enterButton,closeButton;
    JPasswordField passWordTextField;

    public JTextFieldFrame() {
        root = new JPanel();      //定义面板容器
        setContentPane(root);
        setLayout(null);         //设置面板为绝对布局

        //用户名标签
        userNameLabel = new JLabel("用户名：");
        userNameLabel.setBounds(52, 33, 54, 15);
        root.add(userNameLabel);

        //用户名文本框
        userTextField = new JTextField(12);
        userTextField.setBounds(116, 30, 139, 21);
        root.add(userTextField);

        //密码标签
        passWordLabel = new JLabel("密 码：");       //定义标签对象
        passWordLabel.setBounds(52, 74, 54, 15);
        root.add(passWordLabel);

        //密码文本框
        passWordTextField = new JPasswordField(12);
        passWordTextField.setBounds(116, 71, 139, 21);
        passWordTextField.setEchoChar('●');       //设置回显字符
        root.add(passWordTextField);

        //登录按钮
        enterButton = new JButton("登录");          //定义按钮对象
        enterButton.setBounds(64, 116, 69, 23);
        root.add(enterButton);

        //退出按钮
        closeButton = new JButton("退出");
        closeButton.setBounds(174, 116, 69, 23);
        root.add(closeButton);

        //设置窗口风格
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(400, 300, 340, 256);
        setVisible(true);
    }

    //main方法
    public static void main(String[] args) {
        new JTextFieldFrame();
    }
}

`
```

#####3.文本域：JTextArea
```
Swing 中的任何一个文本域都是 JTextArea 类型的对象，JTextArea 常用的构造方法如下：
① public JTextArea()
② public JTextArea(String text)
③ public JTextArea(int rows, int columns)
④ public JTextArea(Document doc)
⑤ public JTextArea(Document doc, String Text, int rows, int columns)

通过上述构造方法，我们可以在初始化文本域时提供默认文本、设置文本域的长与宽。

`
import javax.swing.*;

public class JTextAreaFrame extends JFrame {
    JPanel root;
    JTextArea textArea;

    public JTextAreaFrame(String title) {
        super(title);

        root = new JPanel();      //定义面板容器
        setContentPane(root);
        setLayout(null);         //设置面板为绝对布局

        textArea = new JTextArea();              //定义文本域组件
        textArea.setLineWrap(true);              //设置文本域自动换行
        textArea.setBounds(20, 10, 290, 200);
        root.add(textArea);                      //将文本域添加到面板

        //设置窗口风格
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(400, 300, 340, 260);
        setVisible(true);
    }

    //main方法
    public static void main(String[] args) {
        new JTextAreaFrame("JTextArea");
    }
}
`
JTextArea 类的 setLineWrap() 方法可以设置文本域的自动换行属性。文本域对象的初始状态是不允许换行的，调用该方法并给定参数 true，就可以将文本域设置为自动换行状态啦。
```

####4、列表组件
```
Swing 提供了两种列表组件，分别为下拉列表框与列表框。
下拉列表框与列表框都是带有多个选项的组件，用户可以从中选择需要的选项。和下拉列表框相比，列表框显得更直观一些，它将所有选项罗列在列表框中，
而下拉列表框显得更为便捷和美观，它将所有的项目隐藏起来，当用户需要选用其中的选项时才会显现出来。
```
#####1.下拉列表框：JComboBox
```
(1) JComoBox类
初次使用 Swing 中的下拉列表框时，会感觉到 Java 中的下拉列表框与 Windows 操作系统中的下拉列表框类似，但是实质上两者并不相同，Swing 中的下拉列表框不仅支持用户从中选择选项，同时还支持用户编辑项目中的内容。

下拉列表框是一个带条状的显示区，它具有下拉功能。在下拉列表框的右边存在一个倒三角形的按钮，当用户单击该按钮时，下拉列表框中的选项将会以列表的形式显示出来。

Swing 中的下拉列表框使用 JComboBox 类对象来表示，它是 javax.swing.JComponent 类的子类。

JComboBox 的常用构造方法如下。
① public JComboBox()
② public JComboBox(ComboBoxMode dataModel)
③ public JComboBox(Object[] arrayData)
④ public JComboBox(Vector vector)

在初始化下拉列表框时，既可以选择同时指定下拉列表框中的选项内容，也可以在程序中使用其他方法设置下拉列表框中的内容。下拉列表框中的内容可以被封装在 ComboBoxModel 类型、数组或者 Vector 类型中。

(2) JCcomboBox 模型
在开发程序时，一般将下拉列表框中的选项封装在 ComboBoxModel 类型中。ComboBoxModel 类型为接口，它代表一般模型，可以自定义一个类实现该接口，然后在初始化 JComboBox 对象时向上转型为 ComboBoxModel 接口类型，但是必须实现下面的两个方法：

① public void setSelectedItem(Object item)
② public Object getSelectedItem()

其中，setSelectedItem() 方法是设置下拉列表框的选中项，getSelectedItem() 方法用于返回下拉列表框中的选中项，有了这两个方法，我们就可以轻松地对下拉列表框中的项目进行操作。

自定义的这个类除了可以实现该接口外，还可以继承 AbstractListModel 类，在该类中也有两个操作下拉列表框的重要方法：

① getSize()：返回列表的长度。
② getElementAt(in index)：返回指定索引处的值。

`
import javax.swing.*;

public class JComboBoxFrame extends JFrame {
    JPanel root;
    JLabel messageLabel;
    JComboBox comboBox;

    public JComboBoxFrame(String title) {
        super(title);

        root = new JPanel();      //定义面板容器
        setContentPane(root);
        setLayout(null);         //设置面板为绝对布局

        messageLabel = new JLabel("请选择你的星座：");             //定义标签组件
        messageLabel.setBounds(31, 25, 130, 15);    //设置标签组件大小
        root.add(messageLabel);

        //定义字符串数组对象
        String[] constellations = {
                "白羊座", "金牛座", "双子座", "巨蟹座",
                "狮子座", "处女座", "天秤座", "天蝎座",
                "射手座", "摩羯座", "双鱼座", "水瓶座"
        };
        comboBox = new JComboBox(constellations);                    //定义下拉列表框组件
        comboBox.setBounds(130, 22, 100, 21);     //设置列表大小
        root.add(comboBox);                                         //将下拉列表添加到面板

        //设置窗口风格
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(400, 300, 310, 230);
        setVisible(true);
    }

    //main方法
    public static void main(String[] args) {
        new JComboBoxFrame("JComboBox");
    }
}

`

```
#####2.列表框：JList
```
与下拉列表框相比，列表框将所有选项显示在列表框中，在窗体上占据固定的大小，如果列表框中的选项较多，需要具有滚动效果，那么可以将列表框放入滚动面板中。用户在选择列表框中的某一项后，按住 Shift 键并选择列表框中的其他选项，其他选项也会被选中。用户也可以按住 Ctrl 键并单击列表框中的项目，这样，列表框中的项目就处于非选中状态。

Swing 使用 JList 类对象来表示列表框，下面列举几个常用的构造方法：

① public void JList()
② public void JList(Object[] listData)
③ public void JList(Vector listData)
④ public void JList(ListMode dataModel)

在上述构造方法中，存在一个没有参数的构造方法，当使用该方法进行构造时，用户既可以在初始化列表框后使用 setListData() 方法对列表框进行设置，也可以在初始化的过程中对列表框中的选项进行设置。设置的方式有三种，分别为数组、Vector 类型和 ListModel 模型。

当使用数组作为构造方法的参数时，首先需要创建列表选项的数组，然后再利用构造方法来初始化列表框。

1.使用数组作为初始化列表框的参数，示例代码如下：

String[] contents={"列表1","列表2","列表3","列表4"};
JList jl = new JList(contents);

2.如果使用上述构造方法中的第三种构造方法，那么通常将 Vector 类型的数据作为初始化 JList 组件的参数，示例代码如下：

Vector contents = new Vector();
JList jl = new JList(contents);
contents.add("列表1");
contents.add("列表2");
contents.add("列表3");
contents.add("列表4");

3.如果使用 ListModel 模型为参数，那么需要创建 ListModel 对象。ListModel 是 Swing 包中的一个接口，它提供了获取列表框属性的方法。
但是在通常情况下，为了使用户不完全实现 ListModel 接口中的方法，通常自定义一个类继承实现该接口的抽象类 AbstractListModel。
该类提供了 getElementAt() 与 getSize() 方法，其中，getElementAt() 方法代表根据项目的索引获取列表框中的值，而 getSize() 方法用于获取列表框中项目的个数。

第 4 种构造方法我们具体来看一个实例。
要求：在项目中创建 Jlist 类，使该类继承 JFrame 类成为窗体组件，然后在该类中创建列表框，并添加到窗体中。

`
import javax.swing.*;

public class JListFrame extends JFrame {
    JPanel root;
    JList list;
    JScrollPane js;

    public JListFrame(String title) {
        super(title);

        root = new JPanel();      //定义面板容器
        setContentPane(root);
        setLayout(null);         //设置面板为绝对布局

        list = new JList(new MyListModel());
        js = new JScrollPane(list);
        js.setBounds(10, 10, 100, 100);
        root.add(js);

        //设置窗口风格
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(400, 300, 260, 230);
        setVisible(true);
    }

    class MyListModel extends AbstractListModel {   //继承抽象类AbstractListModel
        private String[] constellations = {
                "白羊座", "金牛座", "双子座", "巨蟹座",
                "狮子座", "处女座", "天秤座", "天蝎座",
                "射手座", "摩羯座", "双鱼座", "水瓶座"
        };

        //设置列表框内容
        public Object getElementAt(int x) {    //重写getElementAt()方法
            if (x < constellations.length)
                return constellations[x++];
            else
                return null;
        }

        public int getSize() {                //重写getSize()方法
            return constellations.length;
        }
    }

    //main方法
    public static void main(String[] args) {
        new JListFrame("JList");
    }
}

`
除了可以使用这个实例中所示的方式创建列表框之外，还可以使用 DefaultListModel 类创建列表框，该类扩展了 AbstractListModel 类，
所以也可以通过 DefaultListModel 对象向上转型为 ListModel 接口初始化列表框。同时，DefaultListModel 类提供 addElement() 方法实现将内容添加到列表框中。

`
import javax.swing.*;

public class JListFrame extends JFrame {
    JPanel root;
    JList list;
    DataModel mode;

    public JListFrame(String title) {
        super(title);

        root = new JPanel();      //定义面板容器
        setContentPane(root);

        mode = new DataModel();

        list = new JList(mode);

        list.setBorder(BorderFactory.createTitledBorder("配件"));

        root.add(new JScrollPane(list));

        //设置窗口风格
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(400, 300, 260, 230);
        setVisible(true);
    }

    class DataModel extends DefaultListModel {

        String[] s = {"主板", "显示器", "内存", "CPU", "硬盘", "电源", "键盘", "鼠标", "显卡"};

        public int getSize() {
            return s.length;
        }

        public String getElementAt(int index) {
            return (index + 1) + "." + s[index++];
        }
    }

    //main方法
    public static void main(String[] args) {
        new JListFrame("JList");
    }
}

`
```







