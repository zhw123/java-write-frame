package com.lean.swing.respo;

import com.lean.swing.bean.Task;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TaskRespository extends JpaRepository<Task, Long> {
}
