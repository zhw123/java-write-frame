//package com.lean.swing;
//
//import java.awt.*;
//import java.awt.event.WindowAdapter;
//import java.awt.event.WindowEvent;
//import java.net.URL;
//
//import javax.swing.*;
//
//import org.springframework.stereotype.Component;
//
///**
// * @author YQA_Administrator
// * @date 2021/12/13 13:53
// * @value jf 窗体对象
// * @value jl 展示标签
// */
//
//@Component
//public class MainWin extends JFrame {
//    Container container = null;
//    JLabel jl = null;
//    URL url = MainWin.class.getResource("/images/ia_900000229.jpg");
//
//    public MainWin() {
//        container = this.getContentPane();
//        jl = new JLabel("这是一个JFrame窗体");
//        //使标签上的文字居中
//        jl.setHorizontalAlignment(SwingConstants.CENTER);
//        //将标签添加到容器中
//        container.add(jl);
//        //设置容器的背景颜色
//        container.setBackground(Color.white);
//        //设置窗体大小
//        this.setSize(700, 500);
//        //设置窗体关闭方式
//        //1.不做任何反应。 * 2.仅仅隐藏。 * 3.关闭窗口。 * 4.关闭窗口，并结束所有线程。 * 注意：默认是2，你会发现窗口虽然不见了，但在任务管理器中还存在，这将导致springIoc容器不能自动清理关闭
//        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//        ImageIcon icon = new ImageIcon(url);
//        // 为标签设置图片
//        jl.setIcon(icon);
//
//        this.addWindowListener(new WindowAdapter() {
//            @Override
//            public void windowClosing(WindowEvent e) {
//                super.windowClosing(e);
//                System.out.println("管理");
//                //do something
//            };
//        });
//        init();
//    }
//
//    public void init() {
//        //设置图标
//        setIcon();
//        //使窗体可视
//        this.setVisible(true);
//    }
//
//    void setIcon() {
//        Image icon = Toolkit.getDefaultToolkit().getImage(url);
//        this.setIconImage(icon);
//    }
//}
