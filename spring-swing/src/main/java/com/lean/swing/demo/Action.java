package com.lean.swing.demo;

import java.awt.Container;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * 方法名称	概述
 * getContentPane()	返回此窗体的 contentPane 对象
 * getDefaultCloseOperation()	返回用户在此窗体上单击“关闭”按钮时执行的操作
 * setContentPane(Container contentPane)	设置 contentPane 属性
 * setDefaultCloseOperation(int operation)	设置用户在此窗体上单击“关闭”按钮时默认执行的操作
 * setDefaultLookAndFeelDecorated (boolean
 * defaultLookAndFeelDecorated)	设置 JFrame 窗口使用的 Windows 外观（如边框、关
 * 闭窗口的 小部件、标题等）
 * setIconImage(Image image)	设置要作为此窗口图标显不的图像
 * setJMenuBar( JMenuBar menubar)	设置此窗体的菜单栏
 * setLayout(LayoutManager manager)	设置 LayoutManager 属性
 */
public class Action extends JFrame{

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;

    public Action() {
        //设置显示窗口标题
        setTitle("大标题，就是左上角显示的标题");
        //设置窗口显示尺寸
        setSize(666,200);
        //设置窗口是否可以关闭
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //创建一个标签
        JLabel lable=new JLabel("lable就是一个文本");
        //获取本窗口的内容窗格
        Container c=getContentPane();
        //将lable标签组件添加到内容窗格上
        c.add(lable);
        //设置本窗口是否可见
        setVisible(true);
    }

    public static void main(String[] args) {
        new Action();
    }
}
