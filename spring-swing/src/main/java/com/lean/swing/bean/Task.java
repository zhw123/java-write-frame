package com.lean.swing.bean;

import java.util.Date;

import javax.persistence.*;

import com.lean.swing.constant.TaskStateEnum;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Entity
@Table
@Accessors(chain = true)
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Date createAt;
    private Date updateAt;
    private TaskStateEnum taskState;
}
