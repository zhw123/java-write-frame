package com.lean.retry;

import com.lean.retry.service.RetryService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
class SpringLeanRetryApplicationTests {

    @Autowired
    RetryService retryService;

    @Test
    public void retryTeest() throws Exception {
        retryService.testRetry();
    }

}
